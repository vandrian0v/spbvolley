<?php

namespace App\Observers;

use App\Models\Tournament;
use Auth;

class TournamentObserver
{
    public function updated(Tournament $tournament): void
    {
        if ($tournament->isDirty('contribution')) {
            $message = sprintf(
                'Изменен взнос турнира с id %d. %d -> %d (Пользователь %s).',
                $tournament->id,
                $tournament->getOriginal('contribution'),
                $tournament->contribution,
                Auth::user()->fullname ?? ''
            );

            notifyMe($message);
            info($message);
        }
    }
}