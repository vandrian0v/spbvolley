<?php

namespace App\Observers;

use App\Models\Photos\Photo;
use Storage;

class PhotoOwnerObserver
{
    /**
     * @param  mixed $model
     * @return void
     */
    public function deleting($model): void
    {
        // Удаление превью
        if ($model->hasPreview()) {
            Storage::delete($model->previewPath());
        }

        // Удаление фотографий
        /** @var Photo $photo */
        foreach ($model->photos as $photo) {
            $photo->delete();
        }
    }
}
