<?php

namespace App\Observers;

use App\Models\Photos\Photo;
use Storage;

class PhotoObserver
{
    /**
     * @param  Photo $photo
     * @return void
     */
    public function deleting(Photo $photo): void
    {
        Storage::delete($photo->fullPath());
    }
}
