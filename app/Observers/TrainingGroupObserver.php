<?php

namespace App\Observers;

use App\Models\TrainingGroup;
use DB;

class TrainingGroupObserver
{
    /**
     * @param TrainingGroup $group
     * @return bool
     */
    public function creating(TrainingGroup $group): bool
    {
        $group->order = DB::table($group->getTable())->max('order') + 1;

        return true;
    }
}