<?php

namespace App\Observers;

use App\Models\Team;
use App\Services\RatingHandler;

class TeamObserver
{
    private $handler;

    public function __construct(RatingHandler $handler)
    {
        $this->handler = $handler;
    }

    public function updating(Team $team)
    {
        if ($team->isDirty('rank')) {
            $team->rating = $this->handler->getTeamRating($team);
        }
    }
}