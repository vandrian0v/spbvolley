<?php

namespace App\Contracts\Notifications;

interface SmsSender
{
    /**
     * @param array $phones
     * @param $text
     * @return mixed
     */
    public function send(array $phones, $text);

    /**
     * @return int
     */
    public function balance();
}