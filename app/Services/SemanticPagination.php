<?php
namespace App\Components\Pagination;

use Illuminate\Pagination\BootstrapThreePresenter;

class SemanticPagination extends BootstrapThreePresenter
{
    public function getPageLinkWrapper($url, $page, $rel = null)
    {
        if ($page == $this->paginator->currentPage()) {
            return $this->getActivePageWrapper($page);
        }

        return $this->getAvailablePageWrapper($url, $page, $rel);
    }

    public function render()
    {
        if ($this->hasPages()) {
            return sprintf(
                '<div class="ui pagination menu borderless small">%s</div>',
                $this->getLinks()
            );
        }

        return '';
    }

    public function getDisabledTextWrapper($text)
    {
        return ' <div class="disabled item">' . $text . '</div>';
    }

    public function getActivePageWrapper($text)
    {
        return '<a class="active item">' . $text . '</a>';
    }

    public function getAvailablePageWrapper($url, $page, $rel = null)
    {
        return '<a class="item" href="' . $url . '">' . $page . '</a>';
    }
}
