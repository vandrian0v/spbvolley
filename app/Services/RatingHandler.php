<?php

namespace App\Services;

use App\Models\User;
use App\Models\Tour;
use App\Models\Team;
use App\Models\Rating;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use DB;

class RatingHandler
{
    /**
     * Season start date.
     * Uses for seasonal_rating filling.
     */
    const SEASON_START = '2019-05-24';

    /** @var Collection */
    private $ratingTypes;

    public function __construct()
    {
        $this->ratingTypes = collect([
            ['alias' => 'male', 'name' => 'Мужчины', 'icon' => 'mars'],
            ['alias' => 'female', 'name' => 'Женщины', 'icon' => 'venus'],
        ]);
    }

    public function recalculateRating()
    {
        $seasonStart = Carbon::parse(self::SEASON_START);

        $query = DB::table('user as u')
            ->select('u.id', DB::raw('sum(t.rating / tc.team_size) as points'))
            ->join('m2m_user_team as ut', 'u.id', '=', 'ut.user_id')
            ->join('team as t', 't.id', '=', 'ut.team_id')
            ->join('tournament as tt', 'tt.id', '=', 't.tournament_id')
            ->join('tournament_category as tc', 'tc.id', '=' , 'tt.category_id')
            ->join('nwtour as n', 'n.id', '=', 'tt.nwtour_id')
            ->where('n.status', Tour::STATUS_FINISHED)
            ->whereRaw('u.gender = tc.gender')
            ->groupBy('u.id');

        $seasonalData = (clone $query)
            ->whereDate('n.end_date', '>=', $seasonStart)
            ->get()
            ->keyBy('id')
            ->toArray();

        $yearlyData = (clone $query)
            ->whereDate('n.end_date', '>=', Carbon::today()->subYear())
            ->get()
            ->keyBy('id')
            ->toArray();

        User::chunk(100, function ($users) use ($seasonalData, $yearlyData) {
            /** @var User $user */
            foreach ($users as $user) {
                $user->seasonal_rating = (int) ($seasonalData[$user->id]->points ?? 0);
                $user->yearly_rating = (int) ($yearlyData[$user->id]->points ?? 0);

                $user->save();
            }
        });
    }

    /**
     * @param $type
     * @param $categoryId
     * @return Collection
     */
    public function getRatingByType(string $type, int $categoryId): Collection
    {
        $users = User::where('gender', $type)
            ->where('yearly_rating', '<>', 0)
            ->ofCategory($categoryId)
            ->orderBy('seasonal_rating', 'desc')
            ->orderBy('yearly_rating', 'desc')
            ->get();

        /** @var User $user */
        foreach ($users as $user) {
            $user->setAppends(['link', 'fullname']);
        }

        return $users;
    }

    /**
     * @param Team $team
     * @return int
     */
    public function getTeamRating(Team $team): int
    {
        if (!$team->rank) {
            return 0;
        }

        $rating = Rating::where('category_id', $team->category_id ?? $team->tournament->category_id)
            ->where('start_rank', '<=', $team->rank)
            ->where('end_rank', '>=', $team->rank)
            ->first();

        return $rating->points ?? 0;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getRatingTypes()
    {
        return $this->ratingTypes;
    }
}
