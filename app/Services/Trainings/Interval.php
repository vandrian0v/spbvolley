<?php

namespace App\Services\Trainings;

use Carbon\Carbon;

class Interval
{
    /** @var int */
    protected $startMD;

    /** @var int */
    protected $endMD;

    /**
     * Interval constructor.
     * @param int $startMonth
     * @param int $startDay
     * @param int $endMonth
     * @param int $endDay
     */
    public function __construct(int $startMonth, int $startDay, int $endMonth, int $endDay)
    {
        $this->startMD = (int) ($startMonth . sprintf('%02d', ($startDay)));
        $this->endMD   = (int) ($endMonth . sprintf('%02d', ($endDay)));
    }

    /**
     * @param Carbon $date
     * @return bool
     */
    public function includes(Carbon $date): bool
    {
        $dateMD  = (int) ($date->month . sprintf('%02d', $date->day));

        if ($this->startMD <= $this->endMD) {
            return $this->startMD <= $dateMD && $this->endMD >= $dateMD;
        } else {
            return $dateMD <= $this->endMD || $dateMD >= $this->startMD;
        }
    }

    /**
     * @param Carbon $date
     * @return bool
     */
    public function notIncludes(Carbon $date): bool
    {
        return !$this->includes($date);
    }
}