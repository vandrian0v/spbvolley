<?php

namespace App\Services\Trainings\SubscriptionHandlers;

use Jenssegers\Date\Date;

class ThreeMonthHandler extends HandlerBase
{
    /**
     * @param int $dayOfWeek
     * @param Date $minDate
     * @param Date|null $maxDate
     * @return array
     */
    public function getIntervalDates(int $dayOfWeek, Date $minDate, Date $maxDate = null): array
    {
        if ($minDate->day > 7) {
            // $startDate = $minDate->copy()->addMonth()->startOfMonth();
            $startDate = $minDate->copy()->endOfMonth()->addDay()->startOfDay();
        } else {
            $date = $minDate->copy()->startOfMonth();
            $next = false;

            // Если сегодня - день тренировки, то разрешаем оплатить (иначе lte)
            while ($date->lt($minDate)) {
                // Были ли эти дни недели уже в месяце
                if ($date->dayOfWeek === $dayOfWeek) {
                    $next = true;
                    break;
                }

                $date->addDay();
            }

            $startDate = $next
                // ? $minDate->copy()->addMonth()->startOfMonth()
                ? $minDate->copy()->endOfMonth()->addDay()->startOfDay()
                : $minDate->copy()->startOfMonth();
        }

        $endDate = $startDate->copy()->addMonth(2)->endOfMonth();

        // Ограничение даты сверху
        if ($maxDate && $endDate->gt($maxDate)) {
            $endDate = $maxDate;
        }

        return [$startDate, $endDate];
    }
}