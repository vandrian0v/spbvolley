<?php

namespace App\Services\Trainings\SubscriptionHandlers;

use Jenssegers\Date\Date;

abstract class HandlerBase
{
    /**
     * @param int $dayOfWeek
     * @param Date $minDate
     * @param Date|null $maxDate
     * @return array
     */
    public function getSelectedDates(int $dayOfWeek, Date $minDate, Date $maxDate = null): array
    {
        /**
         * @var Date $date
         * @var Date $endDate
         */
        list($date, $endDate) = $this->getIntervalDates($dayOfWeek, $minDate, $maxDate);
        $dates = [];

        while ($date->lt($endDate)) {
            if ($date->dayOfWeek === $dayOfWeek) {
                $dates[] = $date->format('Y-m-d');
                $date->addWeek();
            } else {
                $date->addDay();
            }
        }

        return $dates;
    }

    /**
     * @param int $dayOfWeek
     * @param Date $minDate
     * @param Date|null $maxDate
     * @return array
     */
    public abstract function getIntervalDates(int $dayOfWeek, Date $minDate, Date $maxDate = null): array;
}