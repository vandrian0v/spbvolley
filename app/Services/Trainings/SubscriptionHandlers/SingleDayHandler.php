<?php

namespace App\Services\Trainings\SubscriptionHandlers;

use Jenssegers\Date\Date;

class SingleDayHandler extends HandlerBase
{
    /**
     * @param int $dayOfWeek
     * @param Date $minDate
     * @param Date|null $maxDate
     * @return array
     */
    public function getIntervalDates(int $dayOfWeek, Date $minDate, Date $maxDate = null): array
    {
        // При разовой оплате заранее выбранных дат нет
        return [$minDate, $minDate];
    }
}