<?php

namespace App\Services\Trainings;

use Jenssegers\Date\Date;

class Calendar
{
    /** @var Date $minDate */
    protected $minDate = null;

    /** @var Date $maxDate */
    protected $maxDate = null;

    /** @var array $selectedDates */
    protected $selectedDates = [];

    /** @var array $enabledDates */
    protected $enabledDates = [];

    /** @var int $monthCount */
    protected $monthCount = 6;

    /** @var Interval $interval */
    protected $interval = null;

    /**
     * @param array $selectedDates
     * @return self
     */
    public function setSelectedDates(array $selectedDates): self
    {
        $this->selectedDates = $selectedDates;

        return $this;
    }

    /**
     * @param array $enabledDates
     * @return self
     */
    public function setEnabledDates(array $enabledDates): self
    {
        $this->enabledDates = $enabledDates;

        return $this;
    }

    /**
     * @param Date|null $minDate
     * @return self
     */
    public function setMinDate(Date $minDate = null): self
    {
        $this->minDate = $minDate;

        return $this;
    }

    /**
     * @param Date|null $maxDate
     * @return self
     */
    public function setMaxDate(Date $maxDate = null): self
    {
        $this->maxDate = $maxDate;

        return $this;
    }

    /**
     * @param int $monthCount
     * @return self
     */
    public function setMonthCount(int $monthCount): self
    {
        $this->monthCount = $monthCount;

        return $this;
    }

    /**
     * @param Interval|null $interval
     * @return self
     */
    public function setInterval(Interval $interval = null): self
    {
        $this->interval = $interval;

        return $this;
    }

    /**
     * @param Date $date
     * @return array
     */
    public function getCalendarFrom(Date $date): array
    {
        $calendar = [];

        for ($i = 0; $i < $this->monthCount; $i++) {
            $calendar[] = [
                'name'  => $date->format('F Y'),
                'year'  => $date->year,
                'month' => $date->month,
                'weeks' => $this->getMonthCalendar($date),
            ];

            $date->addMonth();
        }

        return $calendar;
    }

    /**
     * @param Date $date
     * @return array
     */
    private function getMonthCalendar(Date $date): array
    {
        $monthCalendar = [];
        $currentDate = $date->copy()->startOfMonth()->startOfWeek();
        $endDate   = $date->copy()->endOfMonth()->endOfWeek()->addDay()->startOfDay();

        $index = 0;

        while ($currentDate->ne($endDate)) {
            $monthCalendar[$index][] = [
                'date'           => $currentDate->format('Y-m-d'),
                'day'            => $currentDate->day,
                'disabled'       => $this->checkDisabled($date, $currentDate),
                'selected'       => $this->checkSelected($date, $currentDate),
            ];

            if ($currentDate->isSunday()) {
                $index++;
            }

            $currentDate->addDay();
        }

        return $monthCalendar;
    }

    /**
     * @param Date $date
     * @param Date $currentDate
     * @return bool
     */
    private function checkDisabled(Date $date, Date $currentDate): bool
    {
        return $currentDate->month !== $date->month                         // месяц отличается
            || !in_array($currentDate->dayOfWeek, $this->enabledDates)      // допустимые дни недели
            || ($this->minDate ? $this->minDate->gte($currentDate) : false) // дата меньше минимальной
            || ($this->maxDate ? $this->maxDate->lte($currentDate) : false) // дата меньше максимальной
            || ($this->interval ? $this->interval->notIncludes($currentDate) : false); // не входит в интервал
    }

    /**
     * @param Date $date
     * @param Date $currentDate
     * @return bool
     */
    private function checkSelected(Date $date, Date $currentDate): bool
    {
        return $currentDate->month === $date->month                           // месяц не отличается
            && in_array($currentDate->format('Y-m-d'), $this->selectedDates)  // входит в массив выбранных
            && ($this->minDate ? $this->minDate->lt($currentDate) : true)     // дата меньше минимальной
            && ($this->maxDate ? $this->maxDate->gt($currentDate) : true)     // дата меньше максимальной
            && ($this->interval ? $this->interval->includes($currentDate) : true); // входит в интервал
    }
}