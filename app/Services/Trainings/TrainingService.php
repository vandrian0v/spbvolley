<?php

namespace App\Services\Trainings;

use App\Models\Payment;
use App\Models\SubscriptionType;
use App\Models\Training;
use App\Models\User;
use App\Models\UserTraining;
use App\Services\Trainings\SubscriptionHandlers\HandlerBase;
use DB;
use Exception;
use Jenssegers\Date\Date;

class TrainingService
{
    /**
     * @param User $user
     * @param array $trainings
     * @param int $subscriptionTypeId
     * @return Payment
     * @throws Exception
     */
    public function createPayment(User $user, array $trainings, int $subscriptionTypeId): Payment
    {
        DB::beginTransaction();

        try {
            $subscriptionType = SubscriptionType::find($subscriptionTypeId);
            $subscriptionHandler = $subscriptionType->getHandler();

            $trainingsModels = Training::with('group', 'subscriptionTypes', 'location')
                ->whereIn('id', array_keys($trainings))
                ->get();

            if (count($trainings) != $trainingsModels->count()) {
                throw new Exception('Некорректные id тренировок.');
            }

            $today = Date::today();
            $userTrainings = [];
            $sum = 0;

            /** @var Training $training */
            foreach ($trainingsModels as $training) {
                $dates = $trainings[$training->id];

                // Проверяем, что все нужные даты выбраны
                $this->validate($subscriptionHandler, $training, $dates, $today);

                foreach ($dates as $date) {
                    $date = Date::parse($date);

                    if ($training->location->has_interval) {
                        $minDate = $training->location->period_start_date;
                        $maxDate = $training->location->period_end_date;

                        // @todo некорректно работают интервалы
                        $minDate->year = $maxDate->year = $today->year;

//                        if ($date->lt($minDate) || $date->gt($maxDate)) {
//                            throw new Exception("Некорректная дата тренировки {$training->id} {$date->format('Y-m-d')}");
//                        }
                    }

                    if ($date->dayOfWeek !== $training->dayOfWeek || $date->lt($today)) {
                        throw new Exception("Некорректная дата тренировки {$training->id} {$date->format('Y-m-d')}");
                    }

                    $user_id     = $user->id;
                    $description = "Группа '{$training->group->name}', ({$training->start_time} – {$training->end_time})";
                    $coach_id    = $training->coach_id;
                    $location_id = $training->location_id;
                    $start_time  = $training->start_time;
                    $end_time    = $training->end_time;
                    $training_id = $training->id;
                    $sum        += $training->subscriptionTypes->where('id', $subscriptionType->id)->first()->pivot->price;

                    $userTrainings[] = new UserTraining(
                        compact(
                            'user_id',
                            'date',
                            'description',
                            'coach_id',
                            'location_id',
                            'start_time',
                            'end_time',
                            'training_id'
                        )
                    );
                }
            }

            /** @var Payment $payment */
            $payment = $user->payments()->create([
                'payment_type_id' => Payment::TYPE_TRAINING,
                'comment' => "Оплата тренировок. {$subscriptionType->name}.",
                'sum' => $sum,
            ]);

            $payment->userTrainings()->saveMany($userTrainings);
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage());
        }

        DB::commit();

        return $payment;
    }

    /**
     * @param HandlerBase $handler
     * @param Training $training
     * @param array $dates
     * @param Date $today
     * @return bool
     * @throws Exception
     */
    private function validate(HandlerBase $handler, Training $training, array $dates, Date $today): bool
    {
        return true;
        $actualDate = $today;
        $maxDate = null;

        if ($training->location->has_interval) {
            $training->location->period_start_date->year = $training->location->period_end_date->year = $today->year;

            // Если сегодня раньше чем открытие стадиона то считаем от времени открытия
            if ($today->lt($training->location->period_start_date)) {
                $actualDate = $training->location->period_start_date;
            }

            $maxDate = $training->location->period_end_date;
        }

        $selectedDates = $handler->getSelectedDates($training->dayOfWeek, $actualDate, $maxDate);

        if (count(array_intersect($selectedDates, $dates)) !== count($selectedDates)) {
            throw new Exception('Не выбраны даты необходимые для оплаты по цене абонемента.');
        }

        return true;
    }
}