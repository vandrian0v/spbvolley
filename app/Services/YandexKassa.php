<?php

namespace App\Services;

use App\Models\Payment;

class YandexKassa
{
    private $payment;

    /**
     * @param Payment $payment
     * @return string
     */
    public function buildUrl(Payment $payment): string
    {
        $this->payment = $payment;

        $attributes = $this->fetchAttributes();

        $url = sprintf(
            '%s?%s',
            config('yandex_kassa.url'),
            http_build_query($attributes)
        );

        return $url;
    }

    /**
     * @return array
     */
    private function fetchAttributes(): array
    {
        return [
            'shopId'         => config('yandex_kassa.shop_id'),
            'scid'           => config('yandex_kassa.scid'),
            'cps_email'      => $this->payment->user->email,
            'custEmail'      => $this->payment->user->email,
            'custName'       => $this->payment->user->fullname,
            'customerNumber' => $this->payment->user->id,
            'sum'            => $this->payment->sum,
            'orderDetails'   => $this->payment->comment,
            'orderNumber'    => $this->payment->id,
            'shopSuccessURL' => route('payments.success', $this->payment),
            'shopFailURL'    => route('home'),
        ];
    }
}