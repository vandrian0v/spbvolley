<?php

namespace App\Services\Tournaments;

use App\Events\TournamentApplicationReceived;
use App\Models\Payment;
use App\Models\Tour;
use App\Models\TournamentCategory;
use App\Models\Tournament;
use App\Models\Team;
use App\Models\User;
use App\Services\Tournaments\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\Collection;
use DB;

class TournamentService
{
    /**
     * @param Tour $tour
     * @param TournamentCategory $category
     * @param Collection $users
     * @return array
     * @throws ValidationException
     */
    public function addTeam(Tour $tour, TournamentCategory $category, Collection $users): array
    {
        $this->validate($tour, $category, $users);

        DB::beginTransaction();

        /** @var Tournament $tournament */
        $tournament = $tour->tournaments()
            ->where('category_id', $category->id)
            ->first();

        if ($tournament->limit_exceeded) {
            DB::rollBack();

            throw new ValidationException(
                'Невозможно подать заявку в связи с достижением максимального количества участников'
            );
        }

        $userIds = $users->pluck('id')->toArray();

        // Ищем команду с теми же участниками
        $team = $tournament->teamWithUsers($userIds);

        if ($team) {
            DB::rollBack();

            if ($team->is_confirmed) {
                throw new ValidationException('Данная команда уже заявлена');
            } else {
                return [
                    'team_id' => $team->id,
                    'payment_required' => true,
                ];
            }
        }

        /** @var Team $team */
        $team = $tournament->teams()->create([]);
        $team->users()->attach($userIds);

        DB::commit();

        if (!($payment_required = $tournament->contribution !== 0)) {
            event(new TournamentApplicationReceived($team));
        }

        return [
            'team_id' => $team->id,
            'payment_required' => $payment_required,
        ];
    }

    /**
     * @param User $user
     * @param Team $team
     * @return bool
     */
    public function createPayment(User $user, Team $team): bool
    {
        /** @var Payment $payment */
        $payment = $user->payments()->create([
            'payment_type_id' => Payment::TYPE_TOURNAMENT_CONTRIBUTION,
            'comment'         => sprintf('Оплата взноса команды #%d: %s', $team->id, $team->usersNames),
            'sum'             => $team->tournament->contribution,
        ]);

        // Привязываем платеж к команде
        $team->payment()->associate($payment);

        return $team->save();
    }

    /**
     * @param Team $team
     * @return bool
     */
    public function refreshPayment(Team $team): bool
    {
        $team->payment->sum = $team->tournament->contribution;

        return $team->payment->save();
    }

    /**
     * @param Tour $tour
     * @param TournamentCategory $category
     * @param Collection $users
     * @throws ValidationException
     */
    private function validate(Tour $tour, TournamentCategory $category, Collection $users): void
    {
        if (!$tour->categories->contains($category->id)) {
            throw new ValidationException('На данном этапе не проводится турнир данной категории');
        }

        if ($users->count() !== $category->team_size) {
            throw new ValidationException('Неверное количество участников номинации');
        }
    }
}