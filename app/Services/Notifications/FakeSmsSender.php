<?php

namespace App\Services\Notifications;

use App\Contracts\Notifications\SmsSender;

class FakeSmsSender implements SmsSender
{
    /**
     * @param array $phones
     * @param $text
     * @return mixed
     */
    public function send(array $phones, $text)
    {
        foreach ($phones as $phone) {
            info("Sms на номер $phone -> $text");
        }
    }

    /**
     * @return int
     */
    public function balance()
    {
        return 1000;
    }
}