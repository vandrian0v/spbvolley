<?php

namespace App\Services\Notifications;

use App\Contracts\Notifications\SmsSender;

class SmsProfiSender implements SmsSender
{
    private $login;
    private $password;

    const HTTPS_ADDRESS = 'http://lcab.smsprofi.ru/';
    const HTTP_ADDRESS = 'http://lcab.smsprofi.ru/';
    const HTTPS_CHARSET = 'utf-8';
    const HTTPS_METHOD = 'curl';
    const USE_HTTPS = 0;

    public function __construct($login, $password)
    {
        $this->login =  $login;
        $this->password =  $password;
    }

    ///Проверка баланса
    public function balance()
    {
        return $this->get($this->request("balance"), "account");
    }

    //отправка смс
    //params = array (text => , source =>, datetime => , action =>, onlydelivery =>, smsid =>)
    public function send(array $phones, $text)
    {
        $params = [
            'action' => 'send',
            'text' => $text
        ];

        $someXML = "";

        foreach ($phones as $phone) {
            $someXML .= "<to number='$phone'></to>";
        }

        $result = $this->request("send", $params, $someXML);

        if ($this->get($result, "code") != 1) {
            $return = array("code" => $this->get($result, "code"), "descr" => $this->get($result, "descr"));
        } else {
            $return = array(
                "code"              => 1,
                "descr"             => $this->get($result, "descr"),
                "datetime"          => $this->get($result, "datetime"),
                "action"            => $this->get($result, "action"),
                "allRecivers"       => $this->get($result, "allRecivers"),
                "colSendAbonent"    => $this->get($result, "colSendAbonent"),
                "colNonSendAbonent" => $this->get($result, "colNonSendAbonent"),
                "priceOfSending"    => $this->get($result, "priceOfSending"),
                "colsmsOfSending"   => $this->get($result, "colsmsOfSending"),
                "price"             => $this->get($result, "price"),
                "smsid"             => $this->get($result, "smsid"),
            );
        }

        return $return;
    }

    public function reports($start = "0000-00-00", $stop = "0000-00-00", $dop = array())
    {
        if (!isset($dop["source"])) {
            $dop["source"] = "%";
        }
        if (!isset($dop["number"])) {
            $dop["number"] = "%";
        }

        $result = $this->request("report", array(
            "start"  => $start,
            "stop"   => $stop,
            "source" => $dop["source"],
            "number" => $dop["number"],
        ));
        if ($this->get($result, "code") != 1) {
            $return = array("code" => $this->get($result, "code"), "descr" => $this->get($result, "descr"));
        } else {
            $return = array(
                "code"  => $this->get($result, "code"),
                "descr" => $this->get($result, "descr"),
            );
            if (isset($result['sms'])) {
                $return["sms"] = $result['sms'];
            }
        }
        return $return;
    }

    public function detailReport($smsid)
    {
        $result = $this->request("report", array("smsid" => $smsid));
        if ($this->get($result, "code") != 1) {
            $return = array("code" => $this->get($result, "code"), "descr" => $this->get($result, "descr"));
        } else {
            $detail = $result["detail"][0];
            $return = array(
                "code"         => $this->get($result, "code"),
                "descr"        => $this->get($result, "descr"),
                "delivered"    => $detail['delivered'],
                "notDelivered" => $detail['notDelivered'],
                "waiting"      => $detail['waiting'],
                "enqueued"     => $detail['enqueued'],
                "cancel"       => $detail['cancel'],
                "onModer"      => $detail['onModer'],
            );
            if (isset($result['sms'])) {
                $return["sms"] = $result['sms'][0];
            }
        }
        return $return;
    }

    public function get($responce, $key)
    {
        if (isset($responce[$key], $responce[$key][0], $responce[$key][0][0])) {
            return $responce[$key][0][0];
        }
        return false;
    }

    public function parseXML($xml)
    {
        if (function_exists("simplexml_load_string")) {
            return $this->XMLToArray($xml);
        } else {
            return $xml;
        }
    }

    public function request($action, $params = array(), $someXML = "")
    {
        $xml = $this->makeXML($params, $someXML);
        if (self::HTTPS_METHOD == "curl") {
            return $this->parseXML($this->request_curl($action, $xml));
        }
        $this->error("В настройках указан неизвестный метод запроса - '" . self::HTTPS_METHOD . "'");
    }

    public function request_curl($action, $xml)
    {
        if (self::USE_HTTPS == 1) {
            $address = self::HTTPS_ADDRESS . "API/XML/" . $action . ".php";
        } else {
            $address = self::HTTP_ADDRESS . "API/XML/" . $action . ".php";
        }
        $ch = curl_init($address);
        curl_setopt($ch, CURLOPT_URL, $address);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function makeXML($params, $someXML = "")
    {
        $xml = "<?xml version='1.0' encoding='UTF-8'?>
		<data>
			<login>" . $this->login . "</login>
			<password>" . $this->password . "</password>
			";
        foreach ($params as $key => $value) {
            $value = $this->getConvertedString($value);
            $xml .= "<$key>$value</$key>";
        }
        $xml .= "$someXML
		</data>";
        return $xml;
    }

    public function getConvertedString($value, $from = false)
    {
        if (self::HTTPS_CHARSET != "utf-8") {
            if (function_exists("iconv")) {
                if (!$from) {
                    return iconv(self::HTTPS_CHARSET, "utf-8", $value);
                } else {
                    return iconv("utf-8", self::HTTPS_CHARSET, $value);
                }
            } else {
                $this->error("Не удается перекодировать переданные параметры в кодировку utf-8 - отсутствует функция iconv");
            }
        }
        return $value;
    }

    public function error($text)
    {
        die($text);
    }

    public function XMLToArray($xml)
    {
        if (!strlen($xml)) {
            $descr = "Не удалось получить ответ от сервера!";
            if (self::USE_HTTPS == 1) {
                $descr .= " Возможно конфигурация вашего сервера не позволяет отправлять HTTPS-запросы. Попробуйте установить значение self::USE_HTTPS = 0 в файле config.php";
            }
            return array("code" => 0, "descr" => $descr);
        }
        $xml = simplexml_load_string($xml);

        $return = array();
        foreach ($xml->children() as $child) {
            $return[$child->getName()][] = $this->makeAssoc((array)$child);
        }
        $return = $this->convertArrayCharset($return);
        return $return;
    }

    public function convertArrayCharset($return)
    {
        foreach ($return as $key => $value) {
            if (is_array($value)) {
                $return[$key] = $this->convertArrayCharset($return[$key]);
            } else {
                $return[$key] = $this->getConvertedString($value, true);
            }
        }
        return $return;
    }

    public function makeAssoc($array)
    {
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_object($value)) {
                    $newValue = array();
                    foreach ($value->children() as $child) {
                        $newValue[] = (string)$child;
                    }
                    $array[$key] = $newValue;
                }
            }
        } else {
            $array = (string)$array;
        }

        return $array;
    }
}
