<?php

namespace App\Services;

use App;
use App\Models\Camp;
use App\Models\Coach;
use App\Models\Post;
use App\Models\Event;
use App\Models\User;
use App\Models\Tour;
use App\Models\Package;
use App\Models\TrainingGroup;
use DB;
use Roumen\Sitemap\Sitemap as SitemapInstance;

class Sitemap
{
    /**
     * @return \Illuminate\Support\Facades\View
     */
    public function render()
    {
        $sitemap = App::make("sitemap");

        $sitemap->setCache('laravel.sitemap', 24 * 60);

        if (!$sitemap->isCached()) {
            $sitemap->add(route('home'));

            // Coaches
            $this->addCoaches($sitemap);

            // Nwtour
            $this->addTour($sitemap);

            // Events
            $this->addEvents($sitemap);

            // Camps
            $this->addCamps($sitemap);

            // Profiles
            $this->addProfiles($sitemap);

            // Content
            $this->addPosts($sitemap);

            // Trainings
            $this->addTrainings($sitemap);
        }

        return $sitemap->render('xml');
    }

    /**
     * @param SitemapInstance $sitemap
     * @return void
     */
    private function addCoaches(SitemapInstance $sitemap): void
    {
        $coaches = Coach::all();

        foreach ($coaches as $coach) {
            $sitemap->add(route('coaches.show', $coach->id));
        }
    }

    /**
     * @param SitemapInstance $sitemap
     * @return void
     */
    private function addTour(SitemapInstance $sitemap): void
    {
        $sitemap->add(route('nwtour.index'));
        $sitemap->add(route('nwtour.ratings'));
        $sitemap->add(route('nwtour.about'));

        $tours = collect(DB::table((new Tour)->getTable())
            ->select('id', 'status')
            ->get());

        $showedTours = $tours->reject(function ($value) {
            return $value->status == 'created';
        });

        $openedTours = $showedTours->reject(function ($value) {
            return $value->status == 'anounsed';
        });

        $finishedTours = $openedTours->where('status', 'finished');

        foreach ($showedTours as $tour) {
            $sitemap->add(route('nwtour.info', $tour->id));
        }

        foreach ($openedTours as $tour) {
            $sitemap->add(route('nwtour.application', $tour->id));
            $sitemap->add(route('nwtour.participants', $tour->id));
        }

        foreach ($finishedTours as $tour) {
            $sitemap->add(route('nwtour.results', $tour->id));
        }
    }

    /**
     * @param SitemapInstance $sitemap
     * @return void
     */
    private function addEvents(SitemapInstance $sitemap): void
    {
        $sitemap->add(route('events.index'));
        $sitemap->add(route('events.locations'));
        $sitemap->add(route('events.trainings'));

        $events = DB::table((new Event)->getTable())
            ->select('id')
            ->get();

        foreach ($events as $event) {
            $sitemap->add(route('events.show', $event->id));
        }

        $packages = DB::table((new Package)->getTable())
            ->select('id')
            ->get();

        foreach ($packages as $package) {
            $sitemap->add(route('packages.show', $package->id));
        }
    }

    /**
     * @param SitemapInstance $sitemap
     * @return void
     */
    private function addCamps(SitemapInstance $sitemap): void
    {
        $sitemap->add(route('camps.index'));

        $camps = DB::table((new Camp)->getTable())
            ->select('id')
            ->get();

        foreach ($camps as $camp) {
            $sitemap->add(route('camps.show', $camp->id));
        }
    }

    /**
     * @param SitemapInstance $sitemap
     * @return void
     */
    private function addProfiles(SitemapInstance $sitemap): void
    {
        $users = DB::table((new User)->getTable())
            ->select('id')
            ->get();

        foreach ($users as $user) {
            $sitemap->add(route('auth.profile.show', $user->id));
        }
    }

    /**
     * @param SitemapInstance $sitemap
     * @return void
     */
    private function addPosts(SitemapInstance $sitemap): void
    {
        $sitemap->add(route('posts.index'));

        $posts = DB::table((new Post)->getTable())
            ->select('url')
            ->get();

        foreach ($posts as $post) {
            $sitemap->add(route('posts.show', $post->url));
        }
    }

    /**
     * @param SitemapInstance $sitemap
     * @return void
     */
    private function addTrainings(SitemapInstance $sitemap): void
    {
        $sitemap->add(route('trainings.index'));

        $trainingGroups = DB::table((new TrainingGroup)->getTable())
            ->select('url')
            ->get();

        foreach ($trainingGroups as $group) {
            $sitemap->add(route('trainings.purchase', $group->url));
        }
    }
}
