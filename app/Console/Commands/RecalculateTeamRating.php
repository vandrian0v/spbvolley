<?php

namespace App\Console\Commands;

use App\Services\RatingHandler;
use Illuminate\Console\Command;
use App\Models\Team;

class RecalculateTeamRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rating:teams-recalculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate teams rating';

    /**
     * Execute the console command.
     *
     * @param RatingHandler $handler
     * @return mixed
     */
    public function handle(RatingHandler $handler)
    {
        Team::withTrashed()->with('tournament.category')->chunk(100, function ($teams) use ($handler) {
            foreach ($teams as $team) {
                /** @var Team $team */
                $team->rating = $handler->getTeamRating($team);
                $team->save();

                $this->info("Team $team->id rating updated.");
            }
        });
    }
}
