<?php

namespace App\Console\Commands;

use App\Services\RatingHandler;
use Illuminate\Console\Command;

class RecalculateRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rating:users-recalculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalcualte users rating';

    /**
     * Execute the console command.
     *
     * @param RatingHandler $handler
     * @return mixed
     */
    public function handle(RatingHandler $handler)
    {
        $handler->recalculateRating();
    }
}
