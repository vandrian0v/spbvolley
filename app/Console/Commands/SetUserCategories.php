<?php

namespace App\Console\Commands;

use App\Models\UserCategory;
use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class SetUserCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:setup-categories';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initial setup of user categories';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::chunk(100, function ($users) {
            /** @var User $user */
            foreach ($users as $user) {
                $isPro = $user->teams()->whereBetween('rank', [1, 4])
                    ->whereHas('tournament', function (Builder $query) {
                        $query->whereIn('category_id', [1, 2]);
                    })->exists();

                if ($isPro) {
                    $this->info("$user->fullname в категории PRO");
                    $user->category_id = UserCategory::PRO;
                    $user->save();
                    continue;
                }

                $isA = $user->teams()->whereBetween('rank', [1, 8])
                    ->whereHas('tournament', function (Builder $query) {
                        $query->whereIn('category_id', [3, 4]);
                    })->exists();

                if ($isA) {
                    $this->info("$user->fullname в категории A");
                    $user->category_id = UserCategory::A;
                    $user->save();
                }
            }
        });
    }
}
