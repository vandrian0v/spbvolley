<?php

use App\Models\User;
use App\Notifications\TelegramNotification;

function notifyMe(string $message)
{
    if (!App::environment('production')) {
        info($message);
    } else {
        $message = str_limit($message, 300, '');
        $message = str_replace('_', '', $message);

        $user = User::find(1);

        $user->notify(new TelegramNotification($message));
    }
}

function csv($callback)
{
    $headers = [
        'Content-type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename=users.csv',
    ];

    return response()->stream($callback, 200, $headers);
}