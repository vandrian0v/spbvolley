<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class CustomSmsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function via($notifiable)
    {
        return [SmsChannel::class, 'database'];
    }

    public function toSms($notifiable)
    {
        return $this->text;
    }

    public function toDatabase($notifiable)
    {
        return [
            'type'    => 'Кастомное sms сообщение',
            'channel' => 'sms',
            'text'    => $this->text,
        ];
    }
}