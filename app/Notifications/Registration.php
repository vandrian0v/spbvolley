<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Registration extends Notification implements ShouldQueue
{
    use Queueable;

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Регистрация на spbvolley.ru')
            ->success()
            ->line('Вы успешно зарегистрировались на сайте spbvolley.ru')
            ->line('На сайте вы можете ознакомиться с расписанием тренировок клуба,
                а также с календарем серии турниров NWTour.');
    }

    public function toDatabase()
    {
        return [
            'type'    => 'Оповещение о регистрации',
            'channel' => 'email',
        ];
    }
}
