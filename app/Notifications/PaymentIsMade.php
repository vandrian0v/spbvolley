<?php

namespace App\Notifications;

use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PaymentIsMade extends Notification implements ShouldQueue
{
    use Queueable;

    private $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Успешная оплата на spbvolley.ru')
            ->success()
            ->line('Оплата прошла успешно')
            ->line(sprintf('Тип оплаты: %s', $this->payment->type->name))
            ->line(str_replace("\n", '<br>', $this->payment->comment))
            ->line(sprintf('Сумма: %d руб.', $this->payment->sum));
    }

    public function toDatabase($notifiable)
    {
        return [
            'type'       => 'Оповещение об успешной оплате',
            'channel'    => 'email',
            'payment_id' => $this->payment->id
        ];
    }
}
