<?php

namespace App\Notifications;

use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PaymentRecieved extends Notification implements ShouldQueue
{
    use Queueable;

    private $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('На сайте произведена оплата')
            ->success()
            ->action('Открыть платеж', route('admin.payments.show', $this->payment->id))
            ->line('Пользователь: ' . $this->payment->user->fullname)
            ->line('Тип оплаты: ' . $this->payment->type->name)
            ->line(sprintf('Сумма: %d руб.', $this->payment->sum))
            ->line(str_replace("\n", '<br>', $this->payment->comment));
    }

    public function toDatabase($notifiable)
    {
        return [
            'type'       => 'На сайте произведена оплата',
            'channel'    => 'email',
            'payment_id' => $this->payment->id,
        ];
    }
}
