<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CustomEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $title;

    private $text;

    public function __construct(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->title)
            ->view('emails.custom', ['text' => $this->text]);
    }

    public function toDatabase($notifiable)
    {
        return [
            'type'    => 'Кастомное email сообщение',
            'channel' => 'email',
            'title'   => $this->title,
            'text'    => preg_replace('/<img src="(.*)">/', '<img src="">', $this->text),
        ];
    }
}