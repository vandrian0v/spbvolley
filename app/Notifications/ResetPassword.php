<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification implements ShouldQueue
{
    use Queueable;

    private $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Восстановление пароля')
            ->line([
                'Мы получили запрос на восстановление пароля для вашего аккаунта.',
                'Нажмите на кнопку для перехода к форме восстановления пароля.',
            ])
            ->action('Сбросить пароль', url('password/reset', $this->token))
            ->line('Если вы не запрашивали восстановление пароля, то просто игнорируйте данное сообщение.');
    }

    public function toDatabase($notifiable)
    {
        return [
            'type'    => 'Восстановление пароля',
            'channel' => 'email',
            'token'   => $this->token,
        ];
    }
}
