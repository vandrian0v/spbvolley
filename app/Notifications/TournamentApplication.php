<?php

namespace App\Notifications;

use App\Models\Tour;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TournamentApplication extends Notification implements ShouldQueue
{
    use Queueable;

    private $tour;

    public function __construct(Tour $tour)
    {
        $this->tour = $tour;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Подача заявки на участие в этапе NWTour')
            ->success()
            ->line('Вы успешно заявились на этап серии турниров NWTour по пляжному волейболу.')
            ->line('Ознsакомиться с информацией о турнире и оплатить заявочный взнос можно нажав на кнопку ниже.')
            ->action('Информация о турнире', route('nwtour.info', $this->tour->id))
            ->line('До встречи на турнире!');
    }

    public function toDatabase($notifiable)
    {
        return [
            'type'    => 'Оповещение о заявке на турнир',
            'channel' => 'email',
            'tour_id' => $this->tour->id,
        ];
    }
}
