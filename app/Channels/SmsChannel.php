<?php

namespace App\Channels;

use App\Contracts\Notifications\SmsSender;
use App\Models\User;
use Illuminate\Notifications\Notification;

class SmsChannel
{
    protected $smsSender;

    /**
     * SmsChannel constructor.
     *
     * @param SmsSender $smsSender
     */
    public function __construct(SmsSender $smsSender)
    {
        $this->smsSender = $smsSender;
    }

    /**
     * @param $notifiable User
     * @param Notification $notification
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$phone = $notifiable->routeNotificationFor('sms', $notification)) {
            return;
        }

        $text = $notification->toSms($notifiable);

        $result = $this->smsSender->send([$phone], $text);

        info("Sms to $notifiable->phone", $result ?? []);
    }
}