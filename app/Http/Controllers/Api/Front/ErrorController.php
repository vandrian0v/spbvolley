<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;

class ErrorController extends BaseController
{
    public function __invoke(Request $request)
    {
        $message = sprintf(
            'JS error: referer: %s, message: %s',
            $request->headers->get('referer'),
            $request->input('error')
        );

        notifyMe($message);
        info($message);
    }
}