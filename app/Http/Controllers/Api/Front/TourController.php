<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Controllers\Api\BaseController;
use App\Models\Tour;
use Illuminate\Http\Request;

class TourController extends BaseController
{
    public function index(Request $request)
    {
        $filters = $request->input('filters', []);

        $tours = Tour::with('tournaments.category')
            ->filter($filters)
            ->where('status', '!=', 'created')
            ->get();

        return $this->success(compact('tours'));
    }

    public function show(Tour $tour)
    {
        $tour->load(
            'tournaments.teams.payment',
            'tournaments.teams.users',
            'tournaments.teams.category',
            'tournaments.category'
        );

        return $this->success(compact('tour'));
    }
}