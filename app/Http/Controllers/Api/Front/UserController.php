<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    public function index(Request $request)
    {
        $filters = $request->input('filters', []);

        $users = User::filter($filters)
            ->with('category')
            ->get();

        return $this->success(compact('users'));
    }

    public function search(Request $request)
    {
        $users = User::search($request->input('search'))
            ->when($request->input('gender') && in_array($request->input('gender'), ['male', 'female']),
                function (Builder $query) use ($request) {
                    return $query->where('gender', $request->input('gender'));
                })
            ->limit(20)
            ->get();

        $users = $users->transform(function (User $user) {
            return [
                'id'    => $user->id,
                'label' => $user->fullname,
            ];
        });

        return $this->success(compact('users'));
    }
}