<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected function success(array $data = null)
    {
        $success = true;

        if ($data === null) {
            $data = [];
        }

        $result = array_merge($data, compact('success'));

        return response()->json($result);
    }

    protected function error($errors, $status = 200)
    {
        $success = false;

        if (is_string($errors)) {
            $errors = [$errors];
        }

        return response()->json(compact('success', 'errors'), $status);
    }
}