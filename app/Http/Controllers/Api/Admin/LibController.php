<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\Rating;
use App\Models\Tour;
use App\Models\TournamentCategory;
use App\Models\TrainingGroup;

class LibController extends BaseController
{
    public function tournamentCategories()
    {
        return $this->success([
            'tournament_categories' => TournamentCategory::all(),
        ]);
    }

    public function tourStatuses()
    {
        return $this->success([
            'tour_statuses' => Tour::STATUS_TITLES,
        ]);
    }

    public function ratings()
    {
        $ratings = Rating::orderBy('start_rank')
            ->get()
            ->groupBy('category_id');

        return $this->success(compact('ratings'));
    }

    public function trainingGroups()
    {
        return $this->success([
            'training_groups' => TrainingGroup::withCount('trainings')->get()
        ]);
    }
}