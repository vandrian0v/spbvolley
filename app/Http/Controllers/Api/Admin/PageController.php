<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends BaseController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $pages = Page::latest()->get();

        return $this->success(compact('pages'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $page = Page::create($request->all());

        return $this->success(compact('page'));
    }

    /**
     * @param Request $request
     * @param Page $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Page $page)
    {
        $page->update($request->all());

        return $this->success(compact('page'));
    }

    /**
     * @param Page $page
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Page $page)
    {
        if ($page->delete()) {
            return $this->success();
        }

        return $this->error('Не удалось удалить страницу');
    }
}