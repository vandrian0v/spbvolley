<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\Admin\Teams\UpdateRequest;
use App\Http\Requests\Api\Admin\Teams\UpdateUsersRequest;
use App\Models\Team;
use Exception;

class TeamController extends BaseController
{
    public function update(UpdateRequest $request, Team $team)
    {
        if ($request->has('is_paid') && $team->tournament->limit_exceeded) {
            return $this->error('Не удалось провести оплату т.к. в категорию уже заявилось максимальное число команд.');
        }

        $team->update($request->all());

        return $this->success();
    }

    public function updateUsers(UpdateUsersRequest $request, Team $team)
    {
        $team->users()->sync($request->input('user_ids'));

        $users = $team->users;

        return $this->success(compact('users'));
    }

    public function destroy(Team $team)
    {
        try {
            $team->delete();

            return $this->success();
        } catch (Exception $e) {
            return $this->error('Не удалось удалить команду');
        }
    }
}