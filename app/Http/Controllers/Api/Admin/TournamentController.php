<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\Admin\Tournaments\UpdateRequest;
use App\Models\Tournament;

class TournamentController extends BaseController
{
    public function update(UpdateRequest $request, Tournament $tournament)
    {
        $input = $request->only('contribution');
        $input['limit'] = $request->input('limit', null);

        if (
            $input['limit'] !== $tournament->limit
            && !is_null($input['limit'])
            && $input['limit'] < $tournament->confirmedTeams()->count()
        ) {
            return $this->error('Не удалось обновить лимит т.к. в категорию уже заявилось больше команд.');
        }

        $tournament->update($input);

        return $this->success();
    }
}