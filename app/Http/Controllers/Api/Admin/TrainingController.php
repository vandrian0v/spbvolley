<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\Training;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrainingController extends BaseController
{
    public function index(Request $request)
    {
        $limit          = $request->input('limit', 10);
        $filters        = $request->input('filters');
        $order          = $request->input('order', 'id');
        $orderDirection = $request->input('orderDirection', 'desc');

        $trainings = Training::with('coach', 'location', 'group')
            ->filter($filters)
            ->orderBy($order, $orderDirection)
            ->paginate($limit);

        return $this->success(compact('trainings'));
    }

    public function group(TrainingGroup $group)
    {
        $today = Carbon::today();

        $group->load(['trainings' => function ($query) use ($today) {
            return $query->orderBy('weekday')
                ->orderBy('start_time')
                ->with('location', 'coach')
                ->with(['userTrainings' => function ($query) use ($today) {
                    $query->paid()
                        ->with('user')
                        ->where('date', '>=', $today);
                }]);
        }]);

        $group->trainings->map(function (Training $training) {
            $training->append('closest_dates');
            $training->setRelation('userTrainings', $training->userTrainings->groupBy(function ($ut) {
                return $ut->date->format('Y-m-d');
            }));
        });

        return $this->success(compact('group'));
    }
}