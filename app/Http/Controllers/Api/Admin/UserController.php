<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    public function index(Request $request)
    {
        $users = User::
            filter($request->input('filters', []))
            ->search($request->input('search'))
            ->limit(20)
            ->get();


        $users = $users->transform(function (User $user) {
            return [
                'id'    => $user->id,
                'label' => $user->fullname,
                'email' => $user->email,
                'phone' => $user->phone,
            ];
        });

        return $this->success(compact('users'));
    }
}