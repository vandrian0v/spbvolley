<?php

namespace App\Http\Controllers\Api\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Storage;

class UploadController
{
    public function uploadImage(Request $request)
    {
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $url = $request->file('file')
                ->storeAs('content/' . Carbon::today()->format('Y-m-d'), $request->file('file')->hashName());

            return response()->json([
                'location' => Storage::url($url)
            ]);
        }
    }
}