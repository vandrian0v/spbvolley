<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\Admin\Tours\SearchRequest;
use App\Http\Requests\Api\Admin\Tours\StoreRequest;
use App\Http\Requests\Api\Admin\Tours\UpdateRequest;
use App\Models\Tour;
use App\Models\Tournament;
use App\Models\TournamentCategory;

class TourController extends BaseController
{
    public function index(SearchRequest $request)
    {
        $limit          = $request->input('limit', 10);
        $filters        = $request->input('filters');
        $order          = $request->input('order', 'id');
        $orderDirection = $request->input('orderDirection', 'desc');

        $tours = Tour::with('tournaments.category')
            ->filter($filters)
            ->orderBy($order, $orderDirection)
            ->paginate($limit);

        return $this->success(compact('tours'));
    }

    public function show(Tour $tour)
    {
        $tour->load('tournaments.category', 'tournaments.teams.users', 'tournaments.teams.payment');

        return $this->success(compact('tour'));
    }

    public function store(StoreRequest $request)
    {
        /** @var Tour $tour */
        $tour = Tour::create($request->all());

        foreach ($request->get('categories') as $categoryId) {
            /** @var TournamentCategory $category */
            $category = TournamentCategory::find($categoryId);

            $tour->tournaments()->create([
                'category_id' => $category->id,
                'contribution' => $category->default_contribution,
            ]);
        }

        return $this->success(compact('tour'));
    }

    public function update(UpdateRequest $request, Tour $tour)
    {
        $tour->update($request->all());

        $categories = collect($request->get('categories'));

        $categoriesToDelete = $tour->categories->diff($categories);
        $categoriesToCreate = $categories->diff($tour->categories);

        foreach ($categoriesToDelete as $categoryId) {
            $tour->tournaments()
                ->where('category_id', $categoryId)
                ->delete();
        }

        foreach ($categoriesToCreate as $categoryId) {
            /** @var TournamentCategory $category */
            $category = TournamentCategory::find($categoryId);

            /** @var Tournament $tournament */
            $tournament = $tour->tournaments()
                ->withTrashed()
                ->where('category_id', $categoryId)
                ->first();

            if ($tournament) {
                $tournament->restore();
            } else {
                $tour->tournaments()->create([
                    'category_id' => $category->id,
                    'contribution' => $category->default_contribution,
                ]);
            }
        }

        $tour->load('tournaments.category', 'tournaments.teams.users');

        return $this->success(compact('tour'));
    }

    public function destroy(Tour $tour)
    {
        try {
            $tour->delete();

            return $this->success();
        } catch (\Exception $e) {
            return $this->error('Не удалось удалить этап');
        }
    }
}