<?php

namespace App\Http\Controllers\Api\Admin;

use App\Contracts\Notifications\SmsSender;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\Admin\Notifications\SendEmailsRequest;
use App\Http\Requests\Api\Admin\Notifications\SendSmsRequest;
use App\Models\User;
use App\Notifications\CustomEmailNotification;
use App\Notifications\CustomSmsNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Notification;
use App\Models\Notification as NotificationModel;

class NotificationController extends BaseController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $limit = (int) $request->input('limit', 50);
        $type  = $request->input('type', 'email');

        NotificationModel::latest()
            ->with('user')
            ->when($type === 'sms', function (Builder $query) {
                return $query->where('type', CustomSmsNotification::class);
            })
            ->when($type === 'email', function (Builder $query) {
                return $query->where('type', CustomEmailNotification::class);
            })
            ->limit($limit)
            ->get();

        return $this->success(compact('notifications'));
    }

    /**
     * @param SendEmailsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmails(SendEmailsRequest $request)
    {
        $users = User::whereIn('id', $request->input('user_ids'))->get();
        $title = $request->input('title');
        $text  = $request->input('text');

        notifyMe(sprintf("Email рассылка на %d пользователей, тема: %s", $users->count(), $title));

        Notification::send(
            $users,
            new CustomEmailNotification($title, $text)
        );

        return $this->success();
    }

    /**
     * @param SendSmsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSms(SendSmsRequest $request)
    {
        $users = User::whereIn('id', $request->input('user_ids'))->get();
        $text  = $request->input('text');

        notifyMe(sprintf("Sms рассылка на %d пользователей, текст: %s", $users->count(), $text));

        Notification::send(
            $users,
            new CustomSmsNotification($text)
        );

        return $this->success();
    }

    /**
     * @param SmsSender $smsSender
     * @return \Illuminate\Http\JsonResponse
     */
    public function smsBalance(SmsSender $smsSender)
    {
        $balance = $smsSender->balance();

        return $this->success(compact('balance'));
    }
}