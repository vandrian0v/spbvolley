<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\Sponsor;
use Illuminate\Http\Request;

class SponsorController extends BaseController
{
    public function index()
    {
        $sponsors = Sponsor::orderBy('id')->get();

        return $this->success(compact('sponsors'));
    }

    public function store(Request $request)
    {
        $sponsor = Sponsor::create($request->all());

        return $this->success(compact('sponsor'));
    }


    public function update(Request $request, Sponsor $sponsor)
    {
        $sponsor->update($request->all());

        return $this->success(compact('sponsor'));
    }

    public function image(Request $request, Sponsor $sponsor)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $request->file('image')->storeAs("{$sponsor->getTable()}/{$sponsor->id}", 'preview');
        }

        return $this->success([
            'url' => $sponsor->preview_url
        ]);
    }

    /**
     * @param Sponsor $sponsor
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Sponsor $sponsor)
    {
        if ($sponsor->delete()) {
            return $this->success();
        }

        return $this->error('Не удалось удалить услугу');
    }
}