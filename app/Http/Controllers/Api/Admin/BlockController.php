<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\Block;
use Illuminate\Http\Request;

class BlockController extends BaseController
{
    public function index()
    {
        $blocks = Block::orderBy('order')->get();

        return $this->success(compact('blocks'));
    }

    public function store(Request $request)
    {
        $block = Block::create($request->all());

        return $this->success(compact('block'));
    }


    public function update(Request $request, Block $block)
    {
        $block->update($request->all());

        return $this->success(compact('block'));
    }

    public function image(Request $request, Block $block)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $request->file('image')->storeAs("{$block->getTable()}/{$block->id}", 'preview');
        }

        return $this->success([
            'url' => $block->preview_url
        ]);
    }

    /**
     * @param Block $block
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Block $block)
    {
        if ($block->delete()) {
            return $this->success();
        }

        return $this->error('Не удалось удалить услугу');
    }
}