<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\TournamentContributionOrderRequest;
use App\Http\Requests\TrainingOrderRequest;
use App\Models\Payment;
use App\Models\Team;
use App\Services\Tournaments\TournamentService;
use App\Services\Trainings\TrainingService;
use App\Services\YandexKassa;
use Exception;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    private $yandexKassa;

    /**
     * OrderController constructor.
     * @param YandexKassa $yandexKassa
     */
    public function __construct(YandexKassa $yandexKassa)
    {
        $this->yandexKassa = $yandexKassa;
    }

    /**
     * @param TournamentContributionOrderRequest $request
     * @param TournamentService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function formTournamentContributionOrder(
        TournamentContributionOrderRequest $request, TournamentService $service
    ) {
        /** @var Team $team */
        $team = Team::find($request->get('team_id'));

        if ($team->payment && $team->payment->is_confirmed) {
            return back()->with('warning', 'Взнос уже оплачен.');
        }

        if ($team->tournament->limit_exceeded) {
            return back()->with('warning', 'Прием заявок в эту категорию окончен.');
        }

        if (!$team->payment) {
            $service->createPayment($request->user(), $team);
        } else {
            $service->refreshPayment($team);
        }

        return $this->redirectToYandex($team->payment);
    }

    /**
     * @param TrainingOrderRequest $request
     * @param TrainingService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function formTrainingOrder(TrainingOrderRequest $request, TrainingService $service)
    {
        $user               = $request->user();
        $trainings          = $request->get('trainings');
        $subscriptionTypeId = $request->get('subscription_type_id');

        try {
            /** @var Payment $payment */
            $payment = $service->createPayment($user, $trainings, $subscriptionTypeId);
        } catch (Exception $e) {
            return back()->with('warning', $e->getMessage());
        }

        return $this->redirectToYandex($payment);
    }

    /**
     * @param Payment $payment
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectToYandex(Payment $payment)
    {
        $url = $this->yandexKassa->buildUrl($payment);

        return redirect()->away($url);
    }
}
