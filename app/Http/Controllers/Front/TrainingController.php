<?php

namespace App\Http\Controllers\Front;

use App\Models\Coach;
use App\Models\TrainingGroup;
use App\Http\Controllers\Controller;

class TrainingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $coaches = Coach::all();

        $groups = TrainingGroup::has('trainings')
            ->with('trainings.subscriptionTypes', 'trainings.coach', 'trainings.location')
            ->sorted()
            ->get();

        return view('front.trainings.index', compact('coaches', 'groups'));
    }

    /**
     * @param TrainingGroup $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function purchase(TrainingGroup $group)
    {
        return view('trainings.purchase', compact('group'));
    }
}
