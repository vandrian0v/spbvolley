<?php

namespace App\Http\Controllers\Front;

use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->has('category')) {
            $category = PostCategory::where('url', $request->get('category'))->first();
        }

        $posts= Post::orderBy('created_at', 'desc')
           ->with('category');

        if (isset($category)) {
            $posts->where('category_id', $category->id);
        }

        $posts = $posts->paginate(10);

        if (isset($category)) {
            $posts->appends(['category' => $category->url]);
        }

        return view('posts.index', compact('posts'));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        if ($post->id === 969) {
            return redirect()->route('camps.show', 29);
        }

        return view('front.posts.show', compact('post'));
    }
}
