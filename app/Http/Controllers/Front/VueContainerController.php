<?php

namespace App\Http\Controllers\Front;

class VueContainerController
{
    public function __invoke()
    {
        return view('front');
    }
}