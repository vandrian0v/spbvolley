<?php

namespace App\Http\Controllers\Front;

use App\Models\Event;
use App\Models\Location;
use App\Models\Package;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $events   = Event::orderBy('date', 'desc')->with('photos', 'location')->get();
        $packages = Package::orderBy('price')->take(3)->get();

        return view('events.index', compact('events', 'packages'));
    }

    /**
     * @param Event $event
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Event $event)
    {
        return view('events.show', compact('event'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trainings()
    {
        return view('events.trainings');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function locations()
    {
        $locations = Location::all();

        $data = [
            'Летнее время' => $locations->where('season', 'summer'),
            'Зимнее время' => $locations->where('season', 'winter'),
        ];

        return view('events.locations', compact('data'));
    }

    /**
     * @param Package $package
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function package(Package $package)
    {
        return view('events.package', compact('package'));
    }
}
