<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\FeedbackMail;
use App\Models\Team;
use App\Models\User;
use App\Services\RatingHandler;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Mail;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trainings()
    {
        $trainings = Auth::user()
            ->trainings()
            ->orderBy('date', 'desc')
            ->paginate(10);

        $today = Carbon::today();

        return view('profile.trainings', compact('trainings', 'today'));
        return view('front.profile.trainings', compact('trainings', 'today'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function payments()
    {
        /** @var User $user */
        $user = Auth::user();

        $payments = $user->payments()
            ->confirmed()
            ->latest()
            ->paginate(10);

        return view('profile.payments', compact('payments'));
        return view('front.profile.payments', compact('payments'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $user = $request->user();

        return view('profile.edit', compact('user'));
        return view('front.profile.edit', compact('user'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function feedback()
    {
        return view('profile.feedback');
        return view('front.profile.feedback');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendFeedback(Request $request)
    {
        $this->validate($request, [
            'text' => 'required|min:10|max:400',
        ]);

        $text = $request->get('text');

        Mail::to(config('mail.from.address'))->send(new FeedbackMail(Auth::user()->fullname, $text));
        notifyMe(sprintf('%d %s: %s', Auth::id(), Auth::user()->fullname, $text));

        return back()->with('notice', 'Ваше сообщение отправлено.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'surname'    => 'required',
            'name'       => 'required',
            'birth_date' => 'date_format:Y-m-d',
            'height'     => 'integer|min:50|max:250',
            'weight'     => 'integer|min:40|max:150',
        ]);

        $input = $request->only('surname', 'name');

        $input['birth_date'] = $request->get('birth_date') ?: null;
        $input['height']     = $request->get('height') ?: null;
        $input['weight']     = $request->get('weight') ?: null;
        $input['hobby']      = $request->get('hobby') ?: null;

        $request->user()->update($input);

        return back()->with('notice', 'Информация успешно обновлена.');
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $teams = $user->teams()
            ->with('users', 'tournament.category', 'tournament.nwtour')
            ->whereNotNull('rank')
            ->where('rank', '!=', 0)
            ->whereHas('tournament', function (Builder $query) {
                $query->whereHas('nwtour', function (Builder $query) {
                    $query->where('status', 'finished');
                });
            })->get();

        return view('profile.show', compact('user', 'teams'));
        return view('front.profile.show', compact('user', 'teams'));
    }
}
