<?php

namespace App\Http\Controllers\Front;

use App\Models\Page;

class PageController
{
    public function show(Page $page)
    {
        return view('front.pages.show', compact('page'));
    }
}