<?php

namespace App\Http\Controllers\Front;

use App\Events\PaymentConfirmed;
use App\Events\TournamentApplicationReceived;
use App\Http\Requests\PaymentAvisoRequest;
use App\Http\Requests\PaymentCancelRequest;
use App\Http\Requests\PaymentCheckRequest;
use App\Models\Payment;
use App\Services\YandexKassa;
use Auth;
use DB;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * @param PaymentCheckRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function check(PaymentCheckRequest $request)
    {
        if (!$request->isHashValid()) {
            return $request->authErrorResponse();
        }

        DB::beginTransaction();

        /** @var Payment $payment */
        $payment = Payment::where('id', $request->get('orderNumber'))->lockForUpdate()->first();

        if ($payment->type === Payment::TYPE_TOURNAMENT_CONTRIBUTION && $payment->team->tournament->limit_exceeded) {
            DB::rollBack();

            return $request->teamsLimitExceededResponse();
        }

        $payment->update([
            'status'           => Payment::STATUS_CHECKED,
            'invoice_id'       => $request->get('invoiceId'),
            'order_sum_amount' => $request->get('orderSumAmount'),
            'shop_sum_amount'  => $request->get('shopSumAmount'),
            'order_currency'   => $request->get('orderSumCurrencyPaycash'),
            'shop_currency'    => $request->get('shopSumCurrencyPaycash'),
        ]);

        DB::commit();

        return $request->successResponse();
    }

    /**
     * @param PaymentAvisoRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function aviso(PaymentAvisoRequest $request)
    {
        if (!$request->isHashValid()) {
            return $request->authErrorResponse();
        }

        DB::beginTransaction();

        /** @var Payment $payment */
        $payment = Payment::where('id', $request->get('orderNumber'))->lockForUpdate()->first();

        $payment->update([
            'status'  => Payment::STATUS_CONFIRMED,
            'user_id' => $request->get('customerNumber'),
        ]);

        DB::commit();

        event(new PaymentConfirmed($payment));

        // Если платеж привязан к команде то приравниванием потверждение платежа к получению заявки
        if ($payment->team) {
            event(new TournamentApplicationReceived($payment->team));
        }

        return $request->successResponse();
    }

    /**
     * @param PaymentCancelRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function cancel(PaymentCancelRequest $request)
    {
        if (!$request->isHashValid()) {
            return $request->authErrorResponse();
        }

        Payment::where('id', $request->get('orderNumber'))
            ->update([
                'status' => Payment::STATUS_CANCELED,
            ]);

        return $request->successResponse();
    }

    /**
     * @param YandexKassa $yandexKassa
     * @return \Illuminate\Http\RedirectResponse
     */
    public function testPayment(YandexKassa $yandexKassa)
    {
        $payment = request()->user()->payments()->create([
            'payment_type_id' => Payment::TYPE_TRAINING,
            'comment' => 'Тестовый платеж',
            'sum' => 1,
        ]);

        $url = $yandexKassa->buildUrl($payment);

        return redirect()->away($url);
    }

    /**
     * @param Payment $payment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success(Payment $payment)
    {
        if ($payment->user_id !== Auth::id() || !$payment->is_confirmed) {
            abort(404);
        }

        return view('front.payments.success', compact('payment'));
    }
}