<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Events\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        /** @var User $user */
        $user = User::create([
            'name'     => trim($data['name']),
            'surname'  => trim($data['surname']),
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
            'gender'   => $data['gender'],
            'phone'    => phone($data['phone'], 'RU')
        ]);

        return $user;
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {
        $user = $this->create($request->all());

        $this->guard()->login($user);

        event(new UserRegistered($user));

        return redirect($this->redirectPath());
    }
}
