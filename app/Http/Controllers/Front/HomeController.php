<?php

namespace App\Http\Controllers\Front;

use App\Models\Block;
use App\Models\Coach;
use App\Models\Post;
use App\Models\PostCategory;
use App\Http\Controllers\Controller;
use App\Models\TrainingGroup;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $coaches = Coach::all();
        $sliderPosts = Post::latest()->slider()->get();
        $blocks = Block::orderBy('order')->get();

        $posts = [];

        $postCategories = PostCategory::whereIn('url', [
            PostCategory::CATEGORY_TRAINING,
            PostCategory::CATEGORY_CAMPS,
            PostCategory::CATEGORY_TOURNAMENTS,
        ])->get();

        foreach ($postCategories as $category) {
            $posts[$category->id] = Post::latest()
                ->ofCategory($category->id)
                ->take(4)
                ->get();
        }

        // @TODO нутыпонел
        $posts[0] = Post::latest()
            ->take(4)
            ->get();

        $tmpCategory = new PostCategory([
            'id' => 0,
            'url' => 'all',
            'name' => 'Общие новости'
        ]);

        $postCategories->prepend($tmpCategory);

        return view('front.home.index', compact('blocks', 'coaches', 'sliderPosts', 'postCategories', 'posts'));
    }

    public function oplata()
    {
        $groups = TrainingGroup::has('trainings')
            ->with('trainings.subscriptionTypes', 'trainings.coach', 'trainings.location')
            ->sorted()
            ->get();

        return view('front.home.oplata', compact('groups'));
    }

    public function individual()
    {
        $coaches = Coach::all();

        $groups = TrainingGroup::has('trainings')
            ->with('trainings.subscriptionTypes', 'trainings.coach', 'trainings.location')
            ->sorted()
            ->get();

        return view('front.home.individual', compact('groups', 'coaches'));
    }

    /**
     * @param Coach $coach
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCoach(Coach $coach)
    {
        return view('front.coaches.show', compact('coach'));
    }
}
