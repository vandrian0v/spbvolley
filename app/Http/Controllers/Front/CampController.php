<?php

namespace App\Http\Controllers\Front;

use App\Models\Camp;
use App\Models\CampReview;
use App\Models\Coach;
use App\Http\Controllers\Controller;

class CampController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $coaches     = Coach::all();
        $futureCamps = Camp::future()->get();
        $pastCamps   = Camp::past()->get();
        $reviews     = CampReview::all();

        return view('front.camps.index', compact('coaches', 'futureCamps', 'pastCamps', 'reviews'));
    }

    /**
     * @param Camp $camp
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Camp $camp)
    {
        return view('front.camps.show', compact('camp'));
    }
}
