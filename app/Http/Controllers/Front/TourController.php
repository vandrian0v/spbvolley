<?php

namespace App\Http\Controllers\Front;

use App\Models\Team;
use App\Models\Tour;
use App\Models\Tournament;
use App\Models\UserCategory;
use App\Services\RatingHandler;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;

class TourController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $query = Tour::with('tournaments.category')
            ->where('status', '!=', 'created');

        $futureQuery = clone $query;
        $pastQuery = clone $query;

        $futureTours = $futureQuery
            ->orderBy('start_date', 'asc')
            ->future()
            ->get();

        $pastTours = $pastQuery
            ->orderBy('start_date', 'desc')
            ->past()
            ->get();

        return view('nwtour.index', compact('futureTours', 'pastTours'));
    }

    /**
     * @param Tour $tour
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function info(Tour $tour)
    {
        if ($tour->status == Tour::STATUS_CREATED) {
            abort(404);
        }

        $tour->load('tournaments.category');

        return view('nwtour.info', compact('tour'));
    }

    /**
     * @param Tour $tour
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function application(Tour $tour)
    {
        if (in_array($tour->status, [Tour::STATUS_CREATED, Tour::STATUS_ANNOUNCED])) {
            abort(404);
        }

        return view('nwtour.application', compact('tour'));
    }

    /**
     * @param RatingHandler $ratingHandler
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ratings(RatingHandler $ratingHandler)
    {
        $ratingTypes = $ratingHandler->getRatingTypes();
        $userCategories = UserCategory::all();

        return view('nwtour.ratings', compact('ratingTypes', 'userCategories'));
    }

    /**
     * @param Tour $tour
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function results(Tour $tour)
    {
        if ($tour->status != Tour::STATUS_FINISHED) {
            abort(404);
        }

        $tour->load([
            'tournaments' => function ($query) {
                /** @var Builder $query */
                $query->with(['teams' => function ($query) {
                    /** @var Builder $query */
                    $query->with('users', 'category')
                        ->whereHas('category')
                        ->whereNotNull('rank')
                        ->where('rank', '!=', 0)
                        ->orderBy('rank');
                }]);
            },
        ]);

        $categories = $tour->tournaments
            ->pluck('teams')
            ->collapse()
            ->groupBy('category.fullname');

        return view('nwtour.results', compact('tour', 'categories'));
    }

    /**
     * @param Tour $tour
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function participants(Tour $tour)
    {
        if (in_array($tour->status, [Tour::STATUS_ANNOUNCED, Tour::STATUS_CREATED])) {
            abort(404);
        }

        $tour->load(
            'tournaments.category',
            'tournaments.teams.users',
            'tournaments.teams.payment',
            'tournaments.confirmedTeams.users',
            'tournaments.confirmedTeams.payment'
        );

        if (in_array($tour->status, [Tour::STATUS_OPENED, Tour::STATUS_CLOSED])) {
            $tour->tournaments->map(function (Tournament $tournament) {
                $tournament->teams = $tournament->teams->sortByDesc(function (Team $team) {
                    return $team->users_rating;
                });
            });
        }

        return view('nwtour.participants', compact('tour'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        return view('nwtour.about');
    }
}
