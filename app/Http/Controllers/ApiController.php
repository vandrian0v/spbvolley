<?php

namespace App\Http\Controllers;

use App\Http\Requests\TournamentApplicationRequest;
use App\Mail\CampApplicationReceived;
use App\Mail\EventApplicationReceived;
use App\Mail\TrainingApplicationReceived;
use App\Models\CampApplication;
use App\Models\EventApplication;
use App\Models\SubscriptionType;
use App\Models\Tour;
use App\Models\Tournament;
use App\Models\TournamentCategory;
use App\Models\Training;
use App\Models\TrainingGroup;
use App\Models\User;
use App\Services\RatingHandler;
use App\Services\Tournaments\Exceptions\ValidationException;
use App\Services\Tournaments\TournamentService;
use App\Services\Trainings\Calendar;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Mail;

class ApiController extends Controller
{
    /**
     * Список проводимых категорий на турнире
     *
     * @param Tour $tour
     * @return array
     */
    public function categories(Tour $tour): array
    {
        $categories = TournamentCategory::find($tour->categories->toArray());

        $categories->map(function ($category) {
            $category->name = $category->fullname;
        });

        return $categories->getDictionary();
    }

    /**
     * @param Tour $tour
     * @return mixed
     */
    public function tournaments(Tour $tour)
    {
        $tournaments = $tour->tournaments()->with('category', 'confirmedTeams')->get();

        $tournaments->map(function (Tournament $tournament) {
            $tournament->setAppends(['limit_exceeded']);
            $tournament->category->name = $tournament->category->fullname;
            $tournament->makeHidden(['teams', 'deleted_at', 'limit', 'nwtour_id']);
        });

        return $tournaments;
    }

    /**
     * Рейтинг для конкретного типа
     *
     * @param RatingHandler $ratingHandler
     * @param $type
     * @param $categoryId
     * @return array
     * @throws \Exception
     */
    public function rating(RatingHandler $ratingHandler, string $type, int $categoryId): array
    {
        return cache()->remember("rating.$type.$categoryId", 60, function () use ($ratingHandler, $type, $categoryId) {
            return $ratingHandler->getRatingByType($type, $categoryId)->toArray();
        });
    }

    /**
     * Тренировочная группа
     *
     * @param TrainingGroup $group
     * @return array
     */
    public function trainingGroup(TrainingGroup $group): array
    {
        $group->load('trainings.subscriptionTypes', 'trainings.location', 'trainings.coach');

        $group->trainings->transform(function ($training) {
            /** @var Training $training */
            $training->subscription_types = $training->subscriptionTypes->getDictionary();
            $training->location->setAppends(['icon', 'interval']);
            $training->coach->setAppends(['preview_url']);

            return $training;
        });

        return $group->toArray();
    }

    /**
     * Типы абонементов
     *
     * @return array
     */
    public function trainingSubscriptionTypes(): array
    {
        return cache()->remember('subscription_types', 60, function () {
            return SubscriptionType::all()->getDictionary();
        });
    }

    /**
     * Заявка на турнир
     *
     * @param TournamentApplicationRequest $request
     * @param TournamentService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function tournamentApplication(TournamentApplicationRequest $request, TournamentService $service)
    {
        $tour = Tour::find($request->get('nwtour_id'));
        $category = TournamentCategory::find($request->get('category_id'));
        // $users = User::whereIn('email', $request->get('users'))->get();
        $users = User::whereIn('id', $request->get('user_ids'))->get();

        try {
            $data = $service->addTeam($tour, $category, $users);

            return response()->json(array_merge(['status' => 'ok'], $data));
        } catch (ValidationException $e) {
            return response()->json(['errors' => [$e->getMessage()]], 422);
        }
    }

    /**
     * Заявка в лагерь
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function campApplication(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'required',
            'phone'   => 'required',
            'camp_id' => 'required|exists:camp,id',
            'comment' => '',
        ]);

        /** @var CampApplication $application */
        $application = CampApplication::create($request->all());

        Mail::to(config('mail.from.address'))->send(new CampApplicationReceived($application));

        return response()->json(['status' => 'ok']);
    }

    /**
     * Заявка на мероприятие
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eventApplication(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'email' => 'required|email',
        ]);

        /** @var EventApplication $application */
        $application = EventApplication::create($request->all());

        Mail::to(config('mail.from.address'))->send(new EventApplicationReceived($application));

        return response()->json(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function trainingApplication(Request $request)
    {
        $this->validate($request, [
//            'name'    => 'required',
//            'email'   => 'required',
            'phone'   => 'required',
//            'comment' => '',
//            'group'   => '',
        ]);

        $data = $request->only('phone');
//
//        if ($request->get('group') == '0') {
//            $data['group'] = 'Индивидуальная тренировка';
//        } else {
//            /** @var TrainingGroup $group */
//            $group = TrainingGroup::find($request->get('group'));
//            $data['group'] = $group->name ?? '';
//        }

        Mail::to(config('mail.from.address'))->send(new TrainingApplicationReceived($data));

        return response()->json(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @param Training $training
     * @param Calendar $calendar
     * @return array
     */
    public function trainingCalendar(Request $request, Training $training, Calendar $calendar): array
    {
        $this->validate($request, [
            'year'                 => 'date_format:Y|required_with:month',
            'month'                => 'integer|between:1,12|required_with:year',
            'subscription_type_id' => 'integer|exists:subscription_type,id',
        ]);

        $today = Date::today();

        $date = Date::parse(sprintf(
            '%d-%d',
            (int)$request->get('year') ?: $today->year,
            (int)$request->get('month') ?: $today->month
        ));

        $minDate = $today->copy()->subDay();

        if ($request->has('subscription_type_id')) {
            /** @var SubscriptionType $subscriptionType */
            $subscriptionType = SubscriptionType::find($request->get('subscription_type_id'));

            $subscriptionHandler = $subscriptionType->getHandler();

            $selectedDates = $subscriptionHandler->getSelectedDates($training->dayOfWeek, $minDate);

            $calendar->setSelectedDates($selectedDates);
        }

        // У моих тренировок 0 - пн, у карбона 0 - вс
        $enabledDates = [$training->dayOfWeek];

        return $calendar
            ->setEnabledDates($enabledDates)
            ->setMinDate($minDate)
            ->setInterval($training->location->getInterval())
            ->getCalendarFrom($date);
    }
}
