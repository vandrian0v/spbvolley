<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EventApplication;

class EventApplicationController extends Controller
{
    /**
     * Список заявок.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = EventApplication::orderBy('id', 'desc')
            ->get();

        return view('admin.events.applications.index', compact('applications'));
    }

    /**
     * Отображение заявки.
     *
     * @param  EventApplication          $application
     * @return \Illuminate\Http\Response
     */
    public function show(EventApplication $application)
    {
        if ($application->is_new) {
            $application->is_new = false;
            $application->save();
        }

        return view('admin.events.applications.show', compact('application'));
    }

    /**
     * Удаление заявки.
     *
     * @param  EventApplication          $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventApplication $application)
    {
        if (!$application->delete()) {
            return back()->with('warning', 'Не удалось удалить заявку.');
        }

        return back()->with('notice', 'Заявка успешно удалена.');
    }
}
