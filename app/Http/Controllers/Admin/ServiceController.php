<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Список услуг.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();

        return view('admin.events.services.index', compact('services'));
    }

    /**
     * Создание услуги.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required']);

        $service = Service::create($request->all());

        if (!$service) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        return back()->with('notice', 'Услуга успешно добавлена.');
    }

    /**
     * Удаление услуги.
     *
     * @param  \App\Models\Service       $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if (!$service->delete()) {
            return back()->with('warning', 'Не удалось удалить услугу.');
        }

        return back()->with('notice', 'Услуга успешно удалена.');
    }
}
