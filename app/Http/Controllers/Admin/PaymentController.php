<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'after_date'  => 'date_format:Y-m-d',
            'before_date' => 'date_format:Y-m-d',
        ]);

        $payments = Payment::latest('updated_at')
            ->with('user')
            ->with('type')
            ->filter($request->get('filter'));

        if ($request->has('search')) {
            $payments = $payments->search($request->get('search'));
        }

        if ($request->has('payment_type_id') && is_numeric($request->get('payment_type_id'))) {
            $payments = $payments->ofType((int) $request->get('payment_type_id'));
        }

        if ($request->has('after_date') && $request->get('after_date')) {
            $payments = $payments->after($request->get('after_date'));
        }

        if ($request->has('before_date') && $request->get('before_date')) {
            $payments = $payments->before($request->get('before_date'));
        }

        $payments = $payments->paginate(15)
            ->appends([
                'filter'          => $request->get('filter'),
                'search'          => $request->get('search'),
                'after_date'      => $request->get('after_date'),
                'before_date'     => $request->get('before_date'),
                'payment_type_id' => $request->get('payment_type_id'),
            ]);

        return view('admin.payments.index', compact('payments'))
            ->with('allCount',       Payment::count())
            ->with('confirmedCount', Payment::ofStatus(Payment::STATUS_CONFIRMED)->count())
            ->with('createdCount',   Payment::ofStatus(Payment::STATUS_CREATED)->count())
            ->with('checkedCount',   Payment::ofStatus(Payment::STATUS_CHECKED)->count())
            ->with('canceledCount',  Payment::ofStatus(Payment::STATUS_CANCELED)->count());
    }

    public function show(Payment $payment)
    {
        return view('admin.payments.show', compact('payment'));
    }
}
