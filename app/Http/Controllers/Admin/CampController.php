<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Camp;
use App\Models\Coach;
use App\Models\Photos\CampPhoto as Photo;
use App\Http\Requests\CampRequest;
use Illuminate\Http\Request;

class CampController extends Controller
{
    /**
     * Список лагерей.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $camps = Camp::sort($request->get('sort'));

        if ($request->has('search')) {
            $camps = $camps->search($request->get('search'));
        }

        $camps = $camps->get();

        return view('admin.camps.index', compact('camps'));
    }

    /**
     * Форма создания нового лагеря.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $camp    = new Camp;
        $coaches = Coach::all();

        return view('admin.camps.create', compact('camp', 'coaches'));
    }

    /**
     * Создание нового лагеря.
     *
     * @param  \App\Http\Requests\CampRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CampRequest $request)
    {
        $camp = $request->createCamp();

        if (!$camp) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        // Тренеры
        $coaches = $request->get('coaches');
        $camp->coaches()->attach($coaches);

        $request->uploadPhotos($camp, 'camp');

        return redirect()->route('admin.camps.edit', $camp->id)
            ->with('notice', 'Лагерь успешно добавлен.');
    }

    /**
     * Форма редактирования лагеря.
     *
     * @param  Camp                      $camp
     * @return \Illuminate\Http\Response
     */
    public function edit(Camp $camp)
    {
        $coaches = Coach::all();

        return view('admin.camps.edit', compact('camp', 'coaches'));
    }

    /**
     * Обновление информации о лагере.
     *
     * @param  CampRequest               $request
     * @param  Camp                      $camp
     * @return \Illuminate\Http\Response
     */
    public function update(CampRequest $request, Camp $camp)
    {
        $request->updateCamp();

        // Тренеры
        $coaches = $request->get('coaches');
        $camp->coaches()->sync($coaches);

        $request->uploadPhotos($camp, 'camp');

        return back()->with('notice', 'Информация о лагере успешно обновлена.');
    }

    /**
     * Удаление лагеря.
     *
     * @param  Camp                      $camp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Camp $camp)
    {
        if (!$camp->delete()) {
            return back()->with('warning', 'Не удалось удалить лагерь.');
        }

        return back()->with('notice', 'Информация о лагере успешно удалена.');
    }

    /**
     * Удаление изображения лагеря по id.
     *
     * @param  Photo                     $photo
     * @return \Illuminate\Http\Response
     */
    public function deletePhoto(Photo $photo)
    {
        $photo->delete();

        return response()->json(['status' => 'ok']);
    }
}
