<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Team;
use App\Models\TrainingLevel;
use App\Models\User;
use App\Models\UserCategory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $users = User::byCategory($request->get('filter'));

        if ($request->has('search')) {
            $users = $users->search($request->get('search'));
        }

        $users = $users->sort($request->get('sort'));
        $users = $users->simplePaginate(15);

        return view('admin.users.index', compact('users'))
            ->with('activeCount',  User::count())
            ->with('adminsCount',  User::admins()->count())
            ->with('proCount',     User::ofCategory(UserCategory::PRO)->count())
            ->with('aCount',       User::ofCategory(UserCategory::A)->count())
            ->with('bCount',       User::ofCategory(UserCategory::B)->count())
            ->with('trashedCount', User::onlyTrashed()->count());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $user = new User;
        $categories = UserCategory::all();
        $trainingLevels = TrainingLevel::all();

        return view('admin.users.create', compact('user', 'categories', 'trainingLevels'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'birth_date'        => 'date_format:Y-m-d',
            'email'             => 'required|email|unique:user',
            'height'            => 'integer|min:50|max:250',
            'name'              => 'required',
            'surname'           => 'required',
            'weight'            => 'integer|min:10|max:150',
            'password'          => 'required|confirmed|min:6',
            'phone'             => 'phone:RU',
            'category_id'       => 'exists:user_category,id',
            'gender'            => 'in:male,female',
            'training_level_id' => 'exists:training_level,id',
        ]);

        $input = $request->only('surname', 'name', 'email', 'gender', 'note');

        $input['phone']       = $request->has('phone') ? phone($request->get('phone'), 'RU') : null;
        $input['birth_date']  = $request->get('birth_date') ?: null;
        $input['height']      = $request->get('height') ?: null;
        $input['weight']      = $request->get('weight') ?: null;
        $input['hobby']       = $request->get('hobby') ?: null;
        $input['category_id'] = $request->get('category_id') ?: null;
        $input['training_level_id'] = $request->get('training_level_id') ?: null;

        if ($request->has('password')) {
            $input['password'] = bcrypt($request->get('password'));
        }

        /** @var User $user */
        $user = User::create($input);

        return redirect()->route('admin.users.edit', $user->id)
            ->with('notice', 'Пользователь создан.');
    }

    /**
     * @param  User          $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $categories = UserCategory::all();
        $trainingLevels = TrainingLevel::all();

        return view('admin.users.edit', compact('user', 'categories', 'trainingLevels'));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  User          $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'birth_date'        => 'date_format:Y-m-d',
            'email'             => 'required|email|unique:user,email,' . $user->id,
            'height'            => 'integer|min:50|max:250',
            'name'              => 'required',
            'surname'           => 'required',
            'weight'            => 'integer|min:10|max:150',
            'password'          => 'confirmed|min:6',
            'phone'             => '',
            'category_id'       => 'exists:user_category,id',
            'gender'            => 'in:male,female',
            'training_level_id' => 'exists:training_level,id',
        ];

        if ($request->input('phone')) {
            $rules['phone'] = 'phone:RU';
        }

        $this->validate($request, $rules);

        $input = $request->only('surname', 'name', 'email', 'gender', 'note');

        $input['phone']       = $request->input('phone') ? phone($request->get('phone'), 'RU') : null;
        $input['birth_date']  = $request->get('birth_date') ?: null;
        $input['height']      = $request->get('height') ?: null;
        $input['weight']      = $request->get('weight') ?: null;
        $input['hobby']       = $request->get('hobby') ?: null;
        $input['category_id'] = $request->get('category_id') ?: null;
        $input['training_level_id'] = $request->get('training_level_id') ?: null;

        if ($request->has('password')) {
            $input['password'] = bcrypt($request->get('password'));
        }

        $user->update($input);

        return back()->with('notice', 'Информация успешно обновлена.');
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        if ($user->is_admin) {
            return back()->with('warning', 'Нельзя удалить администратора.');
        }

        if (!$user->delete()) {
            return back()->with('warning', 'Не удалось удалить пользователя.');
        }

        return back()->with('notice', 'Пользователь успешно удален.');
    }

    /**
     * @param int $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(int $user_id)
    {
        $user = User::withTrashed()->where('id', $user_id)->firstOrFail();

        if (!$user->trashed()) {
            return back()->with('warning', 'Пользователь не был удален.');
        }

        if (!$user->restore()) {
            return back()->with('warning', 'Не удалось восстановить пользователя.');
        }

        return back()->with('notice', 'Пользователь успешно восстановлен.');
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function payments(User $user)
    {
        $payments = Payment::where('user_id', $user->id)
            ->with('type', 'user')
            ->orderBy('id', 'desc')
            ->simplePaginate(20);

        return view('admin.users.payments', compact('user', 'payments'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function teams(User $user)
    {
        $teams = Team::orderBy('id', 'desc')
            ->whereHas('users', function (Builder $query) use ($user) {
                $query->where('user_id', $user->id);
            })
            ->with('tournament.nwtour', 'tournament.category', 'users', 'payment')
            ->paginate(10);

        return view('admin.users.teams', compact('user', 'teams'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trainings(User $user)
    {
        $trainings = $user->trainings()->orderBy('date', 'desc')->paginate(10);

        return view('admin.users.trainings', compact('user', 'trainings'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function csvEmails()
    {
        $callback = function () {
            $handle = fopen('php://output', 'w');

            User::chunk(100, function($users) use ($handle) {
                foreach ($users as $user) {
                    fputcsv($handle, [
                        $user->email,
                        iconv('utf-8', 'windows-1251//IGNORE', $user->fullname),
                    ], ';');
                }
            });

            fclose($handle);
        };

        return csv($callback);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function csvPhones()
    {
        $callback = function () {
            $handle = fopen('php://output', 'w');

            User::whereNotNull('phone')->chunk(100, function($users) use ($handle) {
                foreach ($users as $user) {
                    fputcsv($handle, [
                        $user->phone,
                        iconv('utf-8', 'windows-1251', $user->fullname),
                    ], ';');
                }
            });

            fclose($handle);
        };

        return csv($callback);
    }
}
