<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Team;
use App\Models\User;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        Carbon::setLocale('ru');
    }

    public function index()
    {
        $payments = Payment::latest('updated_at')
            ->confirmed()
            ->take(10)
            ->with('user')
            ->get();

        $users = User::latest()
            ->take(10)
            ->get();

        $teams = Team::latest('id')
            ->with('tournament.nwtour', 'tournament.category', 'users')
            ->take(10)
            ->get();

        return view('admin.dashboard.index')
            ->with(compact('payments', 'users', 'teams'))
            ->with([
                'usersCount'    => User::count(),
                'teamsCount'    => Team::count(),
                'paymentsCount' => Payment::confirmed()->count(),
            ]);
    }
}
