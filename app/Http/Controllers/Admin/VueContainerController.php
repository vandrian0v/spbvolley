<?php

namespace App\Http\Controllers\Admin;

class VueContainerController
{
    public function __invoke()
    {
        return view('admin');
    }
}