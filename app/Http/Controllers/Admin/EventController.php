<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Location;
use App\Models\Photos\EventPhoto as Photo;
use App\Models\Service;
use App\Http\Requests\EventRequest;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Список мероприятий.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = Event::sort($request->get('sort'));

        if ($request->has('search')) {
            $events = $events->search($request->get('search'));
        }

        $events = $events->get();

        return view('admin.events.index', compact('events'));
    }

    /**
     * Форма создания мероприятия.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event     = new Event;
        $services  = Service::all();
        $locations = Location::all();

        return view('admin.events.create', compact('event', 'services', 'locations'));
    }

    /**
     * Создание мероприятия.
     *
     * @param  \App\Http\Requests\EventRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        $event = $request->createEvent();

        if (!$event) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        // Услуги
        $services = $request->get('services');
        $event->services()->attach($services);

        $request->uploadPhotos($event, 'event');

        return redirect()->route('admin.events.edit', $event->id)
            ->with('notice', 'Мероприятие успешно добавлено.');
    }

    /**
     * Форма редактирования мероприятия.
     *
     * @param  \App\Models\Event         $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $services  = Service::all();
        $locations = Location::all();

        return view('admin.events.edit', compact('event', 'services', 'locations'));
    }

    /**
     * Обновление информации о мероприятии.
     *
     * @param  \App\Http\Requests\EventRequest $request
     * @param  \App\Models\Event               $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Event $event)
    {
        $request->updateEvent();

        // Услуги
        $services = $request->get('services');
        $event->services()->sync($services);

        $request->uploadPhotos($event, 'event');

        return back()->with('notice', 'Мероприятие успешно обновлено.');
    }

    /**
     * Удаление мероприятия.
     *
     * @param  \App\Models\Event         $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if (!$event->delete()) {
            return back()->with('warning', 'Не удалось удалить мероприятие.');
        }

        return back()->with('notice', 'Мероприятие успешно удалено.');
    }

    /**
     * Удаление изображения мероприятия по id.
     *
     * @param  \App\Models\Photos\EventPhoto $photo
     * @return \Illuminate\Http\Response
     */
    public function deletePhoto(Photo $photo)
    {
        $photo->delete();

        return response()->json(['status' => 'ok']);
    }
}
