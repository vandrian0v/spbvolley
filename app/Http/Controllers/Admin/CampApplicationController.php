<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CampApplication;

class CampApplicationController extends Controller
{
    /**
     * Список заявок.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = CampApplication::with('camp')
            ->orderBy('id', 'desc')
            ->get();

        return view('admin.camps.applications.index', compact('applications'));
    }

    /**
     * Отображение заявки.
     *
     * @param  CampApplication          $application
     * @return \Illuminate\Http\Response
     */
    public function show(CampApplication $application)
    {
        if ($application->is_new) {
            $application->is_new = false;
            $application->save();
        }

        return view('admin.camps.applications.show', compact('application'));
    }

    /**
     * Удаление заявки.
     *
     * @param  CampApplication          $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(CampApplication $application)
    {
        if (!$application->delete()) {
            return back()->with('warning', 'Не удалось удалить заявку.');
        }

        return back()->with('notice', 'Заявка успешно удалена.');
    }
}
