<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Camp;
use App\Models\Location;
use App\Models\Post;
use App\Http\Requests\PostRequest;
use App\Models\PostCategory;
use App\Models\Tour;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Список статей.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::with('category')->sort($request->get('sort'));

        if ($request->has('search')) {
            $posts = $posts->search($request->get('search'));
        }

        $posts = $posts->simplePaginate(15);

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Форма создания статьи.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = new Post;
        $categories = PostCategory::all();
        $camps = Camp::future()->get();
        $locations = Location::all();
        $tours = Tour::future()->get();

        return view('admin.posts.create', compact('post', 'categories', 'camps', 'locations', 'tours'));
    }

    /**
     * Создание статьи.
     *
     * @param  PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $post = $request->createPost();

        if (!$post) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        return redirect()->route('admin.posts.edit', $post->id)
            ->with('notice', 'Новость успешно добавлена.');
    }

    /**
     * Форма редактирования статьи.
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = PostCategory::all();
        $camps = Camp::future()->get();
        $locations = Location::all();
        $tours = Tour::future()->get();

        return view('admin.posts.edit', compact('post', 'categories', 'camps', 'locations', 'tours'));
    }

    /**
     * Обновление статьи.
     *
     * @param  PostRequest $request
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $request->updatePost();

        return back()->with('notice', 'Новость успешно отредактирована.');
    }

    /**
     * Удаление статьи.
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (!$post->delete()) {
            return back()->with('warning', 'Не удалось удалить новость.');
        }

        return back()->with('notice', 'Новость успешно удалена.');
    }
}
