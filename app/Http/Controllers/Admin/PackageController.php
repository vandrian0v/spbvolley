<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\Models\Package;
use App\Models\Service;

class PackageController extends Controller
{
    /**
     * Список пакетов.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();

        return view('admin.events.packages.index', compact('packages'));
    }

    /**
     * Форма создания пакета.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $package  = new Package;
        $services = Service::all();

        return view('admin.events.packages.create', compact('package', 'services'));
    }

    /**
     * Создание пакета.
     *
     * @param  \App\Http\Requests\PackageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageRequest $request)
    {
        $package = $request->createPackage();

        if (!$package) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        // Услуги
        $services = $request->get('services');
        $package->services()->attach($services);

        return redirect()->route('admin.packages.edit', $package->id)
            ->with('notice', 'Пакет успешно добавлен.');
    }

    /**
     * Форма редактирования пакета.
     *
     * @param  \App\Models\Package       $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        $services = Service::all();

        return view('admin.events.packages.edit', compact('package', 'services'));
    }

    /**
     * Обновление информации о пакете.
     *
     * @param  \App\Http\Requests\PackageRequest $request
     * @param  \App\Models\Package               $package
     * @return \Illuminate\Http\Response
     */
    public function update(PackageRequest $request, Package $package)
    {
        $request->updatePackage();

        // Услуги
        $services = $request->get('services');
        $package->services()->sync($services);

        return back()->with('notice', 'Пакет успешно обновлен.');
    }

    /**
     * Удаление пакета.
     *
     * @param  \App\Models\Package       $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        if (!$package->delete()) {
            return back()->with('warning', 'Не удалось удалить пакет.');
        }

        return back()->with('notice', 'Пакет успешно удален.');
    }
}
