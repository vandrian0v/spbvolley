<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Carbon\Carbon;
use DB;

class ChartController extends Controller
{
    const DAYS_COUNT = 14;

    public function payments()
    {
        $query = DB::table('payment')
            ->select([
                DB::raw('date(updated_at) as date'),
                DB::raw('sum(sum) as sum'),
            ])
            ->where('status', Payment::STATUS_CONFIRMED)
            ->whereDate('updated_at', '>=', Carbon::today()->subDays(self::DAYS_COUNT - 1))
            ->groupBy('date')
            ->orderBy('date');

        $trainingQuery = clone $query;
        $tournamentQuery = clone $query;

        $trainingPayments = $trainingQuery->where('payment_type_id', Payment::TYPE_TRAINING)->get()->keyBy('date')->toArray();
        $tournamentPayments = $tournamentQuery->where('payment_type_id', Payment::TYPE_TOURNAMENT_CONTRIBUTION)->get()->keyBy('date')->toArray();

        $series = [
            ['name' => 'Оплата тренировок', 'data' => []],
            ['name' => 'Оплата взносов', 'data' => []],
        ];
        $categories = [];
        $dates = $this->getLastWeek();

        foreach ($dates as $date) {
            $series[0]['data'][] = $trainingPayments[$date->format('Y-m-d')]->sum ?? null;
            $series[1]['data'][] = $tournamentPayments[$date->format('Y-m-d')]->sum ?? null;
            $categories[] = $date->format('d.m');
        }

        return response()->json([
            'title' => '',
            'yAxis_title' => 'рубли',
            'xAxis_categories' => $categories,
            'series' => $series
        ]);
    }

    public function users()
    {
        $users = DB::table('user')
            ->select([
                DB::raw('date(created_at) as date'),
                DB::raw('count(id) as count'),
            ])->whereDate('created_at', '>=', Carbon::today()->subDays(self::DAYS_COUNT - 1))->groupBy('date')
            ->orderBy('date')
            ->get()
            ->keyBy('date')
            ->toArray();

        $series = [
            ['name' => 'Пользователи', 'data' => []],
        ];
        $categories = [];
        $dates = $this->getLastWeek();

        foreach ($dates as $date) {
            $series[0]['data'][] = $users[$date->format('Y-m-d')]->count ?? null;
            $categories[] = $date->format('d.m');
        }

        return response()->json([
            'title' => '',
            'yAxis_title' => 'количество',
            'xAxis_categories' => $categories,
            'series' => $series
        ]);
    }

    private function getLastWeek()
    {
        $date = Carbon::today()->subDays(self::DAYS_COUNT - 1);

        $days = [];

        foreach (range(1, self::DAYS_COUNT) as $i) {
            $days[] = $date->copy();
            $date->addDay();
        }

        return $days;
    }
}
