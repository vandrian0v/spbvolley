<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CampReview;
use Illuminate\Http\Request;

class CampReviewController extends Controller
{
    /**
     * Список отзывов.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = CampReview::all();

        return view('admin.camps.reviews.index', compact('reviews'));
    }

    /**
     * Форма создания отзыва
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $review = new CampReview;

        return view('admin.camps.reviews.create', compact('review'));
    }

    /**
     * Создание отзыва.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'author'      => 'required',
            'review'      => 'required',
            'description' => 'required',
        ]);

        $review = CampReview::create($request->all());

        if (!$review) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        return redirect()->route('admin.reviews.edit', $review->id)
            ->with('notice', 'Отзыв успешно добавлен.');
    }

    /**
     * Форма редактирования отзыва.
     *
     * @param  CampReview                $review
     * @return \Illuminate\Http\Response
     */
    public function edit(CampReview $review)
    {
        return view('admin.camps.reviews.edit', compact('review'));
    }

    /**
     * Обновление отзыва.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  CampReview                $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CampReview $review)
    {
        $this->validate($request, [
            'author'      => 'required',
            'review'      => 'required',
            'description' => 'required',
        ]);

        $review->update($request->all());

        return back()->with('notice', 'Информация об отзыве успешно обновлена.');
    }

    /**
     * Удаление отзыва.
     *
     * @param  CampReview                $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(CampReview $review)
    {
        if (!$review->delete()) {
            return back()->with('warning', 'Не удалось удалить отзыв.');
        }

        return back()->with('notice', 'Информация об отзыве успешно удалена.');
    }
}
