<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LocationRequest;
use App\Models\Location;
use App\Models\Photos\LocationPhoto as Photo;

class LocationController extends Controller
{
    /**
     * Список местоположений.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();

        return view('admin.locations.index', compact('locations'));
    }

    /**
     * Форма создания местоположения.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location = new Location;

        return view('admin.locations.create', compact('location'));
    }

    /**
     * Создание местоположения.
     *
     * @param  LocationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request)
    {
        $location = $request->createLocation();

        if (!$location) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        $request->uploadPhotos($location, 'location');

        return redirect()->route('admin.locations.edit', $location->id)
            ->with('notice', 'Место проведения успешно добавлено.');
    }

    /**
     * Форма редактирования местоположения.
     *
     * @param  \App\Models\Location      $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return view('admin.locations.edit', compact('location'));
    }

    /**
     * Обновление информации о местоположении.
     *
     * @param LocationRequest $request
     * @param  \App\Models\Location             $location
     * @return \Illuminate\Http\Response
     */
    public function update(LocationRequest $request, Location $location)
    {
        $request->updateLocation();

        $request->uploadPhotos($location, 'location');

        return back()->with('notice', 'Место проведения успешно обновлено.');
    }

    /**
     * Удаление местоположения.
     *
     * @param  \App\Models\Location      $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        if (!$location->delete()) {
            return back()->with('warning', 'Не удалось удалить место проведения.');
        }

        return back()->with('notice', 'Место проведения успешно удалено.');
    }

    /**
     * Удаление изображения местоположения по id.
     *
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function deletePhoto(Photo $photo)
    {
        $photo->delete();

        return response()->json(['status' => 'ok']);
    }
}
