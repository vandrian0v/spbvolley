<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coach;
use App\Models\Photos\CoachPhoto as Photo;
use App\Http\Requests\CoachRequest;

class CoachController extends Controller
{
    /**
     * Список тренеров.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coaches = Coach::all();

        return view('admin.coaches.index', compact('coaches'));
    }

    /**
     * Форма создания тренера.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coach = new Coach;

        return view('admin.coaches.create', compact('coach'));
    }

    /**
     * Создание тренера.
     *
     * @param  \App\Http\Requests\CoachRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CoachRequest $request)
    {
        $coach = $request->createCoach();

        if (!$coach) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        $request->uploadPhotos($coach, 'coach');

        return redirect()->route('admin.coaches.edit', $coach->id)
            ->with('notice', 'Тренер успешно добавлен.');
    }

    /**
     * Форма редактирования тренера.
     *
     * @param  Coach                     $coach
     * @return \Illuminate\Http\Response
     */
    public function edit(Coach $coach)
    {
        return view('admin.coaches.edit', compact('coach'));
    }

    /**
     * Обновление информации о тренере.
     *
     * @param  \App\Http\Requests\CoachRequest $request
     * @param  Coach                           $coach
     * @return \Illuminate\Http\Response
     */
    public function update(CoachRequest $request, Coach $coach)
    {
        $request->updateCoach();

        $request->uploadPhotos($coach, 'coach');

        return back()->with('notice', 'Информация о тренере успешно обновлена.');
    }

    /**
     * Удаление тренера.
     *
     * @param  Coach                     $coach
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coach $coach)
    {
        if (!$coach->delete()) {
            return back()->with('warning', 'Не удалось удалить тренера.');
        }

        return back()->with('notice', 'Информация о тренере успешно удалена.');
    }

    /**
     * Удаление изображения тренера по id.
     *
     * @param  Photo                     $photo
     * @return \Illuminate\Http\Response
     */
    public function deletePhoto(Photo $photo)
    {
        $photo->delete();

        return response()->json(['status' => 'ok']);
    }
}
