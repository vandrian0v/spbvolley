<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coach;
use App\Models\Training;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * @param Training $training
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteTraining(Training $training)
    {
//        if ($training->userTrainings()->whereDate('date', '>=', Carbon::today())->exists()) {
//            return response(['status' => 'error', 'message' => 'Есть клиенты оплатившие тренировку'], 400);
//        }

        if (!$training->delete()) {
            return response()->json(['status' => 'error'], 500);
        }

        return response()->json(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function trainings(Request $request): array
    {
        $this->validate($request, [
            'group'    => 'required|exists:training_group,id',
            'location' => 'required|exists:location,id',
        ]);

        $trainingsByWeekday = array_fill(0, 7, []);

        $trainings = TrainingGroup::find($request->get('group'))
            ->trainings()
            ->with('subscriptionTypes')
            ->where('location_id', $request->get('location'))
            ->get();

        /** @var Training $training */
        foreach ($trainings as $training) {
            $training->subscription_types = $training->subscriptionTypes->getDictionary();
            $training->makeVisible('subscription_types');

            $trainingsByWeekday[$training->weekday][] = $training;
        }

        return $trainingsByWeekday;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function coaches(): array
    {
        return cache()->remember('coaches', 24 * 60, function() {
            return Coach::withTrashed()->get()->pluck('name', 'id')->toArray();
        });
    }
}
