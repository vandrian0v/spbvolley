<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Models\Training;
use App\Models\TrainingGroup;
use App\Models\UserTraining;
use Illuminate\Http\Request;
use DB;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = TrainingGroup::with('trainings')
            ->sorted()
            ->get();

        return view('admin.trainings.index', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|unique:training_group',
            'url'   => 'required|unique:training_group',
        ]);

        $group = TrainingGroup::create($request->all());

        if (!$group) {
            return back()->withInput()
                ->with('warning', 'Во время сохранения произошла ошибка.');
        }

        return back()->with('notice', 'Группа успешно добавлена.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  TrainingGroup             $group
     * @return \Illuminate\Http\Response
     */
    public function edit(TrainingGroup $group)
    {
        $locations = Location::all();

        return view('admin.trainings.edit', compact('group', 'locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * Расписание приходит в формате schedules[location_id][weekday][#]
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  TrainingGroup             $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrainingGroup $group)
    {
        $this->validate($request, [
            'name'                                 => 'required|unique:training_group,name,' . $group->id,
            'url'                                  => 'required|unique:training_group,url,' . $group->id,
            'schedules.*.*.*.id'                   => 'exists:training,id,training_group_id,' . $group->id,
            'schedules.*.*.*.weekday'              => 'required|integer|between:0,6',
            'schedules.*.*.*.coach_id'             => 'required|exists:coach,id',
            'schedules.*.*.*.start_time'           => ['required', 'regex:/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/'],
            'schedules.*.*.*.end_time'             => ['required', 'regex:/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/'],
            'schedules.*.*.*.subscription_types'   => 'required|array',
            'schedules.*.*.*.subscription_types.*' => 'required|integer|min:300',
        ]);

        $group->update($request->only('name', 'url'));

        $schedules = $request->get('schedules', []);

        // Проходим по каждому местоположению
        foreach ($schedules as $locationId => $trainings) {
            if (!Location::where('id', $locationId)->exists()) {
                continue;
            }

            $trainings = collect($trainings)->collapse();

            $trainings->transform(function ($training) use ($locationId) {
                $training['location_id'] = $locationId;

                $training['subscription_types'] = array_map(function($price) {
                    return ['price' => $price];
                }, $training['subscription_types']);

                return $training;
            });

            foreach ($trainings as $data) {
                if ($data['id']) {
                    // Обновление
                    $training = Training::find($data['id']);
                    $training->update($data);
                } else {
                    // Создание
                    $training = $group->trainings()->create($data);
                }

                $training->subscriptionTypes()->sync($data['subscription_types']);
            }
        }

        return back()->with('notice', 'Расписание сохранено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  TrainingGroup             $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrainingGroup $group)
    {
        if (!$group->delete()) {
            return back()->with('warning', 'Не удалось удалить тренировочную группу.');
        }

        return back()->with('notice', 'Тренировочная группа успешно удалена.');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paid()
    {
        $trainings = UserTraining::with('payment.user')
            ->paid()
            ->orderBy('date', 'desc')
            ->paginate(20);

        return view('admin.trainings.paid', compact('trainings'));
    }

    /**
     * @param int $groupId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function orderIncrement(int $groupId)
    {
        DB::transaction(function () use ($groupId) {
            $group = TrainingGroup::where('id', $groupId)
                ->lockForUpdate()
                ->firstOrFail();

            $nextGroup = TrainingGroup::where('order', $group->order - 1)
                ->lockForUpdate()
                ->first();

            if ($nextGroup) {
                $group->decrement('order');
                $nextGroup->increment('order');
            }
        });

        return back();
    }

    /**
     * @param int $groupId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function orderDecrement(int $groupId)
    {
        DB::transaction(function () use ($groupId) {
            $group = TrainingGroup::where('id', $groupId)
                ->lockForUpdate()
                ->firstOrFail();

            $prevGroup = TrainingGroup::where('order', $group->order + 1)
                ->lockForUpdate()
                ->first();

            if ($prevGroup) {
                $group->increment('order');
                $prevGroup->decrement('order');
            }
        });

        return back();
    }
}
