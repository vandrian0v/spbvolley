<?php

namespace App\Http\Requests;

use App\Models\Event;

class EventRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title'        => 'required',
            'date'         => 'required|date',
            'participants' => 'required',
            'description'  => 'required',
            'location_id'  => 'required|integer|exists:location,id',
            'services'     => 'required|array',
            'services.*'   => 'required|integer|exists:service,id',
            'photos'       => 'array',
            'preview'      => 'image|mimes:jpeg,jpg,png',
        ];

        foreach (array_filter($this->get('photos', [])) as $index => $photo) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,jpg,png';
        }

        return $rules;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|Event
     */
    public function createEvent(): Event
    {
        return Event::create($this->fetchData());
    }

    /**
     * @return void
     */
    public function updateEvent(): void
    {
        $this->route('event')->update($this->fetchData());
    }

    /**
     * @return array
     */
    private function fetchData(): array
    {
        return [
            'title'        => $this->get('title'),
            'date'         => $this->get('date'),
            'participants' => $this->get('participants'),
            'description'  => $this->get('description'),
            'location_id'  => $this->get('location_id'),
        ];
    }
}
