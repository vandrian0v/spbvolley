<?php

namespace App\Http\Requests;

use App\Models\Coach;

class CoachRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'name'    => 'required',
            'photos'  => 'array',
            'preview' => 'image|mimes:jpeg,jpg,png',
        ];

        foreach (array_filter($this->get('photos', [])) as $index => $photo) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,jpg,png';
        }

        return $rules;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|Coach
     */
    public function createCoach(): Coach
    {
        return Coach::create($this->fetchData());
    }

    /**
     * @return void
     */
    public function updateCoach(): void
    {
        $this->route('coach')->update($this->fetchData());
    }

    /**
     * @return array
     */
    private function fetchData(): array
    {
        return [
            'name'      => $this->get('name'),
            'career'    => $this->get('career'),
            'education' => $this->get('education'),
            'training'  => $this->get('training'),
            'teachers'  => $this->get('teachers'),
            'status'    => $this->get('status'),
        ];
    }
}
