<?php

namespace App\Http\Requests\Api\Admin\Notifications;

use App\Http\Requests\Api\Admin\Request;
use Illuminate\Validation\Rule;

class SendSmsRequest extends Request
{
    public function rules()
    {
        return [
            'text'       => 'required',
            'user_ids'   => 'required|array',
            'user_ids.*' => [
                'required',
                'integer',
                Rule::exists('user', 'id')->where(function ($query) {
                    $query->whereNotNull('phone');
                }),
            ],
        ];
    }
}