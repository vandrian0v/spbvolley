<?php

namespace App\Http\Requests\Api\Admin\Notifications;

use App\Http\Requests\Api\Admin\Request;

class SendEmailsRequest extends Request
{
    public function rules()
    {
        return [
            'title'      => 'required|string|max:255',
            'text'       => 'required',
            'user_ids'   => 'required|array',
            'user_ids.*' => 'required|integer|exists:user,id',
        ];
    }
}