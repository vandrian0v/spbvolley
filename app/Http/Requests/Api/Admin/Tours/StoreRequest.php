<?php

namespace App\Http\Requests\Api\Admin\Tours;

use App\Http\Requests\Api\Admin\Request;

class StoreRequest extends Request
{
    public function rules()
    {
        return [
            'title'        => 'required',
            'start_date'   => 'required|date_format:Y-m-d',
            'end_date'     => 'required|date_format:Y-m-d',
            'categories'   => 'required|array',
            'categories.*' => 'required|distinct|exists:tournament_category,id',
            'longitude'    => 'numeric',
            'latitude'     => 'numeric',
        ];
    }
}