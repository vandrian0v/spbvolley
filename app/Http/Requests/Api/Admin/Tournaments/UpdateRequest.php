<?php

namespace App\Http\Requests\Api\Admin\Tournaments;

use App\Http\Requests\Api\Admin\Request;

class UpdateRequest extends Request
{
    public function rules()
    {
        return [
            'contribution' => 'required|integer',
            'limit' => 'integer|nullable',
        ];
    }
}