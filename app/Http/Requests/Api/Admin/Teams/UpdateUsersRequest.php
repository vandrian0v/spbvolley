<?php

namespace App\Http\Requests\Api\Admin\Teams;

use App\Http\Requests\Api\Admin\Request;
use App\Models\Team;

class UpdateUsersRequest extends Request
{
    public function rules()
    {
        /** @var Team $team */
        $team = $this->route('team');

        $teamSize = $team->tournament->category->team_size;

        return [
            'user_ids'   => "required|array|size:$teamSize",
            'user_ids.*' => 'distinct|required|integer|exists:user,id',
        ];
    }
}