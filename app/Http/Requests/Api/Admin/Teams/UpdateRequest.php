<?php

namespace App\Http\Requests\Api\Admin\Teams;

use App\Http\Requests\Api\Admin\Request;

class UpdateRequest extends Request
{
    public function rules()
    {
        return [
            'is_paid'     => 'boolean',
            'rank'        => 'nullable|required_with:category_id|integer',
            'category_id' => 'nullable|required_with:rank|integer|exists:tournament_category,id',
        ];
    }
}