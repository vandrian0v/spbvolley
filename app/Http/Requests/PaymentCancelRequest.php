<?php

namespace App\Http\Requests;

class PaymentCancelRequest extends PaymentRequest
{
    protected $tag = 'cancelOrderResponse';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'action'                  => 'required|in:cancelOrder',
            'customerNumber'          => 'required|integer|exists:user,id', // user_id
            'invoiceId'               => 'required',
            'md5'                     => 'required',
            'orderNumber'             => 'required|integer|exists:payment,id', // payment_id
            'orderSumAmount'          => 'required',
            'orderSumBankPaycash'     => 'required',
            'orderSumCurrencyPaycash' => 'required',
            'shopId'                  => 'required|integer|in:' . config('yandex_kassa.shop_id'),
        ];
    }
}
