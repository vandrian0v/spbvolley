<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Post;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title'         => 'required',
            'content'       => 'required',
            'anons'         => 'required',
            'url'           => 'required|unique:post,url',
            'slider'        => 'boolean',
            'image'         => 'image',
            'category_id'   => 'required|integer|exists:post_category,id',
            'camp_id'       => 'integer|exists:camp,id',
            'location_id'   => 'integer|exists:location,id',
            'nwtour_id'     => 'integer|exists:nwtour,id',
        ];

        if ($post = $this->route('post')) {
            $rules['url'] .= ",{$post->id}";
        }

        return $rules;
    }

    /**
     * @return Post
     */
    public function createPost(): Post
    {
        /** @var Post $post */
        $post = Post::create($this->fetchData());

        $this->updateImage($post->id);

        return $post;
    }

    /**
     * @return void
     */
    public function updatePost(): void
    {
        $post = $this->route('post');

        $post->update($this->fetchData());

        $this->updateImage($post->id);
    }

    /**
     * @return array
     */
    private function fetchData(): array
    {
        return [
            'title'        => $this->get('title'),
            'content'      => $this->get('content'),
            'anons'        => $this->get('anons'),
            'url'          => $this->get('url'),
            'slider'       => $this->get('slider', false),
            'category_id'  => $this->get('category_id'),
            'camp_id'      => $this->get('camp_id') ?: null,
            'location_id'  => $this->get('location_id', null) ?: null,
            'nwtour_id'    => $this->get('nwtour_id', null) ?: null,
        ];
    }

    /**
     * @param int $postId
     * @return void
     */
    private function updateImage(int $postId): void
    {
        if ($this->hasFile('image') && $this->file('image')->isValid()) {
            $this->file('image')->storeAs("post/{$postId}", 'image');
        }
    }
}
