<?php

namespace App\Http\Requests;

class PaymentAvisoRequest extends PaymentRequest
{
    protected $tag = 'paymentAvisoResponse';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'action'                  => 'required|in:paymentAviso',
            'customerNumber'          => 'required|integer|exists:user,id', // user_id
            'invoiceId'               => 'required',
            'md5'                     => 'required',
            'orderNumber'             => 'required|integer|exists:payment,id,status,1', // payment_id
            'orderSumAmount'          => 'required',
            'orderSumBankPaycash'     => 'required',
            'orderSumCurrencyPaycash' => 'required',
            'shopId'                  => 'required|integer|in:' . config('yandex_kassa.shop_id'),
        ];
    }
}
