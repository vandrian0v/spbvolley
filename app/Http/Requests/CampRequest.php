<?php

namespace App\Http\Requests;

use App\Models\Camp;

class CampRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title'           => 'required',
            'seo_title'       => 'string|max:255',
            'seo_description' => 'string|max:255',
            'seo_keywords'    => 'string|max:255',
            'future'          => 'boolean',
            'description'     => 'required',
            'start_date'      => 'required|date_format:Y-m-d',
            'end_date'        => 'required|date_format:Y-m-d',
            'coaches'         => 'required|array',
            'coaches.*'       => 'required|integer|exists:coach,id',
            'photos'          => 'array',
            'preview'         => 'image|mimes:jpeg,jpg,png',
        ];

        foreach (array_filter($this->get('photos', [])) as $index => $photo) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,jpg,png';
        }

        return $rules;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|Camp
     */
    public function createCamp(): Camp
    {
        return Camp::create($this->fetchData());
    }

    /**
     * @return void
     */
    public function updateCamp(): void
    {
        $this->route('camp')->update($this->fetchData());
    }

    /**
     * @return array
     */
    private function fetchData(): array
    {
        return [
            'title'            => $this->get('title'),
            'seo_title'        => $this->get('seo_title'),
            'seo_description'  => $this->get('seo_description'),
            'seo_keywords'     => $this->get('seo_keywords'),
            'description'      => $this->get('description'),
            'full_description' => $this->get('full_description'),
            'start_date'       => $this->get('start_date'),
            'end_date'         => $this->get('end_date'),
            'future'           => $this->get('future', false),
        ];
    }
}
