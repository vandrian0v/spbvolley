<?php

namespace App\Http\Requests;

use App\Models\Location;

class LocationRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'name'              => 'required',
            'short_name'        => 'required',
            'latitude'          => 'numeric',
            'longitude'         => 'numeric',
            'season'            => 'required|in:summer,winter',
            'photos'            => 'array',
            'preview'           => 'image|mimes:jpeg,jpg,png',
            'period_start_date' => 'date_format:d.m',
            'period_end_date'   => 'date_format:d.m',
        ];

        foreach (array_filter($this->get('photos', [])) as $index => $photo) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,jpg,png';
        }

        return $rules;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|Location
     */
    public function createLocation(): Location
    {
        return Location::create($this->fetchData());
    }

    /**
     * @return void
     */
    public function updateLocation(): void
    {
        $this->route('location')->update($this->fetchData());
    }

    /**
     * @return array
     */
    private function fetchData(): array
    {
        return array_map(
            function ($value) {
                return $value ?: null;
            }, [
                'name'              => $this->get('name'),
                'short_name'        => $this->get('short_name'),
                'latitude'          => $this->get('latitude'),
                'longitude'         => $this->get('longitude'),
                'description'       => $this->get('description'),
                'season'            => $this->get('season'),
                'address'           => $this->get('address'),
                'period_start_date' => $this->get('period_start_date'),
                'period_end_date'   => $this->get('period_end_date'),
            ]
        );
    }
}
