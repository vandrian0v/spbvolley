<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainingOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'subscription_type_id' => 'required|integer|exists:subscription_type,id',
            'trainings'            => 'required|array',
            'trainings.*'          => 'required|array',
            'trainings.*.*'        => 'required|date_format:Y-m-d|distinct',
        ];
    }
}
