<?php

namespace App\Http\Requests;

class PaymentCheckRequest extends PaymentRequest
{
    protected $tag = 'checkOrderResponse';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'action'                  => 'required|in:checkOrder',
            'customerNumber'          => 'required|integer|exists:user,id', // user_id
            'invoiceId'               => 'required',
            'md5'                     => 'required',
            'orderNumber'             => 'required|integer|exists:payment,id,status,0', // payment_id
            'orderSumAmount'          => 'required',
            'orderSumBankPaycash'     => 'required',
            'orderSumCurrencyPaycash' => 'required',
            'shopId'                  => 'required|integer|in:' . config('yandex_kassa.shop_id'),
        ];
    }
}
