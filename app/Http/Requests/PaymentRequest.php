<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class PaymentRequest extends FormRequest
{
    const STATUS_SUCCESS    = 0;
    const STATUS_AUTH_ERROR = 1;
    const STATUS_DECLINED = 100;
    const STATUS_BAD_DATA = 200;

    protected $tag = '';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Returns true if request hash is valid
     *
     * @return bool
     */
    public function isHashValid(): bool
    {
        $parameters = [
            $this->get('action'),
            $this->get('orderSumAmount'),
            $this->get('orderSumCurrencyPaycash'),
            $this->get('orderSumBankPaycash'),
            $this->get('shopId'),
            $this->get('invoiceId'),
            $this->get('customerNumber'),
            config('yandex_kassa.shop_password'),
        ];

        return strtolower(md5(implode(';', $parameters))) === strtolower($this->get('md5'));
    }

    /**
     * @param array $errors
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $errors = collect($errors)->flatten()->implode(". ");
        $errors = str_limit($errors, '250', '...');

        return $this->buildXml([
            'code'    => self::STATUS_DECLINED,
            'message' => $errors,
        ]);
    }

    /**
     * @param array $attributes
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function buildXml(array $attributes = [])
    {
        $tag = $this->tag;

        $attributes['performedDatetime'] = Carbon::now()->toAtomString();
        $attributes['shopId']            = config('yandex_kassa.shop_id');
        $attributes['invoiceId']         = $this->get('invoiceId');

        $content = view('payments.yandex', compact('tag', 'attributes'));

        return response($content, 200, ['Content-type' => 'application/xml']);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function authErrorResponse()
    {
        return $this->buildXml([
            'code'    => self::STATUS_AUTH_ERROR,
            'message' => 'Значение параметра md5 не совпадает с результатом расчета хэш-функции.',
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function successResponse()
    {
        return $this->buildXml(['code' => self::STATUS_SUCCESS]);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function teamsLimitExceededResponse()
    {
        return $this->buildXml([
            'code' => self::STATUS_DECLINED,
            'message' => 'Невозможно подать заявку в связи с достижением максимального количества участников',
        ]);
    }
}
