<?php

namespace App\Http\Requests;

use App\Models\Package;
use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'        => 'required',
            'description' => 'required',
            'duration'    => 'required|numeric',
            'price'       => 'required|numeric',
            'services'    => 'required|array',
            'services.*'  => 'required|integer|exists:service,id',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|Package
     */
    public function createPackage(): Package
    {
        return Package::create($this->fetchData());
    }

    /**
     * @return void
     */
    public function updatePackage(): void
    {
        $this->route('package')->update($this->fetchData());
    }

    /**
     * @return array
     */
    private function fetchData(): array
    {
        return [
            'name'        => $this->get('name'),
            'description' => $this->get('description'),
            'duration'    => $this->get('duration'),
            'price'       => $this->get('price'),
            'info'        => $this->get('info'),
        ];
    }
}
