<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @param $model
     * @param $modelName
     * @return void
     */
    public function uploadPhotos($model, $modelName): void
    {
        if ($this->hasFile('preview') && $this->file('preview')->isValid()) {
            $this->file('preview')->storeAs("{$modelName}/{$model->id}", 'preview');
        }

        foreach (array_filter($this->file('photos', [])) as $photo) {
            if ($photo->isValid()) {
                $name = uniqid();

                $photo->storeAs("{$modelName}/{$model->id}", $name);

                $model->photos()->create(compact('name'));
            }
        }
    }
}
