<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TournamentApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'users'       => 'required|array',
//            'users.*'     => 'required|email|exists:user,email|distinct',
            'user_ids'    => 'required|array',
            'user_ids.*'  => 'required|integer|exists:user,id|distinct',
            'category_id' => 'required|exists:tournament_category,id',
            'nwtour_id'   => 'required|exists:nwtour,id,status,opened',
        ];
    }
}
