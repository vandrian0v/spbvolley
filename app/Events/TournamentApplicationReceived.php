<?php

namespace App\Events;

use App\Models\Team;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TournamentApplicationReceived implements ShouldBroadcast
{
    use SerializesModels;

    /** @var Team $team */
    public $team;

    /**
     * TournamentApplicationReceived constructor.
     * @param Team $team
     */
    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('admin');
    }

    /**
     * @return array
     */
    public function broadcastWith(): array
    {
        // $url = route('admin.nwtour.teams.show', $this->team->tournament);
        $url = '';
        $message = sprintf(
            'Новая заявка на турнир %s (%s): %s',
            $this->team->tournament->nwtour->title,
            $this->team->tournament->category->fullname,
            $this->team->users->implode('surname', '/')
        );

        return compact('url', 'message');
    }
}
