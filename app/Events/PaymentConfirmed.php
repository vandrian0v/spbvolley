<?php

namespace App\Events;

use App\Models\Payment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\PrivateChannel;

class PaymentConfirmed implements ShouldBroadcast
{
    use SerializesModels;

    public $payment;

    /**
     * PaymentConfirmed constructor.
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('admin');
    }

    /**
     * @return array
     */
    public function broadcastWith(): array
    {
        $url = route('admin.payments.show', $this->payment);
        $message = sprintf(
            '%s совершил платеж на сумму %d руб.',
                $this->payment->user->fullname,
                $this->payment->sum
        );

        return compact('url', 'message');
    }
}
