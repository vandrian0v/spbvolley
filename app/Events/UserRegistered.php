<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\PrivateChannel;

class UserRegistered implements ShouldBroadcast
{
    use SerializesModels;

    public $user;

    /**
     * UserRegistered constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('admin');
    }

    /**
     * @return array
     */
    public function broadcastWith(): array
    {
        $url = route('admin.users.edit', $this->user);
        $message = sprintf(
            'Новый пользователь: %s',
            $this->user->fullname
        );

        return compact('url', 'message');
    }
}
