<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Photos\LocationPhoto as Photo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;
use App\Services\Trainings\Interval;

/**
 * App\Models\Location
 *
 * @property-read Collection|Photo[] $photos
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property float $longitude
 * @property float $latitude
 * @property string $description
 * @property string $season
 * @property string $short_name
 * @property string $icon
 * @property string $address
 * @property Date $period_start_date
 * @property Date $period_end_date
 * @property bool $has_interval
 */
class Location extends Model
{
    use Scopes\PhotoOwner, SoftDeletes;

    protected $table = 'location';

    protected $fillable = [
        'name',
        'short_name',
        'longitude',
        'latitude',
        'description',
        'season',
        'address',
        'period_start_date',
        'period_end_date',
    ];

    protected $visible = [
        'short_name',
        'season',
        'icon',
        'interval',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Builder
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * @return string
     */
    public function getIconAttribute(): string
    {
        return sprintf('/image/%s.png', $this->season === 'winter' ? 'snowflake' : 'sun');
    }

    /**
     * @return string
     */
    public function getNewIconAttribute(): string
    {
        return $this->season === 'winter' ? 'snow' : 'sun';
    }

    /**
     * @return string
     */
    public function getFormattedPeriodStartDateAttribute(): string
    {
        return $this->period_start_date
            ? $this->period_start_date->format('d.m')
            : '';
    }

    /**
     * @return string
     */
    public function getFormattedPeriodEndDateAttribute(): string
    {
        return $this->period_end_date
            ? $this->period_end_date->format('d.m')
            : '';
    }

    /**
     * @return bool
     */
    public function getHasIntervalAttribute(): bool
    {
        return !!$this->period_start_date && !!$this->period_end_date;
    }

    /**
     * @return null|Interval
     */
    public function getInterval(): ?Interval
    {
        if ($this->has_interval) {
            return new Interval(
                $this->period_start_date->month,
                $this->period_start_date->day,
                $this->period_end_date->month,
                $this->period_end_date->day
            );
        }

        return null;
    }

    /**
     * @return string
     */
    public function getIntervalAttribute(): string
    {
        return $this->has_interval
            ? sprintf('%s - %s', $this->period_start_date->format('j F'), $this->period_end_date->format('j F'))
            : '';
    }

    /**
     * @param string|null $date
     * @return Date|null
     */
    public function getPeriodStartDateAttribute(string $date = null): ?Date
    {
        return $date
            ? Date::parse($date)
            : null;
    }

    /**
     * @param string|null $date
     * @return Date|null
     */
    public function getPeriodEndDateAttribute(string $date = null): ?Date
    {
        return $date
            ? Date::parse($date)
            : null;
    }

    /**
     * @param string|null $date
     */
    public function setPeriodStartDateAttribute(string $date = null): void
    {
        $this->attributes['period_start_date'] = $date
            ? Date::createFromFormat('d.m', $date)
            : null;
    }

    /**
     * @param string|null $date
     */
    public function setPeriodEndDateAttribute(string $date = null): void
    {
        $this->attributes['period_end_date'] = $date
            ? Date::createFromFormat('d.m', $date)
            : null;
    }
}
