<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $url
 * @mixin \Eloquent
 */
class PostCategory extends Model
{
    protected $table = 'post_category';
    protected $fillable = [
        'id',
        'url',
        'name',
    ];
    const CATEGORY_ALL = 'all';
    const CATEGORY_TRAINING = 'trainings';
    const CATEGORY_CAMPS = 'camps';
    const CATEGORY_TOURNAMENTS = 'tournaments';
}
