<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Trainings\SubscriptionHandlers\HandlerBase;
use Exception;

/**
 * App\Models\SubscriptionType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $handler_class
 * @property int $trainings_count
 */
class SubscriptionType extends Model
{
    protected $table = 'subscription_type';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'trainings_count',
    ];

    /**
     * @return HandlerBase
     * @throws Exception
     */
    public function getHandler()
    {
        if (is_subclass_of($this->handler_class, HandlerBase::class)) {
            return new $this->handler_class;
        }

        throw new Exception('Invalid subscription handler class');
    }
}
