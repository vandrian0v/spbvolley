<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Tour
 *
 * @property-read mixed $categories
 * @property-read mixed $locale_date
 * @property-read mixed $status_class
 * @property-read mixed $status_text
 * @property-read Collection|\App\Models\Team[] $teams
 * @property-read Collection|\App\Models\Tournament[] $tournaments
 * @method static Builder|Tour filter(array $filters)
 * @method static Builder|Tour filterStatus($filterName)
 * @method static Builder|Tour ofStatus($status)
 * @method static Builder|Tour search($search)
 * @method static Builder|Tour sort($sortName)
 * @method static Builder|Tour sorted()
 * @method static Builder|Tour past()
 * @method static Builder|Tour future()
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property string $status
 * @property string $start_date
 * @property string $end_date
 * @property string $description
 * @property float $longitude
 * @property float $latitude
 * @property string $deleted_at
 */
class Tour extends Model
{
    use SoftDeletes, Scopes\Tour;

    protected $table = 'nwtour';

    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'status',
        'description',
        'longitude',
        'latitude',
    ];

    protected $attributes = [
        'status' => self::STATUS_CREATED,
    ];

    protected $appends = [
        'localeDate',
        'statusClass',
        'statusText',
    ];

    public $timestamps = false;

    const STATUS_CREATED   = 'created';
    const STATUS_ANNOUNCED = 'anounsed';
    const STATUS_OPENED    = 'opened';
    const STATUS_CLOSED    = 'closed';
    const STATUS_FINISHED  = 'finished';

    const STATUSES = [
        self::STATUS_CREATED,
        self::STATUS_ANNOUNCED,
        self::STATUS_OPENED,
        self::STATUS_CLOSED,
        self::STATUS_FINISHED,
    ];

    const STATUS_TITLES = [
        self::STATUS_CREATED   => 'Этап создан',
        self::STATUS_ANNOUNCED => 'Этап анонсирован',
        self::STATUS_OPENED    => 'Регистрация открыта',
        self::STATUS_CLOSED    => 'Регистрация окончена',
        self::STATUS_FINISHED  => 'Этап проведен',
    ];

    const STATUS_CLASSES = [
        self::STATUS_CREATED   => 'primary',
        self::STATUS_ANNOUNCED => 'info',
        self::STATUS_OPENED    => 'danger',
        self::STATUS_CLOSED    => 'warning',
        self::STATUS_FINISHED  => 'success',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Builder
     */
    public function tournaments()
    {
        return $this->hasMany(Tournament::class, 'nwtour_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough|\Illuminate\Database\Eloquent\Builder
     */
    public function teams()
    {
        return $this->hasManyThrough(Team::class, Tournament::class, 'nwtour_id');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getCategoriesAttribute()
    {
        return $this->tournaments->pluck('category_id');
    }

    /**
     * @return string
     */
    public function getLocaleDateAttribute(): string
    {
        $startDate = new Date($this->start_date);
        $endDate   = new Date($this->end_date);

        if ($startDate->month === $endDate->month) {
            if ($startDate->day === $endDate->day) {
                $date = $endDate->format('j F Y');
            } else {
                $date = sprintf(
                    '%d – %s',
                    $startDate->day,
                    $endDate->format('j F Y')
                );
            }
        } else {
            $date = sprintf(
                '%s – %s',
                $startDate->format('j F'),
                $endDate->format('j F Y')
            );
        }

        return $date;
    }

    /**
     * @return string
     */
    public function getStatusTextAttribute(): string
    {
        return self::STATUS_TITLES[$this->status];
    }

    /**
     * @return string
     */
    public function getStatusClassAttribute(): string
    {
        return self::STATUS_CLASSES[$this->status];
    }
}
