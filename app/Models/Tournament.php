<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Tournament
 *
 * @property-read TournamentCategory $category
 * @property-read Tour $nwtour
 * @property Collection|Team[] $teams
 * @property Collection|Team[] $confirmedTeams
 * @mixin \Eloquent
 * @property int $id
 * @property int $category_id
 * @property int $nwtour_id
 * @property int $contribution
 * @property int $limit
 * @property string $deleted_at
 * @property bool $limit_exceeded
 * @method static Builder free()
 */
class Tournament extends Model
{
    use SoftDeletes;

    private $cached_limit_exceeded;

    protected $table = 'tournament';

    protected $fillable = [
        'category_id',
        'contribution',
        'limit',
    ];
    
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function nwtour()
    {
        return $this->belongsTo(Tour::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function category()
    {
        return $this->belongsTo(TournamentCategory::class, 'category_id');
    }

    /**
     * Подтверденные команды (с оплаченным взносом)
     *
     * @return HasMany|Builder
     */
    public function confirmedTeams()
    {
        /** @var Builder $relation */
        $relation = $this->hasMany(Team::class);

        $relation->where(function (Builder $query) {
            // Стоит отметка об оплате
            $query->where('is_paid', true);

            // Бесплатный турнир
            $query->orWhereHas('tournament', function (Builder $query) {
                /** @var Tournament $query */
                $query->free();
            });

            // Есть оплаченный платеж
            $query->orWhereHas('payment', function (Builder $query) {
                /** @var Payment $query */
                $query->confirmed();
            });
        });

        return $relation;
    }

    /**
     * Все команды
     *
     * @return HasMany|Builder
     */
    public function teams()
    {
        /** @var Builder $relation */
        $relation = $this->hasMany(Team::class);

        return $relation;
    }

    /**
     * @return bool
     */
    public function getLimitExceededAttribute(): bool
    {
        if (is_null($this->cached_limit_exceeded)) {
            $this->cached_limit_exceeded = $this->limit
                ? $this->confirmedTeams()->count() >= $this->limit
                : false;
        }

        return $this->cached_limit_exceeded;

    }

    /**
     * @return Collection|static[]
     */
    public function sortedTeams()
    {
        return $this->teams()
            ->orderBy(DB::raw('-rank'), 'desc')
            ->get();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeFree(Builder $query)
    {
        return $query->where('contribution', 0);
    }

    /**
     * @param array $userIds
     * @return Team|null
     */
    public function teamWithUsers(array $userIds): ?Team
    {
        $data = $this->teams()
            ->select('team_id')
            ->join('m2m_user_team', 'team.id', '=', 'm2m_user_team.team_id')
            ->whereIn('m2m_user_team.user_id', $userIds)
            ->groupBy('team_id')
            ->havingRaw('count(*) = ' . count($userIds))
            ->first();

        return $data
            ? Team::find($data->team_id)
            : null;
    }
}
