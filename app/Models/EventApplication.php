<?php

namespace App\Models;

use Date;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\EventApplication
 *
 * @property-read mixed $date
 * @method static Builder|EventApplication new()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property bool $is_new
 * @property string $phone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class EventApplication extends Model
{
    protected $table = 'event_application';

    protected $fillable = [
        'name',
        'email',
        'comment',
        'phone',
    ];

    /**
     * @return string
     */
    public function getDateAttribute(): string
    {
        return (new Date($this->created_at))->format('d F Y');
    }

    /**
     * @param Builder $query
     * @return Builder|self
     */
    public function scopeNew(Builder $query)
    {
        return $query->where('is_new', true);
    }
}
