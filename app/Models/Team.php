<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Team
 *
 * @property-read mixed $users_names
 * @property-read Payment $payment
 * @property-read Tournament $tournament
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $users
 * @mixin \Eloquent
 * @property int $id
 * @property int $tournament_id
 * @property int $rank
 * @property int $payment_id
 * @property int $rating
 * @property int $users_rating
 * @property int $category_id
 * @property bool $is_paid
 * @property bool $is_confirmed
 * @property string $usersNames
 * @method static Team|Builder withTrashed()
 */
class Team extends Model
{
    use SoftDeletes;

    protected $table = 'team';

    protected $fillable = [
        'payment_id',
        'rank',
        'is_paid',
        'category_id',
    ];

    protected $appends = [
        'is_confirmed'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function tournament()
    {
        return $this->belongsTo(Tournament::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Builder
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'm2m_user_team');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function category()
    {
        return $this->belongsTo(TournamentCategory::class);
    }

    /**
     * @param null $excludeUserId
     * @return string
     */
    public function usersLinks($excludeUserId = null):string
    {
        return $this->links('auth.profile.show', $excludeUserId);
    }

    /**
     * @return string
     */
    public function usersAdminLinks(): string
    {
        return $this->links('admin.users.edit');
    }

    /**
     * @param $route
     * @param null $excludeUserId
     * @return string
     */
    private function links($route, $excludeUserId = null): string
    {
        $links = [];

        foreach ($this->users as $user) {
            if ($excludeUserId == $user->id) {
                continue;
            }

            $links[] = sprintf(
                "<a href=\"%s\">%s %s.</a>",
                route($route, $user),
                $user->surname,
                mb_substr($user->name, 0, 1)
            );
        }

        return implode('/', $links);
    }

    /**
     * @return string
     */
    public function getUsersNamesAttribute(): string
    {
        return $this->users->implode('surname', '/');
    }

    /**
     * @return bool
     */
    public function getIsConfirmedAttribute(): bool
    {
        return $this->is_paid
            ? true
            : ($this->tournament->contribution
                ? ($this->payment->is_confirmed ?? false)
                : true
            );
    }

    public function getUsersRatingAttribute()
    {
        return $this->users->sum('yearly_rating');
    }
}
