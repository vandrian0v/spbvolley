<?php

namespace App\Models;

use Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\CampApplication
 *
 * @property-read \App\Models\Camp $camp
 * @property-read mixed $date
 * @method static Builder|CampApplication new()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property int $camp_id
 * @property bool $is_new
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class CampApplication extends Model
{
    protected $table = 'camp_application';

    protected $fillable = [
        'name',
        'email',
        'camp_id',
        'phone',
        'comment',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder
     */
    public function camp()
    {
        return $this->belongsTo(Camp::class);
    }

    /**
     * @return string
     */
    public function getDateAttribute(): string
    {
        return (new Date($this->created_at))->format('d F Y');
    }

    /**
     * @param Builder $query
     * @return Builder|self
     */
    public function scopeNew(Builder $query)
    {
        return $query->where('is_new', true);
    }
}
