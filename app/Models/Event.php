<?php

namespace App\Models;

use App\Models\Photos\EventPhoto as Photo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Jenssegers\Date\Date;

/**
 * App\Models\Event
 *
 * @property-read mixed $locale_date
 * @property-read \App\Models\Location $location
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photos\EventPhoto[] $photos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $services
 * @method static Builder|Event search($title)
 * @method static Builder|Event sort($sortName)
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $participants
 * @property string $date
 * @property int $location_id
 */
class Event extends Model
{
    use Scopes\Event, Scopes\PhotoOwner;

    protected $table = 'event';

    protected $fillable = [
        'title',
        'date',
        'participants',
        'description',
        'location_id',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Eloquent\Builder
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'm2m_event_service');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Builder
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * @return string
     */
    public function getLocaleDateAttribute(): string
    {
        return (new Date($this->date))->format('j F Y');
    }
}
