<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Photos\CoachPhoto as Photo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Coach
 *
 * @property-read Collection|Camp[] $camps
 * @property-read Collection|Photo[] $photos
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $career
 * @property string $education
 * @property string $training
 * @property string $teachers
 * @property string $status
 */
class Coach extends Model
{
    use Scopes\PhotoOwner, SoftDeletes;

    protected $table = 'coach';

    protected $fillable = [
        'name',
        'career',
        'education',
        'training',
        'teachers',
        'status',
    ];

    protected $visible = [
        'id',
        'name',
        'preview_url',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Builder
     */
    public function camps()
    {
        return $this->belongsToMany(Camp::class, 'm2m_camp_coach');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Builder
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
