<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Collection;
use Storage;
use App\Models\Photos\Photo;

/**
 * Class PhotoOwner
 * @package App\Models\Scopes
 * @property Collection $photos
 */
trait PhotoOwner
{
    /**
     * @return string
     */
    public function previewUrl(): string
    {
        return $this->hasPreview()
            ? asset('storage/' . $this->previewPath() . '?' . Storage::lastModified($this->previewPath()))
            : '';
    }

    /**
     * @return string
     */
    public function getPreviewUrlAttribute(): string
    {
        return $this->previewUrl();
    }

    /**
     * @return string
     */
    public function previewPath(): string
    {
        return "{$this->table}/{$this->id}/preview";
    }

    /**
     * @return bool
     */
    public function hasPreview(): bool
    {
        return Storage::exists("{$this->table}/{$this->id}/preview");
    }

    /**
     * @return Collection
     */
    public function photosWithUrl()
    {
        return $this->photos->transform(function ($photo) {
            /** @var Photo $photo */
            $photo->setAppends(['url']);

            return $photo;
        });
    }
}