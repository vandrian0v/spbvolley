<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

trait Tour
{
    /**
     * @param Builder|Tour $query
     * @param array $filters
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $filters)
    {
        if (isset($filters['status'])) $query->filterStatus($filters['status']);
        if (isset($filters['year'])) $query->whereYear('start_date', $filters['year']);
        if (isset($filters['current'])) $query->whereYear('start_date', '>=', Carbon::now()->year);
        if (isset($filters['archive'])) $query->whereYear('start_date', '<', Carbon::now()->year);

        return $query;
    }

    /**
     * Scope a query to only include tours with status.
     *
     * @param  Builder|Tour $query
     * @param  string $filterName
     * @return Builder
     */
    public function scopeFilterStatus(Builder $query, $filterName)
    {
        if ($filterName) {
            switch ($filterName) {
                case 'past':
                    $query->past();
                    break;
                case 'future':
                    $query->future();
                    break;
                default:
                    $query->ofStatus($filterName);
            }
        }

        return $query;
    }

    /**
     * Scope a query to only include tours match to search attr.
     *
     * @param  Builder $query
     * @param  string $search
     * @return Builder
     */
    public function scopeSearch(Builder $query, string $search)
    {
        return $query->where('title', 'like', "%$search%")
            ->orWhere('description', 'like', "%$search%");
    }

    /**
     * Scope a query apply sort to query.
     *
     * @param  Builder $query
     * @param  string $sortName
     * @return Builder
     */
    public function scopeSort(Builder $query, $sortName)
    {
        switch ($sortName) {
            case 'title_asc':
                return $query->orderBy('title', 'asc');
            case 'title_desc':
                return $query->orderBy('title', 'desc');
            case 'date_desc':
                return $query->orderBy('start_date', 'desc');
            default:
                return $query->orderBy('start_date', 'asc');
        }
    }

    /**
     * Scope a query to sort tours.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeSorted($query)
    {
        return $query->where('status', '!=', 'created')
            ->orderByRaw("
                CASE status
                    WHEN 'opened' THEN 1
                    WHEN 'closed' THEN 2
                    WHEN 'anounsed' THEN 3
                    WHEN 'finished' THEN 4
                END
            ")->orderBy('start_date');
    }

    /**
     * Scope a query to only include tours with specific status.
     *
     * @param  Builder $query
     * @param  string $status
     * @return Builder
     */
    public function scopeOfStatus(Builder $query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeFuture(Builder $query)
    {
        return $query->whereDate('end_date', '>=', Carbon::today());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePast(Builder $query)
    {
        return $query->whereDate('end_date', '<', Carbon::today());
    }
}