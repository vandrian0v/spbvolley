<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait Post
{
    /**
     * Scope a query to only include posts match to title attr.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $title
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch(Builder $query, string $title)
    {
        return $query->where('title', 'LIKE', "%$title%");
    }

    /**
     * Scope a query apply sort to query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $sortName
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSort(Builder $query, $sortName)
    {
        switch ($sortName) {
            case 'title_asc':
                return $query->orderBy('title', 'asc');
            case 'title_desc':
                return $query->orderBy('title', 'desc');
            case 'date_asc':
                return $query->orderBy('id', 'asc');
            default:
                return $query->orderBy('id', 'desc');
        }
    }

    /**
     * @param Builder $query
     * @param int $categoryId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCategory(Builder $query, int $categoryId)
    {
        return $query->where('category_id', $categoryId);
    }
}