<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Payment as PaymentModel;

/**
 * Class Payment
 * @package App\Models\Scopes
 *
 * @method static ofStatus($status)
 */
trait Payment
{
    /**
     * Scope a query to only include tours with status.
     *
     * @param  Builder|self $query
     * @param  string $filterName
     * @return Builder|self
     */
    public function scopeFilter(Builder $query, $filterName)
    {
        switch ($filterName) {
            case 'all':
                return $query;
            case 'created':
                return $query->ofStatus(PaymentModel::STATUS_CREATED);
            case 'canceled':
                return $query->ofStatus(PaymentModel::STATUS_CANCELED);
            case 'checked':
                return $query->ofStatus(PaymentModel::STATUS_CHECKED);
            default:
                return $query->ofStatus(PaymentModel::STATUS_CONFIRMED);
        }
    }

    /**
     * @param Builder|self $query
     * @return Builder|self
     */
    public function scopeConfirmed(Builder $query)
    {
        return $query->ofStatus(PaymentModel::STATUS_CONFIRMED);
    }

    /**
     * Scope a query to only include tours match to search attr.
     *
     * @param  Builder $query
     * @param  string $search
     * @return Builder
     */
    public function scopeSearch(Builder $query, string $search)
    {
        return $query->where(function ($query) use ($search) {
            return $query->where('comment', 'like', "%$search%")
                ->orWhereHas('user', function (Builder $query) use ($search) {
                    return $query->where('name', 'like', "%$search%")
                        ->orWhere('surname', 'like', "%$search%");
                });
        });
    }

    /**
     * Scope a query to only include payments created before given date.
     *
     * @param  Builder $query
     * @param  string $date
     * @return Builder
     */
    public function scopeBefore(Builder $query, $date)
    {
        return $query->where('created_at', '<=', $date);
    }

    /**
     * Scope a query to only include payments created after given date.
     *
     * @param  Builder $query
     * @param  string $date
     * @return Builder
     */
    public function scopeAfter(Builder $query, $date)
    {
        return $query->where('created_at', '>=', $date);
    }

    /**
     * Scope a query to only include tours with specific status.
     *
     * @param  Builder $query
     * @param  string $status
     * @return Builder
     */
    public function scopeOfStatus(Builder $query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * Scope a query to only include tours with specific status.
     *
     * @param  Builder $query
     * @param  int $paymentTypeId
     * @return Builder
     */
    public function scopeOfType(Builder $query, int $paymentTypeId)
    {
        return $query->where('payment_type_id', $paymentTypeId);
    }
}