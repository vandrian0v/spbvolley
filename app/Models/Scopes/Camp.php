<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait Camp
{
    /**
     * Scope a query to only include posts match to title attr.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $title
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch(Builder $query, string $title)
    {
        return $query->where('title', 'LIKE', "%$title%");
    }

    /**
     * Scope a query apply sort to query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $sortName
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSort(Builder $query, $sortName)
    {
        switch ($sortName) {
            case 'title_asc':
                return $query->orderBy('title', 'asc');
            case 'title_desc':
                return $query->orderBy('title', 'desc');
            case 'date_asc':
                return $query->orderBy('start_date', 'asc');
            case 'date_desc':
                return $query->orderBy('start_date', 'desc');
            default:
                return $query->orderBy('id', 'desc');
        }
    }


    public function scopeFuture(Builder $query)
    {
        return $query->where('future', true);
    }

    public function scopePast(Builder $query)
    {
        return $query->where('future', false);
    }
}