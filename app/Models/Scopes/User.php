<?php

namespace App\Models\Scopes;

use App\Models\UserCategory;
use Illuminate\Database\Eloquent\Builder;

trait User
{
    /**
     * Scope a query to only include admins users.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeAdmins(Builder $query)
    {
        return $query->where('is_admin', true);
    }

    /**
     * Scope a query to only include admins users or deleted users.
     *
     * @param  Builder|User $query
     * @param array $filters
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $filters)
    {
        if (isset($filters['category']))  $query->byCategory($filters['category']);
        if (isset($filters['has_rank']))  $query->where('yearly_rating', '!=', 0);
        if (isset($filters['gender']))    $query->where('gender', $filters['gender']);
        if (isset($filters['has_phone'])) $query->whereNotNull('phone');

        return $query;
    }

    /**
     * @param Builder|User $query
     * @param $filterName
     * @return Builder
     */
    public function scopeByCategory(Builder $query, $filterName)
    {
        switch ($filterName) {
            case 'admins':
                return $query->admins();
            case 'all':
                return $query->withTrashed();
            case 'pro':
                return $query->ofCategory(UserCategory::PRO);
            case 'a':
                return $query->ofCategory(UserCategory::A);
            case 'b':
                return $query->ofCategory(UserCategory::B);
            case 'trashed':
                return $query->onlyTrashed();
            default:
                return $query;
        }
    }

    /**
     * Scope a query to only include users match to search attr.
     *
     * @param  Builder $query
     * @param  string $search
     * @return Builder
     */
    public function scopeSearch(Builder $query, string $search)
    {
        if (!$search) {
            return $query;
        }

        $explodedSearch = explode(" ", $search);

        foreach ($explodedSearch as $item) {
            $query->where(function (Builder $query) use ($item) {
                $query->orWhere('name', 'like', "%$item%")
                    ->orWhere('surname', 'like', "$item%")
                    ->orWhere('email', 'like', "%$item%");
            });
        }

        return $query;
    }

    /**
     * Scope a query apply sort to query.
     *
     * @param  Builder $query
     * @param  string $sortName
     * @return Builder
     */
    public function scopeSort(Builder $query, $sortName)
    {
        switch ($sortName) {
            case 'name_asc':
                return $query->orderBy('name', 'asc');
            case 'surname_asc':
                return $query->orderBy('surname', 'asc');
            case 'id_desc':
                return $query->orderBy('id', 'desc');
            case 'id_asc':
                return $query->orderBy('id', 'asc');
            default:
                return $query;
        }
    }

    /**
     * @param Builder $query
     * @param int $categoryId
     * @return mixed
     */
    public function scopeOfCategory(Builder $query, int $categoryId)
    {
        return $query->where('category_id', $categoryId);
    }
}