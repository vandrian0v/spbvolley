<?php

namespace App\Models;

use App\Notifications\ResetPassword as ResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Date;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\User
 *
 * @mixin \Eloquent
 * @property-read string $formatted_name
 * @property-read string $fullname
 * @property-read string $link
 * @property-read int $teams_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read Collection|Payment[] $payments
 * @property-read Collection|Team[] $teams
 * @property-read Collection|UserTraining[] $trainings
 * @method static Builder|User admins()
 * @method static Builder|User filter(array $filters)
 * @method static Builder|User byCategory($filterName)
 * @method static Builder|User search($search)
 * @method static Builder|User sort($sortName)
 * @method static Builder|User withTrashed()
 * @method static Builder|User onlyTrashed()
 * @method static Builder|User ofCategory($categoryId)
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $password
 * @property bool $is_admin
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $gender
 * @property Carbon $birth_date
 * @property int $height
 * @property int $weight
 * @property string $hobby
 * @property string $deleted_at
 * @property string $phone
 * @property int $category_id
 * @property int $seasonal_rating
 * @property int $yearly_rating
 */
class User extends Authenticatable
{
    use SoftDeletes, Notifiable, Scopes\User;

    protected $table = 'user';

    protected $casts = ['is_admin' => 'boolean'];

    protected $fillable = [
        'birth_date',
        'email',
        'gender',
        'height',
        'hobby',
        'name',
        'password',
        'surname',
        'weight',
        'phone',
        'category_id',
        'note',
        'training_level_id',
    ];

    protected $visible = [
        'id',
        'gender',
        'link',
        'name',
        'surname',
        'yearly_rating',
        'seasonal_rating',
        'fullname',
        'category_id',
    ];

    protected $appends = [
        'link',
        'fullname',
        'formatted_name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(UserCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Eloquent\Builder
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class, 'm2m_user_team');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Builder|Payment
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * @return HasManyThrough|\Illuminate\Database\Eloquent\Builder
     */
    public function trainings()
    {
        /** @var UserTraining $relation */
        $relation = $this->hasManyThrough(UserTraining::class, Payment::class);

        return $relation->where('payment_type_id', Payment::TYPE_TRAINING)
            ->paid();
    }

    /**
     * @param $value
     */
    public function setNameAttribute($value): void
    {
        $this->attributes['name'] = trim($value);
    }

    /**
     * @param $value
     */
    public function setSurnameAttribute($value): void
    {
        $this->attributes['surname'] = trim($value);
    }

    /**
     * @return string
     */
    public function ageAndBirthDate(): string
    {
        $result = '';

        if ($this->birth_date) {
            $date = new Date($this->birth_date);
            $result = sprintf('%d (%s)', $date->age, $date->format('d F Y'));
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getFormattedNameAttribute(): string
    {
        return sprintf('%s %s.', $this->surname, mb_substr($this->name, 0, 1));
    }

    /**
     * @return string
     */
    public function getFullnameAttribute(): string
    {
        return sprintf('%s %s', $this->surname, $this->name);
    }

    /**
     * @return string
     */
    public function getLinkAttribute(): string
    {
        return route('auth.profile.show', $this->id);
    }

    /**
     * @return int
     */
    public function getTeamsCountAttribute(): int
    {
        return $this->teams->count();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Route notifications for the Sms channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSms($notification)
    {
        return $this->phone;
    }
}
