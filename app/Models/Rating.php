<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Rating
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $category_id
 * @property int $points
 * @property int $start_rank
 * @property int $end_rank
 */
class Rating extends Model
{
    protected $table = 'rating';
}
