<?php

namespace App\Models\Photos;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Photos\Photo
 *
 * @property string $name
 * @mixin \Eloquent
 */
class Photo extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];

    protected $visible = ['id', 'url'];

    /**
     * @return string
     */
    protected function path(): string
    {
        return 'default';
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return asset("storage/{$this->path()}/{$this->name}");
    }

    /**
     * @return string
     */
    public function getUrlAttribute(): string
    {
        return $this->url();
    }

    /**
     * @return string
     */
    public function fullPath(): string
    {
        return sprintf('%s/%s', $this->path(), $this->name);
    }
}
