<?php

namespace App\Models\Photos;

use App\Models\Event;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Photos\EventPhoto
 *
 * @property-read \App\Models\Event $event
 * @mixin \Eloquent
 * @property int $id
 * @property int $event_id
 * @property string $name
 */
class EventPhoto extends Photo
{
    protected $table = 'event_photo';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return string
     */
    protected function path(): string
    {
        return "event/{$this->event->id}";
    }
}
