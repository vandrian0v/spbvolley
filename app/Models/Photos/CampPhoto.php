<?php

namespace App\Models\Photos;

use App\Models\Camp;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Photos\CampPhoto
 *
 * @property-read \App\Models\Camp $camp
 * @mixin \Eloquent
 * @property int $id
 * @property int $camp_id
 * @property string $name
 */
class CampPhoto extends Photo
{
    protected $table = 'camp_photo';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function camp()
    {
        return $this->belongsTo(Camp::class);
    }

    /**
     * @return string
     */
    protected function path(): string
    {
        return "camp/{$this->camp->id}";
    }
}
