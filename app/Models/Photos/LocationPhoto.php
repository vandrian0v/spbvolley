<?php

namespace App\Models\Photos;

use App\Models\Location;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Photos\LocationPhoto
 *
 * @property-read \App\Models\Location $location
 * @mixin \Eloquent
 * @property int $id
 * @property int $location_id
 * @property string $name
 */
class LocationPhoto extends Photo
{
    protected $table = 'location_photo';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @return string
     */
    protected function path(): string
    {
        return "location/{$this->location->id}";
    }
}
