<?php

namespace App\Models\Photos;

use App\Models\Coach;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Photos\CoachPhoto
 *
 * @property-read \App\Models\Coach $coach
 * @mixin \Eloquent
 * @property int $id
 * @property int $coach_id
 * @property string $name
 */
class CoachPhoto extends Photo
{
    protected $table = 'coach_photo';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }

    /**
     * @return string
     */
    protected function path(): string
    {
        return "coach/{$this->coach->id}";
    }
}
