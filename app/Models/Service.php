<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Service
 *
 * @property-read Collection|Event[] $events
 * @property-read Collection|Package[] $packages
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 */
class Service extends Model
{
    protected $table = 'service';

    protected $fillable = [
        'name',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Builder
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'm2m_event_service');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Builder
     */
    public function packages()
    {
        return $this->belongsToMany(Package::class, 'm2m_package_service');
    }
}
