<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 * @property int $id
 * @property string $url
 * @property string $name
 * @property string $preview_url
 */
class Sponsor extends Model
{
    use Scopes\PhotoOwner;

    protected $table = 'sponsor';

    protected $appends = [
        'preview_url'
    ];

    protected $fillable = [
        'name',
        'url',
    ];
}
