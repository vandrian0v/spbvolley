<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Package
 *
 * @property-read Collection|Event[] $events
 * @property-read Collection|Service[] $services
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $description
 * @property float $duration
 * @property int $price
 * @property string $color
 * @property bool $is_min_duration
 * @property string $info
 */
class Package extends Model
{
    protected $table = 'package';

    protected $fillable = [
        'name',
        'description',
        'duration',
        'price',
        'color',
        'info',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Builder
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Builder
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'm2m_package_service');
    }
}
