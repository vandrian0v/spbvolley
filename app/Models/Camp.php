<?php

namespace App\Models;

use App\Models\Photos\CampPhoto as Photo;
use Illuminate\Database\Eloquent\Model;
use Date;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Camp
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Coach[] $coaches
 * @property-read mixed $locale_dates
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photos\CampPhoto[] $photos
 * @method static Builder|Camp future()
 * @method static Builder|Camp past()
 * @method static Builder|Camp search($title)
 * @method static Builder|Camp sort($sortName)
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property bool $future
 * @property string $full_description
 */
class Camp extends Model
{
    use Scopes\Camp, Scopes\PhotoOwner;

    protected $table = 'camp';

    protected $fillable = [
        'title',
        'description',
        'full_description',
        'start_date',
        'end_date',
        'future',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Eloquent\Builder
     */
    public function coaches()
    {
        return $this->belongsToMany(Coach::class, 'm2m_camp_coach')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Builder
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * @return string
     */
    public function getLocaleDatesAttribute(): string
    {
        $startDate = new Date($this->start_date);
        $endDate   = new Date($this->end_date);

        if ($startDate->month == $endDate->month) {
            $date = sprintf(
                '%d – %s',
                $startDate->day,
                $endDate->format('j F Y')
            );
        } else {
            $date = sprintf(
                '%s – %s %s',
                $startDate->format('j F'),
                $endDate->format('j F'),
                $startDate->format('Y')
            );
        }

        return $date;
    }
}
