<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TrainingGroup
 *
 * @property-read Collection|Training[] $trainings
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int $order
 * @method static Builder|TrainingGroup sorted()
 * @method static Builder|TrainingGroup lockForUpdate()
 */
class TrainingGroup extends Model
{
    use SoftDeletes;

    protected $table = 'training_group';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'url',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Builder
     */
    public function trainings()
    {
        return $this->hasMany(Training::class);
    }

    /**
     * @param Builder $query
     * @return Builder|self
     */
    public function scopeSorted(Builder $query)
    {
        return $query->orderBy('order');
    }
}
