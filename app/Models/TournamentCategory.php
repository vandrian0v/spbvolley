<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TournamentCategory
 *
 * @property-read mixed $fullname
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $gender
 * @property int $default_contribution
 * @property int $team_size
 */
class TournamentCategory extends Model
{
    protected $table = 'tournament_category';

    public $timestamps = false;

    private $genders = [
        'male'   => 'м',
        'female' => 'ж',
    ];

    protected $appends = ['fullname'];

    /**
     * @return string
     */
    public function getFullnameAttribute(): string
    {
        $name = $this->name;

        if (in_array($this->gender, array_keys($this->genders))) {
            $name = sprintf('%s (%s)', $name, $this->genders[$this->gender]);
        }

        return $name;
    }
}
