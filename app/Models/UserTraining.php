<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Date\Date;

/**
 * App\Models\UserTraining
 *
 * @method static Builder|UserTraining paid()
 * @mixin \Eloquent
 * @property-read mixed $locale_date
 * @property-read Payment $payment
 * @property-read Coach $coach
 * @property-read Location $location
 * @property int $id
 * @property int $payment_id
 * @property int $coach_id
 * @property int $location_id
 * @property int $training_group_id
 * @property int $training_id
 * @property string $start_time
 * @property string $end_time
 * @property \Carbon\Carbon $date
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class UserTraining extends Model
{
    protected $table = 'user_training';

    protected $fillable = [
        'user_id',
        'date',
        'description',
        'coach_id',
        'location_id',
        'start_time',
        'end_time',
        'training_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date',
    ];

    /**
     * @param Builder $query
     * @return Builder|self
     */
    public function scopePaid(Builder $query)
    {
        return $query->whereHas('payment', function (Builder $query) {
            $query->where('status', Payment::STATUS_CONFIRMED);
        });
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo|Builder
     */
    public function payment()
    {
        /** @var Builder $relation */
        $relation = $this->belongsTo(Payment::class);

        return $relation->where('payment_type_id', Payment::TYPE_TRAINING);
    }

    /**
     * @return BelongsTo|Builder
     */
    public function coach()
    {
        return $this->belongsTo(Coach::class)->withTrashed();
    }

    /**
     * @return BelongsTo|Builder
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @return BelongsTo|Builder
     */
    public function training()
    {
        return $this->belongsTo(Training::class)->withTrashed();
    }

    /**
     * @return string
     */
    public function getLocaleDateAttribute(): string
    {
        return (new Date($this->date))->format('j F Y');
    }
}
