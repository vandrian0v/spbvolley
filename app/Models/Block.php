<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 * @property int $id
 */
class Block extends Model
{
    use Scopes\PhotoOwner;

    protected $table = 'block';

    protected $appends = [
        'preview_url'
    ];

    protected $fillable = [
        'url',
        'text',
        'title',
        'order',
    ];
}
