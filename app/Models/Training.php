<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Training
 *
 * @mixin \Eloquent
 * @property-read Coach $coach
 * @property-read int $day_of_week
 * @property-read int $dayOfWeek
 * @property-read TrainingGroup $group
 * @property-read Location $location
 * @property-read Collection|SubscriptionType[] $subscriptionTypes
 * @property int $id
 * @property int $coach_id
 * @property int $training_group_id
 * @property int $location_id
 * @property int $limit
 * @property string $start_time
 * @property string $end_time
 * @property int $weekday
 * @property array $subscription_types
 * @method static self|Builder filter(array $filters)
 */
class Training extends Model
{
    use SoftDeletes;

    protected $table = 'training';

    protected $fillable = [
        'id',
        'coach_id',
        'end_time',
        'start_time',
        'weekday',
        'location_id',
        'training_group_id',
        'limit',
    ];

    protected $appends = [
        'formatted_name'
    ];

    protected $hidden = [
        'subscriptionTypes',
    ];

    private $weekdays = [
        '0' => 'Понедельник',
        '1' => 'Вторник',
        '2' => 'Среда',
        '3' => 'Четверг',
        '4' => 'Пятница',
        '5' => 'Суббота',
        '6' => 'Воскресенье',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function group()
    {
        return $this->belongsTo(TrainingGroup::class, 'training_group_id')->withTrashed();;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function coach()
    {
        return $this->belongsTo(Coach::class)->withTrashed();
    }

    /**
     * @return BelongsToMany|Builder
     */
    public function subscriptionTypes()
    {
        return $this->belongsToMany(SubscriptionType::class, 'training_price')
            ->withPivot('price');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Builder
     */
    public function userTrainings()
    {
        return $this->hasMany(UserTraining::class);
    }

    /**
     * @return string
     */
    public function formattedName(): string
    {
        return sprintf('%s (%s - %s)', $this->weekdays[$this->weekday], $this->start_time, $this->end_time);
    }

    public function getFormattedNameAttribute()
    {
        return $this->formattedName();
    }

    /**
     * @return int
     */
    public function getDayOfWeekAttribute(): int
    {
        return $this->weekday === 6 ? 0 : $this->weekday + 1;
    }

    public function scopeFilters(Builder $query, array $filters)
    {
        return $query;
    }

    public function t()
    {
        $dates = $this->closestDates(null, 10);
        $userTrainings = $this->userTrainings()
            ->whereIn('date', $dates)
            ->get();

    }

    public function closestDates(Carbon $date = null, int $count = 1)
    {
        $dates = [];

        if (!$date) {
            $date = Carbon::today();
        }

        $date->startOfWeek()->addDays($this->weekday);

        if ($date->lessThan(Carbon::today())) {
            $date->addWeek();
        }

        while (count($dates) < $count) {
            $dates[] = $date->copy()->format('Y-m-d');

            $date->addWeek();
        }

        return $dates;
    }

    public function getClosestDatesAttribute()
    {
        return $this->closestDates(null, 8);
    }
}
