<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Date;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Payment
 *
 * @mixin \Eloquent
 * @property-read string $locale_date
 * @property-read string $status_class
 * @property-read string $status_text
 * @property-read Team $team
 * @property-read PaymentType $type
 * @property-read User $user
 * @property-read Collection|UserTraining[] $userTrainings
 * @method static Builder|Payment after($date)
 * @method static Builder|Payment before($date)
 * @method static Builder|Payment confirmed()
 * @method static Builder|Payment filter($filterName)
 * @method static Builder|Payment ofStatus($status)
 * @method static Builder|Payment ofType($paymentTypeId)
 * @method static Builder|Payment search($search)
 * @property int $id
 * @property int $status
 * @property int $invoice_id
 * @property string $comment
 * @property float $sum
 * @property float $order_sum_amount
 * @property float $shop_sum_amount
 * @property string $order_currency
 * @property string $shop_currency
 * @property int $user_id
 * @property int $is_confirmed
 * @property int $is_not_confirmed
 * @property int $payment_type_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Payment extends Model
{
    use Scopes\Payment;

    protected $table = 'payment';

    const TYPE_TRAINING                = 1;
    const TYPE_TOURNAMENT_CONTRIBUTION = 2;

    const STATUS_CREATED   = 0;
    const STATUS_CHECKED   = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_CANCELED  = 3;

    public $statuses = [
        self::STATUS_CREATED   => 'создан',
        self::STATUS_CHECKED   => 'проверен',
        self::STATUS_CONFIRMED => 'подтвержден',
        self::STATUS_CANCELED  => 'отменен',
    ];

    public $classes = [
        self::STATUS_CREATED   => 'primary',
        self::STATUS_CHECKED   => 'info',
        self::STATUS_CONFIRMED => 'success',
        self::STATUS_CANCELED  => 'danger',
    ];

    protected $fillable = [
        'payment_type_id',
        'status',
        'comment',
        'sum',
        'order_sum_amount',
        'shop_sum_amount',
        'invoice_id',
    ];

    protected $appends = [
        'is_confirmed'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder
     */
    public function type()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasOne|\Illuminate\Database\Eloquent\Builder
     */
    public function team()
    {
        return $this->hasOne(Team::class)->withTrashed();
    }

    /**
     * @return HasMany|\Illuminate\Database\Eloquent\Builder
     */
    public function userTrainings()
    {
        /** @var \Illuminate\Database\Eloquent\Builder $relation */
        $relation = $this->hasMany(UserTraining::class);

        return $relation->orderBy('date');
    }

    /**
     * @return string
     */
    public function getLocaleCreatedAtAttribute(): string
    {
        return (new Date($this->created_at))->format('j F Y H:i:s');
    }

    /**
     * @return string
     */
    public function getLocaleUpdatedAtAttribute(): string
    {
        return (new Date($this->updated_at))->format('j F Y H:i:s');
    }

    /**
     * @return string
     */
    public function getStatusTextAttribute(): string
    {
        return $this->statuses[$this->status];
    }

    /**
     * @return string
     */
    public function getStatusClassAttribute(): string
    {
        return $this->classes[$this->status];
    }

    /**
     * @return bool
     */
    public function getIsConfirmedAttribute(): bool
    {
        return $this->status === self::STATUS_CONFIRMED;
    }

    /**
     * @return bool
     */
    public function getIsNotConfirmedAttribute(): bool
    {
        return !$this->is_confirmed;
    }
}
