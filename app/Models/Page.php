<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Page
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $description
 * @property string $keywords
 */
class Page extends Model
{
    protected $table = 'page';

    protected $fillable = [
        'title',
        'content',
        'description',
        'keywords',
        'url',
    ];
}
