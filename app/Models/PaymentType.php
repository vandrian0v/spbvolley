<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 */
class PaymentType extends Model
{
    protected $table = 'payment_type';
}
