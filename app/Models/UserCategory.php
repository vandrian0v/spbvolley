<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @mixin \Eloquent
 */
class UserCategory extends Model
{
    const PRO = 1;
    const A = 2;
    const B = 3;
    const C = 4;

    protected $table = 'user_category';
}
