<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Jenssegers\Date\Date;
use Storage;

/**
 * App\Models\Post
 *
 * @property-read mixed $locale_date
 * @method static Builder|Post search($title)
 * @method static Builder|Post slider()
 * @method static Builder|Post sort($sortName)
 * @method static Builder|Post ofCategory(int $categoryId)
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $anons
 * @property bool $slider
 * @property int $category_id
 * @property int $camp_id
 * @property int $location_id
 * @property int $nwtour_id
 * @property PostCategory $category
 */
class Post extends Model
{
    use Scopes\Post;

    protected $table = 'post';

    protected $casts = [
        'slider' => 'boolean',
        'slider_title' => 'boolean',
    ];

    protected $fillable = [
        'title',
        'content',
        'anons',
        'url',
        'slider',
        'category_id',
        'camp_id',
        'location_id',
        'nwtour_id',
    ];

    /**
     * @return string
     */
    public function getLocaleDateAttribute(): string
    {
        return (new Date($this->created_at))->format('j F Y');
    }

    /**
     * @return string
     */
    public function imageUrl(): string
    {
        $filename = "post/{$this->id}/image";

        return $this->hasImage()
            ? asset("storage/$filename?" . Storage::lastModified($filename))
            : '/image/default-thumb.jpg';
    }

    /**
     * @return bool
     */
    public function hasImage(): bool
    {
        return Storage::exists("post/{$this->id}/image");
    }

    /**
     * @param Builder $query
     * @return Builder|self
     */
    public function scopeSlider(Builder $query)
    {
        return $query->where('slider', true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder
     */
    public function category()
    {
        return $this->belongsTo(PostCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function camp()
    {
        return $this->belongsTo(Camp::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
