<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingLevel extends Model
{
    protected $table = 'training_level';

    public $timestamps = false;

    protected $fillable = [
        'title',
    ];
}
