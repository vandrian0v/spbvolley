<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CampReview
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $author
 * @property string $review
 * @property string $description
 */
class CampReview extends Model
{
    protected $table = 'camp_review';

    protected $fillable = [
        'author',
        'review',
        'description',
    ];

    public $timestamps = false;
}
