<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackMail extends Mailable implements ShouldQueue
{
    public $username;
    public $text;

    /**
     * Create a new message instance.
     *
     * @param string $username
     * @param string $text
     */
    public function __construct(string $username, string $text)
    {
        $this->username = $username;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новое сообщение в обратной связи')
            ->view('emails.feedback');
    }
}
