<?php

namespace App\Mail;

use App\Models\EventApplication;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventApplicationReceived extends Mailable implements ShouldQueue
{
    public $application;

    /**
     * Create a new message instance.
     *
     * @param EventApplication $application
     */
    public function __construct(EventApplication $application)
    {
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новая заявка на проведение мероприятия')
            ->view('emails.event_application');
    }
}
