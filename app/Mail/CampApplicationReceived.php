<?php

namespace App\Mail;

use App\Models\CampApplication;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampApplicationReceived extends Mailable implements ShouldQueue
{
    public $application;

    /**
     * Create a new message instance.
     *
     * @param CampApplication $application
     */
    public function __construct(CampApplication $application)
    {
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новая заявка в лагерь')
            ->view('emails.camp_application');
    }
}
