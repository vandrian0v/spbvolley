<?php

namespace App\Providers;

use App\Models\Page;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\Models\Post;
use App\Models\TrainingGroup;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('post_by_url', function ($url) {
            return Post::where('url', $url)->firstOrFail();
        });

        Route::bind('group_by_url', function ($url) {
            return TrainingGroup::where('url', $url)->firstOrFail();
        });

        Route::bind('page_by_url', function ($url) {
            return Page::where('url', $url)->firstOrFail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();
    }

    protected function mapWebRoutes()
    {
        Route::prefix('admin')
            ->middleware(['web', 'admin'])
            ->namespace($this->namespace . '\Admin')
            ->as('admin.')
            ->group(base_path('routes/web/admin.php'));

        Route::middleware('web')
            ->namespace($this->namespace . '\Front')
            ->group(base_path('routes/web/front.php'));
    }

    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->as('api.')
            ->group(base_path('routes/api.php'));

        Route::prefix('api/front')
            ->middleware('api')
            ->namespace($this->namespace . '\Api\Front')
            ->as('api.front.')
            ->group(base_path('routes/api/front.php'));

        Route::prefix('api/admin')
            ->middleware(['api', 'admin'])
            ->namespace($this->namespace . '\Api\Admin')
            ->as('api.admin.')
            ->group(base_path('routes/api/admin.php'));
    }
}
