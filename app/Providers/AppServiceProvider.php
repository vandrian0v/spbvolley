<?php

namespace App\Providers;

use App\Models\Camp;
use App\Models\Coach;
use App\Models\Event;
use App\Models\Location;
use App\Models\Team;
use App\Models\Photos\CampPhoto;
use App\Models\Photos\CoachPhoto;
use App\Models\Photos\EventPhoto;
use App\Models\Photos\LocationPhoto;
use App\Models\TrainingGroup;
use App\Models\Tournament;
use App\Observers\PhotoObserver;
use App\Observers\PhotoOwnerObserver;
use App\Observers\TrainingGroupObserver;
use App\Observers\TournamentObserver;
use App\Observers\TeamObserver;
use Illuminate\Support\ServiceProvider;
use Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Удаление фотографий
        Camp::observe(PhotoOwnerObserver::class);
        Coach::observe(PhotoOwnerObserver::class);
        Event::observe(PhotoOwnerObserver::class);
        Location::observe(PhotoOwnerObserver::class);

        // Удаление файлов изображений
        CampPhoto::observe(PhotoObserver::class);
        CoachPhoto::observe(PhotoObserver::class);
        EventPhoto::observe(PhotoObserver::class);
        LocationPhoto::observe(PhotoObserver::class);

        // Проставление ордера
        TrainingGroup::observe(TrainingGroupObserver::class);

        // Логирование изменения взноса
        Tournament::observe(TournamentObserver::class);

        // Проставление рейтинга
        Team::observe(TeamObserver::class);

        Route::pattern('id', '\d+');
    }
}
