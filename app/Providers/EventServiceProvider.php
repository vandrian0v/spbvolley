<?php

namespace App\Providers;

use App\Events\PaymentConfirmed;
use App\Events\TournamentApplicationReceived;
use App\Events\UserRegistered;
use App\Listeners\SendPaymentNotifications;
use App\Listeners\SendRegistrationNotification;
use App\Listeners\SendTournamentApplicationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserRegistered::class => [
            SendRegistrationNotification::class,
        ],
        TournamentApplicationReceived::class => [
            SendTournamentApplicationNotification::class,
        ],
        PaymentConfirmed::class => [
            SendPaymentNotifications::class,
        ],
    ];
}
