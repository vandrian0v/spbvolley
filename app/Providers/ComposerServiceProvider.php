<?php

namespace App\Providers;

use App\Models\CampApplication;
use App\Models\EventApplication;
use App\Models\TrainingGroup;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    private $eventApplicationCount;
    private $campApplicationCount;
    private $activated = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.layouts.partials.menu', function ($view) {
            if (!$this->activated) {
                $this->eventApplicationCount = EventApplication::new()->count();
                $this->campApplicationCount  = CampApplication::new()->count();
                $this->activated = true;
            }

            return $view->with('eventApplicationCount', $this->eventApplicationCount)
                ->with('campApplicationCount', $this->campApplicationCount);
        });

        view()->composer('trainings.partials.modal', function ($view) {
            $groups = TrainingGroup::all();
            $view->with(compact('groups'));
        });

        view()->composer('front.trainings.apply', function ($view) {
            $groups = TrainingGroup::all();
            $view->with(compact('groups'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
