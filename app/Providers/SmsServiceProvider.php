<?php

namespace App\Providers;

use App\Contracts\Notifications\SmsSender;
use App\Services\Notifications\FakeSmsSender;
use App\Services\Notifications\SmsProfiSender;
use Illuminate\Support\ServiceProvider;
use App;

class SmsServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(SmsSender::class, function ($app) {
            if (App::environment('production')) {
                return new SmsProfiSender(config('sms_profi.login'), config('sms_profi.password'));
            } else {
                return new FakeSmsSender;
            }
        });
    }

    public function provides()
    {
        return [SmsSender::class];
    }
}