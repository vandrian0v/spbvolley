<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Notifications\Registration as RegistrationNotification;

class SendRegistrationNotification
{
    public function handle(UserRegistered $event)
    {
        // Оповещение пользователя
        $event->user->notify(new RegistrationNotification);
    }
}
