<?php

namespace App\Listeners;

use App\Events\PaymentConfirmed;
use App\Notifications\PaymentIsMade;
use App\Notifications\PaymentRecieved;
use App\Models\User;

class SendPaymentNotifications
{
    public function handle(PaymentConfirmed $event)
    {
        // Оповещение пользователя
        $event->payment->user->notify(new PaymentIsMade($event->payment));

        // Даниле на почту
        User::find(60)->notify(new PaymentRecieved($event->payment));

        // На клубную почту (rio.bvclub@gmail.com)
        User::find(1609)->notify(new PaymentRecieved($event->payment));
    }
}
