<?php

namespace App\Listeners;

use App\Events\TournamentApplicationReceived;
use App\Notifications\TournamentApplication as TournamentApplicationNotification;
use Notification;

class SendTournamentApplicationNotification
{
    public function handle(TournamentApplicationReceived $event)
    {
        // Оповещение участников
        Notification::send($event->team->users, new TournamentApplicationNotification($event->team->tournament->nwtour));
    }
}
