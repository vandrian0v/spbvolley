var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir(function(mix) {
    mix.stylus('admin/admin.styl', './public/css/admin/app.css');

    mix.styles('camps.css')
       .styles('events.css')
       .styles('style.css')
       .styles('front.css');

    mix.webpack('admin.js', './public/js/admin/app.js')
       .webpack('training_edit.js', './public/js/admin/training_edit.js')
       .webpack('photos.js', './public/js/admin/photos.js')
       .webpack('echo.js', './public/js/admin/echo.js')
       .webpack('charts.js', './public/js/admin/charts.js')
       .webpack('main.js')
       .webpack('training_application.js')
       .webpack('events.js')
       .webpack('camps.js')
       .webpack('rating.js')
       .webpack('tournament_application.js')
       .webpack('training_calculator.js')
       .webpack('training_index.js')
       .webpack('camps_show.js');

    mix.version(['js/**/*.js', 'css/**/*.css']);
});