<?php

return [
    'login' => env('SMS_PROFI_LOGIN'),
    'password' => env('SMS_PROFI_PASSWORD'),
];