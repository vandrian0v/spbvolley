<?php

return [
    'url'           => 'https://money.yandex.ru/eshop.xml',
    'scid'          => env('YANDEX_SCID'),
    'shop_id'       => env('YANDEX_SHOP_ID'),
    'shop_password' => env('YANDEX_SHOP_PASSWORD'),
];