<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('camp', function(Blueprint $table) {
            $table->dropColumn('photo');
            $table->boolean('future')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('camp', function(Blueprint $table) {
            $table->string('photo', 80);
            $table->dropColumn('future');
        });
    }
}
