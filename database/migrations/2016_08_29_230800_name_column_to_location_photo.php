<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NameColumnToLocationPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('location_photo', function (Blueprint $table) {
            $table->dropColumn('ext');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location_photo', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('ext', 3)->nullable();
        });
    }
}
