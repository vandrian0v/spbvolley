<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTableAddNoteAndTrainingLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_level', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
        });

        \App\Models\TrainingLevel::create(['title' => 'Начальный']);
        \App\Models\TrainingLevel::create(['title' => 'Начальный+']);
        \App\Models\TrainingLevel::create(['title' => 'Средний']);
        \App\Models\TrainingLevel::create(['title' => 'Продвинутый']);
        \App\Models\TrainingLevel::create(['title' => 'Про']);

        Schema::table('user', function (Blueprint $table) {
            $table->text('note')->nullable();
            $table->integer('training_level_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn('note');
            $table->dropColumn('training_level_id');
        });

        Schema::dropIfExists('training_level');
    }
}
