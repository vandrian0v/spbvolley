<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHandlerClassField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription_type', function (Blueprint $table) {
            $table->string('handler_class');
            $table->dropColumn('trainings_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_type', function (Blueprint $table) {
            $table->dropColumn('handler_class');
            $table->integer('trainings_count');
        });
    }
}
