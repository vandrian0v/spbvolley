<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixCampPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('camp_photo', function (Blueprint $table) {
            $table->string('name');
            $table->dropColumn('is_main');
            $table->dropColumn('ext');
        });

        Schema::table('coach_photo', function (Blueprint $table) {
            $table->string('name');
            $table->dropColumn('is_main');
            $table->dropColumn('ext');
        });

        Schema::table('event_photo', function (Blueprint $table) {
            $table->string('name');
            $table->dropColumn('is_main');
            $table->dropColumn('ext');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('camp_photo', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->boolean('is_main');
            $table->string('ext', 3);
        });

        Schema::table('coach_photo', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->boolean('is_main');
            $table->string('ext', 3);
        });

        Schema::table('event_photo', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->boolean('is_main');
            $table->string('ext', 3);
        });
    }
}
