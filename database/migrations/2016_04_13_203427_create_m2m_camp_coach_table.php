<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM2mCampCoachTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m2m_camp_coach', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('camp_id')->unsigned();
            $table->integer('coach_id')->unsigned();

            $table->foreign('camp_id')
                ->references('id')->on('camp')
                ->onDelete('cascade');

            $table->foreign('coach_id')
                ->references('id')->on('coach')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m2m_camp_coach');
    }
}
