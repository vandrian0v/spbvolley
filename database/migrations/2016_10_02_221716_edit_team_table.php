<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team', function(Blueprint $table) {
            $table->integer('payment_id')->nullable()->unsigned();
            $table->foreign('payment_id')
                ->references('id')
                ->on('payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team', function(Blueprint $table) {
            $table->dropForeign('team_payment_id_foreign');
            $table->dropColumn('payment_id');
        });
    }
}
