<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\TrainingGroup;

class AddSortingFieldToTrainingGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_group', function (Blueprint $table) {
            $table->unsignedInteger('order')->nullable();
        });

        $groups = TrainingGroup::all();
        $order = 1;

        foreach ($groups as $group) {
            $group->order = $order++;
            $group->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_group', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
