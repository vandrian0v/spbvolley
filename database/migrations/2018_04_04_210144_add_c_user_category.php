<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCUserCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $categories = [
            ['id' => 4, 'name' => 'C'],
        ];

        foreach ($categories as $item) {
            DB::table('user_category')->updateOrInsert(['id' => $item['id']], $item);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('user_category')->where('id', 4)->delete();
    }
}
