<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('location', function(Blueprint $table) {
            $table->text('description')->nullable();
            $table->enum('season', ['summer', 'winter']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location', function(Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('season');
        });
    }
}
