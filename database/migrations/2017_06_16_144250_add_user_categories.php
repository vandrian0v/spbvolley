<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\UserCategory;

class AddUserCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        $categories = [
            ['id' => 1, 'name' => 'PRO'],
            ['id' => 2, 'name' => 'A'],
            ['id' => 3, 'name' => 'B'],
        ];

        foreach ($categories as $item) {
            DB::table('user_category')->updateOrInsert(['id' => $item['id']], $item);
        }

        Schema::table('user', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->default(UserCategory::B);

            $table->foreign('category_id')->references('id')->on('user_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
            $table->dropColumn('category_id');
        });
        Schema::dropIfExists('user_category');
    }
}
