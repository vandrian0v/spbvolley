<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTrainingTableAddColumnTrainingId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_training', function (Blueprint $table) {
            $table->unsignedInteger('training_id')->nullable();

            $table->foreign('training_id')
                ->references('id')
                ->on('training')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_training', function (Blueprint $table) {
            $table->dropForeign('user_training_training_id_foreign');
            $table->dropColumn('training_id');
        });
    }
}
