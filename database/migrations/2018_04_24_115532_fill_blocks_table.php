<?php

use App\Models\Block;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Block::create([
            'title' => 'Тренировки для взрослых',
            'text' => 'Группы 4 уровня игры, персональные тренировки',
            'url' => 'http://spbvolley.ru/training',
            'order' => 1
        ]);

        Block::create([
            'title' => 'Лагерь пляжного волейбола в Греции',
            'text' => '2-12 мая 2018',
            'url' => 'http://spbvolley.ru/content/greece',
            'order' => 2
        ]);

        Block::create([
            'title' => 'Тренировки для детей',
            'text' => 'Возраст 6-9 и 10-14 лет',
            'url' => 'http://spbvolley.ru/training',
            'order' => 3
        ]);

        Block::create([
            'title' => 'Турниры серии NW-Tour',
            'text' => 'Регулярные турниры для профи и любителей разного уровня',
            'url' => 'http://spbvolley.ru/nwtour',
            'order' => 4
        ]);

        Block::create([
            'title' => 'Корпоративные турниры',
            'text' => 'Спорт - лучший тимбилдинг. Волейбол + развлекательная программа',
            'url' => 'http://spbvolley.ru/events',
            'order' => 5
        ]);

        Block::create([
            'title' => 'Экспресс-курс для начинающих',
            'text' => 'Интенсивное обучение пляжному волейболу с нуля',
            'url' => 'http://spbvolley.ru/content/ex_kurs3',
            'order' => 6
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Block::query()->delete();
    }
}
