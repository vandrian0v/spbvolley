<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTrainingTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_training', function (Blueprint $table) {
            $table->unsignedInteger('coach_id')->nullable();
            $table->unsignedInteger('location_id')->nullable();
            $table->string('start_time', 5)->nullable();
            $table->string('end_time', 5)->nullable();

            $table->foreign('location_id')
                ->references('id')->on('location')
                ->onDelete('cascade');

            $table->foreign('coach_id')
                ->references('id')->on('coach')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('user_training', function (Blueprint $table) {
            $table->dropForeign(['coach_id']);
            $table->dropForeign(['location_id']);
            $table->dropColumn(['coach_id', 'location_id', 'start_time', 'end_time']);
        });
    }
}
