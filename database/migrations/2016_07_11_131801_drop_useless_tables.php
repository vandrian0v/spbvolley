<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUselessTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('tournament_result');
        Schema::drop('tournament_stage');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('tournament_stage', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('order');
        });

        Schema::create('tournament_result', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id')->unsigned();
            $table->integer('first_team_id')->unsigned();
            $table->integer('winner_team_id')->unsigned()->nullable();
            $table->integer('second_team_id')->unsigned();
            $table->integer('stage_id')->unsigned();
            $table->string('score');

            $table->foreign('first_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('second_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('winner_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('tournament_id')
                ->references('id')->on('tournament')
                ->onDelete('cascade');

            $table->foreign('stage_id')
                ->references('id')->on('tournament_stage')
                ->onDelete('cascade');
        });
    }
}
