<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nwtour', function ($table) {
            $table->softDeletes();
        });

        Schema::table('tournament', function ($table) {
            $table->softDeletes();
        });

        Schema::table('team', function ($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nwtour', function ($table) {
            $table->dropSoftDeletes();
        });

        Schema::table('tournament', function ($table) {
            $table->dropSoftDeletes();
        });

        Schema::table('team', function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
