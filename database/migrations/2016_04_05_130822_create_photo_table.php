<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->boolean('is_main')->default(false);
            $table->string('ext', 3)->nullable();
        });

        Schema::table('photo', function(Blueprint $table) {
            $table->foreign('event_id')
                ->references('id')->on('event')
                ->onDelete('cascade');
        });

        Schema::table('event', function ($table) {
            $table->dropColumn('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photo');

        Schema::table('event', function ($table) {
            $table->text('image')->nullable();
        });
    }
}
