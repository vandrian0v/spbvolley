<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_photo', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('coach_id')->unsigned();
            $table->boolean('is_main')->default(false);
            $table->string('ext', 3)->nullable();
        });

        Schema::table('coach_photo', function(Blueprint $table) {
            $table->foreign('coach_id')
                ->references('id')->on('coach')
                ->onDelete('cascade');
        });

        Schema::create('camp_photo', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('camp_id')->unsigned();
            $table->boolean('is_main')->default(false);
            $table->string('ext', 3)->nullable();
        });

        Schema::table('camp_photo', function(Blueprint $table) {
            $table->foreign('camp_id')
                ->references('id')->on('camp')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coach_photo');
        Schema::drop('camp_photo');
    }
}
