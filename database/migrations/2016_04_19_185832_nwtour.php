<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nwtour extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nwtour', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->enum('status', ['created', 'anounsed', 'opened', 'closed', 'finished'])
                ->default('created');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('description')->nullable();
        });

        Schema::create('tournament_category', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('gender', ['male', 'female', 'mix', 'other']);
        });

        Schema::create('tournament', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('nwtour_id')->unsigned();

            $table->unique(['category_id', 'nwtour_id']);

            $table->foreign('category_id')
                ->references('id')->on('tournament_category')
                ->onDelete('cascade');

            $table->foreign('nwtour_id')
                ->references('id')->on('nwtour')
                ->onDelete('cascade');
        });

        Schema::create('team', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id')->unsigned();
            $table->integer('rank')->unsigned();

            $table->foreign('tournament_id')
                ->references('id')->on('tournament')
                ->onDelete('cascade');
        });

        Schema::create('tournament_stage', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('order');
        });

        Schema::create('tournament_result', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id')->unsigned();
            $table->integer('first_team_id')->unsigned();
            $table->integer('winner_team_id')->unsigned()->nullable();
            $table->integer('second_team_id')->unsigned();
            $table->integer('stage_id')->unsigned();
            $table->string('score');

            $table->foreign('first_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('second_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('winner_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('tournament_id')
                ->references('id')->on('tournament')
                ->onDelete('cascade');

            $table->foreign('stage_id')
                ->references('id')->on('tournament_stage')
                ->onDelete('cascade');
        });

        Schema::create('m2m_user_team', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('team_id')->unsigned();

            $table->unique(['team_id', 'user_id']);

            $table->foreign('team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m2m_user_team');
        Schema::drop('tournament_result');
        Schema::drop('tournament_stage');
        Schema::drop('team');
        Schema::drop('tournament');
        Schema::drop('tournament_category');
        Schema::drop('nwtour');
    }
}
