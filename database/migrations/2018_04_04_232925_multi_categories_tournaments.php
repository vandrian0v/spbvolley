<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultiCategoriesTournaments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tournament_category')->insert([
            'id'     => 15,
            'name'   => 'C',
            'gender' => 'male',
            'default_contribution'  => 1800,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 16,
            'name'   => 'C',
            'gender' => 'female',
            'default_contribution'  => 1200,
            'team_size' => 2,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
