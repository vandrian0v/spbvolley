<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(0);
            $table->bigInteger('invoice_id')->nullable();
            $table->string('comment');

            $table->float('sum');
            $table->float('order_sum_amount')->nullable();
            $table->float('shop_sum_amount')->nullable();

            $table->string('order_currency', 10)->nullabel();
            $table->string('shop_currency', 10)->nullabel();

            $table->integer('user_id')->unsigned(); //customerNumber
            $table->foreign('user_id')
                ->references('id')
                ->on('user');

            $table->integer('payment_type_id')->unsigned();
            $table->foreign('payment_type_id')
                ->references('id')
                ->on('payment_type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}
