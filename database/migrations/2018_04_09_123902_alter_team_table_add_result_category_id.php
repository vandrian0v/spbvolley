<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Team;

class AlterTeamTableAddResultCategoryId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable();

            $table->foreign('category_id')->references('id')->on('tournament_category');
        });

        Team::whereNotNull('rank')
            ->with('tournament')
            ->orderBy('id')
            ->chunk(100, function ($teams) {
                foreach ($teams as $team) {
                    if ($team->rank) {
                        $team->category_id = $team->tournament->category_id;
                        $team->save();
                    }
                }
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
}
