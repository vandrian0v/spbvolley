<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlaceToLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event', function (Blueprint $table) {
            $table->dropForeign(['place_id']);
        });

        Schema::rename('place', 'location');

        Schema::table('event', function (Blueprint $table) {
            $table->renameColumn('place_id', 'location_id');

            $table->foreign('location_id')
                ->references('id')->on('location')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event', function (Blueprint $table) {
            $table->dropForeign(['location_id']);
        });

        Schema::rename('location', 'place');

        Schema::table('event', function (Blueprint $table) {
            $table->renameColumn('location_id', 'place_id');

            $table->foreign('place_id')
                ->references('id')->on('place')
                ->onDelete('cascade');
        });
    }
}
