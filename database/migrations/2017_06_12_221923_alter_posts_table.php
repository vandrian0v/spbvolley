<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post', function(Blueprint $table) {
            $table->dropColumn('slider_title');
            $table->unsignedInteger('camp_id')->nullable();
            $table->unsignedInteger('location_id')->nullable();
            $table->unsignedInteger('nwtour_id')->nullable();

            $table->foreign('camp_id')->references('id')->on('camp');
            $table->foreign('location_id')->references('id')->on('location');
            $table->foreign('nwtour_id')->references('id')->on('nwtour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post', function(Blueprint $table) {
            $table->boolean('slider_title')->default(false);

            $table->dropForeign(['camp_id']);
            $table->dropForeign(['location_id']);
            $table->dropForeign(['nwtour_id']);

            $table->dropColumn('camp_id');
            $table->dropColumn('location_id');
            $table->dropColumn('nwtour_id');
        });
    }
}
