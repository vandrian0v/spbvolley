<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('training_schedule_id')->unsigned();
            $table->integer('coach_id')->unsigned();
            $table->time('start_time');
            $table->time('end_time');
            $table->tinyInteger('weekday');

            $table->foreign('training_schedule_id')
                ->references('id')->on('training_schedule')
                ->onDelete('cascade');

            $table->foreign('coach_id')
                ->references('id')->on('coach')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('training');
    }
}
