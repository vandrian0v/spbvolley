<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('longitude', 8, 6)->nullable();
            $table->float('latitude', 8, 6)->nullable();
        });

        Schema::create('package', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->float('duration');
            $table->integer('price');
        });

        Schema::create('service', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('event', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->text('participants');
            $table->date('date');
            $table->text('image')->nullable();
            $table->integer('place_id')->unsigned();
        });

        Schema::table('event', function(Blueprint $table) {
            $table->foreign('place_id')
                ->references('id')->on('place')
                ->onDelete('cascade');
        });

        Schema::create('m2m_event_service', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->integer('service_id')->unsigned();
        });

        Schema::table('m2m_event_service', function(Blueprint $table) {
            $table->foreign('event_id')
                ->references('id')->on('event')
                ->onDelete('cascade');

            $table->foreign('service_id')
                ->references('id')->on('service')
                ->onDelete('cascade');
        });

        Schema::create('m2m_package_service', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsigned();
            $table->integer('package_id')->unsigned();
        });

        Schema::table('m2m_package_service', function(Blueprint $table) {
            $table->foreign('package_id')
                ->references('id')->on('package')
                ->onDelete('cascade');

            $table->foreign('service_id')
                ->references('id')->on('service')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m2m_package_service');
        Schema::drop('m2m_event_service');
        Schema::drop('event');
        Schema::drop('package');
        Schema::drop('service');
        Schema::drop('place');
    }
}
