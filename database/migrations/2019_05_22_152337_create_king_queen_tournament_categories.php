<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKingQueenTournamentCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tournament_category')->insert([
            'id'     => 25,
            'name'   => 'Король/Королева пляжа B',
            'gender' => 'mix',
            'default_contribution'  => 500,
            'team_size' => 1,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 26,
            'name'   => 'Король/Королева пляжа C',
            'gender' => 'mix',
            'default_contribution'  => 500,
            'team_size' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
