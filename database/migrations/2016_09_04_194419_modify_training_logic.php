<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\SubscriptionType;
use App\Models\Training;
use App\Models\TrainingSchedule;

class ModifyTrainingLogic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Создаем таблицу типов абонементов
        Schema::create('subscription_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('trainings_count');
        });

        // Создаем абонементы
        SubscriptionType::create(['name' => 'Разовое посещение', 'trainings_count' => 1]);
        SubscriptionType::create(['name' => 'Абонемент на месяц', 'trainings_count' => 4]);
        SubscriptionType::create(['name' => 'Абонемент на три месяца', 'trainings_count' => 12]);

        // m2m таблица с pivot полем price
        Schema::create('training_price', function (Blueprint $table) {
            $table->primary(['training_id', 'subscription_type_id'], 'm2m_subscription_type_training_primary');

            $table->integer('training_id')->unsigned();
            $table->integer('subscription_type_id')->unsigned();
            $table->integer('price');

            $table->foreign('training_id')
                ->references('id')->on('training')
                ->onDelete('cascade');

            $table->foreign('subscription_type_id')
                ->references('id')->on('subscription_type')
                ->onDelete('cascade');
        });

        // Заполняем m2m таблицу на основе имеющихся данных
        foreach (Training::all() as $training) {
            $training->subscriptionTypes()->attach([
                1 => ['price' => $training->price_once],
                2 => ['price' => $training->price_month],
                3 => ['price' => $training->price_quarter],
            ]);
        }

        // Добавляем поля из schedule, удаляем цены
        Schema::table('training', function (Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('coach_id');
            $table->integer('training_group_id')->unsigned()->after('coach_id');

            $table->dropColumn('price_once');
            $table->dropColumn('price_month');
            $table->dropColumn('price_quarter');
        });

        foreach (TrainingSchedule::all() as $schedule) {
            foreach ($schedule->trainings as $training) {
                $training->update([
                    'location_id'       => $schedule->location_id,
                    'training_group_id' => $schedule->training_group_id,
                ]);
            }
        }

        Schema::table('training', function (Blueprint $table) {
            $table->dropForeign('training_training_schedule_id_foreign');
            $table->dropColumn('training_schedule_id');

            $table->foreign('location_id')
                ->references('id')->on('location')
                ->onDelete('cascade');

            $table->foreign('training_group_id')
                ->references('id')->on('training_group')
                ->onDelete('cascade');
        });

        // Дропаем schedule
        Schema::drop('training_schedule');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('training_price');
        Schema::drop('subscription_type');

        Schema::table('training', function (Blueprint $table) {
            $table->dropColumn('location_id');
            $table->dropColumn('training_group_id');

            $table->integer('price_once');
            $table->integer('price_month');
            $table->integer('price_quarter');
        });

        Schema::create('training_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->integer('training_group_id')->unsigned();

            $table->foreign('location_id')
                ->references('id')->on('location')
                ->onDelete('cascade');

            $table->foreign('training_group_id')
                ->references('id')->on('training_group')
                ->onDelete('cascade');
        });
    }
}
