<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\PostCategory;

class AddPostCategories extends Migration
{
    /**
     * Run the migrations.
     * @return void
     * @throws Exception
     */
    public function up()
    {
        Schema::create('post_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
        });

        Artisan::call('db:seed', ['--class' => 'PostCategorySeeder', '--force' => true]);

        $defaultCategory = PostCategory::where('url', 'all')->first();

        if (!$defaultCategory) {
            throw new Exception('Cant find default category');
        }

        Schema::table('post', function (Blueprint $table) use ($defaultCategory) {
            $table->integer('category_id')->unsigned()->default($defaultCategory->id);
        });

        Schema::table('post', function(Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')->on('post_category')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post', function(Blueprint $table) {
            $table->dropForeign(['category_id']);
            $table->dropColumn('category_id');
        });

        Schema::drop('post_category');
    }
}
