<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalUserFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function(Blueprint $table) {
            $table->date('birth_date')->nullable()->default(null);
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->string('hobby')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function(Blueprint $table) {
            $table->dropColumn('birth_date');
            $table->dropColumn('height');
            $table->dropColumn('weight');
            $table->dropColumn('hobby');
        });
    }
}
