<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Tournament;

class AddContributionToTournament extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tournament', function (Blueprint $table) {
            $table->integer('contribution');
        });

        $tournaments = Tournament::with('category')->get();

        foreach ($tournaments as $tournament) {
            $tournament->update([
                'contribution' => $tournament->category->default_contribution,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament', function (Blueprint $table) {
            $table->dropColumn('contribution');
        });
    }
}
