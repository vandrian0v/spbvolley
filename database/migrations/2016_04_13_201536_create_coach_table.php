<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->text('career')->nullable();
            $table->text('education')->nullable();
            $table->text('training')->nullable();
            $table->text('teachers')->nullable();
            $table->string('photo', 80)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coach');
    }
}
