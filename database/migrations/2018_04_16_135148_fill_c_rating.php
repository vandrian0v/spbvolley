<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillCRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 40,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 32,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 28,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 25,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 20,
            'start_rank'  => 5,
            'end_rank'    => 6,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 16,
            'start_rank'  => 7,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 12,
            'start_rank'  => 9,
            'end_rank'    => 12,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 10,
            'start_rank'  => 13,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 6,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 4,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 3,
            'start_rank'  => 33,
            'end_rank'    => 48,
        ]);

        DB::table('rating')->insert([
            'category_id' => 15,
            'points'      => 2,
            'start_rank'  => 49,
            'end_rank'    => 64,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 40,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 32,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 28,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 25,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 20,
            'start_rank'  => 5,
            'end_rank'    => 6,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 16,
            'start_rank'  => 7,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 12,
            'start_rank'  => 9,
            'end_rank'    => 12,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 10,
            'start_rank'  => 13,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 6,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 4,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 3,
            'start_rank'  => 33,
            'end_rank'    => 48,
        ]);

        DB::table('rating')->insert([
            'category_id' => 16,
            'points'      => 2,
            'start_rank'  => 49,
            'end_rank'    => 64,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
