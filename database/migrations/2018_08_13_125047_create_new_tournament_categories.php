<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTournamentCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tournament_category')->insert([
            'id'     => 21,
            'name'   => 'Король пляжа A',
            'gender' => 'male',
            'default_contribution'  => 500,
            'team_size' => 1,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 22,
            'name'   => 'Король пляжа C',
            'gender' => 'male',
            'default_contribution'  => 500,
            'team_size' => 1,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 23,
            'name'   => 'Королева пляжа A',
            'gender' => 'female',
            'default_contribution'  => 500,
            'team_size' => 1,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 24,
            'name'   => 'Королева пляжа C',
            'gender' => 'female',
            'default_contribution'  => 500,
            'team_size' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
