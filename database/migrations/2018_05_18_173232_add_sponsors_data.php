<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSponsorsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Sponsor::create(['name' => 'CoffeeShop', 'url' => 'https://vk.com/csc']);
        \App\Models\Sponsor::create(['name' => 'Курортный район', 'url' => 'http://kurort.ru/']);
        \App\Models\Sponsor::create(['name' => 'Литрес', 'url' => 'https://www.litres.ru/']);
        \App\Models\Sponsor::create(['name' => 'СпортКлиника', 'url' => 'http://sportklinika.ru/']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Sponsor::query()->delete();
    }
}
