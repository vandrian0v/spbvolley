<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApplicationTimestamps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('camp_application', function(Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('event_application', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('camp_application', function(Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at']);
        });

        Schema::table('event_application', function(Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at']);
        });
    }
}
