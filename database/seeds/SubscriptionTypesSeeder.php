<?php

use Illuminate\Database\Seeder;
use App\Services\Trainings\SubscriptionHandlers\SingleDayHandler;
use App\Services\Trainings\SubscriptionHandlers\OneMonthHandler;
use App\Services\Trainings\SubscriptionHandlers\ThreeMonthHandler;

class SubscriptionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Разовое посещение',
                'handler_class' => SingleDayHandler::class,
            ],
            [
                'id' => 2,
                'name' => 'Абонемент на месяц',
                'handler_class' => OneMonthHandler::class,
            ],
            [
                'id' => 3,
                'name' => 'Абонемент на три месяца',
                'handler_class' => ThreeMonthHandler::class,
            ],
        ];

        foreach ($data as $item) {
            DB::table('subscription_type')->updateOrInsert(['id' => $item['id']], $item);
        }
    }
}
