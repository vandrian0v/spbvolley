<?php

use Illuminate\Database\Seeder;

class RatingNewKingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createPro();
        $this->createA();
        $this->createB();
        $this->createC();
    }

    public function createPro()
    {
        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 200,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 180,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 160,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 140,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 120,
            'start_rank'  => 5,
            'end_rank'    => 6,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 100,
            'start_rank'  => 7,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 80,
            'start_rank'  => 9,
            'end_rank'    => 12,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 60,
            'start_rank'  => 13,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 40,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 17,
            'points'      => 20,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 200,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 180,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 160,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 140,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 120,
            'start_rank'  => 5,
            'end_rank'    => 6,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 100,
            'start_rank'  => 7,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 80,
            'start_rank'  => 9,
            'end_rank'    => 12,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 60,
            'start_rank'  => 13,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 40,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 18,
            'points'      => 20,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);
    }

    public function createA()
    {
        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 125,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 110,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 90,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 80,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 60,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 40,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 20,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 21,
            'points'      => 10,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 125,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 110,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 90,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 80,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 60,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 40,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 20,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 23,
            'points'      => 10,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);
    }

    public function createB()
    {
        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 80,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 70,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 60,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 50,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 40,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 25,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 5,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 19,
            'points'      => 3,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 80,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 70,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 60,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 50,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 40,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 25,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 5,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 20,
            'points'      => 3,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);
    }

    public function createC()
    {
        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 20,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 16,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 14,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 12,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 10,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 8,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 6,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 22,
            'points'      => 5,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 20,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 16,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 14,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 12,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 10,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 8,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 6,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 24,
            'points'      => 5,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);
    }
}
