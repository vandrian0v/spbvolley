<?php

use Illuminate\Database\Seeder;

class TournamentCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tournament_category')->insert([
            'id'     => 1,
            'name'   => 'PRO',
            'gender' => 'male',
            'price'  => 1500,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 2,
            'name'   => 'PRO',
            'gender' => 'female',
            'price'  => 1500,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 3,
            'name'   => 'A',
            'gender' => 'male',
            'price'  => 1200,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 4,
            'name'   => 'A',
            'gender' => 'female',
            'price'  => 1200,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 5,
            'name'   => 'B',
            'gender' => 'male',
            'price'  => 1200,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 6,
            'name'   => 'B',
            'gender' => 'female',
            'price'  => 1200,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 7,
            'name'   => 'Микст',
            'gender' => 'mix',
            'price'  => 1200,
            'team_size' => 2,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 8,
            'name'   => 'Король пляжа',
            'gender' => 'male',
            'price'  => 0,
            'team_size' => 1,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 9,
            'name'   => 'Новички',
            'gender' => 'mix',
            'price'  => 0,
            'team_size' => 4,
        ]);

        DB::table('tournament_category')->insert([
            'id'     => 10,
            'name'   => 'Королева пляжа',
            'gender' => 'female',
            'price'  => 0,
            'team_size' => 1,
        ]);
    }
}
