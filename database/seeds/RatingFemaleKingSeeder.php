<?php

use Illuminate\Database\Seeder;

class RatingFemaleKingSeeder extends Seeder
{
    public function run()
    {
        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 200,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 180,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 160,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 140,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 120,
            'start_rank'  => 5,
            'end_rank'    => 6,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 100,
            'start_rank'  => 7,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 80,
            'start_rank'  => 9,
            'end_rank'    => 12,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 60,
            'start_rank'  => 13,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 40,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 10,
            'points'      => 20,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);
    }
}