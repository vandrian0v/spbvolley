<?php

use Illuminate\Database\Seeder;

class RatingProTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 400,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 360,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 320,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 280,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 240,
            'start_rank'  => 5,
            'end_rank'    => 6,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 200,
            'start_rank'  => 7,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 160,
            'start_rank'  => 9,
            'end_rank'    => 12,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 120,
            'start_rank'  => 13,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 80,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 1,
            'points'      => 40,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

                DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 400,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 360,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 320,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 280,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 240,
            'start_rank'  => 5,
            'end_rank'    => 6,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 200,
            'start_rank'  => 7,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 160,
            'start_rank'  => 9,
            'end_rank'    => 12,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 120,
            'start_rank'  => 13,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 80,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 2,
            'points'      => 40,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);
    }
}
