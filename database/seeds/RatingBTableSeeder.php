<?php

use Illuminate\Database\Seeder;

class RatingBTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 160,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 140,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 120,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 100,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 80,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 50,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 10,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 5,
            'points'      => 6,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 160,
            'start_rank'  => 1,
            'end_rank'    => 1,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 140,
            'start_rank'  => 2,
            'end_rank'    => 2,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 120,
            'start_rank'  => 3,
            'end_rank'    => 3,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 100,
            'start_rank'  => 4,
            'end_rank'    => 4,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 80,
            'start_rank'  => 5,
            'end_rank'    => 8,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 50,
            'start_rank'  => 9,
            'end_rank'    => 16,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 10,
            'start_rank'  => 17,
            'end_rank'    => 24,
        ]);

        DB::table('rating')->insert([
            'category_id' => 6,
            'points'      => 6,
            'start_rank'  => 25,
            'end_rank'    => 32,
        ]);
    }
}
