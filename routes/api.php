<?php

Route::middleware('auth')->prefix('trainings')->as('trainings.')->group(function() {
    Route::get('group/{group}',       'ApiController@trainingGroup')->name('group.show');
    Route::get('subscription-types',  'ApiController@trainingSubscriptionTypes')->name('subscription-types');
    Route::get('{training}/calendar', 'ApiController@trainingCalendar')->name('calendar');
});

Route::namespace('Admin')->middleware('admin')->group(function() {
    Route::get('trainings', 'ApiController@trainings')->name('trainings.list');
    Route::get('coaches', 'ApiController@coaches')->name('coaches');
    Route::delete('trainings/{training}', 'ApiController@deleteTraining')->name('trainings.destroy');
    Route::prefix('chart')->group(function() {
        Route::get('payments', 'ChartController@payments')->name('charts.payments');
        Route::get('users',   'ChartController@users')->name('charts.users');
    });
});

Route::get('rating/{type}/{categoryId}', 'ApiController@rating')->name('rating')->where('type', 'male|female');
Route::get('nwtour/{tour}/categories', 'ApiController@categories')->name('nwtour.categories');
Route::get('nwtour/{tour}/tournaments', 'ApiController@tournaments')->name('nwtour.tournaments');

Route::post('nwtour/application',   'ApiController@tournamentApplication')->name('tournament-application.store');
Route::post('camps/application',    'ApiController@campApplication')->name('camp-application.store');
Route::post('events/application',   'ApiController@eventApplication')->name('event-application.store');
Route::post('training/application', 'ApiController@trainingApplication')->name('training-application.store');
