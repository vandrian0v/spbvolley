<?php

Route::prefix('lib')->group(function () {
    Route::get('tournament-categories', 'LibController@tournamentCategories');
    Route::get('tour-statuses', 'LibController@tourStatuses');
    Route::get('ratings', 'LibController@ratings');
    Route::get('training-groups', 'LibController@trainingGroups');
});

Route::prefix('trainings')->group(function () {
    Route::get('group/{group}', 'TrainingController@group');
});

Route::prefix('tours')->group(function () {
    Route::get('', 'TourController@index');
    Route::get('{tour}', 'TourController@show');
    Route::post('', 'TourController@store');
    Route::put('{tour}', 'TourController@update');
    Route::delete('{tour}', 'TourController@destroy');
});

Route::prefix('teams')->group(function () {
    Route::put('{team}', 'TeamController@update');
    Route::put('{team}/users', 'TeamController@updateUsers');
    Route::delete('{team}', 'TeamController@destroy');
});

Route::prefix('tournaments')->group(function () {
    Route::put('{tournament}', 'TournamentController@update');
});

Route::prefix('blocks')->group(function () {
    Route::get('', 'BlockController@index');
    Route::post('', 'BlockController@store');
    Route::put('{block}', 'BlockController@update');
    Route::delete('{block}', 'BlockController@delete');
    Route::post('{block}/image', 'BlockController@image');
});

Route::prefix('pages')->group(function () {
    Route::get('', 'PageController@index');
    Route::post('', 'PageController@store');
    Route::put('{page}', 'PageController@update');
    Route::delete('{page}', 'PageController@delete');
});

Route::prefix('sponsors')->group(function () {
    Route::get('', 'SponsorController@index');
    Route::post('', 'SponsorController@store');
    Route::put('{sponsor}', 'SponsorController@update');
    Route::delete('{sponsor}', 'SponsorController@delete');
    Route::post('{sponsor}/image', 'SponsorController@image');
});

Route::post('upload-image', 'UploadController@uploadImage');

Route::prefix('notifications')->group(function () {
    Route::post('email', 'NotificationController@sendEmails');
    Route::post('sms', 'NotificationController@sendSms');
    Route::get('sms/balance', 'NotificationController@smsBalance');
});

Route::prefix('users')->group(function () {
    Route::get('', 'UserController@index');
});