<?php

Route::prefix('tours')->group(function () {
    Route::get('', 'TourController@index');
    Route::get('{tour}', 'TourController@show');
});

Route::prefix('users')->middleware('auth')->group(function () {
    Route::get('', 'UserController@index');
    Route::get('search', 'UserController@search');
});

Route::post('error', 'ErrorController');