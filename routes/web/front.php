<?php

Route::get('', 'HomeController@home')->name('home');
Route::get('oplata', 'HomeController@oplata')->name('oplata');
Route::get('individualnie-zanyatiya-volleybolom', 'HomeController@individual')->name('individual');

Route::prefix('payment')->as('payments.')->group(function () {
    Route::middleware('admin')->get('test', 'PaymentController@testPayment');
    Route::post('check',  'PaymentController@check')->name('check');
    Route::post('aviso',  'PaymentController@aviso')->name('aviso');
    Route::post('cancel', 'PaymentController@cancel')->name('cancel');
    Route::get('{payment}/success', 'PaymentController@success')->name('success');

    Route::middleware('auth')->group(function () {
        Route::post('nwtour',   'OrderController@formTournamentContributionOrder')->name('nwtour');
        Route::post('training', 'OrderController@formTrainingOrder')->name('training');
    });
});

Route::prefix('training')->as('trainings.')->group(function() {
    Route::get('', 'TrainingController@index')->name('index');

    Route::middleware('auth')->group(function () {
        Route::get('{group_by_url}', 'TrainingController@purchase')->name('purchase');
    });
});

Route::get('coach/{coach}', 'HomeController@showCoach')->name('coaches.show');

Route::get('package/{package}', 'EventController@package')->name('packages.show');

Route::prefix('nwtour')->as('nwtour.')->group(function () {
    Route::get('',         'TourController@index')->name('index');
    Route::get('ratings',  'TourController@ratings')->name('ratings');
    Route::get('about',    'TourController@about')->name('about');

    Route::get('{tour}',              'TourController@info')->name('info');
    Route::get('{tour}/participants', 'TourController@participants')->name('participants');
    Route::get('{tour}/results',      'TourController@results')->name('results');

    Route::middleware('auth')->group(function () {
        Route::get('{tour}/application',  'TourController@application')->name('application');
    });
});

Route::redirect('events/', '/corporate_volleyball');
Route::redirect('events/locations', '/corporate_volleyball/locations');
Route::redirect('events/trainings', '/corporate_volleyball/trainings');
Route::get('events/{event}',   function ($event) {
    return redirect()->route('events.show', $event);
});

Route::prefix('corporate_volleyball')->as('events.')->group(function () {
    Route::get('',          'EventController@index')->name('index');
    Route::get('locations', 'EventController@locations')->name('locations');
    Route::get('trainings', 'EventController@trainings')->name('trainings');
    Route::get('{event}',   'EventController@show')->name('show');
});

Route::prefix('camps')->as('camps.')->group(function () {
    Route::get('',       'CampController@index')->name('index');
    Route::get('{camp}', 'CampController@show')->name('show');
});

Route::prefix('content')->as('posts.')->group(function () {
    Route::get('',              'PostController@index')->name('index');
    Route::get('{post_by_url}', 'PostController@show')->name('show');
});

Route::as('auth.')->group(function () {
    Route::middleware('guest')->namespace('Auth')->group(function () {
        Route::get('login',  'LoginController@showLoginForm')->name('login.show');
        Route::post('login', 'LoginController@login')->name('login.persist');

        Route::get('register',  'RegisterController@showRegistrationForm')->name('register.show');
        Route::post('register', 'RegisterController@register')->name('register.persist');

        Route::prefix('password')->as('password.')->group(function () {
            Route::get('reset',         'ForgotPasswordController@showLinkRequestForm')->name('request.show');
            Route::post('email',        'ForgotPasswordController@sendResetLinkEmail')->name('send');
            Route::get('reset/{token}', 'ResetPasswordController@showResetForm')->name('reset.show');
            Route::post('reset',        'ResetPasswordController@reset')->name('persist');
        });
    });

    Route::middleware('auth')->as('profile.')->prefix('profile')->group(function () {
        Route::get('edit',      'UserController@edit')->name('edit');
        Route::get('trainings', 'UserController@trainings')->name('trainings');
        Route::get('payments',  'UserController@payments')->name('payments');
        Route::get('feedback',  'UserController@feedback')->name('feedback');
        Route::post('',         'UserController@update')->name('update');
        Route::post('feedback', 'UserController@sendFeedback')->name('feedback.send');
    });

    Route::get('logout',         'Auth\LoginController@logout')->name('logout');
    Route::get('profile/{user}', 'UserController@show')->name('profile.show');
});

Route::get('sitemap.xml', function (App\Services\Sitemap $sitemap) {
    return $sitemap->render();
})->name('sitemap');

Route::get('{page_by_url}', 'PageController@show');