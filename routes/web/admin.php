<?php

Route::prefix('tours')->as('tours.')->group(function () {
    Route::get('', 'VueContainerController')->name('index');
    Route::get('create', 'VueContainerController')->name('create');
    Route::get('ratings', 'VueContainerController')->name('ratings');
    Route::get('{tour}', 'VueContainerController')->name('edit');
    Route::get('{tour}/tournament/{tournament}', 'VueContainerController')->name('tournament');
});

Route::prefix('vue-trainings')->group(function () {
    Route::get('', 'VueContainerController');
    Route::get('groups', 'VueContainerController');
    Route::get('groups/{id}', 'VueContainerController');
});

Route::resource('trainings', 'TrainingController',   ['except' => ['show', 'create'], 'parameters' => ['trainings' => 'group']]);

Route::prefix('settings')->as('settings.')->group(function () {
    Route::get('', 'VueContainerController')->name('index');
    Route::get('services', 'VueContainerController')->name('services');
    Route::get('notifications', 'VueContainerController')->name('notifications');
    Route::get('sponsors', 'VueContainerController')->name('sponsors');
    Route::get('pages', 'VueContainerController')->name('pages');
});

Route::get('', 'AdminController@index')->name('index');

Route::prefix('events')->as('events.')->group(function () {
    Route::resource('applications', 'EventApplicationController', ['only' => ['index', 'show', 'destroy']]);
});

Route::prefix('camps')->as('camps.')->group(function () {
    Route::resource('applications', 'CampApplicationController', ['only' => ['index', 'show', 'destroy']]);
});

Route::delete('locations/photo/{photo}', 'LocationController@deletePhoto')->name('locations.photo.destroy');
Route::delete('camps/photo/{photo}',     'CampController@deletePhoto')->name('camps.photo.destroy');
Route::delete('coaches/photo/{photo}',   'CoachController@deletePhoto')->name('coaches.photo.destroy');
Route::delete('events/photo/{photo}',    'EventController@deletePhoto')->name('events.photo.destroy');

Route::resource('posts',     'PostController',       ['except' => 'show']);
Route::resource('camps',     'CampController',       ['except' => 'show']);
Route::resource('coaches',   'CoachController',      ['except' => 'show']);
Route::resource('events',    'EventController',      ['except' => 'show']);
Route::resource('locations', 'LocationController',   ['except' => 'show']);
Route::resource('packages',  'PackageController',    ['except' => 'show']);
Route::resource('reviews',   'CampReviewController', ['except' => 'show']);
Route::resource('services',  'ServiceController',    ['except' => ['show', 'edit', 'update']]);
Route::resource('users',     'UserController',       ['except' => 'show']);
Route::resource('payments',  'PaymentController',    ['only'   => ['index', 'show']]);

Route::get('trainings/paid', 'TrainingController@paid')->name('trainings.paid');
Route::put('trainings/{groupId}/order-increment', 'TrainingController@orderIncrement')->name('trainings.order.incr');
Route::put('trainings/{groupId}/order-decrement', 'TrainingController@orderDecrement')->name('trainings.order.decr');

Route::prefix('users')->group(function () {
    Route::get('csv-emails', 'UserController@csvEmails')->name('users.csv-emails');
    Route::get('csv-phones', 'UserController@csvPhones')->name('users.csv-phones');
    Route::get('{user}/payments',  'UserController@payments')->name('users.payments');
    Route::get('{user}/teams',     'UserController@teams')->name('users.teams');
    Route::get('{user}/trainings', 'UserController@trainings')->name('users.trainings');
    Route::put('{user}/restore',   'UserController@restore')->name('users.restore');
});
