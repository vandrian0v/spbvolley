let mix = require('laravel-mix');

mix
    .js('resources/assets/js/vue/admin/app.js', 'public/js/admin.js')
    // .js('resources/assets/js/vue/front/app.js', 'public/js/front.js')
    .version()
    .webpackConfig({
        output: {
            filename: '[name].js',
            // chunkFilename: 'js/chunks/[name].js?id=[chunkhash]',
            publicPath: '/'
        }
    });