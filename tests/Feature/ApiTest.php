<?php

namespace Tests\Feature;

use App\Models\Location;
use App\Models\Tour;
use App\Models\Training;
use App\Models\TrainingGroup;
use App\Models\User;
use Tests\TestCase;

class ApiTest extends TestCase
{
    public function testLogged()
    {
        $this->actingAs(User::find(1));

        $trainingId = Training::first()->id;
        $groupId = TrainingGroup::first()->id;
        $locationId = Location::first()->id;

        $this->get(route('api.users.search', ['search' => 'search']))->assertStatus(200);
        $this->get(route('api.coaches'))->assertStatus(200);
        $this->get(route('api.trainings.list', ['group' => $groupId, 'location' => $locationId]))->assertStatus(200);
        $this->get(route('api.trainings.group.show', $groupId))->assertStatus(200);
        $this->get(route('api.trainings.subscription-types'))->assertStatus(200);
        $this->get(route('api.trainings.calendar', $trainingId))->assertStatus(200);
        $this->get(route('api.charts.payments', $trainingId))->assertStatus(200);
        $this->get(route('api.charts.users', $trainingId))->assertStatus(200);
    }

    public function testGuest()
    {
        $tourId = Tour::first()->id;

        $this->get(route('api.nwtour.categories', $tourId))->assertStatus(200);
        $this->get(route('api.rating', ['male', 1]))->assertStatus(200);
    }
}