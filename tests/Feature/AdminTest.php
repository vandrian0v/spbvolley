<?php

namespace Tests\Feature;

use App\Models\Camp;
use App\Models\CampApplication;
use App\Models\CampReview;
use App\Models\Coach;
use App\Models\Event;
use App\Models\EventApplication;
use App\Models\Location;
use App\Models\Package;
use App\Models\Payment;
use App\Models\Post;
use App\Models\Tour;
use App\Models\Tournament;
use App\Models\Training;
use App\Models\TrainingGroup;
use App\Models\User;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasic()
    {
        $this->actingAs(User::find(1));

        $this->get(route('admin.index'))->assertStatus(200);
    }

    public function testCamps()
    {
        $id = Camp::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.camps.index'))->assertStatus(200);
        $this->get(route('admin.camps.create'))->assertStatus(200);
        $this->get(route('admin.camps.edit', $id))->assertStatus(200);
    }
    public function testCampsApplications()
    {
        $id = CampApplication::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.camps.applications.index'))->assertStatus(200);
        $this->get(route('admin.camps.applications.show', $id))->assertStatus(200);
    }

    public function testEvents()
    {
        $id = Event::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.events.index'))->assertStatus(200);
        $this->get(route('admin.events.create'))->assertStatus(200);
        $this->get(route('admin.events.edit', $id))->assertStatus(200);
    }

    public function testEventsApplications()
    {
        $id = EventApplication::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.events.applications.index'))->assertStatus(200);
        $this->get(route('admin.events.applications.show', $id))->assertStatus(200);
    }

    public function testCoaches()
    {
        $id = Coach::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.coaches.index'))->assertStatus(200);
        $this->get(route('admin.coaches.create'))->assertStatus(200);
        $this->get(route('admin.coaches.edit', $id))->assertStatus(200);
    }

    public function testReviews()
    {
        $id = CampReview::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.reviews.index'))->assertStatus(200);
        $this->get(route('admin.reviews.create'))->assertStatus(200);
        $this->get(route('admin.reviews.edit', $id))->assertStatus(200);
    }

    public function testPackages()
    {
        $id = Package::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.packages.index'))->assertStatus(200);
        $this->get(route('admin.packages.create'))->assertStatus(200);
        $this->get(route('admin.packages.edit', $id))->assertStatus(200);
    }

    public function testLocations()
    {
        $id = Location::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.locations.index'))->assertStatus(200);
        $this->get(route('admin.locations.create'))->assertStatus(200);
        $this->get(route('admin.locations.edit', $id))->assertStatus(200);
    }

    public function testServices()
    {
        $this->actingAs(User::find(1));

        $this->get(route('admin.services.index'))->assertStatus(200);
    }

    public function testUsers()
    {
        $id = User::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.users.index'))->assertStatus(200);
        $this->get(route('admin.users.create'))->assertStatus(200);
        $this->get(route('admin.users.edit', $id))->assertStatus(200);
        $this->get(route('admin.users.payments', $id))->assertStatus(200);
        $this->get(route('admin.users.teams', $id))->assertStatus(200);
        $this->get(route('admin.users.trainings', $id))->assertStatus(200);
    }

    public function testNWTour()
    {
        $tourId = Tour::first()->id;
        $tournamentId = Tournament::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.nwtour.index'))->assertStatus(200);
        $this->get(route('admin.nwtour.create'))->assertStatus(200);
        $this->get(route('admin.nwtour.edit', $tourId))->assertStatus(200);
        $this->get(route('admin.nwtour.rankings.show', $tournamentId))->assertStatus(200);
        $this->get(route('admin.nwtour.teams.show', $tournamentId))->assertStatus(200);
    }

    public function testContent()
    {
        $id = Post::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.posts.index'))->assertStatus(200);
        $this->get(route('admin.posts.create'))->assertStatus(200);
        $this->get(route('admin.posts.edit', $id))->assertStatus(200);
    }

    public function testTrainings()
    {
        $id = TrainingGroup::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.trainings.index'))->assertStatus(200);
        $this->get(route('admin.trainings.edit', $id))->assertStatus(200);
    }

    public function testPayments()
    {
        $id = Payment::first()->id;
        $this->actingAs(User::find(1));

        $this->get(route('admin.payments.index'))->assertStatus(200);
        $this->get(route('admin.payments.show', $id))->assertStatus(200);
    }
}
