<?php

namespace Tests\Feature;

use App\Models\Camp;
use App\Models\Event;
use App\Models\Package;
use App\Models\Post;
use App\Models\Tour;
use App\Models\TrainingGroup;
use App\Models\User;
use App\Models\Coach;
use Tests\TestCase;

class ApplicationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasic()
    {
        $coachId = Coach::first()->id;

        $this->get(route('home'))->assertStatus(200);
        $this->get(route('coaches.show', $coachId))->assertStatus(200);
        $this->get(route('sitemap'))->assertStatus(200);
    }

    public function testAuth()
    {
        $userId = User::first()->id;

        $this->get(route('auth.login.show'))->assertStatus(200);
        $this->get(route('auth.register.show'))->assertStatus(200);

        $this->actingAs(User::find(1));

        $this->get(route('auth.profile.show', $userId))->assertStatus(200);
        $this->get(route('auth.profile.edit'))->assertStatus(200);
    }

    public function testTrainings()
    {
        $this->actingAs(User::find(1));

        $url = TrainingGroup::first()->url;

        $this->get(route('trainings.index'))->assertStatus(200);
        $this->get(route('trainings.purchase', $url))->assertStatus(200);
    }

    public function testContent()
    {
        $url = Post::first()->url;

        $this->get(route('posts.index'))->assertStatus(200);
        $this->get(route('posts.show', $url))->assertStatus(200);
    }

    public function testNWTour()
    {
        $this->actingAs(User::find(1));
        
        $id = Tour::first()->id;

        $this->get(route('nwtour.index'))->assertStatus(200);
        $this->get(route('nwtour.ratings'))->assertStatus(200);
        $this->get(route('nwtour.about'))->assertStatus(200);
        $this->get(route('nwtour.info', $id))->assertStatus(200);
        $this->get(route('nwtour.participants', $id))->assertStatus(200);
        $this->get(route('nwtour.results', $id))->assertStatus(200);
        $this->get(route('nwtour.application', $id))->assertStatus(200);
    }

    public function testEvents()
    {
        $eventId = Event::first()->id;
        $packageId = Package::first()->id;

        $this->get(route('events.index'))->assertStatus(200);
        $this->get(route('events.locations'))->assertStatus(200);
        $this->get(route('events.trainings'))->assertStatus(200);
        $this->get(route('events.show', $eventId))->assertStatus(200);
        $this->get(route('packages.show', $packageId))->assertStatus(200);
    }

    public function testCamps()
    {
        $id = Camp::first()->id;

        $this->get(route('camps.index'))->assertStatus(200);
        $this->get(route('camps.show', $id))->assertStatus(200);
    }
}
