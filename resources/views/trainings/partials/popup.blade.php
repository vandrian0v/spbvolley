{{ $training->coach->name }}<hr>
<table class='training-popup'>
    @foreach ($training->subscriptionTypes as $type)
        <tr>
            <td style='font-size:.8em;'>{{ $type->name }}:</td>
            <td><strong>{{ $type->pivot->price }}₽</strong></td>
        </tr>
    @endforeach
</table>