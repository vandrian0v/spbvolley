<div class="ui small modal panel-modal">
    <div class="header">Запись на тренировку</div>
    <div class="content">
        <div class="ui form panel-request" :class="{'loading': loading, 'error': error, 'success': success}">
            <div v-show="!success && !error">
                <div class="field">
                    <label>ФИО</label>
                    <input type="text" v-model="name">
                </div>

                <div class="two fields">
                    <div class="field">
                        <label>Контактный email</label>
                        <input type="text" v-model="email">
                    </div>
                    <div class="field">
                        <label>Контактный телефон</label>
                        <input type="text" v-model="phone">
                    </div>
                </div>

                <div class="field">
                    <label>Группа</label>
                    <select class="ui dropdown" v-model="group">
                        <option>Выберите группу</option>
                        <option value="0">Индивидуальная тренировка</option>
                        @foreach ($groups as $group)
                            <option value="{{ $group->id }}">
                                {{ $group->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="ui message success">
                Ваша заявка отправлена! Наш менеджер свяжется с Вами в ближайшее время.
            </div>

            <div class="ui message error">
                Произошла ошибка.
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui button primary" @click="submitApplication"
            :class="{disabled : !formValid || success || error }"
        >
            Отправить
        </div>
        <div class="ui cancel button">Отмена</div>
    </div>
</div>
