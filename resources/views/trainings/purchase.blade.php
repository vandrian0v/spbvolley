@extends('layouts.main.master')

@section('title', 'Оплата тренировок')

@section('content')
  <div class="ui segment training calculator">
    <h2 class="ui header large">Группа «{{ $group->name }}»</h2>

    <calculator :group-id="{{ $group->id }}">
      {{ csrf_field() }}
      @include('layouts.partials.errors')
      @include('trainings.partials.rules')
    </calculator>

  </div>
@stop

@push('javascript')
    <script src="{{ elixir('js/training_calculator.js') }}"></script>
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="/bower/semantic-ui-calendar/dist/calendar.min.css">
@endpush