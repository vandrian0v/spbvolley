@extends('layouts.main.master')

@section('title', 'Тренировки')

@section('content')
    <div class="ui segment training">
        <h2 class="ui header">
            Тренировки
        </h2>

        <div class="training-info">
            <h3 class="ui header">
                Мы предлагаем вам тренировки в следующих форматах:
            </h3>

            <div class="ui accordion">
                <div class="title">
                    <i class="dropdown icon"></i>
                    Тренировка в группе
                </div>
                <div class="content">
                    <ul class="ui list">
                        <li>новички и начальные группы для тех, кто только на первой ступеньке к волейбольному совершенству</li>
                        <li>средние и продвинутые, кто ставит перед собой серьезные цели</li>
                    </ul>
                </div>

                <div class="title">
                    <i class="dropdown icon"></i>
                    Индивидуальная тренировка
                </div>
                <div class="content">
                    <ul class="ui list">
                        <li>тренер работает с вами лично или с небольшой закрытой группой, это дает возможность новичкам быстрее схватить основы игры, а продвинутым игрокам более продуктивно взаимодействовать с тренером</li>
                    </ul>
                </div>

                <div class="title">
                    <i class="dropdown icon"></i>
                    Тренировки по физической подготовке
                </div>
            </div>

            <a href="#coaches">Тренеры клуба</a>
        </div>

        <h3 class="ui header centered">
          Расписание тренировок
        </h3>

        @foreach ($groups as $group)
            <div class="ui vertical segment training-group">
                <div class="ui header">
                    {{ $group->name }}

                    <a href="{{ route('trainings.purchase', $group->url) }}" class="ui {{ $group->color }} button right floated">
                        <i class="payment icon"></i>
                        Оплатить
                    </a>
                </div>

                <table class="ui table fixed {{ $group->color }} definition celled unstackable">
                    <thead>
                        <tr class="center aligned">
                            <th></th>
                            <th>пн</th>
                            <th>вт</th>
                            <th>ср</th>
                            <th>чт</th>
                            <th>пт</th>
                            <th>сб</th>
                            <th>вс</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($group->trainings->groupBy('location_id') as $trainingsByLocation)
                            @php
                                $location = $trainingsByLocation->first()->location;
                            @endphp
                            <tr>
                                <td class="location-td" data-position="right center" data-html="@include('trainings.partials.location_popup')">
                                    <img class="ui image location-icon" src="{{ $location->icon }}">
                                    {{ $location->short_name }}
                                </td>

                                @foreach (range(0, 6) as $i)
                                    <td>
                                        @if ($trainingsByLocation->groupBy('weekday')->has($i))
                                            @foreach ($trainingsByLocation->groupBy('weekday')->get($i) as $training)
                                                <div class="ui center aligned single-training" data-position="right center"
                                                    data-html="@include('trainings.partials.popup')"
                                                >
                                                    {{ $training->start_time }} - {{ $training->end_time }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach

        <div id="coaches" class="ui three cards doubling">
            @foreach ($coaches as $coach)
                <a class="ui card" href="{{ route('coaches.show', $coach->id) }}">
                    <div class="image">
                        @if ($coach->hasPreview())
                            <img src="{{ $coach->previewUrl() }}">
                        @endif
                    </div>
                    <div class="content">
                        <div class="header">
                            {{ $coach->name }}
                        </div>
                        <div class="meta">
                            {{ $coach->status }}
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@stop

@push('javascript')
    <script>
        $(document).ready(function() {
            $('.single-training').popup();
            $('td.location-td').popup();
        });
    </script>
@endpush

