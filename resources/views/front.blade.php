<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @hasSection('title')
        <title>@yield('title') | Клуб Пляжного Волейбола RIO</title>
    @else
        <title>Клуб Пляжного Волейбола RIO</title>
    @endif
    @hasSection('description')
        <description>@yield('description')</description>
    @endif

    @stack('css')
    <link rel="stylesheet" href="/bower/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/front.css') }}">
</head>
<body>
@include('layouts.front.menu')

@yield('content')
<div id="vue-container"> <!-- class="col-sm-12 col-md-12 main"> -->
    <router-view></router-view>
</div>

@include('layouts.front.footer')

<script src="/bower/jquery/dist/jquery.min.js"></script>
<script src="/bower/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{ mix('js/front.js') }}"></script>

@if (App::environment('production'))
    @include('layouts.partials.metrics')
@endif
</body>
</html>