@extends('layouts.front.master')

@section('title', 'Технические работы')

@section('content')
    <section style="padding: 70px 0;">
        <div class="container">
            <div class="text-center">
                <h2>В данный момент на сайте проводятся технические работы.</h2>
            </div>
        </div>
    </section>
@stop