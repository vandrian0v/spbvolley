@extends('layouts.front.master')

@section('title', 'Ошибка сервера')

@section('content')
    <section style="padding: 70px 0;">
        <div class="container">
            <div class="text-center">
                <h2>Ошибка 500. Ошибка сервера.</h2>
            </div>
        </div>
    </section>
@stop