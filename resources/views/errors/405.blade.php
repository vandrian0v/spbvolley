@extends('layouts.front.master')

@section('title', 'Страница не найдена')

@section('content')
    <section style="padding: 70px 0;">
        <div class="container">
            <div class="text-center">
                <h2>Ошибка 405. Запрашиваемая страница не существует.</h2>
            </div>
        </div>
    </section>
@stop