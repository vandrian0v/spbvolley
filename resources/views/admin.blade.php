<!DOCTYPE html>
<html lang="en">
@include('layouts.admin._head')
<body>
@include('layouts.admin._nav')

<div class="container-fluid">
    <div class="row">
        <div id="vue-container" class="col-sm-12 col-md-12 main">
            <router-view></router-view>
        </div>
    </div>
</div>

<script src="/bower/jquery/dist/jquery.min.js"></script>
<script src="/bower/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="{{ mix('js/admin.js') }}"></script>
</body>
</html>
