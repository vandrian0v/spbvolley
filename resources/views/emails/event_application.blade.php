<p>На сайт поступила новая заявка на проведение мероприятия.</p>
<p>Информацию можно посмотреть <a href="{{ route('admin.events.applications.show', $application->id) }}">здесь</a>.</p>