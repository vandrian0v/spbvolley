<p>Запись на тренировку в клубе!</p>

<ul>
    <li>ФИО: {{ $data['name'] ?? '' }}</li>
    <li>Email: {{ $data['email'] ?? '' }}</li>
    <li>Телефон: {{ $data['phone'] ?? '' }}</li>
    <li>Группа: {{ $data['group'] ?? '' }}</li>
    <li>Комментарий: {{ $data['comment'] ?? '' }}</li>
</ul>