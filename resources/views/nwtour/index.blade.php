@extends('layouts.nwtour.master')

@section('title', 'Турнир (турниры) по пляжному волейболу в СПБ')
@section('description', 'Клуб пляжного волейбола RIO турниры для взрослых и начинающих спортсменов.')
@section('keywords', 'турнир турниры по пляжному волейболу в спб')

@section('nwtour_content')
  <h1 style="text-align: center">Турниры по пляжному волейболу в СПБ</h1>

  @php
      $sponsors = App\Models\Sponsor::all();
  @endphp

  <div class="ui mobile only grid segment">
      @foreach ($sponsors as $sponsor)
          <a href="{{ $sponsor->url }}">
              <img style="width: 100%; height: auto;" src="{{ $sponsor->preview_url }}">
          </a>
      @endforeach
  </div>

  <div class="ui top attached tabular menu">
      <a class="active item" data-tab="first">Будущие</a>
      <a class="item" data-tab="second">Прошедшие</a>
  </div>

  <div class="ui bottom attached active tab segment" data-tab="first">
      @foreach ($futureTours as $tour)
          @if (!$loop->first)
              <div class="ui divider"></div>
          @endif
          <div class="ui segment basic left aligned nwtour-info">

              @if ($tour->status == 'opened')
                  <div class="ui buttons small right floated">
                      <a class="ui button basic blue left attached" href="{{ route('nwtour.info', $tour->id) }}">
                          Информация
                      </a>
                      <a class="ui button basic teal right attached" href="{{ route('nwtour.application', $tour->id) }}">
                          Заявиться
                      </a>
                  </div>
              @else
                  <a class="ui button blue basic small right floated" href="{{ route('nwtour.info', $tour->id) }}">
                      Информация
                  </a>
              @endif

              {{ $tour->title }}<br>
              <span>{{ $tour->localeDate }}</span>

              <div class="ui tiny horizontal labels">
                  @foreach ($tour->tournaments as $tournament)
                      <div class="ui label {{ $tournament->category->gender }}">
                          {{ $tournament->category->fullname }}
                      </div>
                  @endforeach
              </div>
          </div>
      @endforeach
  </div>
  <div class="ui bottom attached tab segment" data-tab="second">
      @foreach ($pastTours as $tour)
          @if (!$loop->first)
            <div class="ui divider"></div>
          @endif
          <div class="ui segment basic left aligned nwtour-info">

              @if ($tour->status == 'opened')
                  <div class="ui buttons small right floated">
                      <a class="ui button basic blue left attached" href="{{ route('nwtour.info', $tour->id) }}">
                          Информация
                      </a>
                      <a class="ui button basic teal right attached" href="{{ route('nwtour.application', $tour->id) }}">
                          Заявиться
                      </a>
                  </div>
              @else
                  <a class="ui button blue basic small right floated" href="{{ route('nwtour.info', $tour->id) }}">
                      Информация
                  </a>
              @endif

              {{ $tour->title }}<br>
              <span>{{ $tour->localeDate }}</span>

              <div class="ui tiny horizontal labels">
                  @foreach ($tour->tournaments as $tournament)
                      <div class="ui label {{ $tournament->category->gender }}">
                          {{ $tournament->category->fullname }}
                      </div>
                  @endforeach
              </div>
          </div>
      @endforeach
  </div>
@stop
