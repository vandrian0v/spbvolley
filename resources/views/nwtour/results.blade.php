@extends('layouts.main.master')

@section('title', 'NW-Tour, ' . $tour->title . ' - результаты')

@section('content')
    <div class="ui segment nwtour-segment">

        @include('layouts.nwtour.partials.tour_menu')

        @foreach ($categories as $category => $teams)
            <h4 class="ui header">
                {{ $category }}
            </h4>
            <table class="ui celled table small slim unstackable">
                <thead>
                <th class="collapsing">Место</th>
                <th>Команда</th>
                <th class="collapsing">Рейтинг</th>
                </thead>
                <tbody>
                @foreach ($teams as $team)
                    <tr>
                        <td>{{ $team->rank }}</td>
                        <td>{!! $team->usersLinks() !!}</td>
                        <td>{{ $team->rating }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
@stop
