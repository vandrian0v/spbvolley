@extends('layouts.main.master')

@section('title', 'Подача заявки на турнир серии NW-Tour')

@section('content')
  <div class="ui segment nwtour-segment">

    @include('layouts.nwtour.partials.tour_menu')

    @if ($tour->status == 'opened')
      <div class="ui header center aligned">
        Форма заявки
      </div>

      <div class="ui form" style="margin-bottom: 1em;">
        @include('layouts.partials.errors')
      </div>

      <div class="ui form application-form" :class="{loading: loading}" v-if="!success">
        <div class="ui message warning" :class="{visible: errors.length}">
          <div v-for="error in errors">@{{ error }}</div>
        </div>

        <div class="field">
          <label>Категория</label>
          <select v-model="selected_category_id">
            <option value="">Выберите категорию</option>
            <option v-for="tournament in tournaments" :value="tournament.category_id"
              :disabled="tournament.limit_exceeded"
              v-text="tournament.category.name + (tournament.limit_exceeded ? ' (прием заявок окончен)' : '')"
            >
            </option>
          </select>
        </div>

        <div class="ui grid users fields">
          <div class="field wide eight" v-for="i in usersRange">
            <label>Игрок @{{ i }}</label>
            {{--<input type="text" placeholder="Email" v-model="users[i - 1]">--}}
            <user-select :gender="selectedTournament.category.gender" v-model="users[i - 1]"></user-select>
          </div>
        </div>

        <div class="field" style="margin-top: 1em;">
          <input type="button" class="ui button primary"
            :value="submitText"
            :class="{disabled: !selected_category_id}" @click="sendApplication"
          >
        </div>

        <div class="ui message success visible">
          В качестве идентификатора следует указывать email игрока указанный при регистрации на сайте.
          Если игрок не зарегистрирован, то пройти регистрацию можно
          <a href="{{ route('auth.register.show') }}">здесь</a>.
        </div>
      </div>

      <div class="ui message blue" style="text-align:center;" v-else>
        Ваша заявка подана. Спасибо!
      </div>

      <form id="paymentForm" method="POST" action="{{ route('payments.nwtour') }}" v-if="team_id">
        {{ csrf_field() }}
        <input type="hidden" name="team_id" :value="team_id">
      </form>
    @else
      <div class="ui header center aligned message">
        Прием заявок окончен
      </div>
    @endif
  </div>
@stop

@push('javascript')
  <script>
    var nwtour_id = {{ $tour->id }};
  </script>
  <script src="{{ elixir('js/tournament_application.js') }}"></script>
@endpush