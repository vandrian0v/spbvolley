@extends('layouts.nwtour.master')

@section('title', 'Рейтинг игроков NW-Tour')

@section('nwtour_content')
  <h2 class="ui header center aligned dividing">
    Рейтинг игроков
  </h2>

  <div class="ui form" style="padding: 1em;">
    <div class="inline fields">
      <label for="category">Категория: </label>
      @foreach ($userCategories as $category)
        <div class="field">
          <div class="ui radio checkbox">
            <input type="radio" name="category" v-model="category" tabindex="0" class="hidden" value="{{ $category->id }}"
              id="category-{{ $category->id }}"
            >
            <label for="category-{{ $category->id }}">{{ $category->name }}</label>
          </div>
        </div>
      @endforeach
    </div>
  </div>

  <div class="ui top attached tabular menu stackable">
    @foreach ($ratingTypes as $index => $ratingType)
      <div class="item" :class="{active: active == '{{ $ratingType['alias'] }}'}"
        v-on:click="fetchRating('{{ $ratingType['alias'] }}', false)"
      >
        <i class="{{ $ratingType['icon'] }} icon"></i>
        {{ $ratingType['name'] }}
      </div>
    @endforeach
  </div>

  @verbatim
    <div class="ui bottom attached tab segment active" :class="{loading: loading}" style="overflow-x: auto">
      <div class="ui error message" v-show="error" style="margin: 0;">
        {{ error }}
      </div>

      <table class="ui celled table small slim unstackable" v-show="!error">
        <thead>
          <tr>
            <th>№</th>
            <th>Игрок</th>
            <th class="collapsing">Рейтинг за сезон</th>
            <th class="collapsing">Рейтинг за год</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(user, i) in users">
            <td>{{ i + 1 }}</td>
            <td>
              <a :href="user.link">
                {{ user.fullname }}
              </a>
            </td>
            <td>{{ user.seasonal_rating }}</td>
            <td>{{ user.yearly_rating }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  @endverbatim
@stop

@push('javascript')
  <script src="{{ elixir('js/rating.js') }}"></script>
@endpush
