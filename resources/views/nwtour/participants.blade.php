@extends('layouts.main.master')

@section('title', 'NW-Tour, ' . $tour->title . ' - участники')

@section('content')
  <div class="ui segment nwtour-segment">

    @include('layouts.nwtour.partials.tour_menu')
    <div class="ui form">
      @include('layouts.partials.errors')
    </div>

    @foreach ($tour->tournaments as $tournament)
      @if (!$tournament->teams->isEmpty())
        <h4 class="ui header">
          {{ $tournament->category->fullname }}
          @if ($tournament->limit_exceeded)
            <span style="color: #9F3A38;font-size: .8em;margin-left: 1em;">(Прием заявок окончен)</span>
          @endif
        </h4>
        <table class="ui small celled slim table unstackable">
          <thead>
            <tr>
              <th class="collapsing">#</th>
              <th>Команда</th>
              @if (in_array($tour->status, [App\Models\Tour::STATUS_OPENED, App\Models\Tour::STATUS_CLOSED]))
              <th>Рейтинг</th>
              @endif
              @if ($tournament->contribution)
                <th class="collapsing">Взнос</th>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach ($tournament->teams as $index => $team)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{!! $team->usersLinks() !!}</td>
                @if (in_array($tour->status, [App\Models\Tour::STATUS_OPENED, App\Models\Tour::STATUS_CLOSED]))
                <td>{{ $team->users_rating }}</td>
                @endif
                @if ($tournament->contribution)
                  @if ($team->is_paid || $team->payment && $team->payment->is_confirmed)
                    <td class="three wide positive">Оплачен</td>
                  @elseif ($team->users->contains(request()->user()) && !$tournament->limit_exceeded)
                    <td class="three wide negative" style="padding: 0">
                      <form method="POST" action="{{ route('payments.nwtour') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="team_id" value="{{ $team->id }}">
                        <button class="ui button primary fluid mini" style="border-radius: 0;">
                          <i class="payment icon"></i>
                          Оплатить
                        </button>
                      </form>
                    </td>
                  @else
                    <td class="three wide negative">
                       Не оплачен
                    </td>
                  @endif
                @endif
              </tr>
            @endforeach
          </tbody>
        </table>
      @endif
    @endforeach

    <div class="ui icon info message">
      <i class="payment icon"></i>
      <div class="content">
          Для оплаты взноса необходимо быть <a href="{{ route('auth.login.show') }}">залогиненным</a>
          на сайте и являться игроком заявленной команды.
      </div>
    </div>
  </div>
@stop
