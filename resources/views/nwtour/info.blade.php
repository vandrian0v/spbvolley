@extends('layouts.main.master')

@section('title', 'NW-Tour, ' . $tour->title . ' - информация')

@section('content')
  <div class="ui segment nwtour-segment">

    @include('layouts.nwtour.partials.tour_menu')

    <div>
      {!! $tour->description !!}
    </div>

    <div class="ui message brown">
      <strong>Проводимые категории:</strong>
      @foreach ($tour->tournaments as $tournament)
        <div class="ui label tiny {{ $tournament->category->gender }}">
          {{ $tournament->category->fullname }}
        </div>
      @endforeach
    </div>

    <div id="map"></div>
  </div>
@stop

@push('javascript')
  @if ($tour->latitude && $tour->longitude)
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <script>
      ymaps.ready(init);
      var myMap, myPlacemark;

      function init(){
        $('#map').show();

        myMap = new ymaps.Map("map", {
          center: [{{ $tour->latitude }}, {{ $tour->longitude }}],
          zoom: 14,
          controls: ['smallMapDefaultSet']
        });

        myPlacemark = new ymaps.Placemark([{{ $tour->latitude }}, {{ $tour->longitude }}], {});
        myMap.geoObjects.add(myPlacemark);
      }
    </script>
  @endif
@endpush
