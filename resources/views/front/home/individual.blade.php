@extends('layouts.front.master')

@section('title', 'Индивидуальные занятия волейболом | Индивидуальные тренировки по волейболу')
@section('description', 'Индивидуальные занятия волейболом в Санкт-Петербурге! Мы тренируем волейбольный клуб Зенит!')
@section('keywords', 'Индивидуальные занятия волейболом тренировки по волейболу')

@push('js')
<script src="{{ elixir('js/training_index.js') }}"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
</script>

@endpush

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.6/css/swiper.min.css"/>
    <style>
        #trainings-slider .swiper-slide, #trainings-slider .swiper-container.swiper-container-horizontal {
            max-height: 350px;
        }
    </style>
@endpush

@section('content')
<div id="trainings-index">
    <section id="trainings-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-xs hidden-sm" style="padding-top: 50px;">
                    <img src="/front/icons/logo.svg" style="width: 88px;height: 101px;">
                </div>
                <div class="col-md-4 text-center" style="padding: 50px 0 40px;">
                    <img src="/front/icons/whistle_colored.svg" style="width: 99px;height: 75px;">
                    <h1 style="font-size: 40px;">Индивидуальные тренировки по волейболу</h1>
                    <p style="font-size: 18px;">
                        Команда профессионалов клуба «RIO»
                        обучит пляжному волейболу и направит к первым победам!
                    </p>
                </div>
                <div class="col-md-4 hidden-xs hidden-sm" style="padding-top: 50px; padding-left: 50px; padding-right: 0; font-weight: 300;color:white;line-height: 26px;font-size: 13px;">
                    <div class="hidden-cc" style="display: inline-block;">
                        Телефон:<br>
                        <strong style="font-size: 20px;">8 (965) 081 04 62</strong><br>
                        9:00 - 22:00 без выходных
                    </div>
                    <div style="display: inline-block; float: right;">
                        <a class="btn btn-warning" style="display: block; font-size: 22px; margin-top: 5px;" @click="showTrainingApplyModal('')" onclick="yaCounter31562248.reachGoal('header_apply_click'); return true;">Записаться</a>
                        <a href="#trainings-schedule" class="btn btn-warning" style="margin-top: 10px; display: block;" onclick="yaCounter31562248.reachGoal('header_pay_click'); return true;">Оплатить</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="trainings-slider" style="max-height: 350px;    margin: 50px 0;">
        <swiper :options="swiperOption">
            <swiper-slide v-for="photo in photos">
                <div class="training-slider-image" :style="'background-image: url(' + photo + ');'"></div>
            </swiper-slide>
            {{--<div class="swiper-button-prev" slot="button-prev"></div>--}}
            {{--<div class="swiper-button-next" slot="button-next"></div>--}}
        </swiper>
    </section>

    <section id="training-rio-is">
        <div class="container">
            <div class="row text-center">
                <h2 class="header" style="margin: 0; font-weight: normal;">
                    Пляжный волейбол индивидуальные тренировки
                </h2>
                <div class="orange-divider" style="margin-bottom: 10px;"></div>
            </div>
            <div class="rio-is-list">
                <div class="rio-is-el">
                    <p>Отличная физическая форма</p>
                    <img style="height: 80px" src="/image/power.jpg">
                </div>
                <div class="rio-is-el">
                    <p>Тренировки только с профессионалами</p>
                </div>
                <div class="rio-is-el">
                    <p>Возможность постоянно развиваться</p>
                    <img style="height: 80px" src="/image/arr.jpg">
                </div>
            </div>
            <div class="rio-is-list">
                <div class="rio-is-el">
                    <p>Занятия одним из самых массовых видов спорта</p>
                </div>
                <div class="rio-is-el" style="flex-direction: column;align-items: center;">
                    <p>Новые знакомства и впечатления</p>
                    <img style="width: 40%;" src="/image/hands.jpg">
                </div>
                <div class="rio-is-el">
                    <p>Спортивные успехи и победы на турнирах</p>
                </div>
            </div>
        </div>
    </section>

    <section style="padding: 50px 0 0;">
        <div class="container">
            <div class="row text-center">
                <div class="header ">Индивидуальные занятия волейболом</div>
                <div class="orange-divider" style="margin-bottom: 30px;"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>Многие люди, только начинающие свой путь в волейболе или не имеющих к нему отношения, уверены, что это достаточно простая и легкая игра, не требующая вложений в качестве постоянных изнурительных тренировок. Однако, капнув глубже, выясняется, что данный вид спорта диктует свои правила – необходима четко отработанная техника, хорошие игровые навыки и отлаженность действий.</p>
                    <p>Действительно, некоторые моменты возможно отработать на групповых занятиях, значительно повысив свой уровень как игрока. Но если требуется усовершенствовать определенный элемент, например, подачу, прием подач или нападающий удар, отличным вариантом станет индивидуальная тренировка.</p>
                    <p>Индивидуальные занятия подходят для всех категорий игроков: начинающих, ведь изначально правильно поставленная техника – залог успеха; продолжающих – «западание» какого-либо элемента, прорабатывание слабых сторон повышает уровень игрока. В целом, можно сказать, что такие тренировки направлены на ускоренное развитие волейболиста.</p>
                    <p>Индивидуальное занятие длится 1-1.5 часа. Это самое оптимальное время, за которое можно достигнуть эффекта. Каждое движение, каждое действие четко контролируется и по надобности исправляется тренером, ни одна ошибка не может скрыться от его глаз. Мячей на тренировке обычно около 10-15 – такое большое количество необходимо, чтобы не тратить время на «походы» за мячами и как можно больше времени уделить самому занятию.</p>
                    <p>В основном, индивидуальные тренировки являются тематическими. Например, основной вид такого занятия – постановка нападающего удара. Дело в том, что это один из самых сложных элементов игры и является составным т.е несет в себе сразу нескольких движений. Каждое движение необходимо проработать, а потом соединить все вместе, тогда будет виден результат.</p>
                    <p>Индивидуальные занятия проводят и для других элементов игровых действий: для блока, передачи (паса) сверху, снизу, приема и различных видов подач.</p>
                    <p>Физические возможности людей разные, потому для каждой тренировки подбирается собственный комплекс упражнений, состоящий из основных и авторских, разработанных нашими талантливыми тренерами. И те, и другие служат для того, чтобы решать какие-то определенные задачи в обучении с нуля и совершенствованию уже имеющихся технических навыков.</p>
                    <p>Более точный состав упражнений утверждается после 1-2 занятий. Формируется конкретная программа подготовки и промежуточные тесты, которые помогут показательно увидеть свой прогресс. Так же на тренировках задействовано большое количество новых мячей и другого сопутствующего инвентаря.</p>
                    <p>Благодаря профессиональному и системному подходу ученики нашего клуба пляжного волейбола «Rio» уже сейчас за кратчайшие сроки добиваются выдающихся результатов и успеха!</p>
                </div>
            </div>
        </div>
    </section>

    <section id="trainings-groups" style="padding: 40px 0;">
        <div class="container">
            <div class="row text-center">
                <div class="header">
                    Уровни групп
                </div>
                <div class="orange-divider" style="margin-bottom: 30px;"></div>
            </div>
            <div class="group-list">
                <div class="group">
                    <h4>Начальная группа</h4>
                    <p>
                        Для тех, кто только знакомится с пляжным волейболом или вспоминает его
                    </p>
                </div>
                <div class="group">
                    <h4>Средняя группа</h4>
                    <p>
                        Для тех, кто имеет хороший опыт игры на песке и мечтает совершенствоваться
                    </p>
                </div>
                <div class="group">
                    <h4>Продвинутая группа</h4>
                    <p>
                        Для тех, кто имеет большой опыт игры на песке или классическую спортшколу
                    </p>
                </div>
                <div class="group">
                    <h4>Детские группы</h4>
                    <p>
                        Развитие общей физической подготовки, постановка волейбольной техники
                    </p>
                </div>
            </div>
        </div>
    </section>

    @include('front.coaches')

    <training-apply-form :groups="{{ json_encode($groups) }}"></training-apply-form>

    <modal name="trainings-apply-modal" :adaptive="true" :y-pivot="0.2" :width="1170" height="auto">
        <training-apply-form :groups="{{ json_encode($groups) }}" :group_id="modal_group_id"></training-apply-form>
    </modal>
</div>
@stop
