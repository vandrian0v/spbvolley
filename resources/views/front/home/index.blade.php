@extends('layouts.front.master')

@section('title', 'Клуб и школа пляжного волейбола RIO тренировки для взрослых, начинающих и спортсменов! Волейбольные кэмпы и соревнования!')
@section('description', 'В клубе обучают играющие тренера. Мы тренируем волейбольный Клуб Зенит. Пляжный волейбол.')
@section('keywords', 'Клуб и школа пляжного волейбола RIO тренировки для взрослых, начинающих и спортсменов волейбольные кэмпы и соревнования')

@push('js')
<script src="https://vk.com/js/api/openapi.js"></script>
<script>
    VK.Widgets.Group("vk_groups", {mode: 3, width: "auto", color1: 'FAFAFA', color3: '1B1D7D', color2: '282978'}, 68535444);
</script>
@endpush

@section('content')
<section id="home-info" style="padding-top: 30px;">
    <div class="container no-padding">
        <div class="row" style="margin: 0;">
            <div style="display: flex; align-items: center;">
                <div class="icon logo"></div>
                <div class="grey-divider hidden-xm"></div>
            </div>
            <div class="hidden-xm hidden-sm hidden-xs">
                <h4>Клуб пляжного волейбола</h4>
                <p>Проводим тренировки и все виды соревнований по пляжному волейболу</p>
            </div>
            <div>
                <div style="font-size: 12px; color: #262877;">9:00 - 22:00 без выходных</div>
                <a href="tel:8 (965) 081 04 62" style="font-weight: bold; font-size: 18px; color: #262877 !important;">8 (965) 081 04 62</a>
            </div>
            <a href="#trainings-apply" class="btn btn-warning hidden-xs" onclick="yaCounter31562248.reachGoal('home_write_to_club_click'); return true;">Написать в клуб</a>
        </div>
    </div>
</section>

<section id="home-slider" style="padding-top: 40px;">
    <div class="container no-padding">
        <div id="slider" class="carousel slide" data-ride="carousel" data-interval="5000">
            <ol class="carousel-indicators">
                @foreach ($sliderPosts as $post)
                    <li data-target="#slider" data-slide-to="{{ $loop->index }}" @if ($loop->first) class="active" @endif></li>
                @endforeach
            </ol>

            <div class="carousel-inner">
                @foreach ($sliderPosts as $post)
                    <div class="item @if ($loop->first) active @endif">
                        <div class="col-md-4">
                            <div class="col-md-12 no-padding">
                                <h2>{{ $post->title }}</h2>
                                <div class="anons">{!! $post->anons !!}</div>
                                <a href="{{ route('posts.show', $post->url) }}" class="btn btn-warning">Подробнее</a>
                            </div>
                        </div>
                        <div class="col-md-8 hidden-sm hidden-xs" style="background: url({{ $post->imageUrl() }}) no-repeat; background-size: cover;"></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section id="home-services" style="padding-top: 70px;">
    <div class="container">
        <div class="row" style="margin: 0 0 35px 0; display: inline-block;">
            <div class="header">
                Услуги волейбольного клуба
            </div>
            <div class="orange-divider"></div>
        </div>
        @foreach ($blocks as $block)
            @if ($loop->index % 3 === 0)
                <div class="row text-center">
            @endif
            <div class="col-sm-4 service">
                <a href="{{ $block->url }}" style="background: url({{ $block->preview_url }}); background-size: cover; width: 100%; height: 250px;display: block;"></a>
                <h4>{{ $block->title }}</h4>
                <p>{{ $block->text }}</p>
                <a href="{{ $block->url }}" class="btn btn-primary">Подробнее</a>
            </div>
            @if ($loop->index % 3 === 2)
                </div>
            @endif
        @endforeach
    </div>
</section>

<section id="home-about" style="padding-bottom: 70px; padding-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="vk col-md-3">
                <div id="vk_groups"></div>
            </div>
            <div class="about col-md-9">
                <div class="text-block">
                    <div class="header">
                        О нас
                    </div>
                    <div class="orange-divider" style="margin-bottom: 40px;"></div>
                    <p style="font-size: 16px; color: #282978; line-height: 26px;">
                        Клуб пляжного волейбола "RIO" создавался на фоне роста популярности пляжного волейбола
                        в Санкт-Петербурге и по всей стране. Питер имеет огромную армию поклонников "бич волея",
                        первых чемпионов России, историю первого чемпионата страны...
                        Задача нашего клуба, не только чтобы эта "армия" росла,
                        но чтобы она еще и "мужала": чтобы качество петербургского пляжного волейбола росло
                        вместе с количеством поклонников.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="home-news" style="padding-bottom: 70px;">
    <div class="container">
        <div class="row" style="margin: 0; display: inline-block;">
            <a class="header" href="/content" style="color: #1b1d7d !important;">
                Новости
            </a>
            <div class="orange-divider" style="margin-bottom: 40px;"></div>
        </div>
        <div class="news-category-selectors">
            <ul>
                @foreach ($postCategories as $category)
                    <li role="presentation" @if ($loop->first) class="active" @endif>
                        <a href="#category-{{ $category->id }}" aria-controls="profile" role="tab" data-toggle="tab">
                            {{ $category->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <a class="header text-muted hidden-xs" href="/content" style="color: #777 !important; margin-top: 10px;float: right;font-size: 14px;">
                Подробнее
            </a>
        </div>
        <div class="news-list tab-content">
            @foreach ($postCategories as $category)
                <div role="tabpanel" class="tab-pane @if ($loop->first) active @endif" id="category-{{ $category->id }}">
                    @foreach ($posts[$category->id] as $post)
                        <div class=" post-block">
                            <a href="{{ route('posts.show', $post->url) }}">
                                <img src="{{ $post->imageUrl() }}">
                            </a>
                            <span>{{ $post->localeDate }}</span>
                            <a class="post-title" href="{{ route('posts.show', $post->url) }}">
                                {{ $post->title }}
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</section>

<section id="coaches">
    <div class="container">
        <div class="row" style="margin: 0;">
            <div class="header">
                Тренеры
            </div>
            <div class="orange-divider" style="margin-bottom: 20px;"></div>
        </div>
        <div class="coaches">
            @foreach($coaches as $coach)
                <a href="{{ route('coaches.show', $coach) }}" class="coach">
                    <div class="image" style="background-image: url({{ $coach->previewUrl() }});"></div>
                    <div class="text-block">
                        <div class="name">{{ $coach->name }}</div>
                        <div class="description">{{ $coach->status }}</div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</section>

@include('front.sponsors')
@include('front.trainings.apply')
@stop