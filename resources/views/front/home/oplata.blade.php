@extends('layouts.front.master')

@section('title', 'Оплата тренировок по пляжному волейболу')
@section('description', 'Оплата тренировок по пляжному волейболу')
@section('keywords', 'Оплата тренировок по пляжному волейболу')

@push('js')
<script src="{{ elixir('js/training_index.js') }}"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
</script>

@endpush

@section('content')
<div id="trainings-index">
    <section id="trainings-schedule" style="padding-top: 70px;">
        <div class="container">
            <div class="row text-center">
                <h1 class="header" style="margin-top: 0;font-weight: normal;">
                    Оплата тренировок по пляжному волейболу
                </h1>
                <div class="orange-divider"></div>
            </div>
            <div class="row" style="margin-bottom: 38px;">
                <div class="col-md-12">
                    <h5>ОБОЗНАЧЕНИЯ:</h5>
                    <div class="icon sun" style="position: relative;top: 11px;margin-left: 21px;margin-right: 8px;"></div>
                    <span class="muted-text">– открытая площадка</span>
                    <div class="icon snow" style="position: relative;top: 10px;margin-left: 61px;margin-right: 8px;"></div>
                    <span class="muted-text">– закрытая площадка</span>
                </div>
            </div>

            <div class="schedule">
                <div class="panel-group" id="accordion" style="margin:25px 0">
                    @foreach ($groups as $group)
                        <div class="hidden fake-toggle" data-toggle="collapse" href="#collapse{{ $group->id }}"></div>
                        <div class="panel panel-default">
                            <div class="panel-heading" data-parent="#accordion">
                                <h4 class="panel-title collapsed" data-toggle="collapse" href="#collapse{{ $group->id }}">
                                    {{ $group->name }}
                                </h4>
                                <div style="display: flex;align-items: center;">
                                    <a class="btn btn-pay" @click="showTrainingApplyModal({{ $group->id }})" style="margin-right: 10px;" onclick="yaCounter31562248.reachGoal('table_apply_click'); return true;">Записаться</a>
                                    <a href="{{ route('trainings.purchase', $group->url) }}" class="btn btn-pay" onclick="yaCounter31562248.reachGoal('table_pay_click'); return true;">
                                        <img src="/front/icons/wallet.svg">
                                        Оплатить
                                    </a>
                                    <div class="circle collapsed" data-toggle="collapse" href="#collapse{{ $group->id }}">
                                        <i class="arrow left"></i>
                                    </div>
                                </div>
                            </div>
                            <div id="collapse{{ $group->id }}" class="panel-collapse collapse" aria-expanded="true">
                                <div class="schedule-weekdays">
                                    <div></div>
                                    <div>Пн</div>
                                    <div>Вт</div>
                                    <div>Ср</div>
                                    <div>Чт</div>
                                    <div>Пт</div>
                                    <div>Сб</div>
                                    <div>Вс</div>
                                </div>
                                @foreach ($group->trainings->groupBy('location_id') as $trainingsByLocation)
                                    @php
                                        $location = $trainingsByLocation->first()->location;
                                    @endphp
                                    <div class="schedule-row">
                                        <div class="schedule-cell">
                                            <div class="icon {{ $location->new_icon }}"></div>
                                            <div class="location-text">
                                                <h4>{{ $location->short_name }}</h4>
                                                <span>{{ $location->address }}</span>
                                            </div>
                                        </div>
                                        @foreach (range(0, 6) as $i)
                                            <div class="schedule-cell">
                                                @if ($trainingsByLocation->groupBy('weekday')->has($i))
                                                    @foreach ($trainingsByLocation->groupBy('weekday')->get($i)->sortBy('start_time') as $training)
                                                        <div data-toggle="popover" data-placement="top" data-trigger="hover" data-html="true"
                                                             title="тренер: {{ $training->coach->name }}"
                                                             data-content="
                                                             @foreach ($training->subscriptionTypes as $type)
                                                                 {{ $type->name }}: {{ $type->pivot->price }}₽
                                                                 @if (!$loop->last)<hr>@endif
                                                             @endforeach
                                                             "
                                                        >
                                                            {{ $training->start_time }} - {{ $training->end_time }}
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <modal name="trainings-apply-modal" :adaptive="true" :y-pivot="0.2" :width="1170" height="auto">
        <training-apply-form :groups="{{ json_encode($groups) }}" :group_id="modal_group_id"></training-apply-form>
    </modal>
</div>
@stop
