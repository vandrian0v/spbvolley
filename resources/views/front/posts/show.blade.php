@extends('layouts.front.master')

@section('title', $post->title)

@section('content')
    <section style="padding: 20px 0;">
        <div class="container post-content">
            <h1 style="font-size: 35px;">{{ $post->title }}</h1>
            <div style="font-size: 20px; color: #535353;">{{ $post->localeDate }}</div>
            <div class="orange-divider" style="margin-bottom: 30px;"></div>
            <div>{!! $post->content !!}</div>
        </div>
    </section>
@stop