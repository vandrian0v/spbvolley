@extends('layouts.front.master')

@section('title', $coach->name)

@section('content')
    <section style="padding: 40px 0;">
        <div class="container">
            <div class="col-sm-9 coach-info">
                <h1>{{ $coach->name }}</h1>
                <div style="font-size: 20px; color: #535353;">{{ $coach->status }}</div>
                <div class="orange-divider" style="margin-bottom: 40px;"></div>
                <div style="font-size: 16px; line-height: 26px;">
                    @if ($coach->career)
                        <h4>Профессиональная карьера:</h4>
                        {!! $coach->career !!}
                        <hr>
                    @endif

                    @if ($coach->education)
                        <h4>Образование:</h4>
                        {!! $coach->education !!}
                        <hr>
                    @endif

                    @if ($coach->teachers)
                        <h4>Учителя:</h4>
                        {!! $coach->teachers !!}
                        <hr>
                    @endif

                    @if ($coach->training)
                        <h4>Тренировки:</h4>
                        {!! $coach->training !!}
                    @endif
                </div>
            </div>
            <div class="col-sm-3 coach-photos">
                <img src="{{ $coach->previewUrl() }}">
                @foreach ($coach->photos as $photo)
                    <img src="{{ $photo->url }}">
                @endforeach
            </div>
        </div>
    </section>
@stop