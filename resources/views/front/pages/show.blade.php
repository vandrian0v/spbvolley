@extends('layouts.front.master')

@section('title', $page->title)
@section('description', $page->description)
@section('keywords', $page->keywords)

@section('content')
    <section>
        <div class="container">
            {!! $page->content !!}
        </div>
    </section>
@stop