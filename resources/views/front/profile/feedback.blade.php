@extends('layouts.front.master')

@section('title', 'Обратная связь')

@section('content')
    <form method="POST" class="ui form" action="{{ route('auth.profile.feedback.send') }}">
        @include('layouts.partials.errors')

        {{ csrf_field() }}

        <div class="field">
            <label for="name">Сообщение</label>
            <textarea name="text" required>{{ old('text') }}</textarea>
        </div>

        <button class="ui primary submit button">
            Отправить
        </button>
    </form>
@stop