@extends('layouts.front.master')

@section('title', 'Платежи')

@section('content')
    @if(!$payments->isEmpty())
        <table class="ui celled table">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Сумма</th>
                    <th>Тип платежа</th>
                    <th>Дата</th>
                </tr>
            </thead>
            @foreach($payments as $payment)
                <tr>
                    <td>#{{ $payment->id }}</td>
                    <td>{{ $payment->sum }} ₽</td>
                    <td>{{ $payment->type->name }}</td>
                    <td>{{ $payment->locale_created_at }}</td>
                </tr>
            @endforeach
        </table>
        {{ $payments->links() }}
    @else
        <div class="ui blue message large">У вас нет платежей</div>
    @endif
@endsection