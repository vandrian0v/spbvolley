@extends('layouts.front.master')

@section('title', $user->surname . ' ' . $user->name)

@section('nwtour_content')
    <div class="ui segment grid stackable profile-show">
        <div class="column nine wide">
            <div class="ui header">
                {{ $user->surname }} {{ $user->name }}
            </div>
            <span>
                {{ $user->ageAndBirthDate() }}
            </span>
        </div>
        <div class="column seven wide">
            @if ($user->height)
                <div>Рост: {{ $user->height }}см</div>
            @endif
            @if ($user->weight)
                <div>Вес: {{ $user->weight }}кг</div>
            @endif
            @if ($user->hobby)
                <div>Хобби: {{ $user->hobby }}</div>
            @endif
        </div>
    </div>

    @unless ($teams->isEmpty())
        <div style="overflow-x: auto">
            <table class="ui small slim table unstackable">
                <thead style="font-size: 0.8em;">
                    <th>Дата</th>
                    <th>Этап</th>
                    <th>Кат.</th>
                    <th>Партнер</th>
                    <th>Место</th>
                    <th>Рейтинг</th>
                </thead>
                <tbody>
                    @foreach ($teams as $team)
                        <tr>
                            <td>{{ $team->tournament->nwtour->localeDate }}</td>
                            <td>
                                <a href="{{ route('nwtour.info', $team->tournament->nwtour->id) }}">
                                    {{ $team->tournament->nwtour->title }}
                                </a>
                            </td>
                            <td>
                                {{ $team->tournament->category->name }}
                            </td>
                            <td>
                                {!! $team->usersLinks($user->id) !!}
                            </td>
                            <td>
                                {{ $team->rank }}
                            </td>
                            <td>
                                {{ $team->rating/$team->tournament->category->team_size }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endunless
@stop