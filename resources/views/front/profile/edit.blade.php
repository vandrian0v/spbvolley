@extends('layouts.front.master')

@section('title', 'Редактирование данных')

{{--<div class="ui vertical fluid pointing secondary menu">--}}
    {{--<a class="item {{ Route::is('auth.profile.trainings') ? 'active' : '' }}" href="{{ route('auth.profile.trainings') }}">--}}
        {{--Тренировки--}}
    {{--</a>--}}
    {{--<a class="item {{ Route::is('auth.profile.payments') ? 'active' : '' }}" href="{{ route('auth.profile.payments') }}">--}}
        {{--Платежи--}}
    {{--</a>--}}
    {{--<a class="item {{ Route::is('auth.profile.edit') ? 'active' : '' }}" href="{{ route('auth.profile.edit') }}">--}}
        {{--Редактирование данных--}}
    {{--</a>--}}
    {{--<a class="item {{ Route::is('auth.profile.feedback') ? 'active' : '' }}" href="{{ route('auth.profile.feedback') }}">--}}
        {{--Обратная связь--}}
    {{--</a>--}}
{{--</div>--}}

@push('css')
    <link rel="stylesheet" type="text/css" href="/datepicker/daterangepicker.min.css">
@endpush

@section('content')
    <form method="POST" class="ui form profile-form" action="{{ route('auth.profile.update') }}">
        @include('layouts.front.errors')

        {{ csrf_field() }}

        <div class="two fields">
            <div class="field left aligned">
                <label for="name">Фамилия</label>
                <input type="text" name="surname" placeholder="Фамилия" value="{{ $user->surname }}">
            </div>
            <div class="field">
                <label for="name">Имя</label>
                <input type="text" name="name" placeholder="Имя" value="{{ $user->name }}">
            </div>
        </div>
        <div class="field">
            <label for="name">Дата рождения</label>
            <input type="text" name="birth_date" placeholder="Дата рождения" value="{{ $user->birth_date }}">
        </div>
        <div class="two fields">
            <div class="field">
                <label for="name">Рост, см</label>
                <input type="text" name="height" placeholder="Рост, см" value="{{ $user->height }}">
            </div>
            <div class="field">
                <label for="name">Вес, кг</label>
                <input type="text" name="weight" placeholder="Вес, кг" value="{{ $user->weight }}">
            </div>
        </div>
        <div class="field">
            <label for="name">Хобби</label>
            <input type="text" name="hobby" placeholder="Хобби" value="{{ $user->hobby }}">
        </div>

        <div class="ui primary submit button">
            Сохранить
        </div>
    </form>
@stop

@push('javascript')
    <script src="/bower/moment/min/moment.min.js"></script>
    <script src="/datepicker/daterangepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $(function() {
                $('input[name="birth_date"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    format: 'YYYY-MM-DD',
                });
            });

            $('.profile-form').form({
                fields: {
                    surname     : {
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Введите фамилию'
                            }
                        ]
                    },
                    name     : {
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Введите имя'
                            }
                        ]
                    },
                    height: {
                        optional: true,
                        rules: [
                            {
                                type   : 'integer',
                                prompt : 'Рост должен быть числом'

                            }
                        ]
                    },
                    weight: {
                        optional: true,
                        rules: [
                            {
                                type   : 'integer',
                                prompt : 'Вес должен быть числом'

                            }
                        ]
                    },
                }
            });
        });
    </script>
@endpush