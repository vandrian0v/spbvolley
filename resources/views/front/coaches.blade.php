<section id="coaches">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Тренеры
            </div>
            <div class="orange-divider" style="margin-bottom: 20px;"></div>
        </div>
        <div class="coaches">
            @foreach($coaches as $coach)
                <a href="{{ route('coaches.show', $coach) }}" class="coach">
                    <div class="image" style="background-image: url({{ $coach->previewUrl() }});"></div>
                    <div class="text-block">
                        <div class="name">{{ $coach->name }}</div>
                        <div class="description">{{ $coach->status }}</div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</section>