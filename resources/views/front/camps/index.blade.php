@extends('layouts.front.master')

@section('title', 'Лагерь (кэмп) пляжного волейбола круглый год Испания, Таиланд, Канары, Греция, Словения, Турция, Марокко! Спортивные сборы по волейболу в разных странах')
@section('description', 'Лагеря и кэмпы по пляжному волейболу в теплых странах! Тренировки разного уровня! Хорошее питание и прекрасное настроение')
@section('keywords', 'Лагерь кэмп пляжного волейбола круглый год Испания Таиланд Канары Греция Словения Турция Марокко Спортивные сборы по волейболу в разных странах')

@section('content')
<section id="camps-header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 hidden-xs hidden-sm" style="padding-top: 60px;">
                <img src="/front/icons/logo.svg" style="width: 88px;height: 101px;">
            </div>
            <div class="col-md-6 text-center" style="padding: 100px 15px;">
                <img src="/front/icons/camps_colored.svg" style="width: 99px;height: 75px;">
                <h1 style="font-size: 47px;">Лагерь пляжного волейбола</h1>
                <p style="font-size: 18px;">
                    Тренировочные сборы по пляжному волейболу в различных странах мира
                </p>
            </div>
            <div class="col-md-3 hidden-xs hidden-sm" style="padding-top: 60px;font-weight: 300;color:white;line-height: 26px;font-size: 13px;">
                Телефон:<br>
                <strong style="font-size: 20px;">8 (965) 081 04 62</strong><br>
                9:00 - 22:00 без выходных
            </div>
        </div>
    </div>
</section>

@if ($futureCamps->isNotEmpty())
    <section id="camps-future" style="padding: 70px 0;">
        <div class="container">
            <div class="row text-center">
                <div class="header">
                    Будущие заезды
                </div>
                <div class="orange-divider"></div>
            </div>
        </div>
        @foreach ($futureCamps as $camp)
            <div class="camp-block">
                <div>
                    <div class="title">{{ $camp->title }}</div>
                    <div class="dates">Дата: <span>{{ $camp->localeDates }}</span></div>
                    <div class="description">{!! $camp->description !!}</div>
                    <a class="btn btn-primary" href="{{ route('camps.show', $camp) }}">
                        Подробно
                    </a>
                </div>
                <div class="camp-img" style="background: url({{ $camp->preview_url }}); background-size: cover;"></div>
            </div>
        @endforeach
    </section>
@endif

<section id="camps-info">
    <div class="container">
        <div class="col-sm-12 no-padding">
            <div class="camps-info-text">
                <div class="header">
                    О нас
                </div>
                <div class="orange-divider"></div>
                <p style="font-size: 16px; margin-top: 30px;">
                    Клуб пляжного волейбола "RIO" организовывает лагеря и тренировочные
                    сборы по пляжному волейболу в различных странах мира.
                    Это может быть интересно, во-первых, тем, кому наскучил привычный, "правильный"
                    пляжных отдых в шезлонгах и есть желание совместить приятное с полезным.
                    И, во-вторых, тем, кто хочет поднять уровень своего спортивного мастерства и физических кондиций.
                </p>
            </div>
            <div class="camps-info-block">
                <div class="col-sm-4">
                    <img src="/front/icons/324.png" style="width: 44px;">
                    <h5>Скучно не будет</h5>
                    <p>Вас окружают единомышленники, много волейбола, новых знакомств и общения</p>
                </div>
                <div class="col-sm-4">
                    <img src="/front/icons/325.png" style="width: 48px;">
                    <h5>Профессиональные тренеры</h5>
                    <p>Вы находитесь все дни под пристальным вниманием специалиста</p>
                </div>
                <div class="col-sm-4">
                    <img src="/front/icons/326.png" style="width: 46px">
                    <h5>Результат</h5>
                    <p>Рост спортивного мастерства и отличная физическая форма</p>
                </div>
            </div>
        </div>
    </div>
</section>

@if ($pastCamps->isNotEmpty())
<section id="camps-past" style="padding-top: 30px; padding-bottom: 30px; margin: 40px 0;">
    <div class="container">
        <div class="row">
            <div id="past-camps-carousel" class="carousel slide col-sm-7 no-padding" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                    @foreach ($pastCamps as $camp)
                        <div class="item @if($loop->first) active @endif">
                            <div style="background: url({{ $camp->preview_url }}); padding: 30%; background-size: cover;"></div>
                            <div class="past-camp-title">
                                <a href="{{ route('camps.show', $camp) }}">
                                    {{ $camp->title }}
                                </a>
                                <div class="date">Дата: <span>{{ $camp->locale_dates }}</span></div>
                            </div>
                        </div>

                        @if ($loop->first)
                        <div style="position: absolute; right: 0; bottom: 60px; z-index: 1;">
                            <a class="right carousel-circle" href="#past-camps-carousel" data-slide="prev">
                                <i class="arrow left"></i>
                            </a>

                            <a class="right carousel-circle" href="#past-camps-carousel" data-slide="next">
                                <i class="arrow right"></i>
                            </a>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-sm-5 no-padding">
                <div class="past-camps-block">
                    <div class="header">
                        Прошлые заезды
                    </div>
                    <div class="orange-divider" style="margin-bottom: 20px;"></div>
                    <div style="line-height: 26px; font-size: 15px;">
                        <p>Мы провели 9 лагерей в 7 странах мира: США, Таиланд, Испания, Греция, Словения, Италия и Египет.</p>
                        <p>115 человек провели с нами свой отпуск и доверили свою подготовку.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif

@include('front.coaches')

<section id="camps-reviews">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Отзывы
            </div>
            <div class="orange-divider"></div>
        </div>

        <div id="review-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
            <a class="left carousel-circle" href="#review-carousel" data-slide="prev">
                <i class="arrow left"></i>
            </a>

            <div class="carousel-inner">
                @foreach ($reviews as $review)
                    <div class="item @if($loop->first) active @endif">
                        <div class="author">{{ $review->author }}</div>
                        <div class="description">{{ $review->description }}</div>
                        <p class="review">{{ $review->review }}</p>
                    </div>
                @endforeach
            </div>

            <a class="right carousel-circle" href="#review-carousel" data-slide="next">
                <i class="arrow right"></i>
            </a>
        </div>
    </div>
</section>
@stop
