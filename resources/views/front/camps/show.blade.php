@extends('layouts.front.master')

@section('title', $camp->seo_title ?: $camp->title)
@section('description', $camp->seo_description)
@section('keywords', $camp->seo_keywords)

@if ($camp->future)
    @push('js')
        <script src="{{ elixir('js/camps_show.js') }}"></script>
    @endpush
@endif

@section('content')
    <section style="padding: 20px 0;" id="camps-show">
        <div class="container post-content">
            <h1 style="font-size: 35px;">{{ $camp->title }}</h1>
            <div style="font-size: 20px; color: #535353;">
                Дата проведения: <strong>{{ $camp->localeDates }}</strong>
            </div>
            <div class="orange-divider" style="margin-bottom: 30px;"></div>
            @if ($camp->future)
                <div class="text-center" style="margin-bottom: 30px;">
                    <button @click="showCampApplyModal" class="btn btn-primary camp-application-btn">Подать заявку в лагерь</button>
                </div>
            @endif
            <div>{!! $camp->description !!}</div>
            <hr>
            <div>{!! $camp->full_description !!}</div>
            @if ($camp->future)
                <div class="text-center" style="margin-top: 30px;">
                    <button @click="showCampApplyModal" class="btn btn-primary camp-application-btn">Подать заявку в лагерь</button>
                </div>
            @endif
        </div>

        @if ($camp->future)
            <modal name="camp-apply-modal" :adaptive="true" :y-pivot="0.2" :width="1170" height="auto">
                <camp-apply-form :camp_id="{{ $camp->id }}"></camp-apply-form>
            </modal>
        @endif
    </section>
@stop