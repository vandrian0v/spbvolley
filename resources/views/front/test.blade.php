@extends('layouts.front.master')

@section('content')
<section id="events-header" style="background: grey;">
    <div class="container">
        <div class="row">
            <div class="col-md-3 hidden-xs hidden-sm" style="padding-top: 60px;">
                <img src="/front/icons/logo.svg" style="width: 88px;height: 101px;">
            </div>
            <div class="col-md-6 text-center" style="padding: 100px 15px;">
                <img src="/front/icons/corporate_colored.svg" style="width: 99px;height: 75px;">
                <h1 style="font-size: 47px;">Корпоративный спорт и мероприятия</h1>
                <p style="font-size: 18px;">
                    Проводим и организовываем корпоративные и межкорпоративные соревнования по пляжному волейбол
                </p>
            </div>
            <div class="col-md-3 hidden-xs hidden-sm" style="padding-top: 60px;font-weight: 300;color:white;line-height: 26px;font-size: 13px;">
                Телефон:<br>
                <strong style="font-size: 20px;">8 (965) 081 04 62</strong><br>
                9:00 - 22:00 без выходных
            </div>
        </div>
    </div>
</section>

<section id="events-options" style="padding-top: 70px;">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Варианты мероприятий
            </div>
            <div class="orange-divider"></div>
        </div>

        <div class="schedule">
            <div class="panel-group" id="accordion">
                <div class="hidden fake-toggle" data-toggle="collapse" href="#collapse-1"></div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-parent="#accordion">
                        <h4 class="panel-title collapsed" data-toggle="collapse" href="#collapse-1">
                            Корпоративный спорт
                        </h4>
                        <div style="display: flex;align-items: center;">
                            <div class="circle collapsed" data-toggle="collapse" href="#collapse-1">
                                <i class="arrow left"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapse-1" class="panel-collapse collapse" aria-expanded="true">
                </div>

                <div class="hidden fake-toggle" data-toggle="collapse" href="#collapse-2"></div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-parent="#accordion">
                        <h4 class="panel-title collapsed" data-toggle="collapse" href="#collapse-2">
                            Тренировки
                        </h4>
                        <div style="display: flex;align-items: center;">
                            <div class="circle collapsed" data-toggle="collapse" href="#collapse-2">
                                <i class="arrow left"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapse-2" class="panel-collapse collapse" aria-expanded="true">
                </div>

                <div class="hidden fake-toggle" data-toggle="collapse" href="#collapse-3"></div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-parent="#accordion">
                        <h4 class="panel-title collapsed" data-toggle="collapse" href="#collapse-3">
                            Почему именно мы и почему волейбол?
                        </h4>
                        <div style="display: flex;align-items: center;">
                            <div class="circle collapsed" data-toggle="collapse" href="#collapse-3">
                                <i class="arrow left"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="collapse-3" class="panel-collapse collapse" aria-expanded="true">
                    asdasd
                </div>
            </div>
        </div>
    </div>
</section>

<section id="events-packages" style="padding-top: 70px;">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Пакеты предложений
            </div>
            <div class="orange-divider"></div>
        </div>
    </div>
</section>

<section id="events-proceeded" style="padding-top: 70px;">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Проведенные мероприятия
            </div>
            <div class="orange-divider"></div>
        </div>
    </div>
</section>

<section id="events-location" style="padding-top: 70px;">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Места проведения в Петербурге
            </div>
            <div class="orange-divider"></div>
        </div>
    </div>
</section>

<section id="events-apply" style="padding-top: 70px;">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Подача заявки на проведение мероприятия
            </div>
            <div class="orange-divider"></div>
        </div>
        <form class="row apply-form">
            <div class="col-sm-5 col-sm-offset-1">
                <input type="text" name="name" class="form-control" placeholder="Название организации">
                <input type="text" name="email" class="form-control" placeholder="Email">
                <input type="text" name="phone" class="form-control" placeholder="Телефон">
            </div>
            <div class="col-sm-5" style="height: 188px;">
                <textarea placeholder="Количество участников, место проведения и т.д." name="comment" class="form-control"></textarea>
            </div>
        </form>
        <div class="alert text-center" style="display:none; width: 60%; margin: 15px auto;"></div>
        <div class="row text-center" style="margin-top: 30px;">
            <button class="btn btn-primary apply-form-submit">Задать вопрос</button>
        </div>
    </div>
</section>
@stop