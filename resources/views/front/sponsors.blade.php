<section id="sponsors">
    <div class="container">
        <div class="sponsors">
            <div class="orange-divider" style="margin-top: 0;"></div>
            {{--<a href="https://vk.com/csc">--}}
            {{--<img  src="/image/coffeeshop_logo_scaled.jpg">--}}
            {{--</a>--}}
            {{--<a href="http://nwvolley.ru">--}}
            {{--<img  src="/image/szva_logo.jpg">--}}
            {{--</a>--}}
            {{--<a href="https://vk.com/ugliloungebar">--}}
            {{--<img  src="/image/ugli.jpg">--}}
            {{--</a>--}}
            {{--<a href="https://vk.com/goldars_flowers">--}}
            {{--<img src="/image/goldars.jpg">--}}
            {{--</a>--}}
            {{--<a href="http://spb.lazertag-portal.ru">--}}
            {{--<img src="/image/lazertag.png">--}}
            {{--</a>--}}
            @php
                $sponsors = App\Models\Sponsor::all();
            @endphp

            @foreach ($sponsors as $sponsor)
                <a href="{{ $sponsor->url }}">
                    <img src="{{ $sponsor->preview_url }}">
                </a>
            @endforeach
            {{--<a href="https://vk.com/csc">--}}
                {{--<img src="/image/cf.jpg">--}}
            {{--</a>--}}
            {{--<a href="http://kurort.ru">--}}
                {{--<img src="/image/krrt.png">--}}
            {{--</a>--}}
            {{--<a href="https://litres.ru">--}}
                {{--<img src="/image/litres.png">--}}
            {{--</a>--}}
            {{--<a href="http://sportklinika.ru">--}}
                {{--<img src="/image/sprtclnc.png">--}}
            {{--</a>--}}
            <div class="orange-divider" style="margin-top: 0;"></div>
        </div>
    </div>
</section>