<section id="trainings-apply">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                Записаться на тренировку
            </div>
            <div class="orange-divider"></div>
        </div>
        <div class="row apply-form">
            <div class="col-sm-6 col-sm-offset-2">
                {{--<input type="text" name="name" class="form-control" placeholder="ФИО">--}}
                {{--<input type="text" name="email" class="form-control" placeholder="Email">--}}
                <input type="text" name="phone" class="form-control" placeholder="Телефон">
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary apply-form-submit" onclick="yaCounter31562248.reachGoal('home_apply_click'); return true;">Записаться</button>
                {{--<select class="form-control" name="group">--}}
                    {{--<option value="">Группа</option>--}}
                    {{--@foreach ($groups as $group)--}}
                    {{--<option value="{{ $group->id }}">{{ $group->name }}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}
                {{--<textarea placeholder="Пожелания к тренировкам" name="comment" class="form-control"></textarea>--}}
            </div>
        </div>
        <div class="alert text-center" style="display:none; width: 60%; margin: 15px auto 0;"></div>
    </div>
</section>

@push('js')
<script>
    $('[data-toggle="popover"]').popover();

    $('button.apply-form-submit').click(function () {
        let button = $(this);
        let form = $('div.apply-form');
        let alert = $('#trainings-apply .alert');
        // let name = form.find('input[name="name"]').val();
        // let email = form.find('input[name="email"]').val();
        let phone = form.find('input[name="phone"]').val();
        // let comment = form.find('textarea[name="comment"]').val();
        // let group = form.find('select[name="group"]').val();

        if (!phone) {
            alert.show()
                .removeClass('alert-info')
                .addClass('alert-danger')
                .html('Не все обязательные поля заполнены.');
            return false;
        }

        $.ajax({
            url: '{{ route('api.training-application.store') }}',
            method: 'post',
            data: {
                // name: name,
                // email: email,
                phone: phone,
                // comment: comment,
                // group: group
            }
        }).done(function () {
            form.hide();
            button.hide();
            alert.show()
                .removeClass('alert-danger')
                .addClass('alert-info')
                .html('Ваша заявка успешно отправлена!');
        }).fail(function () {
            alert.show()
                .removeClass('alert-info')
                .addClass('alert-danger')
                .html('Произошла ошибка, попробуйте позднее.');
        });
    });
</script>
@endpush