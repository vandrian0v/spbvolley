@extends('layouts.front.master')

@section('title', 'Тренировки по волейболу | пляжный волейбол тренировки')
@section('description', 'Тренировки по пляжному волейболу во всех крытых центрах Санкт-Петербурга и на Крестовском острове.')
@section('keywords', 'Тренировки по волейболу пляжный волейбол пляжному пляжка')

@push('js')
<script src="{{ elixir('js/training_index.js') }}"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
</script>

@endpush

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.6/css/swiper.min.css"/>
    <style>
        #trainings-slider .swiper-slide, #trainings-slider .swiper-container.swiper-container-horizontal {
            max-height: 350px;
        }
    </style>
@endpush

@section('content')
<div id="trainings-index">
    <section id="trainings-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-xs hidden-sm" style="padding-top: 50px;">
                    <img src="/front/icons/logo.svg" style="width: 88px;height: 101px;">
                </div>
                <div class="col-md-4 text-center" style="padding: 50px 0 40px;">
                    <img src="/front/icons/whistle_colored.svg" style="width: 99px;height: 75px;">
                    <h1>Тренировки по волейболу</h1>
                    <p style="font-size: 18px;">
                        Команда профессионалов клуба «RIO»
                        обучит пляжному волейболу и направит к первым победам!
                    </p>
                    <p>
                        Пробная тренировка 550 рублей
                    </p>
                </div>
                <div class="col-md-4 hidden-xs hidden-sm" style="padding-top: 50px; padding-left: 50px; padding-right: 0; font-weight: 300;color:white;line-height: 26px;font-size: 13px;">
                    <div class="hidden-cc" style="display: inline-block;">
                        Телефон:<br>
                        <strong style="font-size: 20px;">8 (965) 081 04 62</strong><br>
                        9:00 - 22:00 без выходных
                    </div>
                    <div style="display: inline-block; float: right;">
                        <a class="btn btn-warning" style="display: block; font-size: 22px; margin-top: 5px;" @click="showTrainingApplyModal('')" onclick="yaCounter31562248.reachGoal('header_apply_click'); return true;">Записаться</a>
                        <a href="#trainings-schedule" class="btn btn-warning" style="margin-top: 10px; display: block;" onclick="yaCounter31562248.reachGoal('header_pay_click'); return true;">Оплатить</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="trainings-info">
        <div class="container" style="background-color: #fafafa;padding: 30px 45px 20px;">
            <div class="row">
                <div class="col-md-7 header">
                    Мы предлагаем вам тренировки в следующих форматах:
                </div>
            </div>
            <div class="orange-divider"></div>
            <div class="row trainings-icons">
                <div class="col-md-3" style="margin-top: 55px;">
                    <img src="/front/icons/41.png" style="margin-bottom: 28px;margin-top: 2px;">
                    <div class="sub-header">
                        Групповые<br> тренировки
                    </div>
                    <p class="info">4 уровня: новички, начальный, средний и продвинутый</p>
                </div>
                <div class="col-md-3 col-md-offset-1" style="margin-top: 55px;">
                    <img src="/front/icons/40.png" style="margin-bottom: 28px;">
                    <div class="sub-header">
                        Персональная<br> тренировка
                    </div>
                    <p class="info">Только Вы или набранная вами группа и пристальное внимание тренера</p>
                </div>
                <div class="col-md-3 col-md-offset-1" style="margin-top: 55px;">
                    <img src="/front/icons/42.png" style="margin-bottom: 28px; margin-top: 20px;">
                    <div class="sub-header">
                        Тренировки по<br> физической подготовке
                    </div>
                    <p class="info">Развитие общих и специальных физических навыков для пляжного волейбола</p>
                </div>
            </div>
        </div>
    </section>

    <section id="trainings-slider" style="max-height: 350px;">
        <swiper :options="swiperOption">
            <swiper-slide v-for="photo in photos">
                <div class="training-slider-image" :style="'background-image: url(' + photo + ');'"></div>
            </swiper-slide>
            {{--<div class="swiper-button-prev" slot="button-prev"></div>--}}
            {{--<div class="swiper-button-next" slot="button-next"></div>--}}
        </swiper>
    </section>

    <section id="training-rio-is">
        <div class="container">
            <div class="row text-center">
                <h2 class="header" style="margin: 0; font-weight: normal;">
                    Пляжный волейбол тренировки
                </h2>
                <div class="orange-divider" style="margin-bottom: 10px;"></div>
            </div>
            <div class="rio-is-list">
                <div class="rio-is-el">
                    <p>Отличная физическая форма</p>
                    <img style="height: 80px" src="/image/power.jpg">
                </div>
                <div class="rio-is-el">
                    <p>Тренировки только с профессионалами</p>
                </div>
                <div class="rio-is-el">
                    <p>Возможность постоянно развиваться</p>
                    <img style="height: 80px" src="/image/arr.jpg">
                </div>
            </div>
            <div class="rio-is-list">
                <div class="rio-is-el">
                    <p>Занятия одним из самых массовых видов спорта</p>
                </div>
                <div class="rio-is-el" style="flex-direction: column;align-items: center;">
                    <p>Новые знакомства и впечатления</p>
                    <img style="width: 40%;" src="/image/hands.jpg">
                </div>
                <div class="rio-is-el">
                    <p>Спортивные успехи и победы на турнирах</p>
                </div>
            </div>
        </div>
    </section>

    <section id="trainings-groups" style="padding: 40px 0;">
        <div class="container">
            <div class="row text-center">
                <div class="header">
                    Уровни групп
                </div>
                <div class="orange-divider" style="margin-bottom: 30px;"></div>
            </div>
            <div class="group-list">
                <div class="group">
                    <h4>Начальная группа</h4>
                    <p>
                        Для тех, кто только знакомится с пляжным волейболом или вспоминает его
                    </p>
                </div>
                <div class="group">
                    <h4>Средняя группа</h4>
                    <p>
                        Для тех, кто имеет хороший опыт игры на песке и мечтает совершенствоваться
                    </p>
                </div>
                <div class="group">
                    <h4>Продвинутая группа</h4>
                    <p>
                        Для тех, кто имеет большой опыт игры на песке или классическую спортшколу
                    </p>
                </div>
                <div class="group">
                    <h4>Детские группы</h4>
                    <p>
                        Развитие общей физической подготовки, постановка волейбольной техники
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="trainings-schedule">
        <div class="container">
            <div class="row text-center">
                <div class="header">
                    Расписание тренировок
                </div>
                <div class="orange-divider"></div>
            </div>
            <div class="row" style="margin-bottom: 38px;">
                <div class="col-md-12">
                    <h5>ОБОЗНАЧЕНИЯ:</h5>
                    <div class="icon sun" style="position: relative;top: 11px;margin-left: 21px;margin-right: 8px;"></div>
                    <span class="muted-text">– открытая площадка</span>
                    <div class="icon snow" style="position: relative;top: 10px;margin-left: 61px;margin-right: 8px;"></div>
                    <span class="muted-text">– закрытая площадка</span>
                </div>
            </div>

            <div class="schedule">
                <div class="panel-group" id="accordion" style="margin:25px 0">
                    @foreach ($groups as $group)
                        <div class="hidden fake-toggle" data-toggle="collapse" href="#collapse{{ $group->id }}"></div>
                        <div class="panel panel-default">
                            <div class="panel-heading" data-parent="#accordion">
                                <h4 class="panel-title collapsed" data-toggle="collapse" href="#collapse{{ $group->id }}">
                                    {{ $group->name }}
                                </h4>
                                <div style="display: flex;align-items: center;">
                                    <a class="btn btn-pay" @click="showTrainingApplyModal({{ $group->id }})" style="margin-right: 10px;" onclick="yaCounter31562248.reachGoal('table_apply_click'); return true;">Записаться</a>
                                    <a href="{{ route('trainings.purchase', $group->url) }}" class="btn btn-pay" onclick="yaCounter31562248.reachGoal('table_pay_click'); return true;">
                                        <img src="/front/icons/wallet.svg">
                                        Оплатить
                                    </a>
                                    <div class="circle collapsed" data-toggle="collapse" href="#collapse{{ $group->id }}">
                                        <i class="arrow left"></i>
                                    </div>
                                </div>
                            </div>
                            <div id="collapse{{ $group->id }}" class="panel-collapse collapse" aria-expanded="true">
                                <div class="schedule-weekdays">
                                    <div></div>
                                    <div>Пн</div>
                                    <div>Вт</div>
                                    <div>Ср</div>
                                    <div>Чт</div>
                                    <div>Пт</div>
                                    <div>Сб</div>
                                    <div>Вс</div>
                                </div>
                                @foreach ($group->trainings->groupBy('location_id') as $trainingsByLocation)
                                    @php
                                        $location = $trainingsByLocation->first()->location;
                                    @endphp
                                    <div class="schedule-row">
                                        <div class="schedule-cell">
                                            <div class="icon {{ $location->new_icon }}"></div>
                                            <div class="location-text">
                                                <h4>{{ $location->short_name }}</h4>
                                                <span>{{ $location->address }}</span>
                                            </div>
                                        </div>
                                        @foreach (range(0, 6) as $i)
                                            <div class="schedule-cell">
                                                @if ($trainingsByLocation->groupBy('weekday')->has($i))
                                                    @foreach ($trainingsByLocation->groupBy('weekday')->get($i)->sortBy('start_time') as $training)
                                                        <div data-toggle="popover" data-placement="top" data-trigger="hover" data-html="true"
                                                             title="тренер: {{ $training->coach->name }}"
                                                             data-content="
                                                             @foreach ($training->subscriptionTypes as $type)
                                                                 {{ $type->name }}: {{ $type->pivot->price }}₽
                                                                 @if (!$loop->last)<hr>@endif
                                                             @endforeach
                                                             "
                                                        >
                                                            {{ $training->start_time }} - {{ $training->end_time }}
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include('front.coaches')

    <training-apply-form :groups="{{ json_encode($groups) }}"></training-apply-form>

    <modal name="trainings-apply-modal" :adaptive="true" :y-pivot="0.2" :width="1170" height="auto">
        <training-apply-form :groups="{{ json_encode($groups) }}" :group_id="modal_group_id"></training-apply-form>
    </modal>
</div>
@stop
