@extends('layouts.front.master')

@section('title', 'Оплата тренировок')

@section('content')
<section id="trainings-purchase" style="padding-top: 70px;padding-bottom: 70px;">
    <div class="container">
        <div class="row text-center">
            <div class="header">
                {{ $group->name }}
            </div>
            <div class="orange-divider"></div>
        </div>

        {{--<calculator :group-id="{{ $group->id }}">--}}
            {{--{{ csrf_field() }}--}}

            {{--@include('layouts.front.errors')--}}
        {{--</calculator>--}}

        <div class="row text-center">
            <div style="background: #fafafa;padding: 20px 0;">
                <h4>Выберите тип посещения</h4>
                <div class="flex-form">
                    <label><input type="radio" name="subscription_type">Разовое посещение</label>
                    <label><input type="radio" name="subscription_type">Абонемент на месяц</label>
                    <label><input type="radio" name="subscription_type">Абонемент на три месяца</label>
                </div>
            </div>

            <hr>
            <h4 style="margin-bottom: 40px">Выберите дни посещения</h4>
            <div class="training-day">
                <div class="text-left">
                    <label>
                        <input type="checkbox">
                        Вторник <span class="time">10:00 - 12:00</span>

                        <span class="location-info">
                            <span class="icon sun"></span>
                            Елагин <span class="dates">15 мая - 17 сентября</span>
                        </span>
                    </label>
                </div>
                <div style="display: flex;align-items: center;">
                    <div class="text-left">
                        <div class="coach-text">тренер</div>
                        <div style="font-weight: bold">Даниил Кувичка</div>
                        <div class="price">600₽</div>
                    </div>
                    <div style="background-image: url(/storage/coach/1/preview); width: 3em;height: 3em;border-radius: 500rem;margin-left: 15px;background-size: cover;"></div>
                </div>
            </div>
            <div class="calendar">
                <div class="calendar-left">
                    <div class="calendar-row calendar-month">
                        Май 2017
                    </div>
                    <div class="calendar-row calendar-week-days">
                        <div>Пн</div><div>Вт</div><div>Ср</div><div>Чт</div><div>Пт</div><div>Сб</div><div>Вс</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                </div>

                <div class="calendar-right">
                    <div class="calendar-row calendar-month">
                        Май 2017
                    </div>
                    <div class="calendar-row calendar-week-days">
                        <div>Пн</div><div>Вт</div><div>Ср</div><div>Чт</div><div>Пт</div><div>Сб</div><div>Вс</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                    <div class="calendar-row">
                        <div>1</div><div>2</div><div>3</div><div>4</div><div>5</div><div>6</div><div>7</div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="training-day">
                <div class="text-left">
                    <label>
                        <input type="checkbox">
                        Вторник (10:00 - 12:00) 600 руб.
                        {{--<span class="location-info">--}}
                            {{--<span class="icon snow"></span>--}}
                            {{--Елагин 15 мая - 17 сентября--}}
                        {{--</span>--}}
                    </label>
                </div>
                <div style="display: flex;align-items: center;">
                    Даниил Кувичка
                    <div style="background-image: url(/storage/coach/1/preview); width: 3em;height: 3em;border-radius: 500rem;margin-left: 15px;background-size: cover;"></div>
                </div>
            </div>
            <hr>
            <div class="training-day">
                <div class="text-left">
                    <label>
                        <input type="checkbox">
                        Вторник (10:00 - 12:00) 600 руб.
                        {{--<span class="location-info">--}}
                            {{--<span class="icon snow"></span>--}}
                            {{--Елагин 15 мая - 17 сентября--}}
                        {{--</span>--}}
                    </label>
                </div>
                <div style="display: flex;align-items: center;">
                    Даниил Кувичка
                    <div style="background-image: url(/storage/coach/1/preview); width: 3em;height: 3em;border-radius: 500rem;margin-left: 15px;background-size: cover;"></div>
                </div>
            </div>
            <hr>
            <div class="training-day">
                <div class="text-left">
                    <label>
                        <input type="checkbox">
                        Вторник (10:00 - 12:00) 600 руб.
                        {{--<span class="location-info">--}}
                            {{--<span class="icon snow"></span>--}}
                            {{--Елагин 15 мая - 17 сентября--}}
                        {{--</span>--}}
                    </label>
                </div>
                <div style="display: flex;align-items: center;">
                    Даниил Кувичка
                    <div style="background-image: url(/storage/coach/1/preview); width: 3em;height: 3em;border-radius: 500rem;margin-left: 15px;background-size: cover;"></div>
                </div>
            </div>
            <hr>
            <button class="btn btn-primary btn-lg">
                <span class="glyphicon glyphicon-credit-card" aria-hidden="true" style="top: 3px;"></span>
                Перейти к оплате
            </button>
        </div>
    </div>
</section>
@stop

@push('js')
{{--<script src="{{ elixir('js/training_calculator.js') }}"></script>--}}
@endpush