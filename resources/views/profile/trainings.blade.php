@extends('layouts.profile.master')

@section('title', 'Оплаченные тренировки')

@section('content')
    @if(!$trainings->isEmpty())
        <table class="ui celled table">
            <thead>
            <tr>
                <th>Дата</th>
                <th>Время</th>
                <th>Тренер</th>
                <th>Место</th>
            </tr>
            </thead>
            @foreach($trainings as $training)
                <tr class="{{ $training->date->isToday() ? 'positive' : ($training->date->isPast() ? 'disabled' : '') }}">
                    <td>{{ $training->localeDate }}</td>
                    <td>{{ $training->start_time }} - {{ $training->end_time }}</td>
                    <td>
                        <a href="{{ route('coaches.show', $training->coach) }}">
                            {{ $training->coach->name ?? '' }}
                        </a>
                    </td>
                    <td>{{ $training->location->short_name ?? '' }}</td>
                </tr>
            @endforeach
        </table>

        {{ $trainings->links() }}
    @else
        <div class="ui blue message large">У вас нет оплаченных тренировок</div>
    @endif
@endsection