@extends('layouts.admin')

@section('title', 'Лагеря')

@section('content')
  @include('admin.camps._list')
@endsection
