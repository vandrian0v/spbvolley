<ul class="nav nav-tabs primary">
  <li role="presentation" @if (!Route::is('admin.camps.applications*') && Route::is('admin.camps*')) class="active" @endif>
    <a href="{{ route('admin.camps.index') }}">Лагеря</a>
  </li>

  <li role="presentation" @if (Route::is('admin.camps.applications*')) class="active" @endif>
    <a href="{{ route('admin.camps.applications.index') }}">Заявки</a>
  </li>

  <li role="presentation" @if (Route::is('admin.reviews*')) class="active" @endif>
    <a href="{{ route('admin.reviews.index') }}">Отзывы</a>
  </li>
</ul>