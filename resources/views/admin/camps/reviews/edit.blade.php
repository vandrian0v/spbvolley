@extends('layouts.admin')

@section('title', sprintf('Редактирование отзыва от: %s', $review->author))

@section('content')
  <h2 class="page-header">Редактирование отзыва от: <small>{{ $review->author }}</small></h2>
  @include('admin.camps.reviews._form')
@endsection
