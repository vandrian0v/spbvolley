@extends('layouts.admin')

@section('title', 'Добавление отзыва')

@section('content')
  <h2 class="page-header">Добавление отзыва</h2>
  @include('admin.camps.reviews._form')
@endsection
