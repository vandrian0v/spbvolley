@push('scripts')
  <script src="/bower/tinymce/tinymce.min.js"></script>
@endpush

@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/reviews', $review->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($review->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Информация</legend>

    <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
      <label for="author" class="col-sm-2 control-label">Автор</label>
      <div class="col-sm-10">
        <input type="text" name="author" value="{{ old('author', $review->author) }}" id="author" class="form-control" placeholder="Автор отзыва" autocomplete="off">
        @if ($errors->has('author'))
          <span class="help-block">
            <strong>{{ $errors->first('author') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('review') ? ' has-error' : '' }}">
      <label for="review" class="col-sm-2 control-label">Текст отзыва</label>
      <div class="col-sm-10">
        <textarea class="form-control implement-html-editor-as-basic" rows="4" name="review" id="review">{{ old('review', $review->review) }}</textarea>
        @if ($errors->has('review'))
          <span class="help-block">
            <strong>{{ $errors->first('review') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
      <label for="description" class="col-sm-2 control-label">Описание</label>
      <div class="col-sm-10">
        <input type="text" name="description" value="{{ old('description', $review->description) }}" id="description" class="form-control" placeholder="Описание" autocomplete="off">
        @if ($errors->has('description'))
          <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($review->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.reviews.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
