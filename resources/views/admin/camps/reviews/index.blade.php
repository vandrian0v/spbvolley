@extends('layouts.admin')

@section('title', 'Отзывы о лагерях')

@section('content')
  @include('admin.camps.reviews._list')
@endsection
