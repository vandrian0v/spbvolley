@include('admin.common._alert')
@include('admin.camps._tabs')

<hr>

@include('admin.camps.reviews._well')

<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
      <tr>
        <th>Автор</th>
        <th>Лагерь</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($reviews as $review)
        <tr>
          <td>
            <div>
              <div class="text-muted">{{ $review->author }}</div>
            </div>
          </td>

          <td>
            <span class="text-middle text-muted">
              {{ $review->description }}
            </span>
          </td>

          <td>
            <div role="group" class="btn-group pull-right">
              <a href="{{ route('admin.reviews.edit', $review->id) }}" class="btn btn-raised">изменить</a>

              <form method="POST" action="{{ route('admin.reviews.destroy', $review->id) }}" role="form" id="delete-review-item-{{ $review->id }}" class="hidden">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>

              <button type="submit" form="delete-review-item-{{ $review->id }}" class="btn btn-raised btn-warning"
                data-confirm="Отзыв будет удален! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))">
                удалить
              </button>
            </div>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

