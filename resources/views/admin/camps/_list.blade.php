@include('admin.common._alert')
@include('admin.camps._tabs')

<hr>

@include('admin.camps._well')

<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
      <tr>
        <th>Название</th>
        <th>Дата</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($camps as $camp)
        <tr>
          <td>
            <div>
              <div class="text-muted">{{ $camp->title }}</div>
            </div>

            <div class="btn-group hidden" role="group">
              @include('admin.camps._ctrl')
            </div>
          </td>

          <td>
            <span class="text-middle text-muted">
              {{ $camp->localeDates }}
            </span>
          </td>

          <td>
            <span class="text-middle text-muted">
              {{ $camp->future ? 'Будущий' : 'Прошедший' }}
            </span>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
