@extends('layouts.admin')

@section('title', sprintf('Редактирование лагеря: %s', $camp->title))

@section('content')
  <h2 class="page-header">Редактирование лагеря: <small>{{ $camp->title }}</small></h2>
  @include('admin.camps._form')
@endsection
