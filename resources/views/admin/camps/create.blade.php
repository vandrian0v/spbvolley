@extends('layouts.admin')

@section('title', 'Добавление лагеря')

@section('content')
  <h2 class="page-header">Добавление лагеря</h2>
  @include('admin.camps._form')
@endsection
