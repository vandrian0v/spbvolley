<ul class="dropdown-menu">
  <li @if (request('sort') == 'date_asc') class="active" @endif>
    <a href="{{ route('admin.camps.index', ['sort' => 'date_asc']) }}">По дате <span class="caret"></span></a>
  </li>

  <li class="@if (request('sort') == 'date_desc') active @endif dropup">
    <a href="{{ route('admin.camps.index', ['sort' => 'date_desc']) }}">По дате <span class="caret"></span></a>
  </li>

  <li @if (request('sort') == 'title_asc') class="active" @endif>
    <a href="{{ route('admin.camps.index', ['sort' => 'title_asc']) }}">По названию <span class="caret"></span></a>
  </li>

  <li class="@if (request('sort') == 'title_desc') active @endif dropup">
    <a href="{{ route('admin.camps.index', ['sort' => 'title_desc']) }}">По названию <span class="caret"></span></a>
  </li>
</ul>
