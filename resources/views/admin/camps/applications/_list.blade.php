@include('admin.common._alert')
@include('admin.camps._tabs')

<hr>

<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
      <tr>
        <th>ФИО</th>
        <th>Лагерь</th>
        <th>Email</th>
        <th>Телефон</th>
        <th>Дата заявки</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($applications as $application)
        <tr>
          <td>
            <div class="text-muted">
              {{ $application->name }}
            </div>

            <div class="btn-group hidden" role="group">
              @include('admin.camps.applications._ctrl')
            </div>
          </td>

          <td>
            <span class="text-middle">
              {{ $application->camp->title }}
            </span>
          </td>

          <td>
            <span class="text-middle">
              <i class="fa fa-envelope-o" aria-hidden="true"></i>
              <a href="mailto:{{ $application->email }}" class="text-muted">{{ $application->email }}</a>
            </span>
          </td>

          <td>
            <span class="text-middle">
              <i class="fa fa-phone" aria-hidden="true"></i>
              <span class="text-muted">{{ $application->phone }}</span>
            </span>
          </td>

          <td>
            <span class="text-middle text-muted">
              {{ $application->date }}
            </span>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
