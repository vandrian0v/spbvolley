@extends('layouts.admin')

@section('title', 'Заявки в лагерь')

@section('content')
  @include('admin.camps.applications._list')
@endsection
