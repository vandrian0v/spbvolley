@extends('layouts.admin')

@section('title', sprintf('Заявки в лагерь от %s', $application->name))

@section('content')
  @include('admin.camps.applications._card')
@endsection
