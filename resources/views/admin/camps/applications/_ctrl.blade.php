<a href="{{ route('admin.camps.applications.show', $application->id) }}" class="btn btn-primary btn-xs" role="button">
  <i class="fa fa-eye" aria-hidden="true"></i> Открыть
</a>

<form method="POST" action="{{ route('admin.camps.applications.destroy', $application->id) }}" role="form" id="delete-application-item-{{ $application->id }}" class="hidden">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}
</form>
<button type="submit" form="delete-application-item-{{ $application->id }}" class="btn btn-danger btn-xs" data-confirm="Мероприятие будет удалено! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))">
  <i class="fa fa-trash" aria-hidden="true"></i> Удалить
</button>
