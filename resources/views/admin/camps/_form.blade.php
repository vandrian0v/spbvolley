@push('scripts')
  <script src="{{ elixir('js/admin/photos.js') }}"></script>
  <script src="/bower/tinymce/tinymce.min.js"></script>
@endpush

@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/camps', $camp->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($camp->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Информация</legend>

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
      <label for="title" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" name="title" value="{{ old('title', $camp->title) }}" id="title" class="form-control" placeholder="Название лагеря" autocomplete="off">
        @if ($errors->has('title'))
          <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('seo_title') ? ' has-error' : '' }}">
      <label for="seo_title" class="col-sm-2 control-label">Title</label>
      <div class="col-sm-10">
        <input type="text" name="seo_title" value="{{ old('seo_title', $camp->seo_title) }}" id="seo_title" class="form-control" placeholder="title" autocomplete="off">
        @if ($errors->has('seo_title'))
          <span class="help-block">
            <strong>{{ $errors->first('seo_title') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('seo_description') ? ' has-error' : '' }}">
      <label for="seo_description" class="col-sm-2 control-label">Description</label>
      <div class="col-sm-10">
        <input type="text" name="seo_description" value="{{ old('seo_description', $camp->seo_description) }}" id="seo_description" class="form-control" placeholder="description" autocomplete="off">
        @if ($errors->has('seo_description'))
          <span class="help-block">
            <strong>{{ $errors->first('seo_description') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('seo_keywords') ? ' has-error' : '' }}">
      <label for="seo_keywords" class="col-sm-2 control-label">Keywords</label>
      <div class="col-sm-10">
        <input type="text" name="seo_keywords" value="{{ old('seo_keywords', $camp->seo_keywords) }}" id="seo_keywords" class="form-control" placeholder="keywords" autocomplete="off">
        @if ($errors->has('seo_keywords'))
          <span class="help-block">
            <strong>{{ $errors->first('seo_keywords') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <label for="slider" class="col-sm-2 control-label" style="margin-top: 6px;">Будущий</label>
      <div class="col-sm-10">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="future" id="slider" value="1"
            @if (old('future', $camp->future)) checked @endif >
          </label>
        </div>
      </div>
    </div>

    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
      <label for="start_date" class="col-sm-2 control-label">Дата начала</label>
      <div class="col-sm-10">
        <input type="date" name="start_date" value="{{ old('start_date', $camp->start_date) }}" id="start_date" class="form-control" placeholder="2016-06-20" autocomplete="off">
        @if ($errors->has('start_date'))
          <span class="help-block">
            <strong>{{ $errors->first('start_date') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
      <label for="end_date" class="col-sm-2 control-label">Дата окончания</label>
      <div class="col-sm-10">
        <input type="date" name="end_date" value="{{ old('end_date', $camp->end_date) }}" id="end_date" class="form-control" placeholder="2016-06-20" autocomplete="off">
        @if ($errors->has('end_date'))
          <span class="help-block">
            <strong>{{ $errors->first('end_date') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
      <label for="description" class="col-sm-2 control-label">Описание</label>
      <div class="col-sm-10">
        <textarea class="form-control implement-html-editor-as-middle" rows="4" name="description" id="description">{{ old('description', $camp->description) }}</textarea>
        @if ($errors->has('description'))
          <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('full_description') ? ' has-error' : '' }}">
      <label for="full_description" class="col-sm-2 control-label">Полное описание</label>
      <div class="col-sm-10">
        <textarea class="form-control implement-html-editor-as-middle" rows="4" name="full_description" id="full_description">
          {{ old('full_description', $camp->full_description) }}
        </textarea>
        @if ($errors->has('full_description'))
          <span class="help-block">
            <strong>{{ $errors->first('full_description') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('coaches') ? ' has-error' : '' }}">
      <label for="coaches" class="col-sm-2 control-label">Тренеры</label>
      <div class="col-sm-10">
        <select class="form-control" name="coaches[]" multiple id="coaches" title="Тренеры не выбраны">
          @foreach ($coaches as $coach)
            <option value="{{ $coach->id }}" {{ $camp->coaches->contains($coach->id) ? 'selected' : '' }}>
              {{ $coach->name }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('coaches'))
          <span class="help-block">
            <strong>{{ $errors->first('coaches') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <legend>Фотографии</legend>

    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
      <label for="preview" class="col-md-2 control-label">Превью</label>
      <div class="col-md-4">
        <div class="thumbnail">
          @if ($camp->hasPreview())
            <img src="{{ $camp->previewUrl() }}">
          @endif

          <div class="caption">
            <p>Изображение обновится после сохранения.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="preview" id="preview" accept="image/*">
      </div>
    </div>

    <div class="photos form-group{{ $errors->has('photos[]') ? ' has-error' : '' }}">
      <label class="col-md-2 control-label">Фотографии</label>
      <div class="col-md-4">
        <photos path="/admin/camps/photo/" :photos='{{ $camp->photosWithUrl() }}'></photos>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="photos[]" multiple id="photos" accept="image/*">
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($camp->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.camps.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
