<a href="{{ route('admin.camps.edit', $camp->id) }}" class="btn btn-primary btn-xs" role="button">
  <i class="fa fa-pencil" aria-hidden="true"></i> Редактировать
</a>

<form method="POST" action="{{ route('admin.camps.destroy', $camp->id) }}" role="form" id="delete-camp-item-{{ $camp->id }}" class="hidden">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}
</form>
<button type="submit" form="delete-camp-item-{{ $camp->id }}" class="btn btn-danger btn-xs" data-confirm="Лагерь будет удален! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))">
  <i class="fa fa-trash" aria-hidden="true"></i> Удалить
</button>
