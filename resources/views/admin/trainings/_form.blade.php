@push('scripts')
  <script src="{{ elixir('js/admin/training_edit.js') }}"></script>
@endpush

@include('admin.common._alert')
@include('admin.common._errors')

<form method="POST" action="{{ route('admin.trainings.update', $group->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}
  {{ method_field('PUT') }}

  <fieldset>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Название группы</label>
      <div class="col-sm-10">
        <input type="text" name="name" value="{{ old('name', $group->name) }}" id="name" class="form-control" placeholder="Название группы" autocomplete="off">
        @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Url</label>
      <div class="col-sm-10">
        <input type="text" name="url" value="{{ old('url', $group->url) }}" id="url" class="form-control" placeholder="Url" autocomplete="off">
        @if ($errors->has('url'))
          <span class="help-block">
            <strong>{{ $errors->first('url') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <div class="panel panel-default schedules">
    <div class="panel-body">
      @foreach ($locations as $location)
        <div class="row">
          <div class="col-md-12">
              <label>{{ $location->short_name }}</label>
              <schedule
                :location="{{ $location->id }}"
                :group="{{ $group->id }}"
                :coaches="coaches"
                :subscription-types="subscriptionTypes"
              >
              </schedule>
          </div>
        </div>
      @endforeach
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-4">
      <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
    </div>

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.trainings.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>