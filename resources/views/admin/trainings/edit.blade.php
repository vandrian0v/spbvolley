@extends('layouts.admin')

@section('title', sprintf('Редактирование тренировочной группы: %s', $group->name))

@section('content')
  <h2 class="page-header">Редактирование тренировочной группы: {{ $group->name }}</h2>
  @include('admin.trainings._form')
@endsection
