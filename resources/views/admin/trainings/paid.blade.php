@extends('layouts.admin')

@section('title', 'Оплаченные тренировки')

@section('content')
    @include('admin.common._alert')
    @include('admin.trainings._tabs')

    <hr>

    <div class="table-responsive">
        <table class="table table-middle table-list table-hover table-condensed">
            <tbody>
            @foreach ($trainings as $training)
                <tr>
                    <td>{{ $training->localeDate }}</td>
                    <td>
                        <a href="{{ route('admin.users.edit', $training->payment->user) }}">
                            {{ $training->payment->user->fullname }}
                        </a>
                    </td>
                    <td class="text-muted">{{ $training->description }}</td>
                    <td class="text-muted">
                        <a href="{{ route('admin.payments.show', $training->payment) }}" class="btn">
                            платеж
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $trainings->links('vendor.pagination.bootstrap') }}
    <div>
@endsection
