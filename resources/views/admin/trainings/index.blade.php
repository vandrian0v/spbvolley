@extends('layouts.admin')

@section('title', 'Тренировочные группы')

@section('content')
    @include('admin.common._alert')
    @include('admin.common._errors')
    @include('admin.trainings._tabs')

    <hr>

    @include('admin.trainings._well')

    <div class="table-responsive">
        <table class="table table-middle table-list table-hover table-small-padding">
            <tbody>
            @foreach($groups as $group)
                <tr>
                    <td>
                        <a href="{{ route('trainings.purchase', $group->url) }}" class="text-muted" target="_blank">
                            {{ $group->name }}
                            <span class="text-info">({{ $group->trainings->count() }})</span>
                        </a>
                    </td>
                    <td>
                        <form method="POST" action="{{ route('admin.trainings.order.incr', $group) }}" style="display: inline;">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button>
                                <i class="fa fa-arrow-up" aria-hidden="true"></i>
                            </button>
                        </form>
                        <form method="POST" action="{{ route('admin.trainings.order.decr', $group) }}" style="display: inline;">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button>
                                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                    <td>
                        <div role="group" class="btn-group pull-right">
                            <a href="{{ route('admin.trainings.edit', $group->id) }}" class="btn btn-raised btn-sm">изменить</a>
                            <form method="POST" action="{{ route('admin.trainings.destroy', $group->id) }}" role="form" id="delete-group-{{ $group->id }}" class="hidden">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <button type="submit" form="delete-group-{{ $group->id }}" class="btn btn-raised btn-warning btn-sm"
                                    data-confirm="Группа будет удалена! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))">
                                удалить
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
