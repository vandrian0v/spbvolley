<ul class="nav nav-tabs primary">
    <li role="presentation" @if (Route::is('admin.trainings.index')) class="active" @endif>
        <a href="{{ route('admin.trainings.index') }}">Расписание</a>
    </li>

    <li role="presentation" @if (Route::is('admin.trainings.paid')) class="active" @endif>
        <a href="{{ route('admin.trainings.paid') }}">Оплаченные тренировки</a>
    </li>
</ul>