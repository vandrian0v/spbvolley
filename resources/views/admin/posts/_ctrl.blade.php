<a href="{{ route('admin.posts.edit', $post->id) }}" class="btn btn-primary btn-xs" role="button">
  <i class="fa fa-pencil" aria-hidden="true"></i> Редактировать
</a>

<form method="POST" action="{{ route('admin.posts.destroy', $post->id) }}" role="form" id="delete-news-item-{{ $post->id }}" class="hidden">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}
</form>
<button type="submit" form="delete-news-item-{{ $post->id }}" class="btn btn-danger btn-xs" data-confirm="Статья будет удалена! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))">
  <i class="fa fa-trash" aria-hidden="true"></i> Удалить
</button>
