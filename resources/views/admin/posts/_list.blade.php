@include('admin.common._alert')
@include('admin.common._errors')

@include('admin.posts._well')

<div class="table-responsive">
  <table class="table table-middle table-list table-hover">
    <thead>
      <tr>
        <th>Название</th>
        <th>Категория</th>
        <th>Дата публикации</th>
      </tr>
    </thead>
    <tbody>
      @foreach($posts as $post)
        <tr>
          <td>
            <div>
              <a href="{{ route('posts.show', $post->url) }}" class="text-muted" target="_blank">{{ $post->title }}</a>
            </div>

            <div class="btn-group hidden" role="group" aria-label="ctrl-news-item">
              @include('admin.posts._ctrl')
            </div>
          </td>

          <td>
            <span>{{ $post->category->name }}</span>
          </td>

          <td>
            <span>{{ $post->localeDate }}</span>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{ $posts->appends(['sort' => request('sort')])->links() }}
</div>
