@push('scripts')
  <script src="/bower/tinymce/tinymce.min.js"></script>
@endpush

@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/posts', $post->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($post->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
      <label for="title" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" name="title" value="{{ old('title', $post->title) }}" id="title" class="form-control" placeholder="Название новости" autocomplete="off">
        @if ($errors->has('title'))
          <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
      <label for="url" class="col-sm-2 control-label">Url</label>
      <div class="col-sm-10">
        <input type="text" name="url" value="{{ old('url', $post->url) }}" id="url" class="form-control" placeholder="Url новости" autocomplete="off">
        @if ($errors->has('url'))
          <span class="help-block">
            <strong>{{ $errors->first('url') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
      <label for="category_id" class="col-sm-2 control-label">Категория</label>
      <div class="col-sm-10">
          <select class="form-control" name="category_id" id="category_id">
          @foreach ($categories as $category)
            <option value="{{ $category->id }}" {{ $post->category_id == $category->id ? 'selected' : '' }}>
              {{ $category->name }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('category_id'))
          <span class="help-block">
            <strong>{{ $errors->first('category_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('anons') ? ' has-error' : '' }}">
      <label for="anons" class="col-sm-2 control-label">Анонс</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-basic" name="anons" id="anons" placeholder="Короткий анонс">{{ old('anons', $post->anons) }}</textarea>
        @if ($errors->has('anons'))
          <span class="help-block">
            <strong>{{ $errors->first('anons') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
      <label for="content" class="col-sm-2 control-label">Текст</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-middle" name="content" id="content" placeholder="Текст новости">{{ old('content', $post->content) }}</textarea>
        @if ($errors->has('content'))
          <span class="help-block">
            <strong>{{ $errors->first('content') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
      <label for="image" class="col-md-2 control-label">Изображение</label>
      <div class="col-md-3">
        <div class="thumbnail">
          @if ($post->hasImage())
            <img src="{{ $post->imageUrl() }}">
          @endif

          <div class="caption">
            Изображение обновится после сохранения.
          </div>
        </div>
      </div>

      <div class="col-md-7">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="image" id="image" accept="image/*">
      </div>
    </div>
  </fieldset>

  <fieldset>
    <div class="form-group">
      <label for="slider" class="col-sm-2 control-label" style="margin-top: 6px;">Слайдер</label>
      <div class="col-sm-10">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="slider" id="slider" value="1"
            @if (old('slider', $post->slider)) checked @endif >
          </label>
        </div>
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <div class="form-group{{ $errors->has('camp_id') ? ' has-error' : '' }}">
      <label for="camp_id" class="col-sm-2 control-label">Лагерь</label>
      <div class="col-sm-10">
        <select class="form-control" name="camp_id" id="camp_id" title="Не выбран">
          <option></option>
          @if ($post->camp_id)
            <option value="{{ $post->camp_id }}" selected>
              {{ $post->camp->title }}
            </option>
          @endif
          @foreach ($camps as $camp)
            @unless ($camp->id === $post->camp_id)
              <option value="{{ $camp->id }}">
                {{ $camp->title }}
              </option>
            @endunless
          @endforeach
        </select>
        @if ($errors->has('camp_id'))
          <span class="help-block">
            <strong>{{ $errors->first('camp_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
      <label for="location_id" class="col-sm-2 control-label">Местоположение</label>
      <div class="col-sm-10">
        <select class="form-control" name="location_id" id="location_id" title="Не выбрано">
          <option></option>
          @if ($post->location_id)
            <option value="{{ $post->location_id }}" selected>
              {{ $post->location->name }}
            </option>
          @endif
          @foreach ($locations as $location)
            @unless ($location->id === $post->location_id)
              <option value="{{ $location->id }}">
                {{ $location->name }}
              </option>
            @endunless
          @endforeach
        </select>
        @if ($errors->has('location_id'))
          <span class="help-block">
            <strong>{{ $errors->first('location_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('nwtour_id') ? ' has-error' : '' }}">
      <label for="nwtour_id" class="col-sm-2 control-label">Турнир</label>
      <div class="col-sm-10">
        <select class="form-control" name="nwtour_id" id="nwtour_id" title="Не выбран">
          <option></option>
          @foreach ($tours as $tour)
            <option value="{{ $tour->id }}" {{ $post->nwtour_id == $tour->id ? 'selected' : '' }}>
              {{ $tour->title }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('nwtour_id'))
          <span class="help-block">
            <strong>{{ $errors->first('nwtour_id') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($post->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.posts.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
