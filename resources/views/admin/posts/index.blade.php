@extends('layouts.admin')

@section('title', 'Новости')

@section('content')
  @include('admin.posts._list')
@endsection
