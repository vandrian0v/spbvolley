@extends('layouts.admin')

@section('title', 'Добавление новости')

@section('content')
  <h2 class="page-header">Добавление новости</h2>
  @include('admin.posts._form')
@endsection
