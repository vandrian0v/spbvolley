@extends('layouts.admin')

@section('title', sprintf('Редактирование новости: %s', $post->title))

@section('content')
  <h2 class="page-header">Редактирование новости: <small>{{ $post->title }}</small></h2>
  @include('admin.posts._form')
@endsection
