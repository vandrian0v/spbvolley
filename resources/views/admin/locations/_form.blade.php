@push('css')
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endpush

@push('scripts')
  <script src="{{ elixir('js/admin/photos.js') }}"></script>
  <script src="/bower/tinymce/tinymce.min.js"></script>
  <script src="/bower/moment/min/moment.min.js"></script>
  <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <script>
    $(function () {
      $('input[name=period_start_date]').daterangepicker({
          autoUpdateInput: false,
          singleDatePicker: true,
          locale: {
              format: 'DD.MM'
          }
      }).on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('DD.MM'));
      });
      $('input[name=period_end_date]').daterangepicker({
          autoUpdateInput: false,
          singleDatePicker: true,
          locale: {
              format: 'DD.MM'
          }
      }).on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('DD.MM'));
      });
    });
  </script>
@endpush

@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/locations', $location->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($location->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Информация</legend>

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" name="name" value="{{ old('name', $location->name) }}" id="name" class="form-control" placeholder="Полное название" autocomplete="off">
        @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('short_name') ? ' has-error' : '' }}">
      <label for="short_name" class="col-sm-2 control-label">Короткое название</label>
      <div class="col-sm-10">
        <input type="text" name="short_name" value="{{ old('short_name', $location->short_name) }}" id="short_name" class="form-control" placeholder="Короткое название" autocomplete="off">
        @if ($errors->has('short_name'))
          <span class="help-block">
            <strong>{{ $errors->first('short_name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
      <label for="description" class="col-sm-2 control-label">Описание</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-middle" name="description" id="description">{{ old('description', $location->description) }}</textarea>
        @if ($errors->has('description'))
          <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
      <label for="address" class="col-sm-2 control-label">Адрес</label>
      <div class="col-sm-10">
        <input type="text" name="address" value="{{ old('address', $location->address) }}" id="address" class="form-control" placeholder="Адрес" autocomplete="off">
        @if ($errors->has('address'))
          <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
      <label for="latitude" class="col-sm-2 control-label">Широта</label>
      <div class="col-sm-10">
        <input type="text" name="latitude" value="{{ old('latitude', $location->latitude) }}" id="latitude" class="form-control" placeholder="30,1234" autocomplete="off">
        @if ($errors->has('latitude'))
          <span class="help-block">
            <strong>{{ $errors->first('latitude') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
      <label for="longitude" class="col-sm-2 control-label">Долгота</label>
      <div class="col-sm-10">
        <input type="text" name="longitude" value="{{ old('longitude', $location->longitude) }}" id="longitude" class="form-control" placeholder="60,1234" autocomplete="off">
        @if ($errors->has('longitude'))
          <span class="help-block">
            <strong>{{ $errors->first('longitude') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('season') ? ' has-error' : '' }}">
      <label for="season" class="col-sm-2 control-label">Сезон</label>
      <div class="col-sm-10">
        <select class="form-control" name="season">
          <option value="summer" {{ $location->season === 'summer' ? 'selected' : '' }}>Летний</option>
          <option value="winter" {{ $location->season === 'winter' ? 'selected' : '' }}>Зимний</option>
        </select>
        @if ($errors->has('season'))
          <span class="help-block">
            <strong>{{ $errors->first('season') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('period_start_date') ? ' has-error' : '' }}">
      <label for="season" class="col-sm-2 control-label">Дата начала тренировок</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="period_start_date" value="{{ $location->formatted_period_start_date }}">
        @if ($errors->has('period_start_date'))
          <span class="help-block">
            <strong>{{ $errors->first('period_start_date') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('period_end_date') ? ' has-error' : '' }}">
      <label for="season" class="col-sm-2 control-label">Дата конца тренировок</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="period_end_date" value="{{ $location->formatted_period_end_date }}">
        @if ($errors->has('period_end_date'))
          <span class="help-block">
            <strong>{{ $errors->first('period_end_date') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <legend>Фотографии</legend>

    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
      <label for="preview" class="col-md-2 control-label">Превью</label>
      <div class="col-md-4">
        <div class="thumbnail">
          @if ($location->hasPreview())
            <img src="{{ $location->previewUrl() }}">
          @endif

          <div class="caption">
            <p>Изображение обновится после сохранения.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="preview" id="preview" accept="image/*">
      </div>
    </div>

    <div class="photos form-group{{ $errors->has('photos[]') ? ' has-error' : '' }}">
      <label class="col-md-2 control-label">Фотографии</label>
      <div class="col-md-4">
        <photos path="/admin/locations/photo/" :photos='{{ $location->photosWithUrl() }}'></photos>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="photos[]" multiple id="photos" accept="image/*">
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($location->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.locations.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
