@extends('layouts.admin')

@section('title', 'Места тренировок')

@section('content')
    @include('admin.common._alert')
    @include('admin.locations._well')

    <div class="table-responsive">
        <table class="table table-middle table-hover table-small-padding">
            <tbody>
            @foreach ($locations as $location)
                <tr>
                    <td>
                        <span class="text-middle text-nowrap">
                            @if ($location->hasPreview())
                                <a href="{{ $location->previewUrl() }}" data-lightbox="location-photo-{{ $location->id }}">
                                    <i class="fa fa-image" aria-hidden="true"></i>
                                </a>
                                @endif
                            <div style="display: inline" class="text-muted">{{ $location->name }}</div>
                        </span>
                    </td>
                    <td>
                        <span class="text-middle text-muted">
                            {{ $location->short_name }}
                        </span>
                    </td>
                    <td>
                        <div role="group" class="btn-group pull-right">
                            <a href="{{ route('admin.locations.edit', $location->id) }}" class="btn btn-raised btn-sm">изменить</a>

                            <form method="POST" action="{{ route('admin.locations.destroy', $location->id) }}" role="form" id="delete-location-{{ $location->id }}" class="hidden">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <button type="submit" form="delete-location-{{ $location->id }}" class="btn btn-raised btn-warning btn-sm"
                                    data-confirm="Тренер будет удален! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))"
                            >
                                удалить
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
