@extends('layouts.admin')

@section('title', sprintf('Редактирование местап проведения тренировок: %s', $location->name))

@section('content')
  <h2 class="page-header">Редактирование местап проведения тренировок: <small>{{ $location->name }}</small></h2>
  @include('admin.locations._form')
@endsection
