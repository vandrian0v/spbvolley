@extends('layouts.admin')

@section('title', 'Добавление места проведения тренировок')

@section('content')
  <h2 class="page-header">Добавление места проведения тренировок</h2>
  @include('admin.locations._form')
@endsection
