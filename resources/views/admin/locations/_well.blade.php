<div class="well">
  <div class="row">
    <div class="col-sm-5">
      <div class="btn-group">
        <a href="{{ route('admin.locations.create') }}" class="btn btn-info btn-raised">
          Новое местоположение<div class="ripple-container"></div>
        </a>
      </div>
    </div>
  </div>
</div>
