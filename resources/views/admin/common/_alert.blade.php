@if (session()->has('notice'))
  <div class="alert alert-dismissible alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div>
      <p><i class="fa fa-info-circle" aria-hidden="true"></i> {{ session('notice') }}</p>
    </div>
  </div>
@endif

@if (session()->has('warning'))
  <div class="alert alert-dismissible alert-warning">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div>
      <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ session('warning') }}</p>
    </div>
  </div>
@endif