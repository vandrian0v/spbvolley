@if (count($errors) > 0)
  <div class="row errors-row">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <h3 class="panel-title">Ошибка валидации</h3>
      </div>
      <div class="panel-body">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
@endif