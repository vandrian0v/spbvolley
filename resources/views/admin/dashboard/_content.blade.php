<div class="row">
  <div class="col-xs-12 col-sm-4">
    <h3>Последние платежи</h3>
    <hr>
    @foreach ($payments as $payment)
      <p>
        <a href="{{ route('admin.payments.show', $payment->id) }}" class="str-truncated">
          {{ $payment->sum }}₽
          <span class="text-muted">
            {{ $payment->user->formattedName }}
          </span>
        </a>

        <span class="pull-right">
          <time class="text-muted">{{ $payment->updated_at->diffForHumans() }}</time>
        </span>
      </p>
    @endforeach
  </div>

  <div class="col-xs-12 col-sm-4">
    <h3>Последние пользователи</h3>
    <hr>
    @foreach ($users as $user)
      <p>
        <a href="{{ route('admin.users.edit', $user->id) }}" class="str-truncated">{{ $user->fullname }}</a>
        <span class="pull-right">
          <time class="text-muted">{{ $user->created_at->diffForHumans() }}</time>
        </span>
      </p>
    @endforeach
  </div>

  <div class="col-xs-12 col-sm-4">
    <h3>Последние заявки</h3>
    <hr>
    @foreach ($teams as $team)
      <p>
        <span class="str-truncated">
          {{ $team->usersNames }}
        </span>
        <span class="pull-right">
          <time class="text-muted">
            {{--<a href="{{ route('admin.nwtour.teams.show', $team->tournament) }}">--}}
              {{ $team->tournament->category->fullname }}
            {{--</a>--}}
          </time>
        </span>
      </p>
    @endforeach
  </div>
</div>
