@extends('layouts.admin')

@section('content')
  @include('admin.dashboard._panels')
  @include('admin.dashboard._charts')
  @include('admin.dashboard._content')
@endsection
