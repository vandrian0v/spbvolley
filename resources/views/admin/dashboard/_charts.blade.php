@push('scripts')
    <script src="/bower/highcharts/highcharts.js"></script>
    <script src="{{ elixir('js/admin/charts.js') }}"></script>
@endpush

<div class="row charts">
    <chart type="payments"></chart>
    <chart type="users"></chart>
</div>