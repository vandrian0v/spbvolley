<div class="row placeholders">
  <div class="col-xs-12 col-sm-4 placeholder">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title"><strong>Платежи</strong></h3>
      </div>
      <div class="panel-body">
        <a href="{{ route('admin.payments.index') }}" class="card-number">{{ $paymentsCount }}</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-4 placeholder">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title"><strong>Пользователи</strong></h3>
      </div>
      <div class="panel-body">
        <a href="{{ route('admin.users.index') }}" class="card-number">{{ $usersCount }}</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-4 placeholder">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title"><strong>Заявки</strong></h3>
      </div>
      <div class="panel-body">
        <a href="{{ route('admin.tours.index') }}" class="card-number">{{ $teamsCount }}</a>
      </div>
    </div>
  </div>
</div>
