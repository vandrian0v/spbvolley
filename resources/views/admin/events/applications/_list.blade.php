@include('admin.common._alert')
@include('admin.events._tabs')

<hr>

<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
      <tr>
        <th>Название организации</th>
        <th>Email</th>
        <th>Телефон</th>
        <th>Дата заявки</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($applications as $application)
        <tr>
          <td>
            <div class="text-muted">
              {{ $application->name }}
            </div>

            <div class="btn-group hidden" role="group">
              @include('admin.events.applications._ctrl')
            </div>
          </td>

          <td>
            <span class="text-middle">
              <i class="fa fa-envelope-o" aria-hidden="true"></i>
              <a href="mailto:{{ $application->email }}" class="text-muted">{{ $application->email }}</a>
            </span>
          </td>

          <td>
            <span class="text-middle">
              <i class="fa fa-phone" aria-hidden="true"></i>
              <span class="text-muted">{{ $application->phone }}</span>
            </span>
          </td>

          <td>
            <span class="text-middle text-muted">
              {{ $application->date }}
            </span>
          </td>


          {{-- <td>
            <div role="group" class="btn-group pull-right">
              <a href="{{ route('admin.events.applications.show', $application->id) }}" class="btn btn-raised">открыть</a>

                <form method="POST" action="{{ route('admin.events.applications.destroy', $application->id) }}" role="form" id="delete-application-{{ $application->id }}" class="hidden">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                </form>

              <button type="submit" form="delete-application-{{ $application->id }}" class="btn btn-raised btn-warning"
                data-confirm="Заявка будет удалена! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))"
              >
                удалить
              </button>
            </div>
          </td> --}}
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
