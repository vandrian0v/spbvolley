<div class="form-horizontal">
  <fieldset>
    <div class="form-group">
      <label class="col-md-2 control-label">Название организации</label>

      <div class="col-md-10">
        <span class="form-control">{{ $application->name }}</span>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-2 control-label">Email</label>

      <div class="col-md-10">
        <span class="form-control">{{ $application->email }}</span>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-2 control-label">Телефон</label>

      <div class="col-md-10">
        <span class="form-control">{{ $application->phone }}</span>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-2 control-label">Комментарий</label>

      <div class="col-md-10">
        <div class="form-control" style="height:auto">{{ $application->comment }}</div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-2 control-label">Дата заявки</label>

      <div class="col-md-10">
        <span class="form-control">{{ $application->date }}</span>
      </div>
    </div>
  </fieldset>
</div>

<div class="form-group">
  <div class="col-md-4 col-md-offset-2">
    <a href="{{ route('admin.events.applications.index') }}" class="btn btn-default btn-raised">Назад</a>
  </div>
</div>