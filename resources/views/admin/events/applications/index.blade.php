@extends('layouts.admin')

@section('title', 'Заявки на мероприятия')

@section('content')
  @include('admin.events.applications._list')
@endsection
