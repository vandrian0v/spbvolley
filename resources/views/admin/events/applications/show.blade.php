@extends('layouts.admin')

@section('title', sprintf('Заявки на мероприятия от %s', $application->name))

@section('content')
  @include('admin.events.applications._card')
@endsection
