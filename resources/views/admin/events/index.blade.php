@extends('layouts.admin')

@section('title', 'Мероприятия')

@section('content')
  @include('admin.events._list')
@endsection
