@push('scripts')
  <script src="{{ elixir('js/admin/photos.js') }}"></script>
  <script src="/bower/tinymce/tinymce.min.js"></script>
@endpush

@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/events', $event->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($event->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Информация</legend>

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
      <label for="title" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" name="title" value="{{ old('title', $event->title) }}" id="title" class="form-control" placeholder="Название мероприятия" autocomplete="off">
        @if ($errors->has('title'))
          <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
      <label for="date" class="col-sm-2 control-label">Дата проведения</label>
      <div class="col-sm-10">
        <input type="date" name="date" value="{{ old('date', $event->date) }}" id="date" class="form-control" placeholder="2016-06-20" autocomplete="off">
        @if ($errors->has('date'))
          <span class="help-block">
            <strong>{{ $errors->first('date') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('participants') ? ' has-error' : '' }}">
      <label for="participants" class="col-sm-2 control-label">Информация об участниках</label>
      <div class="col-sm-10">
        <textarea class="form-control" rows="4" name="participants" id="participants">{{ old('participants', $event->participants) }}</textarea>
        @if ($errors->has('participants'))
          <span class="help-block">
            <strong>{{ $errors->first('participants') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
      <label for="description" class="col-sm-2 control-label">Описание</label>
      <div class="col-sm-10">
        <textarea class="form-control implement-html-editor-as-middle" rows="4" name="description" id="description">{{ old('description', $event->description) }}</textarea>
        @if ($errors->has('description'))
          <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
      <label for="location_id" class="col-sm-2 control-label">Место проведения</label>
      <div class="col-sm-10">
        <select class="form-control" name="location_id" style="width:auto" id="location_id">
          @foreach ($locations as $location)
            <option value="{{ $location->id }}" {{ $event->location_id == $location->id ? 'selected' : '' }}>
              {{ $location->name }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('location_id'))
          <span class="help-block">
            <strong>{{ $errors->first('location_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('services') ? ' has-error' : '' }}">
      <label for="services" class="col-sm-2 control-label">Предоставляемые услуги</label>
      <div class="col-sm-10">
        <select class="form-control" name="services[]" multiple id="services" title="Услуги не выбраны">
          @foreach ($services as $service)
            <option value="{{ $service->id }}" {{ $event->services->contains($service->id) ? 'selected' : '' }}>
              {{ $service->name }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('services'))
          <span class="help-block">
            <strong>{{ $errors->first('services') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <legend>Фотографии</legend>

    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
      <label for="preview" class="col-md-2 control-label">Превью</label>
      <div class="col-md-4">
        <div class="thumbnail">
          @if ($event->hasPreview())
            <img src="{{ $event->previewUrl() }}">
          @endif

          <div class="caption">
            <p>Изображение обновится после сохранения.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="preview" id="preview" accept="image/*">
      </div>
    </div>

    <div class="photos form-group{{ $errors->has('photos[]') ? ' has-error' : '' }}">
      <label class="col-md-2 control-label">Фотографии</label>
      <div class="col-md-4">
        <photos path="/admin/events/photo/" :photos='{{ $event->photosWithUrl() }}'></photos>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="photos[]" multiple id="photos" accept="image/*">
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($event->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.events.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
