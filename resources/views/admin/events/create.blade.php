@extends('layouts.admin')

@section('title', 'Добавление мероприятия')

@section('content')
  <h2 class="page-header">Добавление мероприятия</h2>
  @include('admin.events._form')
@endsection
