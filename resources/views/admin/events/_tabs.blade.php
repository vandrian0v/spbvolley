<ul class="nav nav-tabs primary">
  <li role="presentation" @if (!Route::is('admin.events.applications*') && Route::is('admin.events*')) class="active" @endif>
    <a href="{{ route('admin.events.index') }}">Мероприятия</a>
  </li>

  <li role="presentation" @if (Route::is('admin.events.applications*')) class="active" @endif>
    <a href="{{ route('admin.events.applications.index') }}">Заявки</a>
  </li>

  <li role="presentation" @if (Route::is('admin.packages*')) class="active" @endif>
    <a href="{{ route('admin.packages.index') }}">Пакеты</a>
  </li>

  <li role="presentation" @if (Route::is('admin.services*')) class="active" @endif>
    <a href="{{ route('admin.services.index') }}">Услуги</a>
  </li>
</ul>