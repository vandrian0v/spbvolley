@extends('layouts.admin')

@section('title', 'Услуги')

@section('content')
    @include('admin.common._alert')
    @include('admin.events._tabs')

    <hr>

    @include('admin.events.services._well')

    <div class="table-responsive">
        <table class="table table-middle table-hover table-small-padding">

            <tbody>
            @foreach ($services as $service)
                <tr>
                    <td>
                        <span class="text-middle text-muted">
                          {{ $service->name }}
                        </span>
                    </td>
                    <td>
                        <div role="group" class="btn-group pull-right">
                            <form method="POST" action="{{ route('admin.services.destroy', $service->id) }}" role="form" id="delete-service-{{ $service->id }}" class="hidden">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <button type="submit" form="delete-service-{{ $service->id }}" class="btn btn-raised btn-warning btn-sm"
                                    data-confirm="Услуга будет удалена! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))"
                            >
                                удалить
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
