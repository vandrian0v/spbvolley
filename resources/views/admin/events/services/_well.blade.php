<div class="well">
  <div class="row">
    <div class="col-sm-12">
      <form method="POST" action="{{ route('admin.services.store') }}" class="bs-component form-inline" accept-charset="UTF-8">
        {{ csrf_field() }}

        <div class="form-group label-floating is-empty col-sm-4" style="margin-top: 15px;">
          <label class="control-label" for="name">Название услуги</label>
          <div class="input-group col-xs-12">
            <input type="text" name="name" autocomplete="off" id="name" class="form-control" spellcheck="false">
          </div>
        </div>
        <div class="form-group col-sm-2" style="margin-top: 15px;">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-fab btn-fab-mini btn-primary">
              <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
          </span>
        </div>
      </form>
    </div>
  </div>
</div>
