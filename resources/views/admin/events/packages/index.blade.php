@extends('layouts.admin')

@section('title', 'Пакеты')

@section('content')
    @include('admin.common._alert')
    @include('admin.events._tabs')

    <hr>

    @include('admin.events.packages._well')

    <div class="table-responsive">
        <table class="table table-middle table-hover table-small-padding">
            <thead>
            <tr>
                <th>Название</th>
                <th>Длительность</th>
                <th>Стоимость</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($packages as $package)
                <tr>
                    <td>
                        <span class="text-muted text-middle">
                          {{ $package->name }}
                        </span>
                    </td>

                    <td>
                        <span class="text-muted text-middle">
                          {{ $package->duration }} ч.
                        </span>
                    </td>

                    <td>
                        <span class="text-muted text-middle">
                          {{ $package->price }} руб.
                        </span>
                    </td>

                    <td>
                        <div role="group" class="btn-group pull-right">
                            <a href="{{ route('admin.packages.edit', $package->id) }}" class="btn btn-raised btn-sm">изменить</a>

                            <form method="POST" action="{{ route('admin.packages.destroy', $package->id) }}" role="form" id="delete-package-{{ $package->id }}" class="hidden">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <button type="submit" form="delete-package-{{ $package->id }}" class="btn btn-raised btn-warning btn-sm"
                                    data-confirm="Тренер будет удален! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))"
                            >
                                удалить
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
