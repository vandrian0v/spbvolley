@extends('layouts.admin')

@section('title', sprintf('Редактирование пакета: %s', $package->name))

@section('content')
  <h2 class="page-header">Редактирование пакета: <small>{{ $package->name }}</small></h2>
  @include('admin.events.packages._form')
@endsection
