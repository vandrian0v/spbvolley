@extends('layouts.admin')

@section('title', 'Добавление пакета')

@section('content')
  <h2 class="page-header">Добавление пакета</h2>
  @include('admin.events.packages._form')
@endsection
