@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/packages', $package->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($package->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Информация</legend>

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" name="name" value="{{ old('name', $package->name) }}" id="name" class="form-control" placeholder="Название пакета" autocomplete="off">
        @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
      <label for="description" class="col-sm-2 control-label">Описание</label>
      <div class="col-sm-10">
        <input type="text" name="description" value="{{ old('description', $package->description) }}" id="description" class="form-control" placeholder="Описание пакета" autocomplete="off">
        @if ($errors->has('description'))
          <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
      <label for="duration" class="col-sm-2 control-label">Длительность, ч.</label>
      <div class="col-sm-10">
        <input type="text" name="duration" value="{{ old('duration', $package->duration) }}" id="duration" class="form-control" placeholder="Длительность" autocomplete="off">
        @if ($errors->has('duration'))
          <span class="help-block">
            <strong>{{ $errors->first('duration') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
      <label for="price" class="col-sm-2 control-label">Стоимость, руб.</label>
      <div class="col-sm-10">
        <input type="text" name="price" value="{{ old('price', $package->price) }}" id="price" class="form-control" placeholder="Стоимость" autocomplete="off">
        @if ($errors->has('price'))
          <span class="help-block">
            <strong>{{ $errors->first('price') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
      <label for="info" class="col-sm-2 control-label">Доп. информация</label>
      <div class="col-sm-10">
        <textarea name="info" id="info" class="form-control" autocomplete="off">{{ old('info', $package->info) }}</textarea>
        @if ($errors->has('info'))
          <span class="help-block">
            <strong>{{ $errors->first('info') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('services') ? ' has-error' : '' }}">
      <label for="services" class="col-sm-2 control-label">Предоставляемые услуги</label>
      <div class="col-sm-10">
        <select class="form-control selectpicker" name="services[]" multiple id="services" title="Услуги не выбраны">
          @foreach ($services as $service)
            <option value="{{ $service->id }}" {{ $package->services->contains($service->id) ? 'selected' : '' }}>
              {{ $service->name }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('services'))
          <span class="help-block">
            <strong>{{ $errors->first('services') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($package->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.packages.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
