@extends('layouts.admin')

@section('title', sprintf('Редактирование мероприятия: %s', $event->title))

@section('content')
  <h2 class="page-header">Редактирование мероприятия: <small>{{ $event->title }}</small></h2>
  @include('admin.events._form')
@endsection
