@include('admin.common._alert')
@include('admin.events._tabs')

<hr>

@include('admin.events._well')

<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
      <tr>
        <th>Название</th>
        <th>Дата</th>
        <th>Место</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($events as $event)
        <tr>
          <td class="col-md-9">
            <div>
              <div class="text-muted">{{ $event->title }}</div>
            </div>

            <div class="btn-group hidden" role="group">
              @include('admin.events._ctrl')
            </div>
          </td>

          <td>
            <span class="text-middle text-muted">
              {{ $event->localeDate }}
            </span>
          </td>

          <td>
            <span class="text-middle text-muted">
              {{ $event->location->short_name }}
            </span>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
