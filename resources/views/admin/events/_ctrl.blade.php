<a href="{{ route('admin.events.edit', $event->id) }}" class="btn btn-primary btn-xs" role="button">
  <i class="fa fa-pencil" aria-hidden="true"></i> Редактировать
</a>

<form method="POST" action="{{ route('admin.events.destroy', $event->id) }}" role="form" id="delete-event-item-{{ $event->id }}" class="hidden">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}
</form>
<button type="submit" form="delete-event-item-{{ $event->id }}" class="btn btn-danger btn-xs" data-confirm="Мероприятие будет удалено! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))">
  <i class="fa fa-trash" aria-hidden="true"></i> Удалить
</button>
