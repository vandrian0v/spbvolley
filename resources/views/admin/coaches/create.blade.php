@extends('layouts.admin')

@section('title', 'Добавление тренера')

@section('content')
  <h2 class="page-header">Добавление тренера</h2>
  @include('admin.coaches._form')
@endsection
