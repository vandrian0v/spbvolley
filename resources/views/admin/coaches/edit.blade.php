@extends('layouts.admin')

@section('title', sprintf('Редактирование тренера: %s', $coach->name))

@section('content')
  <h2 class="page-header">Редактирование тренера: <small>{{ $coach->name }}</small></h2>
  @include('admin.coaches._form')
@endsection
