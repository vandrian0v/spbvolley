@extends('layouts.admin')

@section('title', 'Тренеры')

@section('content')
    @include('admin.common._alert')
    @include('admin.coaches._well')

    <div class="table-responsive">
        <table class="table table-middle table-hover table-small-padding">
            <tbody>
            @foreach ($coaches as $coach)
                <tr>
                    <td>
                        <span class="text-middle text-nowrap">
                          @if ($coach->hasPreview())
                                <a href="{{ $coach->previewUrl() }}" data-lightbox="coach-photo-{{ $coach->id }}">
                              <i class="fa fa-image" aria-hidden="true"></i>
                            </a>
                            @endif
                            <div style="display: inline" class="text-muted">{{ $coach->name }}</div>
                        </span>
                    </td>
                    <td>
                        <span class="text-middle text-muted">
                          {{ $coach->status }}
                        </span>
                    </td>
                    <td>
                        <div role="group" class="btn-group pull-right">
                            <a href="{{ route('admin.coaches.edit', $coach->id) }}" class="btn btn-raised btn-sm">изменить</a>

                            <form method="POST" action="{{ route('admin.coaches.destroy', $coach->id) }}" role="form" id="delete-coach-{{ $coach->id }}" class="hidden">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <button type="submit" form="delete-coach-{{ $coach->id }}" class="btn btn-raised btn-warning btn-sm"
                                    data-confirm="Тренер будет удален! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))"
                            >
                                удалить
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
