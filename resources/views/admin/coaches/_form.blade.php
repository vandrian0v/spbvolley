@push('scripts')
  <script src="/bower/tinymce/tinymce.min.js"></script>
  <script src="{{ elixir('js/admin/photos.js') }}"></script>
@endpush

@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/coaches', $coach->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($coach->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Информация</legend>

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Имя</label>
      <div class="col-sm-10">
        <input type="text" name="name" value="{{ old('name', $coach->name) }}" id="name" class="form-control" placeholder="ФИО тренера" autocomplete="off">
        @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
      <label for="status" class="col-sm-2 control-label">Статус в клубе</label>
      <div class="col-sm-10">
        <input type="text" name="status" value="{{ old('status', $coach->status) }}" id="status" class="form-control" placeholder="Статус тренера" autocomplete="off">
        @if ($errors->has('status'))
          <span class="help-block">
            <strong>{{ $errors->first('status') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('career') ? ' has-error' : '' }}">
      <label for="career" class="col-sm-2 control-label">Карьера</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-basic" name="career" id="career">{{ old('career', $coach->career) }}</textarea>
        @if ($errors->has('career'))
          <span class="help-block">
            <strong>{{ $errors->first('career') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('education') ? ' has-error' : '' }}">
      <label for="education" class="col-sm-2 control-label">Образование</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-basic" name="education" id="education">{{ old('education', $coach->education) }}</textarea>
        @if ($errors->has('education'))
          <span class="help-block">
            <strong>{{ $errors->first('education') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('training') ? ' has-error' : '' }}">
      <label for="training" class="col-sm-2 control-label">Тренировки</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-basic" name="training" id="training">{{ old('training', $coach->training) }}</textarea>
        @if ($errors->has('training'))
          <span class="help-block">
            <strong>{{ $errors->first('training') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('teachers') ? ' has-error' : '' }}">
      <label for="teachers" class="col-sm-2 control-label">Учителя</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-basic" name="teachers" id="teachers">{{ old('teachers', $coach->teachers) }}</textarea>
        @if ($errors->has('teachers'))
          <span class="help-block">
            <strong>{{ $errors->first('teachers') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <legend>Фотографии</legend>

    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
      <label for="preview" class="col-md-2 control-label">Превью</label>
      <div class="col-md-4">
        <div class="thumbnail">
          @if ($coach->hasPreview())
            <img src="{{ $coach->previewUrl() }}">
          @endif

          <div class="caption">
            <p>Изображение обновится после сохранения.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="preview" id="preview" accept="image/*">
      </div>
    </div>

    <div class="photos form-group{{ $errors->has('photos[]') ? ' has-error' : '' }}">
      <label class="col-md-2 control-label">Фотографии</label>
      <div class="col-md-4">
        <photos path="/admin/coaches/photo/" :photos='{{ $coach->photosWithUrl() }}'></photos>
      </div>

      <div class="col-md-6">
        <input type="text" readonly class="form-control" placeholder="Загрузить...">
        <input type="file" name="photos[]" multiple id="photos" accept="image/*">
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($coach->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.coaches.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
