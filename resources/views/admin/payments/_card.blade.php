<div>
  <div class="table-responsive">
    <table class="table table-bordered">
      <tr>
        <td class="text-muted col-md-6">Пользователь</td>
        <td>
          <a href="{{ route('admin.users.edit', $payment->user->id) }}">
              {{ $payment->user->fullname }}
            </a>
        </td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Статус</td>
        <td>
          <span class="text-{{ $payment->statusClass }}">
            {{ $payment->statusText }}
          </span>
        </td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Тип</td>
        <td>{{ $payment->type->name }}</td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Сумма</td>
        <td>{{ $payment->sum }}</td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Сумма с комиссией</td>
        <td>{{ $payment->shop_sum_amount }}</td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Дата создания платежа</td>
        <td>{{ $payment->locale_created_at }}</td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Дата последнего изменения</td>
        <td>{{ $payment->locale_updated_at }}</td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Комментарий</td>
        <td>{!! str_replace("\n", '<br>', $payment->comment) !!}</td>
      </tr>

      <tr>
        <td class="text-muted col-md-6">Invoice id</td>
        <td>{{ $payment->invoice_id }}</td>
      </tr>
    </table>
  </div>
</div>

<div class="well">
  @if ($payment->payment_type_id === $payment::TYPE_TRAINING)
    <h4 class="text-muted">Тренировки</h4>
    <div class="table-responsive">
      <table class="table">
        @foreach ($payment->userTrainings as $training)
          <tr><td>{{ $training->locale_date }}</td><td>{{ $training->description }}</td></tr>
        @endforeach
      </table>
    </div>
  @elseif ($payment->payment_type_id === $payment::TYPE_TOURNAMENT_CONTRIBUTION)
    <h4 class="text-muted">Взнос</h4>
    <div class="btn-group btn-group-justified btn-group-raised">
      <a href="{{ route('admin.tours.tournament', [$payment->team->tournament->nwtour, $payment->team->tournament]) }}" class="btn">
        Категория {{ $payment->team->tournament->category->fullname }}
      </a>
      <a href="{{ route('admin.tours.edit', $payment->team->tournament->nwtour) }}" class="btn">
        {{ $payment->team->tournament->nwtour->title }}
      </a>
    </div>
  @endif
</div>

<div>
  <div class="col-md-4">
    <a href="{{ route('admin.payments.index') }}" class="btn btn-default btn-raised">К списку платежей</a>
  </div>
</div>