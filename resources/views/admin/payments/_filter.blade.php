<ul class="nav nav-pills">
    <li role="presentation" @unless(request()->has('filter')) class="active" @endunless>
        <a href="{{ route('admin.payments.index') }}">Подтвержденные <span class="badge">{{ $confirmedCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'all') class="active" @endif>
        <a href="{{ route('admin.payments.index', ['filter' => 'all']) }}">Все <span class="badge">{{ $allCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'canceled') class="active" @endif>
        <a href="{{ route('admin.payments.index', ['filter' => 'canceled']) }}">Отмененные <span class="badge">{{ $canceledCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'created') class="active" @endif>
        <a href="{{ route('admin.payments.index', ['filter' => 'created']) }}">Созданные <span class="badge">{{ $createdCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'checked') class="active" @endif>
        <a href="{{ route('admin.payments.index', ['filter' => 'checked']) }}">Проверенные <span class="badge">{{ $checkedCount }}</span></a>
    </li>
</ul>