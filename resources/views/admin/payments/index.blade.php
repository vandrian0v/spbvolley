@extends('layouts.admin')

@section('title', 'Платежи')

@section('content')
    @include('admin.common._alert')
    @include('admin.common._errors')
    @include('admin.payments._filter')

    <hr>

    @include('admin.payments._well')

    <div class="table-responsive">
        <table class="table table-middle table-hover table-small-padding">
            <tbody>
            @foreach ($payments as $payment)
                <tr>
                    <td>
                        <span class="text-muted">
                          #{{ $payment->id }}
                        </span>
                    </td>
                    <td>
                        <span class="text-muted">
                          {{ $payment->sum }} ₽
                        </span>
                    </td>
                    <td>
                        <span class="text-muted">
                            <a href="{{ route('admin.users.edit', $payment->user->id) }}">
                                {{ $payment->user->formattedName }}
                            </a>
                        </span>
                    </td>
                    <td>
                        <span class="text-muted" style="text-transform: uppercase;">
                            {{ $payment->type->name }}
                        </span>
                    </td>
                    <td>
                        <span class="text-{{ $payment->statusClass }}">
                            {{ $payment->statusText }}
                        </span>
                    </td>
                    <td>
                        <span class="text-muted">
                            {{ $payment->locale_updated_at }}
                        </span>
                    </td>
                    <td>
                        <div role="group" class="btn-group pull-right">
                            <a href="{{ route('admin.payments.show', $payment->id) }}" class="btn btn-sm btn-raised">открыть</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $payments->links() }}
    </div>
@endsection
