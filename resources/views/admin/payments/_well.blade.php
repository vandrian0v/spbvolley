<div class="well">
  <div class="row">
    <div class="col-sm-12">
      <form method="GET" action="{{ route('admin.payments.index') }}" class="bs-component" accept-charset="UTF-8">
        @if (request()->has('filter'))
          <input type="hidden" name="filter" value="{{ request('filter') }}">
        @endif
        <div class="form-group col-sm-3" style="margin: 0;">
          <label class="control-label" for="search">Поиск (пользователь/коммент)</label>
          <div class="input-group col-xs-12">
            <input type="text" name="search" autocomplete="off" id="search" class="form-control" value="{{ request('search')}}">
          </div>
        </div>
        <div class="form-group col-sm-3" style="margin: 0;">
          <label class="control-label" for="search">Тип</label>
          <div class="input-group col-xs-12">
            <select class="form-control" name="payment_type_id">
              <option @if (!request('payment_type_id')) selected @endif value="">Все</option>
              <option @if (request('payment_type_id') === '1') selected @endif value="1">Тренировки</option>
              <option @if (request('payment_type_id') === '2') selected @endif value="2">Взносы</option>
            </select>
          </div>
        </div>
        <div class="form-group is-focused col-sm-2" style="margin: 0;">
          <label class="control-label" for="after_date">Дата от</label>
          <div class="input-group col-xs-12">
            <input type="date" name="after_date" autocomplete="off" id="after_date" class="form-control" value="{{ request('after_date')}}">
          </div>
        </div>
        <div class="form-group is-focused col-sm-2" style="margin: 0;">
          <label class="control-label" for="before_date">Дата до</label>
          <div class="input-group col-xs-12">
            <input type="date" name="before_date" autocomplete="off" id="before_date" class="form-control" value="{{ request('before_date')}}">
          </div>
        </div>
        <div class="form-group col-sm-2" style="margin: 0;">
          <label>&nbsp;</label>
          <span class="input-group-btn">
            <button type="submit" class="btn btn-fab  btn-primary">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </span>
        </div>
      </form>
    </div>
  </div>
</div>