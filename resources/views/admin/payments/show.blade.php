@extends('layouts.admin')

@section('title', sprintf('Платеж #%d', $payment->id))

@section('content')
  <h2 class="page-header">Платеж <small>#{{ $payment->id }}</small></h2>
  @include('admin.payments._card')
@endsection
