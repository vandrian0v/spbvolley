@push('scripts')
  <script src="/bower/tinymce/tinymce.min.js"></script>
@endpush

@if ($tour->exists)
  <h2 class="page-header">Редактирование этапа: <small>{{ $tour->title }}</small>
    <a href="{{ route('admin.nwtour.csv-emails', $tour) }}" class="btn btn-sm btn-sup btn-raised users-download" style="margin-top: 5px;">
      <i class="fa fa-download" aria-hidden="true"></i>
      <span>Скачать email-ы</span>
    </a>
  </h2>
@else
  <h2 class="page-header">Добавление этапа</h2>
@endif

@include('admin.common._alert')
@include('admin.common._errors')

<form enctype="multipart/form-data" method="POST" action="{{ url('admin/nwtour', $tour->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($tour->exists)
  {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Информация</legend>

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
      <label for="title" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" name="title" value="{{ old('title', $tour->title) }}" id="title" class="form-control" placeholder="Название этапа" autocomplete="off">
        @if ($errors->has('title'))
          <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
      <label for="description" class="col-sm-2 control-label">Описание</label>
      <div class="col-sm-10">
        <textarea class="implement-html-editor-as-middle" name="description" id="description" placeholder="Описание этапа">{{ old('description', $tour->description) }}</textarea>
        @if ($errors->has('description'))
          <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
        @endif
      </div>
    </div>

    @if ($tour->exists)
    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
      <label for="status" class="col-sm-2 control-label">Статус</label>
      <div class="col-sm-10">
        <select class="selectpicker form-control" name="status">
          @foreach (App\Models\Tour::STATUS_TITLES as $key => $value)
            <option value="{{ $key }}" {{ $key == $tour->status ? 'selected' : '' }}>
                {{ $value }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('status'))
          <span class="help-block">
            <strong>{{ $errors->first('status') }}</strong>
          </span>
        @endif
      </div>
    </div>
    @endif
  </fieldset>

  <hr>

  <fieldset>
    <legend>Даты проведения</legend>

    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
      <label for="start_date" class="col-sm-2 control-label">Дата начала</label>
      <div class="col-sm-10">
        <input type="date" name="start_date" value="{{ old('start_date', $tour->start_date) }}" id="start_date" class="form-control datetimepicker" placeholder="2016-09-02" autocomplete="off">
        @if ($errors->has('start_date'))
          <span class="help-block">
            <strong>{{ $errors->first('start_date') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
      <label for="end_date" class="col-sm-2 control-label">Дата конца</label>
      <div class="col-sm-10">
        <input type="date" name="end_date" value="{{ old('end_date', $tour->end_date) }}" id="end_date" class="form-control datetimepicker" placeholder="2016-09-03" autocomplete="off">
        @if ($errors->has('end_date'))
          <span class="help-block">
            <strong>{{ $errors->first('end_date') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <legend>Координаты</legend>

    <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
      <label for="latitude" class="col-sm-2 control-label">Широта</label>
      <div class="col-sm-10">
        <input type="text" name="latitude" value="{{ old('latitude', $tour->latitude) }}" id="latitude" class="form-control" placeholder="30,321" autocomplete="off">
        @if ($errors->has('latitude'))
          <span class="help-block">
            <strong>{{ $errors->first('latitude') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
      <label for="longitude" class="col-sm-2 control-label">Долгота</label>
      <div class="col-sm-10">
        <input type="text" name="longitude" value="{{ old('longitude', $tour->longitude) }}" id="longitude" class="form-control" placeholder="60,123" autocomplete="off">
        @if ($errors->has('longitude'))
          <span class="help-block">
            <strong>{{ $errors->first('longitude') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <legend>Категории</legend>

    <div class="form-group{{ $errors->has('categories') ? ' has-error' : '' }}">
      <label for="categories" class="col-sm-2 control-label">Проводимые категории</label>

      <div class="col-sm-10">
        <select id="categories" name="categories[]" multiple class="form-control selectpicker" title="Категории не выбраны">
          @foreach ($categories as $category)
            <option value="{{ $category->id }}" {{ $tour->categories->contains($category->id) ? 'selected' : '' }}>
              {{ $category->fullname }}
            </option>
          @endforeach
        </select>
        @if ($errors->has('categories'))
          <span class="help-block">
            <strong>{{ $errors->first('categories') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($tour->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.nwtour.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
