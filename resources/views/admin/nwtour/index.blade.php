@extends('layouts.admin')

@section('title', 'NWTour')

@section('content')
    @include('admin.nwtour._tabs')
    <hr>
    @include('admin.common._alert')
    @include('admin.nwtour._filter')
    <hr>
    @include('admin.nwtour._well')

    <div class="table-responsive">
        <table class="table table-middle table-hover table-small-padding">
            <tbody>
                @foreach ($tours as $tour)
                <tr>
                    <td>
                        <span class="text-middle text-nowrap">
                             <div class="text-muted" style="display: inline">{{ $tour->title }}</div>
                        </span>
                    </td>
                    <td>
                        <span class="text-middle text-{{ $tour->statusClass }}">
                            {{ $tour->statusText }}
                        </span>
                    </td>
                    <td>
                        <span class="text-middle">
                            {{ $tour->localeDate }}
                        </span>
                    </td>
                    <td>
                        <div role="group" class="btn-group pull-right">
{{--                            <a href="{{ route('admin.nwtour.edit', $tour->id) }}" class="btn btn-sm btn-raised">изменить</a>--}}

                            <form method="POST" action="{{ route('admin.nwtour.destroy', $tour->id) }}" role="form"
                                  id="delete-tour-{{ $tour->id }}" class="hidden">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>

                            <button type="submit" form="delete-tour-{{ $tour->id }}" class="btn btn-sm btn-raised btn-warning"
                                    data-confirm="Этап будет удален! Вы уверены?"
                                    onclick="return confirm(this.getAttribute('data-confirm'))">
                                удалить
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
