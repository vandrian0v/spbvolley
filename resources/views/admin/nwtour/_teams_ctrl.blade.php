@if (!$team->payment || $team->payment->is_not_confirmed)
  <form method="POST" action="{{ route('admin.nwtour.team.update', $team->id) }}" role="form" id="update-team-item-{{ $team->id }}" class="hidden">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="hidden" name="is_paid" value="{{ $team->is_paid ? 0 : 1 }}">
  </form>
  <button type="submit" form="update-team-item-{{ $team->id }}" class="btn btn-{{ $team->is_paid ? 'danger' : 'info' }} btn-xs">
    <i class="fa fa-credit-card" aria-hidden="true"></i>
    @if ($team->is_paid)
      Отменить оплату
    @else
      Провести оплату
    @endif
  </button>
@endif


<form method="POST" action="{{ route('admin.nwtour.team.destroy', $team->id) }}" role="form" id="delete-team-item-{{ $team->id }}" class="hidden">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}
</form>
<button type="submit" form="delete-team-item-{{ $team->id }}" class="btn btn-danger btn-xs" data-confirm="Команда будет удалена! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))">
  <i class="fa fa-trash" aria-hidden="true"></i> Удалить
</button>