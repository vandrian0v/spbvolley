@extends('layouts.admin')

@section('title', sprintf('Редактирование этапа: %s, взнос', $tour->title))

@section('sidebar')
  @include('admin.nwtour._sidebar')
@endsection

@section('content')
  <h2 class="page-header">Редактирование этапа: <small>{{ $tour->title }}</small></h2>

  @include('admin.common._alert')
  @include('admin.common._errors')
  @include('admin.nwtour._tournament_tabs')

  <hr>

  <form method="POST" action="{{ route('admin.nwtour.parameters.update', $tournament->id) }}" role="form" class="form-horizontal" accept-charset="utf-8">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <fieldset>
      <legend>Параметры проводимой категории</legend>

      <div class="form-group{{ $errors->has('contribution') ? ' has-error' : '' }}">
        <label for="contribution" class="col-sm-2 control-label">Сумма взноса</label>
        <div class="col-sm-10">
          <input type="number" name="contribution" value="{{ old('contribution', $tournament->contribution) }}" id="contribution" class="form-control" placeholder="Взнос турнира в рублях" autocomplete="off">
          @if ($errors->has('contribution'))
            <span class="help-block">
              <strong>{{ $errors->first('contribution') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="form-group{{ $errors->has('limit') ? ' has-error' : '' }}">
        <label for="limit" class="col-sm-2 control-label">Лимит количества команд</label>
        <div class="col-sm-10">
          <input type="number" name="limit" value="{{ old('limit', $tournament->limit) }}" id="limit" class="form-control" placeholder="Ограничение кол-ва команд" autocomplete="off">
          @if ($errors->has('limit'))
            <span class="help-block">
              <strong>{{ $errors->first('limit') }}</strong>
            </span>
          @endif
        </div>
      </div>
    </fieldset>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    </div>

  </form>
@endsection
