<ul class="nav nav-tabs primary">
  <li role="presentation" @if (Route::is('admin.nwtour.parameters.show')) class="active" @endif>
    <a href="{{ route('admin.nwtour.parameters.show', $tournament->id) }}">Параметры</a>
  </li>

  <li role="presentation" @if (Route::is('admin.nwtour.teams.show')) class="active" @endif>
    <a href="{{ route('admin.nwtour.teams.show', $tournament->id) }}">Команды</a>
  </li>

  <li role="presentation" @if (Route::is('admin.nwtour.rankings.show')) class="active" @endif>
    <a href="{{ route('admin.nwtour.rankings.show', $tournament->id) }}">Результаты</a>
  </li>
</ul>