<div class="well">
  <div class="row">
    <div class="col-sm-5">
      <form method="GET" action="{{ route('admin.nwtour.index') }}" class="bs-component" accept-charset="UTF-8">
        @if (request()->has('filter'))
          <input type="hidden" name="filter" value="{{ request('filter') }}">
        @endif
        <div class="form-group label-floating is-empty" style="margin-top: 15px;">
          <label class="control-label" for="namesearch">Название/Описание</label>
          <div class="input-group">
            <input type="search" name="search" autocomplete="off" id="namesearch" class="form-control" spellcheck="false">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-fab btn-fab-mini btn-primary">
                <i class="fa fa-search" aria-hidden="true"></i>
              </button>
            </span>
          </div>
        </div>
      </form>
    </div>

    <div class="col-sm-6 col-sm-offset-1">
      <div class="btn-group pull-right">
        <div class="btn-group">
          <a href="#" data-target="#" class="btn btn-raised dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Сортировать <span class="caret"></span>
            <div class="ripple-container"></div>
          </a>
          @include('admin.nwtour._sort')
        </div>
        <a href="{{ route('admin.nwtour.create') }}" class="btn btn-info btn-raised">Новый этап<div class="ripple-container"></div></a>
      </div>
    </div>
  </div>
</div>
