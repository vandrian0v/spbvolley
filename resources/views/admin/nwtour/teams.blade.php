@extends('layouts.admin')

@section('title', sprintf('Редактирование этапа: %s, команды', $tour->title))

@section('sidebar')
  @include('admin.nwtour._sidebar')
@endsection

@section('content')
  <h2 class="page-header">Редактирование этапа: <small>{{ $tour->title }}</small></h2>

  @include('admin.common._alert')
  @include('admin.common._errors')
  @include('admin.nwtour._tournament_tabs')

  <hr>

  <div class="table-responsive">
    <table class="table table-middle table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th>Игроки</th>
          @if ($tournament->contribution)
            <th>Взнос</th>
          @endif
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($tournament->teams as $team)
          <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>
              <span>
                {!! $team->usersLinks() !!}
              </span>
            </td>
            @if ($tournament->contribution)
              <td>
                @if ($team->payment && $team->payment->is_confirmed)
                  <a href="{{ route('admin.payments.show', $team->payment) }}">{{ $team->payment->id }}</a>
                @elseif ($team->is_paid)
                  <span class="text-success">Вручную помечен как оплаченный</span>
                @else
                  <span class="text-warning">Не оплачен</span>
                @endif
              </td>
            @endif
            <td>
              <div role="group" class="pull-right btn-group" style="margin: 0;">
                @include('admin.nwtour._teams_ctrl')
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
