@extends('layouts.admin')

@section('title', sprintf('Редактирование этапа: %s', $tour->title))

@section('sidebar')
  @include('admin.nwtour._sidebar')
@endsection

@section('content')
  @include('admin.nwtour._form')
@endsection
