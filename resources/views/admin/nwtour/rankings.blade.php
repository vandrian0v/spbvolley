@extends('layouts.admin')

@section('title', sprintf('Редактирование этапа: %s, результаты', $tour->title))

@section('sidebar')
  @include('admin.nwtour._sidebar')
@endsection

@section('content')
  <h2 class="page-header">Редактирование этапа: <small>{{ $tour->title }}</small></h2>

  @include('admin.common._alert')
  @include('admin.common._errors')
  @include('admin.nwtour._tournament_tabs')

  <hr>

  <form method="POST" action="{{ route('admin.nwtour.rankings.update', $tournament->id) }}" class="table-responsive">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <table class="table table-middle table-hover">
      @foreach ($tournament->sortedTeams() as $team)
        <tr>
          <td>
            <select class="selectpicker col-md-auto" title="Место не выбрано" name="ranks[{{ $loop->index }}]" data-live-search="true">
              @foreach (range(1, $tournament->teams->count()) as $value)
                <option value="{{ $value }}" {{ $team->rank == $value ? 'selected' : '' }}>
                  {{ $value }}
                </option>
              @endforeach
            </select>

            <select class="selectpicker" title="Команда не выбрана" name="teams[{{ $loop->index }}]" data-live-search="true">
              @foreach ($tournament->teams as $t)
              <option value="{{ $t->id }}" {{ $team->rank && $team->id == $t->id ? 'selected' : '' }}>
                {{ $t->usersNames }}
              </option>
              @endforeach
            </select>
          </td>
        </tr>
      @endforeach
    </table>

    <div class="form-group">
      <div class="col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    </div>
  </form>
@endsection
