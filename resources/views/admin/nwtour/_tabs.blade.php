<ul class="nav nav-tabs primary">
    <li role="presentation" @if (Route::is('admin.nwtour.index')) class="active" @endif>
        <a href="{{ route('admin.nwtour.index') }}">Турниры</a>
    </li>

    <li role="presentation" @if (Route::is('admin.nwtour.ratings')) class="active" @endif>
        <a href="{{ route('admin.nwtour.ratings') }}">Рейтинг</a>
    </li>
</ul>