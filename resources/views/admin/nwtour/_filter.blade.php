<ul class="nav nav-pills">
    <li role="presentation" @if(!request()->has('filter') || request('filter') == 'future' ) class="active" @endif>
        <a href="{{ route('admin.nwtour.index', ['filter' => 'future']) }}">Будущие <span class="badge">{{ $futureCount }}</span></a>
    </li>

    <li role="presentation" @if(request('filter') == 'past') class="active" @endif>
        <a href="{{ route('admin.nwtour.index', ['filter' => 'past']) }}">Прошедшие <span class="badge">{{ $pastCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'created') class="active" @endif>
        <a href="{{ route('admin.nwtour.index', ['filter' => 'created']) }}">Созданные <span class="badge">{{ $createdCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'anounsed') class="active" @endif>
        <a href="{{ route('admin.nwtour.index', ['filter' => 'anounsed']) }}">Анонсированные <span class="badge">{{ $anounsedCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'opened') class="active" @endif>
        <a href="{{ route('admin.nwtour.index', ['filter' => 'opened']) }}">Открытые <span class="badge">{{ $openedCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'closed') class="active" @endif>
        <a href="{{ route('admin.nwtour.index', ['filter' => 'closed']) }}">Закрытые <span class="badge">{{ $closedCount }}</span></a>
    </li>

    <li role="presentation" @if (request('filter') == 'finished') class="active" @endif>
        <a href="{{ route('admin.nwtour.index', ['filter' => 'finished']) }}">Проведенные <span class="badge">{{ $finishedCount }}</span></a>
    </li>
</ul>