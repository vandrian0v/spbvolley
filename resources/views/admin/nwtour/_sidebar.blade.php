<ul class="nav nav-sidebar">
  <li class="{{ Route::is('admin.nwtour.edit') ? 'active' : ''}}">
    <a class="item" href="{{ route('admin.nwtour.edit', $tour->id) }}">
      Информация
    </a>
  </li>
</ul>
<hr>
<ul class="nav nav-sidebar">
  @foreach ($tour->tournaments as $t)
    <li class="{{ isset($tournament) && $t->id == $tournament->id ? 'active' : ''}}">
      <a class="item" href="{{ route('admin.nwtour.teams.show', [$t->id]) }}">
          {{ $t->category->fullname }}
      </a>
    </li>
  @endforeach
</ul>
