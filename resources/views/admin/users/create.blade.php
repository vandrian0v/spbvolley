@extends('layouts.admin')

@section('title', 'Новый пользователь')

@section('content')
  <h2 class="page-header">Новый пользователь</h2>
  @include('admin.users._form')
@endsection
