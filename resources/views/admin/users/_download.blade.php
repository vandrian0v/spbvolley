<a href="{{ route('admin.users.csv-emails') }}" class="btn btn-sm btn-sup btn-raised users-download">
    <i class="fa fa-download" aria-hidden="true"></i>
    <span>Скачать email-ы</span>
</a>

<a href="{{ route('admin.users.csv-phones') }}" class="btn btn-sm btn-sup btn-raised users-download">
    <i class="fa fa-download" aria-hidden="true"></i>
    <span>Скачать телефоны</span>
</a>
