<ul class="dropdown-menu">
  <li @if (request('sort') == 'name_asc') class="active" @endif>
    <a href="{{ route('admin.users.index', ['sort' => 'name_asc', 'page' => request('page')]) }}">По имени</a>
  </li>

  <li @if (request('sort') == 'surname_asc') class="active" @endif>
    <a href="{{ route('admin.users.index', ['sort' => 'surname_asc', 'page' => request('page')]) }}">По фамилии</a>
  </li>

  <li @if (request('sort') == 'id_desc') class="active" @endif>
    <a href="{{ route('admin.users.index', ['sort' => 'id_desc', 'page' => request('page')]) }}">Последние</a>
  </li>

  <li @if (request('sort') == 'id_asc') class="active" @endif>
    <a href="{{ route('admin.users.index', ['sort' => 'id_asc', 'page' => request('page')]) }}">Первые</a>
  </li>
</ul>
