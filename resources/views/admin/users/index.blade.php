@extends('layouts.admin')

@section('title', 'Пользователи')

@section('content')
    @include('admin.common._alert')
    @include('admin.users._filter')
    @include('admin.users._download')

    <hr>

    @include('admin.users._well')

    <div class="table-responsive">
        <table class="table table-middle table-hover table-small-padding">
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>
                        <span class="text-middle text-nowrap">
                            <i class="fa fa-{{ $user->gender }}" aria-hidden="true"></i>
                            <div class="text-muted" style="display: inline">{{ $user->fullname }}</div>
                        </span>

                        @if ($user->is_admin)
                            <span class="text-danger">(Админ)</span>
                        @endif
                    </td>
                    <td>
                        <span class="text-middle">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <a href="mailto:{{ $user->email }}" class="text-muted">{{ $user->email }}</a>
                        </span>
                    </td>
                    <td>
                        <span class="text-middle">
                            @if ($user->phone)
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="tel:{{ $user->phone }}" class="text-muted">{{ $user->phone }}</a>
                            @endif
                        </span>
                    </td>
                    <td>
                        <div role="group" class="btn-group pull-right">
                            @if ($user->trashed())
                                <form method="POST" action="{{ route('admin.users.restore', $user) }}" role="form" id="restore-user-{{ $user->id }}" class="hidden">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                </form>

                                <button type="submit" form="restore-user-{{ $user->id }}" class="btn btn-raised btn-success btn-sm"
                                        data-confirm="Пользователь будет восстановлен! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))"
                                >
                                    восстановить
                                </button>
                            @else
                                <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-raised btn-sm">изменить</a>

                                @unless(Auth::id() === $user->id)
                                    <form method="POST" action="{{ route('admin.users.destroy', $user) }}" role="form" id="delete-user-{{ $user->id }}" class="hidden">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                @endunless

                                <button type="submit" form="delete-user-{{ $user->id }}" class="btn btn-raised btn-warning btn-sm"
                                        data-confirm="Пользователь будет удален! Вы уверены?" onclick="return confirm(this.getAttribute('data-confirm'))"
                                        @if ($user->is_admin) disabled @endif
                                >
                                    удалить
                                </button>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $users->appends(['filter' => request('filter'), 'sort' => request('sort')])->links() }}
    </div>
@endsection
