@extends('layouts.admin')

@section('title', sprintf('Заявки пользователя: %s', $user->fullname))

@section('content')
  @include('admin.users._tabs')

  <hr>

  <h2 class="page-header">Заявки пользователя: {{ $user->fullname }}</h2>
  <div class="table-responsive">
      <table class="table table-middle table-hover">
        <tbody>
          @forelse ($teams as $team)
            <tr>
              <td>
                <span class="text-muted">
                  {!! $team->usersAdminLinks() !!}
                </span>
              </td>

              <td>
                <a href="{{ route('admin.tours.edit', $team->tournament->nwtour->id) }}" class="text-muted">
                  {{ $team->tournament->nwtour->title }}
                </a>
              </td>

              <td>
                <span class="text-muted">
                  {{ $team->tournament->category->fullname }}
                </span>
              </td>

              <td>
                @if ($team->payment_id)
                <a href="{{ route('admin.payments.show', $team->payment_id) }}" class="btn btn-primary">
                  Взнос
                </a>
                @endif
              </td>

            </tr>
          @empty
            <tr><td>Нет заявок</td></tr>
          @endforelse
        </tbody>
      </table>

      {{ $teams->links('vendor.pagination.bootstrap') }}
    </div>
@endsection
