<ul class="nav nav-tabs primary">
  <li role="presentation" @if (!Route::is('admin.users.payments') && !Route::is('admin.users.teams') && !Route::is('admin.users.trainings') && Route::is('admin.users*')) class="active" @endif>
    <a href="{{ route('admin.users.edit', $user) }}">Карточка</a>
  </li>

  <li role="presentation" @if (Route::is('admin.users.payments')) class="active" @endif>
    <a href="{{ route('admin.users.payments', $user) }}">Платежи</a>
  </li>

  <li role="presentation" @if (Route::is('admin.users.teams')) class="active" @endif>
    <a href="{{ route('admin.users.teams', $user) }}">Заявки</a>
  </li>

  <li role="presentation" @if (Route::is('admin.users.trainings')) class="active" @endif>
    <a href="{{ route('admin.users.trainings', $user) }}">Оплаченные тренировки</a>
  </li>
</ul>