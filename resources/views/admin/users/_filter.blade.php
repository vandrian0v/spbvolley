<ul class="nav nav-pills users-nav">
  <li role="presentation" @unless (request()->has('filter')) class="active" @endunless>
    <a href="{{ route('admin.users.index') }}">Активные <span class="badge">{{ $activeCount }}</span></a>
  </li>

  <li role="presentation" @if (request('filter') == 'admins') class="active" @endif>
    <a href="{{ route('admin.users.index', ['filter' => 'admins']) }}">Админы <span class="badge">{{ $adminsCount }}</span></a>
  </li>

  <li role="presentation" @if (request('filter') == 'pro') class="active" @endif>
    <a href="{{ route('admin.users.index', ['filter' => 'pro']) }}">PRO <span class="badge">{{ $proCount }}</span></a>
  </li>

  <li role="presentation" @if (request('filter') == 'a') class="active" @endif>
    <a href="{{ route('admin.users.index', ['filter' => 'a']) }}">A <span class="badge">{{ $aCount }}</span></a>
  </li>

  <li role="presentation" @if (request('filter') == 'b') class="active" @endif>
    <a href="{{ route('admin.users.index', ['filter' => 'b']) }}">B <span class="badge">{{ $bCount }}</span></a>
  </li>

  <li role="presentation" @if (request('filter') == 'trashed') class="active" @endif>
    <a href="{{ route('admin.users.index', ['filter' => 'trashed']) }}">Удаленные <span class="badge">{{ $trashedCount }}</span></a>
  </li>
</ul>