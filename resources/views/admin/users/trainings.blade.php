@extends('layouts.admin')

@section('title', sprintf('Оплаченные тренировки пользователя: %s', $user->fullname))

@section('content')
    @include('admin.users._tabs')

    <hr>

    <h2 class="page-header">Оплаченные тренировки пользователя: {{ $user->fullname }}</h2>
    <div class="table-responsive">
        <table class="table table-middle table-hover table-condensed">
            <tbody>
            @forelse ($trainings as $training)
                <tr>
                    <td class="text-muted">{{ $training->localeDate }}</td>
                    <td>
                        <a href="{{ route('admin.users.edit', $user) }}">
                            {{ $training->payment->user->fullname }}
                        </a>
                    </td>
                    <td class="text-muted">{{ $training->description }}</td>
                    <td class="text-muted">
                        <a href="{{ route('admin.payments.show', $training->payment) }}" class="btn">
                            платеж
                        </a>
                    </td>
                </tr>
            @empty
                <tr><td>Нет оплаченных тренировок</td></tr>
            @endforelse
            </tbody>
        </table>

        {{ $trainings->links('vendor.pagination.bootstrap') }}
    </div>
@endsection
