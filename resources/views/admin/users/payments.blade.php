@extends('layouts.admin')

@section('title', sprintf('Платежи пользователя: %s', $user->fullname))

@section('content')
  @include('admin.users._tabs')

  <hr>

  <h2 class="page-header">Платежи пользователя: {{ $user->fullname }}</h2>
  <div class="table-responsive">
      <table class="table table-middle table-hover">
        <tbody>
          @forelse ($payments as $payment)
            <tr>
              <td>
                <span class="text-muted">
                  #{{ $payment->id }}
                </span>
              </td>

              <td>
                <span class="text-muted">
                  {{ $payment->sum }} ₽
                </span>
              </td>

              <td>
                <span class="text-muted">
                  <a href="{{ route('admin.users.edit', $payment->user->id) }}">
                    {{ $payment->user->formattedName }}
                  </a>
                </span>
              </td>

              <td>
                <span class="text-muted" style="text-transform: uppercase;">
                  {{ $payment->type->name }}
                </span>
              </td>

              <td>
                <span class="text-{{ $payment->statusClass }}">
                  {{ $payment->statusText }}
                </span>
              </td>

              <td>
                <span class="text-muted">
                  {{ $payment->locale_updated_at }}
                </span>
              </td>

              <td>
                <div role="group" class="btn-group pull-right">
                  <a href="{{ route('admin.payments.show', $payment->id) }}" class="btn btn-raised">открыть</a>
                </div>
              </td>
            </tr>
          @empty
            <tr><td>Нет платежей</td></tr>
          @endforelse
        </tbody>
      </table>

      {{ $payments->links() }}
    </div>
@endsection
