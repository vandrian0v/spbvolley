@extends('layouts.admin')

@section('title', sprintf('Редактирование пользователя: %s', $user->fullname))

@section('content')
  @include('admin.users._tabs')

  <hr>

  <h2 class="page-header">Редактирование пользователя: {{ $user->fullname }}</h2>
  @include('admin.users._form')
@endsection
