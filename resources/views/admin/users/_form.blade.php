@include('admin.common._alert')
@include('admin.common._errors')

<form method="POST" action="{{ url('admin/users', $user->id) }}" role="form" class="form-horizontal" accept-charset="utf-8" autocomplete="off">
  {{ csrf_field() }}

  @if ($user->exists)
    {{ method_field('PUT') }}
  @endif

  <fieldset>
    <legend>Основное</legend>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Имя</label>
      <div class="col-sm-10">
        <input type="text" name="name" value="{{ old('name', $user->name) }}" id="name" class="form-control" placeholder="Имя" autocomplete="off" required>
        @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Фамилия</label>
      <div class="col-sm-10">
        <input type="text" name="surname" value="{{ old('surname', $user->surname) }}" id="surname" class="form-control" placeholder="Фамилия" autocomplete="off" required>
        @if ($errors->has('surname'))
          <span class="help-block">
            <strong>{{ $errors->first('surname') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email" class="col-sm-2 control-label">Email</label>
      <div class="col-sm-10">
        <input type="email" name="email" value="{{ old('email', $user->email) }}" id="email" class="form-control" placeholder="Email" autocomplete="off" required>
        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
      <label for="phone" class="col-sm-2 control-label">Телефон</label>
      <div class="col-sm-10">
        <input type="text" name="phone" value="{{ old('phone', $user->phone) }}" id="phone" class="form-control" placeholder="+7(999)8887766" autocomplete="off">
        @if ($errors->has('phone'))
          <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
      <label for="category_id" class="col-sm-2 control-label">Категория</label>
      <div class="col-sm-10">
        <select name="category_id" id="category_id" class="form-control">
          <option value="">Не выбрана</option>
          @foreach ($categories as $category)
            <option value="{{ $category->id }}" @if ($category->id === $user->category_id) selected @endif>{{ $category->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('category_id'))
          <span class="help-block">
            <strong>{{ $errors->first('category_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('training_level_id') ? ' has-error' : '' }}">
      <label for="training_level_id" class="col-sm-2 control-label">Уровень</label>
      <div class="col-sm-10">
        <select name="training_level_id" id="training_level_id" class="form-control">
          <option value="">Не выбран</option>
          @foreach ($trainingLevels as $trainingLevel)
            <option value="{{ $trainingLevel->id }}" @if ($trainingLevel->id === $user->training_level_id) selected @endif>{{ $trainingLevel->title }}</option>
          @endforeach
        </select>
        @if ($errors->has('training_level_id'))
          <span class="help-block">
            <strong>{{ $errors->first('training_level_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
      <label for="gender" class="col-sm-2 control-label">Пол</label>
      <div class="col-sm-10">
        <select name="gender" id="gender" class="form-control">
          <option value="male" @if ('male' === $user->gender) selected @endif>Мужской</option>
          <option value="female" @if ('female' === $user->gender) selected @endif>Женский</option>
        </select>
        @if ($errors->has('gender'))
          <span class="help-block">
            <strong>{{ $errors->first('gender') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <hr>

  <fieldset>
    <legend>Дополнительно</legend>
    <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
      <label for="note" class="col-sm-2 control-label">Примечание</label>
      <div class="col-sm-10">
        <input type="text" name="note" value="{{ old('note', $user->note) }}" id="note" class="form-control" placeholder="Примечание" autocomplete="off">
        @if ($errors->has('note'))
          <span class="help-block">
            <strong>{{ $errors->first('note') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
      <label for="height" class="col-sm-2 control-label">Рост</label>
      <div class="col-sm-10">
        <input type="text" name="height" value="{{ old('height', $user->height) }}" id="height" class="form-control" placeholder="Рост" autocomplete="off">
        @if ($errors->has('height'))
          <span class="help-block">
            <strong>{{ $errors->first('height') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
      <label for="weight" class="col-sm-2 control-label">Вес</label>
      <div class="col-sm-10">
        <input type="text" name="weight" value="{{ old('weight', $user->weight) }}" id="weight" class="form-control" placeholder="Вес" autocomplete="off">
        @if ($errors->has('weight'))
          <span class="help-block">
            <strong>{{ $errors->first('weight') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('hobby') ? ' has-error' : '' }}">
      <label for="hobby" class="col-sm-2 control-label">Хобби</label>
      <div class="col-sm-10">
        <input type="text" name="hobby" value="{{ old('hobby', $user->hobby) }}" id="hobby" class="form-control" placeholder="Хобби" autocomplete="off">
        @if ($errors->has('hobby'))
          <span class="help-block">
            <strong>{{ $errors->first('hobby') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
      <label for="birth_date" class="col-sm-2 control-label">Дата рождения</label>
      <div class="col-sm-10">
        <input type="text" name="birth_date" value="{{ old('birth_date', $user->birth_date) }}" id="birth_date" class="form-control" placeholder="1990-01-01" autocomplete="off">
        @if ($errors->has('birth_date'))
          <span class="help-block">
            <strong>{{ $errors->first('birth_date') }}</strong>
          </span>
        @endif
      </div>
    </div>

  </fieldset>

  <hr>

  <fieldset>
    <legend>Пароль</legend>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      <label for="password" class="col-sm-2 control-label">Пароль</label>
      <div class="col-sm-10">
        <input type="password" name="password" id="password" class="form-control" placeholder="Пароль" autocomplete="off">
        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
      <label for="password-confirm" class="col-sm-2 control-label">Подтверждение</label>
      <div class="col-sm-10">
        <input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="Подтверждение пароля" autocomplete="off">
        @if ($errors->has('password_confirmation'))
          <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </fieldset>

  <div class="form-group">
    @if ($user->exists)
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Сохранить</button>
      </div>
    @else
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-info btn-raised">Создать</button>
      </div>
    @endif

    <div class="col-sm-offset-2 col-sm-4">
      <a href="{{ route('admin.users.index') }}" class="btn btn-default btn-raised">Отмена</a>
    </div>
  </div>
</form>
