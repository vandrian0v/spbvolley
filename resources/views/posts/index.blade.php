@extends('layouts.main.master')

@section('title', 'Новости')

@section('content')
    <div class="ui segment news">
        <h2 class="ui header">Новости</h2>

        @foreach ($posts as $post)
            <h4 class="ui header post">
                <div class="post-date">
                    {{ $post->localeDate }}
                </div>
                <a class="post-link" href="{{ route('posts.show', $post->url) }}">
                    {{ $post->title }}
                </a>
                <a class="ui horizontal mini label" href="{{ route('posts.index', ['category' => $post->category->url]) }}">
                    {{ $post->category->name }}
                </a>
                <div class="sub header">
                    {!! $post->anons !!}
                </div>
            </h4>

            @if (!$loop->last)
                <div class="ui divider"></div>
            @endif
        @endforeach

        {!! $posts->links('vendor.pagination.semantic-ui')  !!}
    </div>
@stop