<?xml version="1.0" encoding="UTF-8"?>
<{{ $tag }}
@foreach ($attributes as $key => $value)
  {{ $key }}="{{ $value }}"
@endforeach />