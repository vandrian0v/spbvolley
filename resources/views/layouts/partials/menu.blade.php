<div class="ui menu five item fixed topmenu">
    <div class="ui container">
        <a class="toc item">
            <i class="sidebar icon"></i>
        </a>
        <a class="item" href="{{ route('trainings.index') }}">
            {{--<i class="sun icon"></i>--}}
            Тренировки
        </a>
        <a class="item" href="{{ route('events.index') }}">
            {{--<i class="checked calendar icon"></i>--}}
            Корпоративные <br>мероприятия
        </a>
        <a class="item" href="{{ route('camps.index') }}">
            {{--<i class="plane icon"></i>--}}
            Спортивные лагеря
        </a>
        <a class="item" href="{{ route('nwtour.index') }}">
            {{--<img src="/image/nwtour_icon_white.png">--}}
            NW-Tour
        </a>
        {{--<a class="item" href="{{ route('posts.index') }}">--}}
            {{--<i class="newspaper icon"></i>--}}
            {{--Новости--}}
        {{--</a>--}}
        @if (Auth::check())
            <div class="ui dropdown item">
                <span style="font-size: .8em;">
                    {{ Auth::user()->surname }} {{ Auth::user()->name }}
                </span> <i class="dropdown icon"></i>
                <div class="menu">
                    <a class="item" href="{{ route('auth.profile.trainings') }}">
                        <i class="edit icon"></i>
                        Личный кабинет
                    </a>
                    <a class="item" href="{{ route('auth.logout') }}">
                        <i class="sign out icon"></i>
                        Выйти
                    </a>
                </div>
            </div>
        @else
            <a class="item" href="{{ route('auth.login.show') }}">
                <i class="sign in icon"></i>
                Войти
            </a>
        @endif
    </div>
</div>