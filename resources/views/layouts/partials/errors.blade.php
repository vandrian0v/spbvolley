<div class="ui error message {{ count($errors) ? 'visible' : '' }}">
    <ul class="list">
        @unless (!count($errors))
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        @endunless
    </ul>
</div>

@if (session()->has('notice'))
    <div class="ui info message">
        {!! session('notice') !!}
    </div>
@endif

@if (session()->has('warning'))
    <div class="ui error message visible">
        {!! session('warning') !!}
    </div>
@endif