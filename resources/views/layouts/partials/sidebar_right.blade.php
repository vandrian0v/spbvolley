<div class="ui segment">
    <a href="{{ route('home') }}">
        <img class="ui image centered" src="/image/logo_scaled.png">
    </a>
</div>

@if (Request::url() === 'http://spbvolley.ru/nwtour/25')
    <div class="ui segment">
        <strong style="display: block;text-align: center;">Генеральный партнер</strong>
        <img style="margin-bottom: 2em;" class="ui image centered auto-height" src="/image/rosgeologia.png">
        <strong style="display: block;text-align: center;">Официальные спонсоры</strong>
        <img class="ui image centered auto-height" src="/image/avrora.jpg">
        <img style="margin-bottom: 2em;" class="ui image centered auto-height" src="/image/logo_komus.png">
        <strong style="display: block;text-align: center;">Официальный чай</strong>
        <img class="ui image centered auto-height" src="/image/tess.jpg">
    </div>
@endif

{{--<div class="ui card segment elagin center aligned">--}}
    {{--<h4 class="content header">--}}
        {{--<a href="/camps/25" style="color:black;">Greece camp 2018</a>--}}
    {{--</h4>--}}
    {{--<div>--}}
        {{--<a href="/camps/25">--}}
            {{--<img class="ui image fluid" src="/image/20s.jpg">--}}
        {{--</a>--}}
    {{--</div>--}}
    {{--<div class="content">--}}
        {{--Лагерь пляжного волейбола в Греции--}}
    {{--</div>--}}
    {{--<div class="extra content">--}}
        {{--Две смены: 2-12 мая 2018--}}
    {{--</div>--}}
{{--</div>--}}

<div class="ui segment vk-block">
    <div id="vk_groups"></div>
</div>