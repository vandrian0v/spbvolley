<div class="ui vertical medium inverted sidebar accordion menu">
    <a class="item" href="{{ route('trainings.index') }}">
        Тренировки
        {{--<i class="sun icon"></i>--}}
    </a>
    <a class="item" href="{{ route('events.index') }}">
        Корпоративные мероприятия
        {{--<i class="checked calendar icon"></i>--}}
    </a>
    <a class="item" href="{{ route('camps.index') }}">
        Спортивные лагеря
        {{--<i class="plane icon"></i>--}}
    </a>
    <a class="item" href="{{ route('nwtour.index') }}">
        NW-Tour
        {{--<img src="/image/nwtour_icon_filled.png">--}}
    </a>
    {{--<a class="item" href="{{ route('posts.index') }}">--}}
        {{--Новости--}}
        {{--<i class="newspaper icon"></i>--}}
    {{--</a>--}}
    @if (Auth::check())
        <div class="item">
            <a class="title">
                {{ Auth::user()->surname }} {{ Auth::user()->name }}<i class="dropdown icon"></i>
            </a>
            <div class="content menu">
                <a class="item" href="{{ route('auth.profile.trainings') }}">
                    <i class="edit icon"></i>
                    Личный кабинет
                </a>
                <a class="item" href="{{ route('auth.logout') }}">
                    <i class="sign out icon"></i>
                    Выйти
                </a>
            </div>
        </div>
    @else
        <a class="item" href="{{ route('auth.login.show') }}">
            <i class="sign in icon"></i>
            Войти
        </a>
    @endif
</div>