{{--@if (Request::url() !== 'http://spbvolley.ru/nwtour/25')--}}
    <div class="ui segment">
        <a href="{{ route('nwtour.index') }}">
            <img class="ui image centered" src="/image/nwtour_logo.jpg">
        </a>
    </div>

    @php
        $sponsors = App\Models\Sponsor::all();
    @endphp

    <div class="ui segment">
        @foreach ($sponsors as $sponsor)
            <a href="{{ $sponsor->url }}">
                <img style="width: 100%; height: auto; @if(!$loop->first) padding-top: 30px !important; @endif" src="{{ $sponsor->preview_url }}">
            </a>
        @endforeach
        {{--<a href="https://vk.com/csc">--}}
            {{--<img style="width: 100%; height: auto;" class="" src="/image/cf.jpg">--}}
        {{--</a>--}}
        {{--<a href="http://kurort.ru">--}}
            {{--<img style="width: 100%; height: auto; padding-top: 30px !important;" class=" " src="/image/krrt.png">--}}
        {{--</a>--}}
        {{--<a href="https://litres.ru">--}}
            {{--<img style="width: 100%; height: auto; padding-top: 30px !important;" class=" " src="/image/litres.png">--}}
        {{--</a>--}}
        {{--<a href="http://sportklinika.ru">--}}
            {{--<img style="width: 100%; height: auto; padding-top: 30px !important;" class=" " src="/image/sprtclnc.png">--}}
        {{--</a>--}}
    </div>
{{--@else--}}
    {{--<div class="ui segment">--}}
        {{--<img class="ui image centered auto-height" src="/image/nika1.jpg">--}}
        {{--<img class="ui image centered auto-height" src="/image/vfv.png">--}}
        {{--<img class="ui image centered auto-height" src="/image/szva_logo.jpg">--}}
    {{--</div>--}}

    {{--<div class="ui segment">--}}
        {{--<a href="{{ route('nwtour.index') }}">--}}
            {{--<img class="ui image centered" src="/image/nwtour_logo.jpg">--}}
        {{--</a>--}}
    {{--</div>--}}
{{--@endif--}}