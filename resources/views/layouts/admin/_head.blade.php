<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>
        RIO. Admin panel.
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Scada:400,400italic,700' type='text/css'>

  <link rel="stylesheet" type="text/css" href="/bower/bootstrap/dist/css/bootstrap.min.css">
  {{--<link rel="stylesheet" type="text/css" href="/bower/bootstrap-select/dist/css/bootstrap-select.min.css">--}}
  {{--<link rel="stylesheet" type="text/css" href="/bower/bootstrap-material-design/dist/css/bootstrap-material-design.min.css">--}}
  {{--<link rel="stylesheet" type="text/css" href="/bower/bootstrap-material-design/dist/css/ripples.min.css">--}}
  <link rel="stylesheet" type="text/css" href="/bower/lightbox2/dist/css/lightbox.min.css">
  <link rel="stylesheet" type="text/css" href="{{ elixir('css/admin/app.css') }}">

  <style>
    .navbar {
      color: #fff;
    }

    .navbar .navbar-nav>.active>a, .navbar .navbar-nav>.active>a:focus, .navbar .navbar-nav>.active>a:hover {
      color: inherit;
      background-color: rgba(255,255,255,.1);
    }
  </style>

  @stack('styles')

</head>
