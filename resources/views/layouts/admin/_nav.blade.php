<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #222;min-height:44px">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-logo" href="{{ route('admin.index') }}">
        <img src="/image/only_rio.png" height="20">
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">

        <li class="{{ Route::is('admin.payments*') ? 'active' : '' }}">
          <a href="{{ route('admin.payments.index') }}">
            Payments
          </a>
        </li>

        <li class="{{ Route::is('admin.tours*') ? 'active' : '' }}">
          <a href="{{ route('admin.tours.index') }}">
            NWTour
          </a>
        </li>

        <li class="{{ Route::is('admin.trainings*') ? 'active' : '' }}">
          <a href="{{ route('admin.trainings.index') }}">
            Trainings
          </a>
        </li>

        <li class="{{ Route::is('admin.events*') || Route::is('admin.packages*') || Route::is('admin.services*') ? 'active' : '' }}">
          <a href="{{ route('admin.events.index') }}">
            Events
          </a>
        </li>

        <li class="{{ Route::is('admin.camps*') || Route::is('admin.reviews*') ? 'active' : '' }}">
          <a href="{{ route('admin.camps.index') }}">
            Camps
          </a>
        </li>

        <li class="{{ Route::is('admin.coaches*') ? 'active' : '' }}">
          <a href="{{ route('admin.coaches.index') }}">
            Coaches
          </a>
        </li>

        <li class="{{ Route::is('admin.locations*') ? 'active' : '' }}">
          <a href="{{ route('admin.locations.index') }}">
            Locations
          </a>
        </li>

        <li class="{{ Route::is('admin.posts*') ? 'active' : '' }}">
          <a href="{{ route('admin.posts.index') }}">
            Posts
          </a>
        </li>

        <li class="{{ Route::is('admin.users*') ? 'active' : '' }}">
          <a href="{{ route('admin.users.index') }}">
            Users
          </a>
        </li>

        <li class="{{ Route::is('admin.settings*') ? 'active' : '' }}">
          <a href="{{ route('admin.settings.index') }}">
            Settings
          </a>
        </li>

      </ul>
    </div>
  </div>
</nav>
