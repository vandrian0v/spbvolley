@extends('layouts.master')

@section('title', 'Корпоративный волейбол и корпоративные турниры и тренировки по волейболу в компании')
@section('description', 'Профессиональное проведение корпоративных турниров и тренировок по волейболу! Судьи, призы, анимация! Корпоративные тренировки!')
@section('keywords', 'Корпоративный волейбол и корпоративные турниры по волейболу в компании тренировки')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/events.css') }}">
    <link rel="stylesheet" type="text/css" href="/bower/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/bower/owl.carousel/dist/assets/owl.theme.default.min.css">
@endpush

@section('body')
    @include('layouts.event.partials.fixed_menu')

    @include('layouts.event.partials.sidebar_menu')

    @include('events.partials.panel')

    @include('events.partials.modal')

    <div class="pusher">
        <div class="ui vertical masthead center aligned segment">

            @include('layouts.event.header')

            @include('layouts.event.partials.menu')

            @yield('content')

        </div>
    </div>
@stop

@push('javascript')
    <script src="/bower/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="{{ elixir('js/events.js') }}"></script>
@endpush