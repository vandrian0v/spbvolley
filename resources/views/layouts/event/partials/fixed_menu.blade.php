<div class="ui medium fixed hidden menu fluid seven item">
    <div class="ui container">
        <a class="item" href="{{ route('events.index') }}#info">О нас</a>
        <a class="item" href="{{ route('events.index') }}#packages">Пакеты</a>
        <a class="item" href="{{ route('events.index') }}#portfolio">Портфолио</a>
        <a class="item" href="{{ route('events.trainings') }}">Тренировки</a>
        <a class="item" href="{{ route('events.locations') }}">Места проведения</a>
        <a class="item" href="{{ route('events.index') }}#contacts">Контакты</a>
        <a class="item" href="{{ route('home') }}">Основной сайт</a>
    </div>
</div>