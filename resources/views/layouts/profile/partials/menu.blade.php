<div class="ui vertical fluid pointing secondary menu">
    <a class="item {{ Route::is('auth.profile.trainings') ? 'active' : '' }}" href="{{ route('auth.profile.trainings') }}">
        Тренировки
    </a>
    <a class="item {{ Route::is('auth.profile.payments') ? 'active' : '' }}" href="{{ route('auth.profile.payments') }}">
        Платежи
    </a>
    <a class="item {{ Route::is('auth.profile.edit') ? 'active' : '' }}" href="{{ route('auth.profile.edit') }}">
        Редактирование данных
    </a>
    <a class="item {{ Route::is('auth.profile.feedback') ? 'active' : '' }}" href="{{ route('auth.profile.feedback') }}">
        Обратная связь
    </a>
</div>