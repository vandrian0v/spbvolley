@extends('layouts.master')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/style.css') }}">
@endpush

@section('body')
    @include('layouts.partials.sidebar_menu')

    <div class="pusher">
        <div class="ui vertical masthead segment">

            @include('layouts.partials.menu')

            <div class="ui container grid stackable main-container">
                <div class="fourteen wide column centered grid">
                    <div class="ui segment stackable relaxed grid">
                        <div class="four wide column">
                            @include('layouts.profile.partials.menu')
                        </div>
                        <div class="twelve wide column">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('javascript')
    <script>
        $(document).ready(function() {
            $('.ui.dropdown').dropdown();
            $('.ui.sidebar').sidebar('attach events', '.toc.item');
            $('.ui.sidebar').sidebar('attach events', '.sidebar.menu a:not(.title)');
        });
    </script>
@endpush