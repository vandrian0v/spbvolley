<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @hasSection('title')
    <title>@yield('title')</title>
    @else
    <title>Клуб Пляжного Волейбола RIO</title>
    @endif
    @hasSection('description')
    <meta name="description" content="@yield('description')">
    @endif
    @hasSection('keywords')
    <meta name="keywords" content="@yield('keywords')">
    @endif

    {{-- css --}}
    @stack('css')
    <link rel="stylesheet" type="text/css" href="/bower/semantic/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="/bower/lightbox2/dist/css/lightbox.min.css">
    {{--<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>--}}
    {{-- end css --}}

    @include('layouts.partials.pixels')
  </head>
  <body>
    @yield('body')

    @include('layouts.footer')

    {{-- js --}}
    <script src="/bower/jquery/dist/jquery.min.js"></script>
    <script src="/bower/semantic/dist/semantic.min.js"></script>
    <script src="/bower/vue/dist/vue.min.js"></script>
    <script src="/bower/vue-resource/dist/vue-resource.min.js"></script>
    <script src="/bower/lightbox2/dist/js/lightbox.min.js"></script>
    <script>
      Vue.http.headers.common['X-CSRF-TOKEN'] = '{{ csrf_token() }}';
    </script>
    @stack('javascript')
    {{-- end js --}}

    @if (App::environment('production'))
      @include('layouts.partials.metrics')
    @endif
  </body>
</html>