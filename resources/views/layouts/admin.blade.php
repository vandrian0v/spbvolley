<!DOCTYPE html>
<html lang="en">
  @include('layouts.admin._head')

  <body>
    @include('layouts.admin._nav')

    <div class="container-fluid">
      <div class="row">

        @hasSection('sidebar')
          <div class="col-xs-3 col-sm-3 col-md-2 sidebar">
            @yield('sidebar')
          </div>

          <div class="col-xs-9 col-xs-offset-3 col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        @else
          <div class="col-sm-12 col-md-10 col-md-offset-1 main">
        @endif

          @yield('content')
        </div>
      </div>
    </div>

    <script src="/bower/jquery/dist/jquery.min.js"></script>
    <script src="/bower/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/bower/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/bower/lightbox2/dist/js/lightbox.min.js"></script>
    <script src="/bower/vue/dist/vue.min.js"></script>
    <script src="/bower/vue-resource/dist/vue-resource.min.js"></script>
    <script>
      Vue.http.headers.common['X-CSRF-TOKEN'] = '{{ csrf_token() }}';
    </script>

    @stack('scripts')

    <script src="{{ elixir('js/admin/app.js') }}"></script>
  </body>
</html>
