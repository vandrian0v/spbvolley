@extends('layouts.master')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/style.css') }}">
@endpush

@section('body')
    @include('layouts.partials.sidebar_menu')

    <div class="pusher">
        <div class="ui vertical masthead segment">

            @include('layouts.partials.menu')

            <div class="ui container grid stackable main-container">
                <div class="three wide column computer tablet only sidebar-block">
                    @include('layouts.partials.sidebar_left')
                </div>

                <div class="ten wide column">
                    @yield('content')
                </div>

                <div class="three wide column sidebar-block">
                    @include('layouts.partials.sidebar_right')
                </div>
            </div>
        </div>
    </div>

    @include('trainings.partials.panel')

    @include('trainings.partials.modal')
@stop

@push('javascript')
    <script src="https://vk.com/js/api/openapi.js"></script>
    <script src="{{ elixir('js/main.js') }}"></script>
    <script src="{{ elixir('js/training_application.js') }}"></script>
@endpush