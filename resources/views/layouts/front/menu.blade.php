<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <div class="icon logo"></div>
            </a>
        </div>

        <div class="col-md-12 no-padding">
            <div class="collapse navbar-collapse no-md-padding" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav col-md-12 no-padding">
                    <li>
                        <a href="{{ route('trainings.index') }}">
                            <div class="icon whistle"></div>
                            <span>Тренировки</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('events.index') }}">
                            <div class="icon corporate"></div>
                            <span>Корпоративные мероприятия</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('camps.index') }}">
                            <div class="icon camps"></div>
                            <span>Спортивные лагеря</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('nwtour.index') }}">
                            <div class="icon trophy"></div>
                            <span>NW-Tour<br>Турниры</span>
                        </a>
                    </li>
                    @if (Auth::check())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="last">{{ Auth::user()->surname }} {{ Auth::user()->name }}</span>
                                <span class="caret"></span>
                                <div class="fixer"></div>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('auth.profile.trainings') }}">Личный кабинет</a></li>
                                <li><a href="{{ route('auth.logout') }}">Выйти</a></li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('auth.login.show') }}">
                                <span class="last">Вход в личный кабинет</span>
                                <div class="icon door"></div>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</nav>

<div id="preload">
    <img src="/front/icons/whistle_colored.svg">
    <img src="/front/icons/corporate_colored.svg">
    <img src="/front/icons/camps_colored.svg">
    <img src="/front/icons/trophy_colored.svg">
    <img src="/front/icons/door_colored.svg">
</div>