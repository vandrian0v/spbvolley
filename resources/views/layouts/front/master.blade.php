<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="yandex-verification" content="e392b1f831ceac6c" />

        @hasSection('title')
            <title>@yield('title')</title>
        @else
            <title>Клуб Пляжного Волейбола RIO</title>
        @endif
        @hasSection('description')
            <meta name="description" content="@yield('description')">
        @endif
        @hasSection('keywords')
            <meta name="keywords" content="@yield('keywords')">
        @endif

        @stack('css')
        <link rel="stylesheet" href="/bower/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/front.css') }}">
        <link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">

        @include('layouts.partials.pixels')
    </head>
    <body>
        @include('layouts.front.menu')

        @yield('content')

        @include('layouts.front.footer')

        <script src="/bower/jquery/dist/jquery.min.js"></script>
        <script src="/bower/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/bower/vue/dist/vue.min.js"></script>
        <script src="/bower/vue-resource/dist/vue-resource.min.js"></script>
        <script>
            Vue.http.headers.common['X-CSRF-TOKEN'] = '{{ csrf_token() }}';
        </script>
        @if (request()->path() !== 'register')
        <script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=1998f24834abae85295d50742e34872c" charset="UTF-8" async></script>
        @endif
        @stack('js')

        @if (App::environment('production'))
            @include('layouts.partials.metrics')
        @endif
    </body>
</html>