<footer class="footer">
    <div style="">
        <div class="copyright">
            © Клуб RIO | 2014-{{ date('Y') }}
        </div>
        <div class="phone">
            <a href="tel:8 (965) 081 04 62">8 (965) 081 04 62</a> (9:00 - 22:00 без выходных)
        </div>
        <div class="email">
            <a href="mailto:rio.bvclub@gmail.com">rio.bvclub@gmail.com</a>
        </div>
    </div>
</footer>