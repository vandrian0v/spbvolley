@if (count($errors) > 0)
    <div class="row errors-row">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Ошибка валидации</h3>
            </div>
            <div class="panel-body">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif

@if (session()->has('notice'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div>
            <p><i class="fa fa-info-circle" aria-hidden="true"></i> {{ session('notice') }}</p>
        </div>
    </div>
@endif

@if (session()->has('warning'))
    <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div>
            <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ session('warning') }}</p>
        </div>
    </div>
@endif