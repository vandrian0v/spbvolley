@extends('layouts.main.master')

@section('content')
    <div class="ui segment nwtour-segment">
        @include('layouts.nwtour.partials.menu')

        @yield('nwtour_content')
    </div>
@stop