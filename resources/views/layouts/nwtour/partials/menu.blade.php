<div class="ui secondary tabular four item menu nwtour-menu segment stackable">
    <a class="item {{ Route::is('nwtour.index') ? 'active' : '' }}"
    href="{{ route('nwtour.index') }}">
        <i class="calendar icon"></i>
        Календарь
    </a>
    <a class="item {{ Route::is('nwtour.ratings') ? 'active' : '' }}"
       href="{{ route('nwtour.ratings') }}">
        <i class="bar chart icon"></i>
        Рейтинги
    </a>
    <a class="item {{ Route::is('nwtour.about') ? 'active' : '' }}"
    href="{{ route('nwtour.about') }}">
        <i class="file text icon"></i>
        Инфо о NW-Tour
    </a>

    {{--<a class="item {{ Route::is('nwtour.partners') ? 'active' : '' }}"--}}
    {{--href="{{ route('nwtour.partners') }}">--}}
        {{--<i class="users icon"></i>--}}
        {{--Партнеры--}}
    {{--</a>--}}
</div>
