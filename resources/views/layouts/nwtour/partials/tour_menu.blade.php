<div class="ui brown secondary pointing menu four item stackable" style="">
    <a class="item {{ Route::is('nwtour.info') ? 'active' : '' }}" href="{{ route('nwtour.info', $tour->id) }}">
        <i class="info icon"></i>
        Информация
    </a>

    @if ($tour->status != 'anounsed')
        <a class="item {{ Route::is('nwtour.participants') ? 'active' : '' }}" href="{{ route('nwtour.participants', $tour->id) }}">
            <i class="group icon"></i>
            Участники
        </a>
    @else
        <div class="disabled item">
            <i class="group icon"></i>
            Участники
        </div>
    @endif

    @if (in_array($tour->status, ['finished', 'anounsed']))
        <div class="disabled item">
            <i class="payment icon"></i>
            Форма заявки
        </div>
    @else
        <a class="item {{ Route::is('nwtour.application') ? 'active' : '' }}" href="{{ route('nwtour.application', $tour->id) }}">
            <i class="payment icon"></i>
            Форма заявки
        </a>
    @endif

    @if ($tour->status == 'finished')
        <a class="item {{ Route::is('nwtour.results') ? 'active' : '' }}" href="{{ route('nwtour.results', $tour->id) }}">
            <i class="trophy icon"></i>
            Результаты
        </a>
    @else
        <div class="disabled item">
            <i class="trophy icon"></i>
            Результаты
        </div>
    @endif
</div>

<h2 class="ui header">
    {{ $tour->title }}
    <div class="sub header">
        {{ $tour->localeDate }}
    </div>
</h2>