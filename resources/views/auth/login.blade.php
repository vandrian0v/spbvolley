@extends('layouts.front.master')

@section('title', 'Вход в личный кабинет')

@section('content')
<div class="container" style="margin-top: 22px;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Вход</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('auth.login.persist') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Пароль</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="remember" value="1">

                        <div class="form-group">
                            <div class="text-center">
                                <button type="submit" class="btn btn-warning">
                                    Войти
                                </button>

                                <a href="{{ route('auth.register.show') }}" class="btn btn-primary">
                                    Регистрация
                                </a>

                                <a class="btn btn-link" href="{{ route('auth.password.request.show') }}">
                                    Забыли пароль?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer text-center">
                    Позвоните нам если вам нужна помощь!<br>
                    <a href="tel:8 (965) 081 04 62">8 (965) 081 04 62</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
