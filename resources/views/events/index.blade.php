@extends('layouts.event.master')

@section('content')

    @include('events.partials.info')

    @include('events.partials.packages')

    @include('events.partials.portfolio')

    @include('events.partials.feedback')

    @include('events.partials.contacts')

@stop