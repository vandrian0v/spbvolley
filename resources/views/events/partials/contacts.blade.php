<div class="ui stripe vertical segment" id="contacts">
    <h1 class="ui header">Контакты</h1>
    <div class="ui container text">
        <div class="ui two column middle aligned center stackable grid">
            <div class="center aligned column grid">
                <div class="ui list" style="margin-left: 1.5em !important;">
                    <div class="item">
                        <i class="wait icon"></i>
                        <div class="content">
                            10:00 - 22:00 без выходных
                        </div>
                    </div>
                    <div class="item">
                        <i class="call icon"></i>
                        <div class="content">
                            8-921-900-89-93
                        </div>
                    </div>
                    <div class="item">
                        <i class="mail icon"></i>
                        <div class="content">
                            <a href="mailto:rio.bvclub@gmail.com">rio.bvclub@gmail.com</a>
                        </div>
                    </div>
                    <div class="item">
                        <i class="linkify icon"></i>
                        <div class="content">
                          Основной сайт: <a href="{{ route('home') }}">spbvolley.ru</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="center aligned column">
                <a href="https://vk.com/riobeachvolley" target="_blank" class="vk ui icon button large">
                    <i class="vk icon"></i>
                </a>
                <a href="https://www.facebook.com/%D0%9A%D0%BB%D1%83%D0%B1-%D0%BF%D0%BB%D1%8F%D0%B6%D0%BD%D0%BE%D0%B3%D0%BE-%D0%B2%D0%BE%D0%BB%D0%B5%D0%B9%D0%B1%D0%BE%D0%BB%D0%B0-RIO-243357389168183/"
                 target="_blank" class="facebook ui icon button large">
                    <i class="facebook icon"></i>

                </a>
                <a href="https://www.instagram.com/riobeachvolley/" target="_blank" class="instagram ui icon button large">
                    <i class="instagram icon"></i>
                </a>
            </div>
        </div>
    </div>
</div>