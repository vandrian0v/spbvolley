<div class="ui vertical stripe segment" id="packages">
    <div class="ui container">
        <h1 class="ui header">Пакеты</h1>
        <div class="ui main grid">
            <div class="ui stackable three column row">
                @foreach ($packages as $package)
                    <div class="dashboard-stat column">
                        <a href="{{ route('packages.show', ['id' => $package->id]) }}">
                            <div class="ui segments package">
                                <div class="ui segment">
                                    <h3 class="ui header">
                                        {{ $package->name }}
                                    </h3>
                                </div>
                                <div class="ui {{ $package->color }} inverted dashboard center aligned segment">
                                    <div class="ui dashboard statistic">
                                        <div>
                                            {{ $package->description }}
                                        </div>
                                        <div style="margin-top: 25px;">
                                            Длительность {{ $package->is_min_duration ? 'от ' : ''}}{{ (float) $package->duration }} ч.
                                        </div>
                                    </div>
                                </div>
                                <p class="ui segment" style="color: black;">
                                    от {{ number_format($package->price, 0, '.', ' ') }} руб.
                                </p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="ui button big black basic event-request" style="margin-top: 3em; background: white !important;">
            Оставить заявку
        </div>
        <div class="ui event-request message hidden" style="margin-top: 3em">
            Ваша заявка отправлена, спасибо!
        </div>
    </div>
</div>

<div class="ui small modal packages-modal">
    <div class="header">Подача заявки на проведение мероприятия</div>
    <div class="content">
        <form class="ui form event-request">
            {!! csrf_field() !!}

            <div class="field">
                <label>Название организации*</label>
                <input type="text" name="name">
            </div>

            <div class="two fields">
                <div class="field">
                    <label>Контактный email*</label>
                    <input type="text" name="email">
                </div>
                <div class="field">
                    <label>Контактный телефон</label>
                    <input type="text" name="phone">
                </div>
            </div>

            <div class="field">
                <label>Комментарий</label>
                <textarea rows="2" name="comment" placeholder="Количество участников, желаемое место проведения, и т.д."></textarea>
            </div>

        </form>
    </div>
    <div class="actions">
        <div class="ui approve button primary">Отправить</div>
        <div class="ui cancel button">Отмена</div>
    </div>
</div>