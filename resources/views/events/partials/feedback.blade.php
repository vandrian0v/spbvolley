<div class="ui stripe vertical segment" id="feedback">
    <div class="ui container text">
        <h1 class="ui header">Подача заявки на проведение мероприятия</h1>
        <form class="ui form feedback">
            {{ csrf_field() }}

            <div class="field">
                <label>Название организации*</label>
                <input type="text" name="name">
            </div>

            <div class="two fields">
                <div class="field">
                    <label>Контактный email*</label>
                    <input type="text" name="email">
                </div>
                <div class="field">
                    <label>Контактный телефон</label>
                    <input type="text" name="phone">
                </div>
            </div>

            <div class="field">
                <label>Комментарий</label>
                <textarea rows="2" name="comment" placeholder="Количество участников, желаемое место проведения, и т.д."></textarea>
            </div>
        </form>
        <div class="ui button big primary feedback" style="margin-top: 1rem;">
            Отправить
        </div>
        <div class="ui message hidden feedback" style="margin-top: 1em; text-align: center;">
            Ваша заявка отправлена, спасибо!
        </div>
    </div>
</div>