<div class="ui small modal panel-modal">
    <div class="header">Подача заявки на проведение мероприятия</div>
    <div class="content">
        <form class="ui form panel-request">
            {{ csrf_field() }}

            <div class="field">
                <label>Название организации*</label>
                <input type="text" name="name">
            </div>

            <div class="two fields">
                <div class="field">
                    <label>Контактный email*</label>
                    <input type="text" name="email">
                </div>
                <div class="field">
                    <label>Контактный телефон</label>
                    <input type="text" name="phone">
                </div>
            </div>

            <div class="field">
                <label>Комментарий</label>
                <textarea rows="2" name="comment" placeholder="Количество участников, желаемое место проведения, и т.д."></textarea>
            </div>
        </form>
    </div>
    <div class="actions">
        <div class="ui approve button primary">Отправить</div>
        <div class="ui cancel button">Отмена</div>
    </div>
</div>