<div class="ui vertical stripe segment" id="portfolio">
    <h1 class="ui header">Портфолио</h1>

    <div class="ui middle aligned main container grid">
        <div class="three column row center aligned">
            <div class="column one wide"  style="padding: 0;">
                <i class="chevron left icon floated left big"></i>
            </div>

            <div class="column fourteen wide">
                <div class="portfolio-cards owl-carousel owl-theme">
                    @foreach ($events as $event)
                        <div class="ui card" style="width: 100%;">
                            <div class="image">
                                @if ($event->hasPreview())
                                    <img src="{{ $event->previewUrl() }}">
                                @endif
                            </div>
                            <div class="content">
                                <a href="{{ route('events.show', $event->id) }}" class="header">
                                    {{ $event->title }}
                                </a>
                            </div>
                            <div class="extra content">
                                {{ $event->localeDate }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="column one wide" style="padding: 0;">
                <i class="chevron right icon floated right big"></i>
            </div>
        </div>
    </div>
</div>
