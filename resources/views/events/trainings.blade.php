@extends('layouts.event.master')

@section('content')
     <div class="ui stripe vertical segment trainings">
        <div class="ui text container left aligned">
            <h2 class="ui header center aligned">Корпоративные тренировки</h2>
            <p>Клуб пляжного волейбола RIO организовывает корпоративные тренировки по пляжному и классическому волейболу для сотрудников компаний и организаций. </p>
            <p>Это прекрасная возможность сплотить свой коллектив, популяризировать спорт и здоровый образ жизни у своих сотрудников, наладить неформальное общение среди коллег и провести с удовольствием и пользой время.</p>
            <p>Совместные занятия командными видами спорта – один из самых эффективных видов тимбилдинга. </p>
            <p>Организация тренировочного процесса возможна вдвух вариантах:</p>
            <ul class="ui list">
                <li>долгосрочный цикл обучения</li>
                <li>цикл подготовки перед корпоративным или межкорпоративным турниром</li>
            </ul>
            <p>Оплата всех услуг возможна безналичным расчетом по договору. С Вами будут работать высококлассные профессиональные тренеры.</p>
            <p>Для уточнения деталей обращайтесь по телефону <i class="call icon"></i>900 89 93</p>

            <div class="ui three column doubling stackable grid">
                <div class="column">
                    <a href="/photos/training/1.jpg" data-lightbox="photos_3">
                        <img class="ui image rounded" src="/photos/training/1.jpg">
                    </a>
                </div>
                <div class="column">
                    <a href="/photos/training/2.jpg" data-lightbox="photos_3">
                        <img class="ui image rounded" src="/photos/training/2.jpg">
                    </a>
                </div>
                <div class="column">
                    <a href="/photos/training/3.jpg" data-lightbox="photos_3">
                        <img class="ui image rounded" src="/photos/training/3.jpg">
                    </a>
                </div>
                <div class="column">
                    <a href="/photos/training/4.jpg" data-lightbox="photos_3">
                        <img class="ui image rounded" src="/photos/training/4.jpg">
                    </a>
                </div>
                <div class="column">
                    <a href="/photos/training/5.jpg" data-lightbox="photos_3">
                        <img class="ui image rounded" src="/photos/training/5.jpg">
                    </a>
                </div>
                <div class="column">
                    <a href="/photos/training/6.jpg" data-lightbox="photos_3">
                        <img class="ui image rounded" src="/photos/training/6.jpg">
                    </a>
                </div>
            </div>
        </div>
     </div>
@stop