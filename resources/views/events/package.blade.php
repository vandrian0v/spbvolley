@extends('layouts.event.master')

@section('content')
    <div class="ui stripe vertical segment">
        <h2 class="ui header">Пакет {{ $package->name }}</h2>

        <div class="ui container text left aligned">
            <div class="ui two column stackable grid">

                <div class="column">
                    <div class="ui raised segment">
                        <a class="ui blue ribbon label">Информация</a>
                        <div style="margin-top: 16px;">
                            {{ $package->description }}
                        </div>
                        <div class="ui divider"></div>
                        <div>
                            Длительность: {{ $package->is_min_duration ? 'от ' : ''}}{{ (float) $package->duration }} ч.<br>
                        </div>
                        <div class="ui divider"></div>
                        <div>
                            Стоимость: от {{ number_format($package->price, 0, '.', ' ') }} руб.
                        </div>
                    </div>
                    @if ($package->info)
                        <div class="ui raised segment">
                            {{ $package->info }}
                        </div>
                    @endif
                    <div class="ui button green event-request fluid">Оставить заявку</div>
                    <div class="ui event-request info message hidden" style="text-align: center;">
                        Ваша заявка отправлена, спасибо!
                    </div>
                </div>

                <div class="column">
                    <div class="ui segment">
                        <a class="ui teal right ribbon label">Список услуг</a>
                        <div class="ui list">
                            @foreach ($package->services as $service)
                                    <div class="item" style="padding-bottom: .8em">
                                    <i class="right triangle icon"></i>
                                            {{ $service->name }}
                                    </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

     <div class="ui small modal packages-modal">
        <div class="header">Подача заявки на проведение мероприятия</div>
        <div class="content">
            <form class="ui form event-request">
                {!! csrf_field() !!}

                <div class="field">
                    <label>Название организации*</label>
                    <input type="text" name="name">
                </div>

                <div class="two fields">
                    <div class="field">
                        <label>Контактный email*</label>
                        <input type="text" name="email">
                    </div>
                    <div class="field">
                        <label>Контактный телефон</label>
                        <input type="text" name="phone">
                    </div>
                </div>

                <div class="field">
                    <label>Комментарий</label>
                    <textarea rows="2" name="comment"></textarea>
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="ui approve button primary">Отправить</div>
            <div class="ui cancel button">Отмена</div>
        </div>
    </div>
@stop