@extends('layouts.event.master')

@section('title', $event->title)

@push('css')
    <style>
        .pusher {
            background: url('/image/wov.png') !important;
        }
    </style>
@endpush

@section('content')
    <div class="ui segment container left aligned" style="margin-bottom: 1em;">
        <h2 class="ui header center aligned">{{ $event->title }}</h2>
        <div class="ui relaxed divided items">
            <div class="item">
                <div class="ui large image rounded" style="padding-left: 1rem;">
                    @if ($event->hasPreview())
                        <img src="{{ $event->previewUrl() }}">
                    @endif
                </div>
                <div class="content">
                    <div class="meta">
                        <span>Дата проведения:</span> {{ $event->localeDate }}
                        <div class="ui divider"></div>
                        <span>Кол-во участников:</span> {{ $event->participants }}
                        <div class="ui divider"></div>
                        <span>Место проведения:</span> {{ $event->location->name }}
                    </div>
                </div>
            </div>
            <div class="item ui grid stackable">
                <div class="column eight wide">
                    {!! $event->description !!}
                </div>
                <div class="column eight wide">
                    <div class="ui bulleted list">
                        @foreach ($event->services as $service)
                            <div class="item">{{ $service->name }}</div>
                        @endforeach
                    </div>
                </div>
            </div>

            @if ($event->photos->count())
                <div class="item">
                    <div class="ui small images portfolio-gallery owl-carousel owl-theme">
                        @foreach ($event->photos as $photo)
                            <a href="{{ $photo->url() }}" data-lightbox="photos">
                                <img class="ui image rounded" src="{{ $photo->url() }}">
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop