@extends('layouts.event.master')

@push('css')
    <style>
        .pusher {
            background: url('/image/wov.png') !important;
        }
    </style>
@endpush

@section('content')
    @foreach ($data as $title => $season)
        <div class="ui stripe vertical segment locations-season">
            <div class="ui left aligned internally celled two column doubling grid container">
                <h1 class="ui header">{{ $title }}</h1>
                @foreach ($season as $location)
                    <div class="row">
                        <div class="five wide column">
                            <div class="ui image rounded">
                                @if ($location->hasPreview())
                                    <img src="{{ $location->previewUrl() }}">
                                @endif
                            </div>
                        </div>
                        <div class="eleven wide column">
                            <div class="ui header">
                                {{ $location->name }}
                            </div>
                            <p>
                                {!! $location->description !!}
                            </p>
                            @if ($location->photos->count())
                                <div class="ui small rounded images">
                                    @foreach ($location->photos as $photo)
                                        <a href="{{ $photo->url() }}" data-lightbox="photos_{{ $location->id }}">
                                            <img class="ui image" src="{{ $photo->url() }}">
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endforeach
@stop