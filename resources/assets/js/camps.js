$(document).ready(function() {
  function addApplication(form, button, message) {
    if (form.form('validate form')) {
      button.addClass('loading');

      $.ajax({
        type: 'post',
        url: '/api/camps/application',
        data: form.serialize(),
        success: function(data) {
          if (data.status == 'ok') {
            button.hide();
            form.hide();
            if (message) {
              message.show();
            }
          } else {
            button.removeClass('loading');
          }
        },
        error: function() {
          button.removeClass('loading');
        }
      });

      return true;
    }

    return false;
  }

  $('.ui.sticky').sticky({
    context: '.pusher'
  });

  $('.ui.dropdown').dropdown();

    // fix menu when passed
  $('.topmenu').visibility({
    once: false,
    onBottomPassed: function() {
      $('.fixed.menu').transition('fade in');
    },
    onBottomPassedReverse: function() {
      $('.fixed.menu').transition('fade out');
    }
  });

  // create sidebar and attach to menu open
  $('.ui.sidebar').sidebar('attach events', '.toc.item');
  $('.ui.sidebar').sidebar('attach events', '.sidebar.menu a');

  $('#reviews .chevron.left').click(function() {
    $('.reviews').trigger('prev.owl.carousel');
  });

  $('#reviews .chevron.right').click(function() {
    $('.reviews').trigger('next.owl.carousel');
  });

  $('#past .chevron.left').click(function() {
    $('.past-camps').trigger('prev.owl.carousel');
  });

  $('#past .chevron.right').click(function() {
    $('.past-camps').trigger('next.owl.carousel');
  });

  $('.feedback.button').click(function() {
    var form = $('.feedback.form');
    var button = $('.feedback.button');
    var message = $('.feedback.message');

    addApplication(form, button, message);
  });

  $('.ui.small.modal.panel-modal').modal({
    onApprove: function(e) {
      var form  = $('.panel-request.form');
      var button  = $('.fixed-panel .button');

      return addApplication(form, button, null);
    },
    onHide: function() {
      $('.panel-request.form').form('reset');
    }
  }).modal('attach events', '.fixed-panel .button', 'show');

  $('.reviews').owlCarousel({
    loop: true,
    items: 1,
    dots: true,
    autoHeight : true,
  });

  $('.past-camps').owlCarousel({
    autoHeight: true,
    margin: 30,
    nav: false,
    dots: true,
    dotsEach: true,
    loop: true,
    responsive: {
      0:{
        items: 1
      },
      500: {
        items: 2
      }
    }
  });

  $('.camp-gallery').owlCarousel({
    margin: 10,
    nav: false,
    dots: true,
    dotsEach: true,
    responsive: {
      0:{
        items:1
      },
      350: {
        items:3
      },
      800:{
        items:4
      },
      1000:{
        items:6
      }
    }
  });

  $('.feedback.form').form({
    fields: {
      name: {
        rules: [{
          type: 'empty',
          prompt: 'Имя должно быть заполнено'
        }]
      },
      email: {
        rules: [{
          type: 'email',
          prompt: 'Контактный email должен быть корректным email-ом'
        }]
      }
    },
    inline: true
  });

  $('.panel-request.form').form({
    fields: {
      camp_id: {
        rules: [{
          type: 'empty',
          prompt: 'Выберите лагерь в который хотите поехать'
        }]
      },
      name: {
        rules: [{
          type: 'empty',
          prompt: 'Имя должно быть заполнено'
        }]
      },
      email: {
        rules: [{
          type: 'email',
          prompt: 'Контактный email должен быть корректным email-ом'
        }]
      }
    },
    inline: true
  });

});
