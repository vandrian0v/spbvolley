import Vue from 'vue'
import VueResource from 'vue-resource'
import VueLocalStorage from 'vue-ls'
import router from './router'
// import store from './store'

Vue.use(VueResource);
Vue.use(VueLocalStorage, {namespace: 'vuejs_'});

new Vue({
    el: '#vue-container',
    router,
    // store
});