import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/nwtour',
            component: () => import(/* webpackChunkName: "tours-index" */ '../pages/tours/index'),
            children: [
                {
                    path: '',
                    component: () => import(/* webpackChunkName: "tours-calendar" */ '../pages/tours/calendar')
                },
                {
                    path: '/nwtour/ratings',
                    component: () => import(/* webpackChunkName: "tours-ratings" */ '../pages/tours/ratings')
                },
                {
                    path: '/nwtour/about',
                    component: () => import(/* webpackChunkName: "tours-about" */ '../pages/tours/about')
                },
                {
                    path: '/nwtour/archive',
                    component: () => import(/* webpackChunkName: "tours-archive" */ '../pages/tours/archive')
                },
            ]
        },

        {
            path: '/nwtour/:tour_id(\\d+)',
            props: true,
            component: () => import(/* webpackChunkName: "tours-show" */ '../pages/tours/show')
        }
    ]
})