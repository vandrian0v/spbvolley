import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin/settings',
            component: require('../pages/settings/index.vue').default,
            children: [
                {
                    path: '/admin/settings/services',
                    component: require('../pages/settings/services.vue').default
                },
                {
                    path: '/admin/settings/sponsors',
                    component: require('../pages/settings/sponsors.vue').default
                },
                {
                    path: '/admin/settings/notifications',
                    component: require('../pages/settings/notifications.vue').default
                },
                {
                    path: '/admin/settings/pages',
                    component: require('../pages/settings/pages.vue').default
                }
            ]
        },
        {
            path: '/admin/tours',
            component: require('../pages/tours/index.vue').default
        },
        {
            path: '/admin/tours/create',
            component: require('../pages/tours/create.vue').default
        },
        {
            path: '/admin/tours/:tour_id(\\d+)',
            props: true,
            component: require('../pages/tours/edit.vue').default,
            children: [
                {
                    path: '',
                    component: require('../pages/tours/edit/info.vue').default,
                },
                {
                    path: 'tournament/:tournament_id(\\d+)',
                    props: true,
                    component: require('../pages/tours/edit/tournament.vue').default,
                }
            ]
        },
        {
            path: '/admin/tours/ratings',
            component: require('../pages/tours/ratings.vue').default,
        },
        {
            path: '/admin/vue-trainings',
            component: require('../pages/trainings/index.vue').default,
            children: [
                {
                    path: 'groups',
                    component: require('../pages/trainings/groups.vue').default
                },
                {
                    path: 'groups/:group_id(\\d+)',
                    props: true,
                    component: require('../pages/trainings/group.vue').default
                }
            ]
        }
    ]
})