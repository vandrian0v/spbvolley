import Vue from 'vue'
import VueResource from 'vue-resource'
import VueLocalStorage from 'vue-ls'
import router from './router'
import store from './store'
import VModal from 'vue-js-modal'

Vue.use(VueResource);
Vue.use(VueLocalStorage, {namespace: 'vuejs_'});
Vue.use(VModal);

new Vue({
    el: '#vue-container',
    router,
    store
});