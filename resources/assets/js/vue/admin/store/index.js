import Vue from 'vue'
import Vuex from 'vuex'
import tournamentCategories from './modules/tournament-categories'
import tourStatuses from './modules/tour-statuses'

Vue.use(Vuex);

const API_BASE_URL = '/api/admin'

export default new Vuex.Store({
    state: {},
    getters: {
        apiBaseUrl() {
            return API_BASE_URL
        }
    },
    mutations: {},
    actions: {},
    modules: {
        tournamentCategories,
        tourStatuses
    }
})