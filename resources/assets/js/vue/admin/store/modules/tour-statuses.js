import Vue from 'vue'

export default {
    namespaced: true,
    state: {
        all: [],
        initialized: false,
        promise: null
    },
    getters: {
        all(state) {
            return state.all
        },
        baseUrl(state, getters, rootState, rootGetters) {
            return `${rootGetters.apiBaseUrl}/lib/tour-statuses`
        }
    },
    mutations: {
        setAll(state, torunament_categories) {
            state.all = torunament_categories
        },
        setInitialized(state, initialized) {
            state.initialized = initialized
        },
        setPromise(state, promise) {
            state.promise = promise
        }
    },
    actions: {
        apiGetTourStatuses({getters, state, commit}) {
            if (state.promise) {
                return state.promise;
            }

            let promise = new Promise((resolve, reject) => {
                Vue
                    .http
                    .get(getters.baseUrl)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(response => {
                        reject(response)
                    })
            })

            commit('setPromise', promise)

            return promise
        },
        init({commit, dispatch, state}, {force = false} = {}) {
            return new Promise((resolve, reject) => {
                if (!state.initialized || force) {
                    let tourStatuses
                    if (tourStatuses = Vue.ls.get('tour-statuses')) {
                        commit('setAll', tourStatuses)
                        resolve()
                    } else {
                        dispatch('apiGetTourStatuses')
                            .then(response => {
                                commit('setAll', response.body.tour_statuses)
                                Vue.ls.set('tour-statuses', response.body.tour_statuses, 24 * 60 * 60 * 1000)
                                commit('setInitialized', true)

                                resolve()
                            })
                            .catch(() => {
                                reject()
                            })
                    }
                } else {
                    resolve()
                }
            })
        }
    }
}