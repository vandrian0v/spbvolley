import photos from './components/Photos.vue';

new Vue({
  el: '.photos',

  components: {
    photos: photos
  }
});