import schedule from './components/Schedule.vue';

new Vue({
  el: '.schedules',

  components: {
    schedule: schedule
  },

  data: {
    coaches: {},
    subscriptionTypes: {}
  },

  methods: {
    fetchCoaches() {
      this.$http.get('/api/coaches')
        .then((response) => {
          this.coaches = response.body;
        });
    },
    fetchSubscriptionTypes() {
        this.$http.get('/api/trainings/subscription-types')
          .then((response) => {
            this.subscriptionTypes = response.body;
          });
      }
  },

  mounted() {
    this.fetchCoaches();
    this.fetchSubscriptionTypes();
  }
});