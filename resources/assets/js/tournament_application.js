import UserSelect from './components/UserSelect'

new Vue({
  el: '.nwtour-segment',

  components: {
    UserSelect
  },

  data: {
    nwtour_id: nwtour_id,
    tournaments: {},
    errors: [],
    loading: false,
    success: false,
    selected_category_id: false,
    users: [],
    team_id: false
  },

  computed: {
    selectedTournament() {
      if (!this.selected_category_id) {
        return false;
      }

      let category_id = this.selected_category_id;

      return this.tournaments.find(function (tournament) {
        return category_id === tournament.category_id;
      });
    },
    usersRange() {
      if (!this.selectedTournament) {
        return 0;
      }

      return this.selectedTournament.category.team_size;
    },
    submitText() {
        if (!this.selectedTournament) {
            return 'Оплатить взнос и заявиться';
        }

        let contribution = this.selectedTournament.contribution;

        return contribution ? ('Оплатить взнос (' + contribution + ' руб.) и заявиться') : 'Заявиться';
    },
    selectedUsers() {
      return this.users.filter(u => !!u).map(u => u.id)
    }
  },

  watch: {
    // При смене категории очищаем ошибки
    selected_category_id() {
      this.errors = [];
      this.users = [];
    }
  },

  mounted() {
    this.fetchTournaments();
  },

  methods: {
    fetchTournaments() {
      this.$http.get('/api/nwtour/' + this.nwtour_id + '/tournaments').then((response) => {
        this.tournaments = response.body;
      }, () => {
        this.errors.push('Произошла ошибка, пожалуйста попробуйте позднее');
      });
    },
    sendApplication() {
      this.loading = true;
      this.errors = [];

      this.$http.post('/api/nwtour/application', {
        category_id: this.selected_category_id,
        // users: this.users,
        user_ids: this.selectedUsers,
        nwtour_id: this.nwtour_id
      }).then((response) => {
        this.loading = false;

        if (response.body.status === 'ok') {
          // Нужно ли редиректить на оплату
          if (response.body.payment_required === false) {
            this.success = true;
          } else {
            this.team_id = response.body.team_id;
            this.$nextTick(function() {
                document.getElementById("paymentForm").submit();
            });
          }
        } else {
          this.errors.push('Произошла ошибка, пожалуйста попробуйте позднее');
        }
      }, (response) => {
        this.loading = false;
        let data = response.body.errors;

        for (let key in data) {
            this.errors.push(data[key]);
        }
      });
    }
  }
});