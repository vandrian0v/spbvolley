import chart from './components/Chart.vue';

new Vue({
  el: '.charts',

  components: {
    chart: chart
  }
});
