$(document).ready(function() {
  function addApplication(form, button, message) {
    if (form.form('validate form')) {
      button.addClass('loading');

      $.ajax({
        type: 'post',
        url: '/api/events/application',
        data: form.serialize(),
        success: function(data) {
          if (data.status == 'ok') {
            button.hide();
            form.hide();
            if (message) {
              message.show();
            }
          } else {
            button.removeClass('loading');
          }
        },
        error: function() {
          button.removeClass('loading');
        }
      });

      return true;
    }

    return false;
  }

  $('.ui.sticky').sticky({
    context: '.pusher'
  });

  // fix menu when passed
  $('.topmenu').visibility({
    once: false,
    onBottomPassed: function() {
      $('.fixed.menu').transition('fade in');
    },
    onBottomPassedReverse: function() {
      $('.fixed.menu').transition('fade out');
    }
  });

  // create sidebar and attach to menu open
  $('.ui.sidebar').sidebar('attach events', '.toc.item');
  $('.ui.sidebar').sidebar('attach events', '.sidebar.menu a');

  $('.ui.accordion').accordion();

  $('.ui.dropdown').dropdown();

  $('.ui.small.modal.packages-modal').modal({
    onApprove: function(e) {
      var form  = $('.event-request.form');
      var button  = $('.event-request.button');
      var message = $('.event-request.message');

      return addApplication(form, button, message);
    },
    onHide: function() {
      $('.event-request.form').form('reset');
    }
  }).modal('attach events', '.event-request.button', 'show');

  $('.ui.small.modal.panel-modal').modal({
    onApprove: function(e) {
      var form  = $('.panel-request.form');
      var button  = $('.fixed-panel .button');

      return addApplication(form, button, null);
    },
    onHide: function() {
      $('.panel-request.form').form('reset');
    }
  }).modal('attach events', '.fixed-panel .button', 'show');

  $('.feedback.button').click(function() {
    var form = $('.feedback.form');
    var button = $('.feedback.button');
    var message = $('.feedback.message');

    addApplication(form, button, message);
  });

  function resize(event) {
    $('#portfolio .portfolio-cards').each(function() {
      var highestBox = 0;

      $('.card', this).each(function() {
        if($(this).height() > highestBox) {
          highestBox = $(this).height();
        }
      });

      $('.card',this).height(highestBox + 19);
    });

    $('.owl-stage-outer').height($('.owl-stage-outer').height() + 1);
  }

  $('#portfolio .portfolio-cards').owlCarousel({
    autoplay: true,
    autoplayTimeout: 4000,
    onInitialized: resize,
    margin: 30,
    nav: false,
    dots: true,
    dotsEach: true,
    loop: true,

    responsive: {
      0:{
        items: 1
      },
      500: {
        items: 2
      },
      801:{
        items: 3
      }
    }
  });

  $('.portfolio-gallery').owlCarousel({
    margin: 10,
    nav: false,
    dots: true,
    dotsEach: true,
    responsive: {
      0:{
        items:1
      },
      350: {
        items:3
      },
      800:{
        items:4
      },
      1000:{
        items:6
      }
    }
  });

  $('#portfolio .chevron.left').click(function() {
    $('#portfolio .portfolio-cards').trigger('prev.owl.carousel');
  });

  $('#portfolio .chevron.right').click(function() {
    $('#portfolio .portfolio-cards').trigger('next.owl.carousel');
  });

  $('.event-request.form').form({
    fields: {
      name: {
        rules: [{
          type: 'empty',
          prompt: 'Название организации должно быть заполнено'
        }]
      },
      email: {
        rules: [{
          type: 'email',
          prompt: 'Контактный email должен быть корректным email-ом'
        }]
      }
    },
    inline: true
  });

  $('.panel-request.form').form({
    fields: {
      name: {
        rules: [{
          type: 'empty',
          prompt: 'Название организации должно быть заполнено'
        }]
      },
      email: {
        rules: [{
          type: 'email',
          prompt: 'Контактный email должен быть корректным email-ом'
        }]
      }
    },
    inline: true
  });

  $('.feedback.form').form({
    fields: {
      name: {
        rules: [{
          type: 'empty',
          prompt: 'Название организации должно быть заполнено'
        }]
      },
      email: {
        rules: [{
          type: 'email',
          prompt: 'Контактный email должен быть корректным email-ом'
        }]
      }
    },
    inline: true
  });
});
