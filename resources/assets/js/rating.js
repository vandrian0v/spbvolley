new Vue({
  el: '.nwtour-segment',

  data: {
    category: 2,
    users: [],
    active: 'male',
    loading: false,
    error: false
  },

  mounted() {
    this.fetchRating(this.active, true);
  },

  watch: {
      category: function () {
        this.fetchRating(this.active, true);
      }
  },

  methods: {
    fetchRating (type, force) {
      if (force || type !== this.active) {
        this.users = [];
        this.active = type;
        this.loading = true;
        this.error = false;

        this.$http.get('/api/rating/' + type + '/' + this.category, {
          before(request) {
            if (this.previousRequest) {
              this.previousRequest.abort();
            }

            this.previousRequest = request;
          }
        }).then((response) => {
          this.users = response.body;
          this.loading = false;
        }, (response) => {
          if (response.status === 429) {
            this.error = 'Слишком много запросов, пожалуйста, подождите.';
          }

          this.loading = false;
        });
      }
    }
  }
});