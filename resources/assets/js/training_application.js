$(document).ready(function() {
  $('.ui.small.modal.panel-modal').modal('attach events', '.fixed-panel .button', 'show');
});

new Vue({
  el: '.panel-modal',

  data: {
    loading: false,
    success: false,
    error:   false,
    name:  '',
    email: '',
    phone: '',
    group: ''
  },

  computed: {
    formValid() {
      return this.name
        && this.email
        && this.phone
        && this.group;
    }
  },

  methods: {
    submitApplication() {
      this.loading = true;

      this.$http.post('/api/training/application', {
        name:  this.name,
        email: this.email,
        phone: this.phone,
        group: this.group
      }).then((response) => {
        this.loading = false;

        if (response.body.status == 'ok') {
          this.success = true;
        } else {
          this.error = true;
        }
      }, (response) => {
        this.loading = false;
        this.error = true;
      });
    }
  }
});