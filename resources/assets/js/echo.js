// import Echo from "laravel-echo"
//
// try {
//     window.Echo = new Echo({
//         broadcaster: 'socket.io',
//         host: window.location.hostname + ':6001'
//     });
//
//     let notify = function (e) {
//         $.notify({
//             url: e.url,
//             target: '_self',
//             message: e.message
//         }, {
//             delay: 0,
//             placement: {
//                 from: 'bottom'
//             }
//         });
//     };
//
//     window.Echo.private(`admin`)
//         .listen('PaymentConfirmed', notify)
//         .listen('TournamentApplicationReceived', notify)
//         .listen('UserRegistered', notify);
// } catch (e) {}