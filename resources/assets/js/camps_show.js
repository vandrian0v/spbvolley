import VModal from 'vue-js-modal'
import CampApplyForm from './components/CampApplyForm.vue'

Vue.use(VModal);

new Vue({
    el: '#camps-show',
    components: {
        CampApplyForm
    },
    methods: {
        showCampApplyModal() {
            this.$modal.show('camp-apply-modal');
        }
    }
});