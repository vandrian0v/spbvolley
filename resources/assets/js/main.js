$(document).ready(function() {
  VK.Widgets.Group("vk_groups", {mode: 3, width: "auto"}, 68535444);

  $('.ui.accordion').accordion();

  $('.ui.dropdown').dropdown();

  $('select').dropdown();

  $('.ui.sidebar').sidebar('attach events', '.toc.item');
  $('.ui.sidebar').sidebar('attach events', '.sidebar.menu a:not(.title)');

  $('.tabular.menu .item').tab();

  window.addEventListener("error", function (e) {
	$.post('/api/front/error', {
		error: e.message
	})
  });
});