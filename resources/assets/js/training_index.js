import VModal from 'vue-js-modal'
import TrainingApplyForm from './components/TrainingApplyForm.vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'

Vue.use(VModal);
Vue.use(VueAwesomeSwiper);

new Vue({
    el: '#trainings-index',
    components: {
        TrainingApplyForm
    },
    data: {
        modal_group_id: null,
        photos: [
            '/image/trainings/1-1.jpg',
            '/image/trainings/2-2.jpg',
            '/image/trainings/3.jpg',
            '/image/trainings/4-4.jpg',
            '/image/trainings/5-5.jpg',
            '/image/trainings/6-6.jpg',
            '/image/trainings/7-7.jpg',
            '/image/trainings/8-8.jpg',
            '/image/trainings/9-9.jpg',
            '/image/trainings/10-10.jpg',
            '/image/trainings/11.jpg',
            '/image/trainings/12-12.jpg'
        ],
        swiperOption: {
            autoplay: 2500,
            slidesPerView: 3,
            slidesPerColumnFill: 'row',
            spaceBetween: 20,
            loop: true,
            centeredSlides: false,
            breakpoints: {
                600: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                900: {
                    slidesPerView: 2,
                    spaceBetween: 10
                }
            }
        }
    },
    methods: {
        showTrainingApplyModal(group_id = null) {
            this.modal_group_id = group_id;
            this.$modal.show('trainings-apply-modal');
        },
    },
    mounted() {

    }
});